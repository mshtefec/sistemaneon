<?php

namespace Sistema\FACTURACIONBundle\Services;

use Symfony\Component\Security\Core\Event\AuthenticationEvent;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
// utilizo para crear la orden de salida desde la orden
use Sistema\STOCKBundle\Entity\OrdenSalida;
use Sistema\STOCKBundle\Entity\CantOrdenSalida;
// utilizo para crear el asiento de la orden
use Sistema\FACTURACIONBundle\Entity\Asiento;

class CuentaListener {
    /**
     * @var string
     */
    protected $em;
    protected $DEBE = 0;
    protected $HABER = 1;

    /**
     * @param SecurityContext $securityContext
     * @param Router $router The router
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }

    //post de comprobante
    public function creaAsientos($entity, $persona) {
        $cuentaEmpresa = $this->em->getRepository('SistemaFACTURACIONBundle:CuentaEmpresa')->find(1);
        if ($persona == "cliente") {
            if ($entity->getOrden()) {
                $cuentaCorriente = $entity->getOrden()->getCliente()->getCuentaCorriente();
            } else {
                $cuentaCorriente = null;
            }

            foreach ($entity->getAsientos() as $asiento) {
                
                //MODIFICACION DE FECHA FORM
                $fecha = $asiento->getFecha()->format('Y-m-d');
                $time = new \DateTime('now');
                //agrego a la fecha del form el time
                $newFecha = $fecha . ' ' . $time->format('H:i:s');
                $newDateTimeNow = new \DateTime();
                $newDateTimeNow->createFromFormat('Y-m-d H:i:s', $newFecha);
                //seteo la fecha con su time
                $asiento->setFecha($newDateTimeNow);
                //FIN MODIFICACION DE FECHA FORM
                
                // if ($asiento->getTipo() == $this->HABER) {
                if ($asiento->getHaber() > 0) {
                    //actualizo asiento para la entity cuenta corriente cliente
                    if (!is_null($cuentaCorriente)) {
                        $cuentaCorriente->addAsiento($asiento);   
                    }
                // } else if ($asiento->getTipo() == $this->DEBE) {
                } else {
                    //actualizo asiento para la entity cuenta empresa
                    $cuentaEmpresa->addAsiento($asiento);
                }
            }
        } else if ($persona == "proveedor") {
            $cuentaCorriente = $entity->getProveedor()->getCuentaCorriente();
            
            foreach ($entity->getAsientos() as $asiento) {
                
                //MODIFICACION DE FECHA FORM
                $fecha = $asiento->getFecha()->format('Y-m-d');
                $time = new \DateTime('now');
                //agrego a la fecha del form el time
                $newFecha = $fecha . ' ' . $time->format('H:i:s');
                $newDateTimeNow = new \DateTime();
                $newDateTimeNow->createFromFormat('Y-m-d H:i:s', $newFecha);
                //seteo la fecha con su time
                $asiento->setFecha($newDateTimeNow);
                //FIN MODIFICACION DE FECHA FORM
                
                // if ($asiento->getTipo() == $this->HABER) {
                if ($asiento->getDebe() > 0) {
                    //actualizo asiento para la entity cuenta corriente cliente
                    $cuentaCorriente->addAsiento($asiento);
                // } else if ($asiento->getTipo() == $this->DEBE) {
                } else {
                    //actualizo asiento para la entity cuenta empresa
                    $cuentaEmpresa->addAsiento($asiento);
                }
            }
        }
        if ($entity->getAnulado() == false) {
            $cuentaEmpresa->setMonto($cuentaEmpresa->getMonto() + $entity->getMonto());
            if (!is_null($cuentaCorriente)) {
                $cuentaCorriente->setMonto($cuentaCorriente->getMonto() - $entity->getMonto());
            }
        }
    }

    //update comprobante
    public function actualizarMontos($entity, $resgAnulado, $resgMonto, $persona) {
        if ($persona == "cliente") {
            $cuentaCorriente = $entity->getOrden()->getCliente()->getCuentaCorriente();
        } else if ($persona == "proveedor") {
            $cuentaCorriente = $entity->getOrden()->getProveedor()->getCuentaCorriente();
        }
        $cuentaEmpresa = $this->em->getRepository('SistemaFACTURACIONBundle:CuentaEmpresa')->find(1);

        if ($entity->getAnulado() && $resgAnulado == false) {
            $total = $cuentaEmpresa->getMonto() - $resgMonto;
            $cuentaEmpresa->setMonto($total);
            //actualizo cuenta corriente del cliente
            $total = $cuentaCorriente->getMonto() + $resgMonto;
            $cuentaCorriente->setMonto($total);
        } else {
            //pregunto si la factura venta estaba anulada antes de ser modificada
            if ($resgAnulado && $entity->getAnulado() == false) {

                $cuentaEmpresa->setMonto($cuentaEmpresa->getMonto() + $entity->getMonto());
                $cuentaCorriente->setMonto($cuentaCorriente->getMonto() - $entity->getMonto());

                //actualizo monto cuenta empresa
            } else if ($resgAnulado == false && $entity->getAnulado() == false) {

                //elimino montos anteriores
                $total = $cuentaEmpresa->getMonto() - $resgMonto;
                $total = $total + $entity->getMonto();
                $cuentaEmpresa->setMonto($total);
                //actualizo cuenta corriente del cliente
                $total = $cuentaCorriente->getMonto() + $resgMonto;
                $total = $total - $entity->getMonto();
                $cuentaCorriente->setMonto($total);
            }
        }
    }

    //delete comprobante
    public function actualizarMontosCuandoElimina($entity, $persona) {
        if ($persona == "cliente") {
            $cuentaCorriente = $entity->getOrden()->getCliente()->getCuentaCorriente();
        } else if ($persona == "proveedor") {
            $cuentaCorriente = $entity->getOrden()->getProveedor()->getCuentaCorriente();
        }
        $cuentaEmpresa = $this->em->getRepository('SistemaFACTURACIONBundle:CuentaEmpresa')->find(1);
        $total = $cuentaEmpresa->getMonto() - $entity->getMonto();
        $cuentaEmpresa->setMonto($total);
        $total = $cuentaCorriente->getMonto() + $entity->getMonto();
        $cuentaCorriente->setMonto($total);
    }

}
