<?php

namespace Sistema\FACTURACIONBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\FACTURACIONBundle\Entity\Comprobante;
use Sistema\FACTURACIONBundle\Form\ComprobanteType;
use Sistema\FACTURACIONBundle\Form\ComprobanteFilterType;
use Exporter\Source\DoctrineORMQuerySourceIterator;
use Exporter\Source\ArraySourceIterator;
use Exporter\Writer\XlsWriter;
use Exporter\Handler;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Comprobante controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/comprobante")
 */
class ComprobanteController extends Controller {

    private $FILTROCONIVA = 100;
    private $FILTROSINIVA = 101;

    /**
     * Lists all Comprobante entities.
     *
     * @Route("/", name="admin_comprobante")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->get('security_role')->controlRolesUser();
        //list($filterForm, $queryBuilder) = $this->filter();
        list($filterForm, $comprobantes) = $this->filter($this->FILTROSINIVA);
        // $paginator  = $this->get('knp_paginator');
        // $pagination = $paginator->paginate(
        //     $queryBuilder,
        //     $this->get('request')->query->get('page', 1),
        //     (isset($this->container->parameters['knp_paginator.page_range'])) ? $this->container->parameters['knp_paginator.page_range'] : 10
        // );

        return array(
            'entities' => $comprobantes->getQuery()->getResult(),
            'filterForm' => $filterForm->createView(),
        );
    }

    /**
     * Lists all Comprobante with Iva entities.
     *
     * @Route("/conIva", name="admin_conIva")
     * @Method("GET")
     * @Template()
     */
    public function conIvaAction() {
        list($filterForm, $comprobantes) = $this->filter($this->FILTROCONIVA);

        return array(
            'entities' => $comprobantes->getQuery()->getResult(),
            'filterForm' => $filterForm->createView(),
        );
    }

    /**
     * Process filter request.
     *
     * @return array
     */
    protected function filter($condicion) {
        $request = $this->getRequest();
        $session = $request->getSession();
        $filterForm = $this->createFilterForm(null, $condicion);
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository('SistemaFACTURACIONBundle:Comprobante')
                ->createQueryBuilder('a')
                ->orderBy('a.id', 'DESC')
        ;
        // Bind values from the request
        $filterForm->handleRequest($request);
        // Reset filter
        if ($filterForm->get('reset')->isClicked()) {
            $session->remove('ComprobanteControllerFilter');
            $filterForm = $this->createFilterForm(null, $condicion);
        }

        // Filter action
        if ($filterForm->get('filter')->isClicked()) {
            if ($filterForm->isValid()) {

                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();

                $session->set('ComprobanteControllerFilter', $filterData);
            }
        } else {

            // Get filter from session
            if ($session->has('ComprobanteControllerFilter')) {
                //si no vienen parameters por get creo filtro vacio
                //  if ($request->query->count()) {
                $filterData = $session->get('ComprobanteControllerFilter');

                $filterForm = $this->createFilterForm($filterData, $condicion);

                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            } else {
                $now = new \DateTime('Today');
                $oneWeekBefore = new \DateTime('Today');
                $oneWeekBefore->modify('-7 day');
                $queryBuilder
                        ->andWhere("a.fecha BETWEEN :fechaBefore AND :fechaNow")
                        ->setParameter('fechaNow', $now->format('Y-m-d'))
                        ->setParameter('fechaBefore', $oneWeekBefore->format('Y-m-d'))
                ;
                if ($condicion == $this->FILTROCONIVA) {
                    $queryBuilder
                            ->andWhere("a.iva>0")
                    ;
                }
                $filterForm = $this->createFilterForm(null, $condicion);
            }
            /* } else {

              $now = new \DateTime('Today');
              $oneWeekBefore = new \DateTime('Today');
              $oneWeekBefore->modify('-7 day');
              $queryBuilder
              ->andWhere("a.fecha BETWEEN :fechaBefore AND :fechaNow")
              ->setParameter('fechaNow', $now->format('Y-m-d'))
              ->setParameter('fechaBefore', $oneWeekBefore->format('Y-m-d'))
              ;
              //$this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
              } */
        }

        return array($filterForm, $queryBuilder);
    }

    /**
     * Create filter form.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createFilterForm($filterData = null, $condicion = null) {

        if ($condicion == $this->FILTROCONIVA) {
            $form = $this->createForm(new ComprobanteFilterType(), $filterData, array(
                'action' => $this->generateUrl('admin_conIva'),
                'method' => 'GET',
            ));
        } else if ($condicion == $this->FILTROSINIVA) {
            $form = $this->createForm(new ComprobanteFilterType(), $filterData, array(
                'action' => $this->generateUrl('admin_comprobante'),
                'method' => 'GET',
            ));
        }


        $form
                ->add('filter', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.index.filter',
                    'attr' => array('class' => 'btn btn-success col-lg-1'),
                ))
                ->add('reset', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.index.reset',
                    'attr' => array('class' => 'btn btn-danger col-lg-1 col-lg-offset-1'),
                ))
        ;

        return $form;
    }

    /**
     * Finds and displays a Comprobante entity.
     *
     * @Route("/{id}", name="admin_comprobante_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaFACTURACIONBundle:Comprobante')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Comprobante entity.');
        }

        //$deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
                //'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Finds and displays a Comprobantes entity.
     *
     * @param type $id Id de la entidad
     *
     * @Route("/imprimir-comprobantes/{condicion}", name="admin_comprobantes_imprimir")
     * @Method("GET")
     * @Template()
     * @return view
     */
    public function imprimirComprobantespdfAction($condicion) {
        set_time_limit(0);

        $this->get('security_role')->controlRolesUser();

        if ($condicion == "conIva") {
            $queryBuilder = $this->obtenerConsultaFiltro($this->FILTROCONIVA);
        } else if ($condicion == "sinIva") {
            $queryBuilder = $this->obtenerConsultaFiltro($this->FILTROSINIVA);
        }


//        ladybug_dump_die($queryBuilder);
        if (!$queryBuilder) {
            throw $this->createNotFoundException('Unable to find Comprobantes entity.');
        }

        $config = array(
            'titulo' => 'Reporte O.T. Comprobantes',
            'PDF_HEADER_LOGO' => 'logo_neon.jpg',
            'PDF_HEADER_LOGO_WIDTH' => '55',
            'PDF_HEADER_TITLE' => '',
            'PDF_HEADER_STRING' => '                                                                        M.M.O. Raúl Roberto Rojas
                                                                        Av. San Martín 2386 Resistencia - Chaco
                                                                        Tel: 362-4489609 / Cel: 362-4641854           
                                                                        Correo: cobranza@neon3r.com.ar',
            'pie' => ''
        );

        $html = $this->renderView('SistemaFACTURACIONBundle:Comprobante:show.pdf.twig', array('entities' => $queryBuilder->getQuery()->getResult()));

        return $this->get('io_tcpdf_mws')->quick_pdf($html, $config);
    }

    public function obtenerConsultaFiltro($condicion) {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $session = $request->getSession();
        $queryBuilder = $em->getRepository('SistemaFACTURACIONBundle:Comprobante')
                ->createQueryBuilder('a')
                ->orderBy('a.id', 'DESC')
        ;
        $filterData = $session->get('ComprobanteControllerFilter');
        if (!is_null($filterData)) {
            if ($condicion == $this->FILTROCONIVA) {
                $filterForm = $this->createFilterForm($filterData, $condicion);
            } else if ($condicion == $this->FILTROSINIVA) {
                $filterData = $session->get('ComprobanteControllerFilter');
                $filterForm = $this->createFilterForm($filterData, $condicion);
            }
            $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
        } else {
            $now = new \DateTime('Today');
            $oneWeekBefore = new \DateTime('Today');
            $oneWeekBefore->modify('-7 day');
            $queryBuilder
                    ->andWhere("a.fecha BETWEEN :fechaBefore AND :fechaNow")
                    ->setParameter('fechaNow', $now->format('Y-m-d'))
                    ->setParameter('fechaBefore', $oneWeekBefore->format('Y-m-d'))
            ;
        }
        if ($condicion == $this->FILTROCONIVA) {
            $queryBuilder
                    ->andWhere("a.iva>0")
            ;
        }
        return $queryBuilder;
    }

    /**
     * Finds and displays a Comprobantes entity.
     *
     * @param type $id Id de la entidad
     *
     * @Route("/xls-comprobantes/{condicion}", name="admin_comprobantes_xls")
     * @Method("GET")
     * @Template()
     * @return view
     */
    public function imprimirComprobantesxlsAction($condicion) {
        set_time_limit(0);
        $this->get('security_role')->controlRolesUser();

        if ($condicion == "conIva") {
            $comprobantesQuery = $this->obtenerConsultaFiltro($this->FILTROCONIVA);
        } else if ($condicion == "sinIva") {
            $comprobantesQuery = $this->obtenerConsultaFiltro($this->FILTROSINIVA);
        }

        if (!$comprobantesQuery) {
            throw $this->createNotFoundException('Unable to find Articulos sin Stock entity.');
        }
        $comprobantes = $comprobantesQuery->getQuery()->getResult();
        //  $format = 'xls';
        $filename = 'Comprobantes.xls';
        $datos = array();
        $total = 0;
        foreach ($comprobantes as $comprobante) {
            if ($comprobante->getAnulado()) {
                $anulado = "si";
            } else {
                $anulado = "no";
            }
            $total = $total + $comprobante->getMonto();
            $datos[] = array(
                'Id' => $comprobante->getId(),
                'Fecha' => $comprobante->getFecha()->format('d/m/Y'),
                'Descripción' => $comprobante->getDescripcion(),
                'Monto' => $comprobante->getMonto(),
                'Anulado' => $anulado,
                'FormaPago' => $comprobante->getFormaPago(),
            );
        }
        $datos[] = array(
            'Id' => "",
            'Fecha' => "",
            'Descripción' => "Total",
            'Monto' => $total,
            'Anulado' => "",
            'FormaPago' => "",
        );
        $writer = new XlsWriter('php://output');
        $contentType = 'application/vnd.ms-excel';
        $source = new \Exporter\Source\ArraySourceIterator($datos);
        $callback = function() use ($source, $writer) {
            $handler = \Exporter\Handler::create($source, $writer);
            $handler->export();
        };

        return new StreamedResponse($callback, 200, array(
            'Content-Type' => $contentType,
            'Content-Disposition' => sprintf('attachment; filename=%s', $filename)
        ));
        //$datos = array('Id', 'Fecha', 'Descripcion', 'Monto', 'Anulado', 'FormaPago');
        //return $this->exportCsvAction('xls', $comprobantes, 'Comprobantes.xls', $datos);
    }

    /**
     * Export Csv.
     */
    public function exportCsvAction($format, $query, $filename, $campos) {
        switch ($format) {
            case 'xls':
                $content_type = 'application/vnd.ms-excel';
                break;
            case 'json':
                $content_type = 'application/json';
                break;
            case 'csv':
                $content_type = 'text/csv';
                break;
            default:
                $content_type = 'text/csv';
                break;
        }
        // Location to Export this to
        $export_to = 'php://output';
        // Data to export
        $exporter_source = new DoctrineORMQuerySourceIterator($query->getQuery(), $campos, "d-m-Y");
        // Get an Instance of the Writer
        $exporter_writer = '\Exporter\Writer\\' . ucfirst($format) . 'Writer';
        $exporter_writer = new $exporter_writer($export_to);
        // Generate response
        $response = new Response();
        // Set headers
        $response->headers->set('Cache-Control', 'must-revalidate, post-check=0, pre-check=0');
        $response->headers->set('Content-type', $content_type);
        $response->headers->set('Expires', 0);
        //$response->headers->set('Content-length', filesize($filename));
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Content-disposition', 'filename=' . $filename . ';');
        // Send headers before outputting anything
        $response->sendHeaders();
        // Export to the format
        Handler::create($exporter_source, $exporter_writer)->export();
        return $response;
    }

}
