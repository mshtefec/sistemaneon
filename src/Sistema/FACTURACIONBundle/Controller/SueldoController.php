<?php

namespace Sistema\FACTURACIONBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\FACTURACIONBundle\Entity\Sueldo;
use Sistema\FACTURACIONBundle\Entity\Anticipo;
use Sistema\FACTURACIONBundle\Entity\SueldoFecha;
use Sistema\FACTURACIONBundle\Form\SueldoType;
use Sistema\FACTURACIONBundle\Form\GenerarSueldoType;
use Sistema\FACTURACIONBundle\Form\SueldoFilterType;

/**
 * Sueldo controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/sueldo")
 */
class SueldoController extends Controller {

    /**
     * Lists all Sueldo entities.
     *
     * @Route("/", name="admin_sueldo")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->get('security_role')->controlRolesUser();
        list($filterForm, $queryBuilder) = $this->filter();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $queryBuilder, $this->get('request')->query->get('page', 1), (isset($this->container->parameters['knp_paginator.page_range'])) ? $this->container->parameters['knp_paginator.page_range'] : 10
        );

        $form = $this->crearGenerarForm();
        //$form->handleRequest($request);

        return array(
            'form' => $form->createView(),
            'entities' => $pagination,
            'filterForm' => $filterForm->createView(),
        );
    }

    /**
     * Lists all Sueldo entities.
     *
     * @Route("/index/embed", name="admin_sueldo_embed")
     * @Method("GET")
     * @Template()
     */
    public function indexEmbedAction() {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('SistemaFACTURACIONBundle:Sueldo')->getSueldos();

        $frmModalSueldo = new Sueldo();
        $form = $this->crearFormularioModal($frmModalSueldo);
        return array(
            'entities' => $entity,
            'formModal' => $form->createView(),
        );
    }

    /**
     * Process filter request.
     *
     * @return array
     */
    protected function filter() {
        $em = $this->getDoctrine()->getManager();
        $filterForm = $this->createFilterForm();
        $request = $this->getRequest();
        $session = $request->getSession();

        $queryBuilder = $em->getRepository('SistemaFACTURACIONBundle:Sueldo')
                ->createQueryBuilder('a')
                ->groupBy('a.inicio')
                ->orderBy('a.inicio', 'DESC')
        //
        ;
        // Bind values from the request
        $filterForm->handleRequest($request);
        // Reset filter
        if ($filterForm->get('reset')->isClicked()) {
            $session->remove('SueldoControllerFilter');
            $filterForm = $this->createFilterForm();
        }

        // Filter action
        if ($filterForm->get('filter')->isClicked()) {
            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('SueldoControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('SueldoControllerFilter')) {
                $filterData = $session->get('SueldoControllerFilter');
                $filterForm = $this->createFilterForm($filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }

    /**
     * Create filter form.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createFilterForm($filterData = null) {
        $form = $this->createForm(new SueldoFilterType(), $filterData, array(
            'action' => $this->generateUrl('admin_sueldo'),
            'method' => 'GET',
        ));

        $form
                ->add('filter', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.index.filter',
                    'attr' => array('class' => 'btn btn-success col-lg-1'),
                ))
                ->add('reset', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.index.reset',
                    'attr' => array('class' => 'btn btn-danger col-lg-1 col-lg-offset-1'),
                ))
        ;

        return $form;
    }

    /**
     * Creates a new Sueldo entity.
     *
     * @Route("/", name="admin_sueldo_create")
     * @Method("POST")
     * @Template("SistemaFACTURACIONBundle:Sueldo:new.html.twig")
     */
    public function createAction(Request $request) {
        $this->get('security_role')->controlRolesUser();
        $entity = new Sueldo();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl('admin_sueldo_new') : $this->generateUrl('admin_sueldo_show', array('id' => $entity->getId()));
            return $this->redirect($nextAction);
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Sueldo entity.
     *
     * @param Sueldo $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Sueldo $entity) {
        $form = $this->createForm(new SueldoType(), $entity, array(
            'action' => $this->generateUrl('admin_sueldo_create'),
            'method' => 'POST',
        ));

        $form
            ->add('save', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label' => 'views.new.save',
                'attr' => array('class' => 'btn btn-success col-lg-2')
            ))
            ->add('saveAndAdd', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label' => 'views.new.saveAndAdd',
                'attr' => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
            ))
        ;

        return $form;
    }

    /**
     * Displays a form to create a new Sueldo entity.
     *
     * @Route("/new", name="admin_sueldo_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->get('security_role')->controlRolesUser();
        $entity = new Sueldo();
        $form = $this->createCreateForm($entity);


        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Sueldo entity.
     *
     * @Route("/{id}", name="admin_sueldo_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaFACTURACIONBundle:Sueldo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sueldo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Sueldo entity.
     *
     * @Route("/{id}/edit", name="admin_sueldo_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaFACTURACIONBundle:Sueldo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sueldo entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Sueldo entity.
     *
     * @param Sueldo $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Sueldo $entity) {
        $form = $this->createForm(new SueldoType(), $entity, array(
            'action' => $this->generateUrl('admin_sueldo_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form
            ->add('save', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label' => 'views.new.save',
                'attr' => array('class' => 'btn btn-success col-lg-2')
            ))
            ->add('saveAndAdd', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label' => 'views.new.saveAndAdd',
                'attr' => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
            ))
        ;

        return $form;
    }

    /**
     * Edits an existing Sueldo entity.
     *
     * @Route("/{id}", name="admin_sueldo_update")
     * @Method("PUT")
     * @Template("SistemaFACTURACIONBundle:Sueldo:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaFACTURACIONBundle:Sueldo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sueldo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked() ? $this->generateUrl('admin_sueldo_new') : $this->generateUrl('admin_sueldo_show', array('id' => $id));
            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Sueldo entity.
     *
     * @Route("/{id}", name="admin_sueldo_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $this->get('security_role')->controlRolesUser();
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SistemaFACTURACIONBundle:Sueldo')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Sueldo entity.');
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.delete.success');
        }

        return $this->redirect($this->generateUrl('admin_sueldo'));
    }

    /**
     * Creates a form to delete a Sueldo entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        $mensaje = $this->get('translator')->trans('views.recordactions.confirm', array(), 'MWSimpleCrudGeneratorBundle');
        $onclick = 'return confirm("' . $mensaje . '");';
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_sueldo_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label' => 'views.recordactions.delete',
                'attr' => array(
                    'class' => 'btn btn-danger col-lg-11',
                    'onclick' => $onclick,
                )
            ))
            ->getForm()
        ;
    }

    /**
     * Creates a new Sueldo entity.
     *
     * @Route("/generar/sueldo-empleados", name="admin_sueldo_generar")
     * @Method({"GET", "POST"})
     */
    public function generarSueldo(Request $request) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $form = $this->crearGenerarForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $primeraQuincena = $form->get('primera')->isClicked();
            $segundaQuincena = $form->get('segunda')->isClicked();
            //$mesCompleto = $form->get('mes')->isClicked();
            $date = new \DateTime($form['fecha']->getData()->format('d-m-Y'));
            $date2 = new \DateTime($form['fecha']->getData()->format('d-m-Y'));

            $date->format('d-m-Y');
            $date2->format('d-m-Y');

            $date->modify('first day of this month');
            $date2->modify('first day of this month');

            if ($primeraQuincena) {
                $date2->modify('+14 days');
            } elseif ($segundaQuincena) {
                $date->modify('+15 days');
                $date2->modify('last day of this month');
                /* }elseif ($mesCompleto){
                  $date2->modify('last day of this month'); */
            }
            $checkSueldo = $em->getRepository('SistemaFACTURACIONBundle:SueldoFecha')->checkSueldo($date, $date2);
            if (is_null($checkSueldo)) {
                //$this->get('session')->getFlashBag()->add('danger', 'El Rango de Fecha Seleccionado Ya Existe!!');
                $sueldoFecha = new SueldoFecha();
                $sueldoFecha->setInicio($date);
                $sueldoFecha->setFin($date2);
            } else {
                $sueldoFecha = $checkSueldo;
            }
            $em->persist($sueldoFecha);
            $entity = $em->getRepository('SistemaRRHHBundle:Empleado')->generarSueldos($date, $date2);

            $cantidadHoras = 0;
            $tieneSueldos  = false;
            foreach ($entity as $empleado) {
                $tieneSueldos    = true;
                $total           = 0;
                $auxCantidadHora = 0;
                foreach ($empleado->getOrdenes()->getValues() as $EmpleadoOrdenes) {
                    $cantidadHoras  = 0;
                    $montoSumaTotal = 0;
                    foreach ($EmpleadoOrdenes->getEmpleadoHoras() as $horas) {
                        $timeInterval      = $horas->getDesde()->diff($horas->getHasta());
                        $intervalInSeconds = (new \DateTime())->setTimeStamp(0)->add($timeInterval)->getTimeStamp();
                        $intervalInMinutes = $intervalInSeconds / 60;
                        $totalIntervalo    = $intervalInMinutes / 60;
                        $cantidadHoras    += ((float) $totalIntervalo);
                        $total            += $horas->getCostoHora() * ((float) $totalIntervalo);
                    }

                    $auxCantidadHora = $cantidadHoras + $auxCantidadHora;
                }
                if (is_null($checkSueldo)) {
                    $tieneSueldos = false;
                    $sueldo = new Sueldo();
                    $sueldo->setInicio($date);
                    $sueldo->setFin($date2);
                    $sueldo->setPrecioHora($empleado->getCargo()->getCostoHora());
                    $sueldo->setCantidadHora($auxCantidadHora);
                    $sueldo->setEmpleado($empleado);
                    $sueldo->setTotal($total);
                    $sueldo->setRemuneracionEspecial(0);
                    $sueldo->setDiasVacaciones(0);
                    $sueldo->setSueldoFecha($sueldoFecha);
                } else {
                    $tieneSueldos = true;
                    $sueldo = $em->getRepository('SistemaFACTURACIONBundle:Sueldo')->findEmpleadoIdFecha($empleado->getId(), $checkSueldo->getId());
                    if (!is_null($sueldo)) {
                        $sueldo->setPrecioHora($empleado->getCargo()->getCostoHora());
                        $sueldo->setCantidadHora($auxCantidadHora);
                        $sueldo->setTotal($total);
                    } else {
                        $sueldo = new Sueldo();
                        $sueldo->setInicio($date);
                        $sueldo->setFin($date2);
                        $sueldo->setPrecioHora($empleado->getCargo()->getCostoHora());
                        $sueldo->setCantidadHora($auxCantidadHora);
                        $sueldo->setEmpleado($empleado);
                        $sueldo->setTotal($total);
                        $sueldo->setRemuneracionEspecial(0);
                        $sueldo->setDiasVacaciones(0);
                        $sueldo->setSueldoFecha($sueldoFecha);
                    }
                }
                $em->persist($sueldo);
            }
            $empleadosAsalariados = $em->getRepository('SistemaRRHHBundle:Empleado')->getEmpleadosAsalariados();
            foreach ($empleadosAsalariados as $empleado) {
                $tieneSueldos    = true;
                if (is_null($checkSueldo)) {
                    $tieneSueldos = false;
                    $sueldo = new Sueldo();
                    $sueldo->setInicio($date);
                    $sueldo->setFin($date2);
                    $sueldo->setPrecioHora(0);
                    $sueldo->setCantidadHora(0);
                    $sueldo->setEmpleado($empleado);
                    $sueldo->setTotal($empleado->getCargo()->getSueldoMensual() / 2);
                    $sueldo->setRemuneracionEspecial(0);
                    $sueldo->setDiasVacaciones(0);
                    $sueldo->setSueldoFecha($sueldoFecha);
                } else {
                    $tieneSueldos = true;
                    $sueldo = $em->getRepository('SistemaFACTURACIONBundle:Sueldo')->findEmpleadoIdFecha($empleado->getId(), $checkSueldo->getId());
                    if (!is_null($sueldo)) {
                        $sueldo->setPrecioHora(0);
                        $sueldo->setCantidadHora(0);
                        $sueldo->setTotal($empleado->getCargo()->getSueldoMensual() / 2);
                    } else {
                        $sueldo = new Sueldo();
                        $sueldo->setInicio($date);
                        $sueldo->setFin($date2);
                        $sueldo->setPrecioHora(0);
                        $sueldo->setCantidadHora(0);
                        $sueldo->setEmpleado($empleado);
                        $sueldo->setTotal($empleado->getCargo()->getSueldoMensual() / 2);
                        $sueldo->setRemuneracionEspecial(0);
                        $sueldo->setDiasVacaciones(0);
                        $sueldo->setSueldoFecha($sueldoFecha);
                    }
                }
                $em->persist($sueldo);
            }

            $em->flush();

            if (!$tieneSueldos) {
                $this->get('session')->getFlashBag()->add('success', 'flash.create.success');
            } else {
                //$this->get('session')->getFlashBag()->add('danger', 'No se generaron Sueldos porque no se encontraron Empleados en el Rango Seleccionado');
                $this->get('session')->getFlashBag()->add('info', 'Se actualizaron los Sueldos en el Rango Seleccionado');
            }
            return $this->redirect($this->generateUrl('admin_sueldo'));
        }
    }

    /**
     * Creates a new Sueldo entity.
     *
     * @Route("/listar/sueldo-empleados/{inicio}/{fin}", name="admin_sueldo_listar")
     * @Method("GET")
     * @Template()
     */
    public function listarAction($inicio, $fin) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entitySueldos = $em->getRepository('SistemaFACTURACIONBundle:Sueldo')->getSueldos($inicio, $fin);
        
        $frmModalSueldo = new Sueldo();
        $formSueldo = $this->crearFormularioModal($frmModalSueldo);

        $frmModalAnticipo = new Anticipo();
        $formAnticipo = $this->crearFormularioModalAnticipo($frmModalAnticipo);

        return array(
            'inicio' => $inicio,
            'fin' => $fin,
            'entities' => $entitySueldos,
            'formModal' => $formSueldo->createView(),
            'formModalAnticipo' => $formAnticipo->createView()
        );
    }

    public function crearGenerarForm() {
        return $this->createFormBuilder()
                    ->setAction($this->generateUrl('admin_sueldo_generar'))
                    ->setMethod('POST')
                    ->add('fecha', 'bootstrapdatetime', array(
                        'label_attr' => array(
                            'class' => 'col-lg-2 col-md-2 col-sm-2',
                        ),
                        'attr' => array(
                            'class' => 'col-lg-3 col-md-3 col-sm-3',
                        ),
                        'read_only' => true,
                        'widget_type' => 'month',
                        'data' => new \DateTime("today"),
                    ))
                    ->add('primera', 'submit', array(
                        'label' => 'Primera Quincena',
                        'attr' => array(
                            'class' => 'col-lg-2 col-sm-2 col-sd-2 col-lg-push-1 col-sd-push-1 col-sm-push-1 btn btn-success'
                        )
                    ))
                    ->add('segunda', 'submit', array(
                        'label' => 'Segunda Quincena',
                        'attr' => array(
                            'class' => 'col-lg-2 col-sm-2 col-sd-2 col-lg-push-2 col-sd-push-2 col-sm-push-2 btn btn-success'
                        )
                    ))
                    /* ->add('mes', 'submit', array(
                      'label' => 'Mes Completo',
                      'attr' => array(
                      'class' => 'col-lg-2 col-sm-2 col-sd-2 col-lg-push-4 col-sd-push-4 col-sm-push-4 btn btn-success'
                      )
                     */
                    ->getForm()
        ;
    }

    /**
     * Creates a new Rubro entity.
     *
     * @Route("/generar/anticipo", name="anticipo_sueldo")
     * @Method("POST")
     */
    public function generarAnticipoSueldo(Request $request) {
        $all = $request->request->all();
        $id = (integer) $all['form']['idAnticipo'];
        $inicio = (string) $all['form']['inicio'];
        $fin = (string) $all['form']['fin'];
        $this->get('security_role')->controlRolesUser();

        $em = $this->getDoctrine()->getManager();

        $sueldo = $em->getReference('SistemaFACTURACIONBundle:Sueldo', $id);
        //$sueldo = $em->getRepository('SistemaFACTURACIONBundle:Sueldo')->find($id);
        //$entity = $em->getRepository('SistemaFACTURACIONBundle:Anticipo')->find($id);
        $entity = new Anticipo;
        $form = $this->crearFormularioModalAnticipo($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entity->setSueldo($sueldo);
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                    'success', 'El Anticipo se ha Generado Correctamente'
            );
        } else {
            $this->get('session')->getFlashBag()->add(
                    'danger', 'El Anticipo no se ha Generado'
            );
        }

        return $this->redirect(
                        $this->generateUrl('admin_sueldo_listar', array('inicio' => $inicio, 'fin' => $fin))
        );
    }

    /**
     * Creates a new Rubro entity.
     *
     * @Route("/actualizar/Vacacion-Remuneracion", name="update_remuneraciones_vacaciones")
     * @Method("POST")
     */
    public function actualizarVacacionesRemuneraciones(Request $request) {
        $all = $request->request->all();
        $id = (integer) $all['form']['idSueldo'];
        $this->get('security_role')->controlRolesUser();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaFACTURACIONBundle:Sueldo')->find($id);

        $entityOld = clone $entity;

        $form = $this->crearFormularioModal($entity);
        $form->handleRequest($request);

        if (($entity->getRemuneracionEspecial() != $entityOld->getRemuneracionEspecial()) && !is_null($entity->getRemuneracionEspecial())) {
            $entity->setTotal($entity->getTotal() - $entityOld->getRemuneracionEspecial());
            $entity->setTotal($entity->getTotal() + $entity->getRemuneracionEspecial());
        }

        if (($entity->getDiasVacaciones() != $entityOld->getDiasVacaciones()) && !is_null($entity->getDiasVacaciones())) {
            $entity->setTotal($entity->getTotal() - $entityOld->getDiasVacaciones());
            $entity->setTotal($entity->getTotal() + $entity->getDiasVacaciones());
        }

        if ($form->isValid()) {
            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Sueldo entity.');
            }
            $em->flush($entity);
            $this->get('session')->getFlashBag()->add(
                    'success', 'El Sueldo se ha Actualizado Correctamente'
            );
        } else {
            $this->get('session')->getFlashBag()->add(
                    'danger', 'El sueldo no se han Actualizado'
            );
        }

        return $this->redirect($this->generateUrl('admin_sueldo'));
    }

    public function crearFormularioModal(Sueldo $sueldo) {
        return $this->createFormBuilder($sueldo)
                    ->setAction($this->generateUrl('update_remuneraciones_vacaciones'))
                    ->setMethod('POST')
                    ->add('idSueldo', 'hidden', array('mapped' => false))
                    ->add('remuneracionEspecial', 'number', array(
                        'required' => false,
                        'label' => 'Remuneracion Especial',
                        'label_attr' => array(
                            'class' => 'col-lg-3 control-label',
                        ),
                        'attr' => array('class' => 'col-lg-2')
                    ))
                    ->add('diasVacaciones', 'number', array(
                        'label' => 'Remuneracion Dias Vacaciones',
                        'required' => false,
                        'attr' => array('class' => 'col-lg-3'),
                        'label_attr' => array(
                            'class' => 'col-lg-3 control-label',
                        ),
                    ))
                    ->add('save', 'submit', array(
                        'label' => 'Guardar',
                        'attr' => array(
                            'class' => 'btn btn-success'
                        )
                    ))
                    ->getForm()
        ;
    }

    public function crearFormularioModalAnticipo(Anticipo $anticipo) {
        return $this->createFormBuilder($anticipo)
                    ->setAction($this->generateUrl('anticipo_sueldo'))
                    ->setMethod('POST')
                    ->add('idAnticipo', 'hidden', array('mapped' => false))
                    ->add('inicio', 'hidden', array('mapped' => false))
                    ->add('fin', 'hidden', array('mapped' => false))
                    ->add('fecha', 'bootstrapdatetime', array(
                        'required' => true,
                        'label' => 'Fecha',
                        'data' => new \DateTime("today"),
                        'label_attr' => array(
                            'class' => 'col-lg-2 col-sm-2 col-sd-2 control-label',
                        ),
                        'attr' => array('class' => 'col-lg-4 col-sm-4 col-sd-4')
                    ))
                    ->add('monto', 'number', array(
                        'label' => 'Monto',
                        'required' => true,
                        'attr' => array('class' => 'col-lg-4 col-sm-4 col-sd-4'),
                        'label_attr' => array(
                            'class' => 'col-lg-2 col-sm-2 col-sd-2 control-label',
                        ),
                    ))
                    ->add('concepto', null, array(
                        'label' => 'Concepto',
                        'required' => true,
                        'attr' => array('class' => 'col-lg-6 col-sm-6 col-sd-6', 'style' => 'height:200px;'),
                        'label_attr' => array(
                            'class' => 'col-lg-2 col-sm-2 col-sd-2 control-label',
                        ),
                    ))
                    ->add('sueldo', null, array(
                        'attr' => array('style' => 'display:none;'),
                        'label' => false
                    ))
                    ->add('save', 'submit', array(
                        'label' => 'Guardar',
                        'attr' => array(
                            'class' => 'btn btn-success'
                        )
                    ))
                    ->getForm()
        ;
    }

    /**
     * Finds and displays a Orden entity.
     *
     * @param type $id Id de la entidad
     *
     * @Route("/imprimir-reporte-sueldo/{id}/{idSueldo}/{fecha1}/{fecha2}", name="admin_reporte_sueldo_imprimir")
     * @Method("GET")
     * @Template()
     * @return view
     */
    public function imprimirReporteSueldoPdfAction($id, $idSueldo, $fecha1, $fecha2) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $date = new \DateTime($fecha1);
        $date2 = new \DateTime($fecha2);

        $date->format('d-m-Y');
        $date2->format('d-m-Y');

        //$entity = $em->getRepository('SistemaRRHHBundle:Empleado')->generarComtrolEmpleado($id, $date, $date2);
        $entity = $em->getRepository('SistemaRRHHBundle:Empleado')->find($id);
        $horas = $em->getRepository('SistemaSTOCKBundle:EmpleadoHora')->obtenerHorasSueldoEmpleado($id, $date, $date2);
        $anticipos = $em->getRepository('SistemaFACTURACIONBundle:Anticipo')->findBySueldo($idSueldo);
        // ladybug_dump_die($horas);
        //ladybug_dump_die($entity->getSueldo()[0]);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Orden entity.');
        }
        /*
          $cantidadHoras = 0;
          $total = 0;
          foreach ($entity->getOrdenes()->getValues() as $EmpleadoOrdenes) {
          foreach ($EmpleadoOrdenes->getEmpleadoHoras() as $horas) {
          $timeInterval = $horas->getDesde()->diff($horas->getHasta());
          $intervalInSeconds = (new \DateTime())->setTimeStamp(0)->add($timeInterval)->getTimeStamp();
          $intervalInMinutes = $intervalInSeconds / 60;
          $totalIntervalo = $intervalInMinutes / 60;
          $cantidadHoras = ((float) $totalIntervalo) + $cantidadHoras;
          }
          $total = ($EmpleadoOrdenes->getCostoHora() * $cantidadHoras) + $total;
         */
        $config = array(
            'titulo' => 'Reporte de Horario',
            'PDF_HEADER_LOGO' => 'logo_neon.jpg',
            'PDF_HEADER_LOGO_WIDTH' => '55',
            'PDF_HEADER_TITLE' => '',
            'PDF_HEADER_STRING' => '                                                                    Reporte Control de Horas
                                                                    M.M.O. Raúl Roberto Rojas
                                                                    Av. San Martín 2386 Resistencia - Chaco                            
                                                                    Empleado: ' . $entity->__toString() . '
                Tel: 362-4489609 / Cel: 362-4641854                                    Email: gerencia@neon3r.com.ar          Fecha: desde ' . $date->format('d/m/Y') . ' hasta ' . $date2->format('d/m/Y') . '
                            
                            ',
            'pie' => ''
        );

        $html = $this->renderView('SistemaFACTURACIONBundle:Sueldo:show.pdf.twig', array(
            'entity' => $entity,
            'horas' => $horas,
            'anticipos' => $anticipos,
        ));

        return $this->get('io_tcpdf_mws')->quick_pdf($html, $config);
    }

}
