<?php

namespace Sistema\FACTURACIONBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\FACTURACIONBundle\Entity\ReciboCompra;
use Sistema\FACTURACIONBundle\Form\ReciboCompraType;
use Sistema\FACTURACIONBundle\Form\ReciboCompraFilterType;
use Sistema\FACTURACIONBundle\Entity\Asiento;
use Sistema\RRHHBundle\Entity\Proveedor;

/**
 * ReciboCompra controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/recibocompra")
 */
class ReciboCompraController extends Controller {

    /**
     * Lists all ReciboCompra entities.
     *
     * @Route("/", name="admin_recibocompra")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->get('security_role')->controlRolesUser();
        list($filterForm, $queryBuilder) = $this->filter();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $queryBuilder, $this->get('request')->query->get('page', 1), (isset($this->container->parameters['knp_paginator.page_range'])) ? $this->container->parameters['knp_paginator.page_range'] : 10
        );

        return array(
            'entities' => $pagination,
            'filterForm' => $filterForm->createView(),
        );
    }

    /**
     * Process filter request.
     *
     * @return array
     */
    protected function filter() {
        $request = $this->getRequest();
        $session = $request->getSession();
        $filterForm = $this->createFilterForm();
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository('SistemaFACTURACIONBundle:ReciboCompra')
                ->createQueryBuilder('a')
                ->orderBy('a.id', 'DESC')
        ;
        // Bind values from the request
        $filterForm->handleRequest($request);
        // Reset filter
        if ($filterForm->get('reset')->isClicked()) {
            $session->remove('ReciboCompraControllerFilter');
            $filterForm = $this->createFilterForm();
        }

        // Filter action
        if ($filterForm->get('filter')->isClicked()) {
            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('ReciboCompraControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('ReciboCompraControllerFilter')) {
                $filterData = $session->get('ReciboCompraControllerFilter');
                $filterForm = $this->createFilterForm($filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }

    /**
     * Create filter form.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createFilterForm($filterData = null) {
        $form = $this->createForm(new ReciboCompraFilterType(), $filterData, array(
            'action' => $this->generateUrl('admin_recibocompra'),
            'method' => 'GET',
        ));

        $form
                ->add('filter', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.index.filter',
                    'attr' => array('class' => 'btn btn-success col-lg-1'),
                ))
                ->add('reset', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.index.reset',
                    'attr' => array('class' => 'btn btn-danger col-lg-1 col-lg-offset-1'),
                ))
        ;

        return $form;
    }

    /**
     * Creates a new ReciboCompra entity.
     *
     * @Route("/", name="admin_recibocompra_create")
     * @Method("POST")
     * @Template("SistemaFACTURACIONBundle:ReciboCompra:new.html.twig")
     */
    public function createAction(Request $request) {
        $this->get('security_role')->controlRolesUser();
        $entity = new ReciboCompra();
        //agrego cuentas segun proceso
        $proceso = $request->request->get('sistema_facturacionbundle_recibocompra')['proceso'];
        $procesoService = $this->get('proceso');
        $entityProceso  = $procesoService->create($entity, $proceso);
        $entity->setProceso($entityProceso);
        //fin agrego
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            //MODIFICACION DE FECHA FORM
            $fecha = $entity->getFecha()->format('Y-m-d');
            $time = new \DateTime('now');
            //agrego a la fecha del form el time
            $newFecha = $fecha . ' ' . $time->format('H:i:s');
            $newDateTimeNow = new \DateTime();
            $newDateTimeNow->createFromFormat('Y-m-d H:i:s', $newFecha);
            //seteo la fecha con su time
            $entity->setFecha($newDateTimeNow);
            //FIN MODIFICACION DE FECHA FORM
            $proveedorGasto = $em->getRepository('SistemaRRHHBundle:Proveedor')->getProveedorGastos();

            //pregunto si existe el proveedor gastos sino lo creo
            if (count($proveedorGasto) == 0) {
                $proveedorGasto = new Proveedor();
                $proveedorGasto->setCuitcuil("ninguno");
                $proveedorGasto->setEmpresa("gastos");
            }
            $entity->setProveedor($proveedorGasto);
            $servicioCuenta = $this->get('actualizarCuentas');
            $servicioCuenta->creaAsientos($entity, "proveedor");
            //Si es ROLE_ADMIN entra y si hay mas de 1 caja abierta por usuario las trae a todas.
            if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
                $cajaAbierta = $em->getRepository('SistemaFACTURACIONBundle:Caja')
                    ->findCajaAbierta();
            } else {
                $cajaAbierta = $em->getRepository('SistemaFACTURACIONBundle:Caja')
                    ->findCajaAbierta($this->getUser()->getId());
            }
            //Si trae cajaAbierta entra
            if (!empty($cajaAbierta)) {
                $entity->setCaja($cajaAbierta[0]);
                $em->persist($proveedorGasto);
                $em->persist($entity);
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', 'flash.create.success');
                
                if ($form->get('saveAndAdd')->isClicked()) {
                    $numero = $entity->getNumero();
                    $form->getData()->setNumero($numero + 1);
                    $nextAction = $this->generateUrl('admin_recibocompra_new', array('proceso' => $entity->getProceso()->getId()));
                } else {
                    $nextAction = $this->generateUrl('admin_recibocompra_show', array('id' => $entity->getId()));
                }
                
                return $this->redirect($nextAction);
            } else {
                $this->get('session')->getFlashBag()->add('danger', 'No hay ninguna Caja Abierta');
            }
        }
        $this->get('session')->getFlashBag()->add('danger', 'Los asientos deben estar balanceados y sin valores negativos');

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a ReciboCompra entity.
     *
     * @param ReciboCompra $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ReciboCompra $entity) {
        $form = $this->createForm(new ReciboCompraType(), $entity, array(
            'action' => $this->generateUrl('admin_recibocompra_create'),
            'method' => 'POST',
        ));

        $form
                ->add(
                        'save', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.save',
                    'attr' => array('class' => 'btn btn-success col-lg-2')
                        )
                )
                ->add(
                        'saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
                        )
                )
        ;

        return $form;
    }

    /**
     * Displays a form to create a new ReciboCompra entity.
     *
     * @Route("/new/{proceso}", name="admin_recibocompra_new", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function newAction($proceso) {
        $this->get('security_role')->controlRolesUser();
        $entity = new ReciboCompra();
        $em = $this->getDoctrine()->getManager();
        $ultimoNumero = $em->getRepository('SistemaFACTURACIONBundle:ReciboCompra')->findUltimoNumero();
        if (!empty($ultimoNumero)) {
          $entity->setNumero($ultimoNumero[0]['numero'] + 1);
        } else {
          $entity->setNumero(1);
        }
        $procesoService = $this->get('proceso');
        $entityProceso = $procesoService->create($entity, $proceso);
        $entity->setProceso($entityProceso);
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a ReciboCompra entity.
     *
     * @Route("/{id}", name="admin_recibocompra_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaFACTURACIONBundle:ReciboCompra')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ReciboCompra entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing ReciboCompra entity.
     *
     * @Route("/{id}/edit", name="admin_recibocompra_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaFACTURACIONBundle:ReciboCompra')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ReciboCompra entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a ReciboCompra entity.
     *
     * @param ReciboCompra $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(ReciboCompra $entity) {
        $form = $this->createForm(new ReciboCompraType(), $entity, array(
            'action' => $this->generateUrl('admin_recibocompra_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form
                ->add(
                        'save', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.save',
                    'attr' => array('class' => 'btn btn-success col-lg-2')
                        )
                )
                ->add(
                        'saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
                        )
                )
        ;

        return $form;
    }

    /**
     * Edits an existing ReciboCompra entity.
     *
     * @Route("/{id}", name="admin_recibocompra_update")
     * @Method("PUT")
     * @Template("SistemaFACTURACIONBundle:ReciboCompra:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaFACTURACIONBundle:ReciboCompra')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ReciboCompra entity.');
        }
        $resgAnulado = $entity->getAnulado();
        $resgMonto = $entity->getMonto();
        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            // $servicioCuenta = $this->get('actualizarCuentas');
            // $servicioCuenta->actualizarMontos($entity, $resgAnulado, $resgMonto, "proveedor");

            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            if ($editForm->get('saveAndAdd')->isClicked()) {
                $numero = $entity->getNumero();
                $editForm->getData()->setNumero($numero + 1);
                $nextAction = $this->generateUrl('admin_recibocompra_new', array('proceso' => $entity->getProceso()->getId()));
            } else {
                $nextAction = $this->generateUrl('admin_recibocompra_show', array('id' => $entity->getId()));
            }

            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a ReciboCompra entity.
     *
     * @Route("/{id}", name="admin_recibocompra_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $this->get('security_role')->controlRolesUser();
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SistemaFACTURACIONBundle:ReciboCompra')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ReciboCompra entity.');
            }
            /* if (!$entity->getAnulado()) {
              $servicioCuenta = $this->get('actualizarCuentas');
              $servicioCuenta->actualizarMontosCuandoElimina($entity, "proveedor");
              } */

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.delete.success');
        }

        return $this->redirect($this->generateUrl('admin_recibocompra'));
    }

    /**
     * Creates a form to delete a ReciboCompra entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        $mensaje = $this->get('translator')->trans('views.recordactions.confirm', array(), 'MWSimpleCrudGeneratorBundle');
        $onclick = 'return confirm("' . $mensaje . '");';
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('admin_recibocompra_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array(
                            'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                            'label' => 'views.recordactions.delete',
                            'attr' => array(
                                'class' => 'btn btn-danger col-lg-11',
                                'onclick' => $onclick,
                            )
                        ))
                        ->getForm()
        ;
    }

}
