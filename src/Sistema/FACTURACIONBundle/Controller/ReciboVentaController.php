<?php

namespace Sistema\FACTURACIONBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\FACTURACIONBundle\Entity\ReciboVenta;
use Sistema\FACTURACIONBundle\Form\ReciboVentaType;
use Sistema\FACTURACIONBundle\Form\ReciboVentaFilterType;
use Sistema\FACTURACIONBundle\Entity\Asiento;

/**
 * ReciboVenta controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/reciboventa")
 */
class ReciboVentaController extends Controller {

    /**
     * Lists all ReciboVenta entities.
     *
     * @Route("/", name="admin_reciboventa")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->get('security_role')->controlRolesUser();
        list($filterForm, $queryBuilder) = $this->filter();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $queryBuilder, $this->get('request')->query->get('page', 1), (isset($this->container->parameters['knp_paginator.page_range'])) ? $this->container->parameters['knp_paginator.page_range'] : 10
        );

        return array(
            'entities' => $pagination,
            'filterForm' => $filterForm->createView(),
        );
    }

    /**
     * Process filter request.
     *
     * @return array
     */
    protected function filter() {
        $request = $this->getRequest();
        $session = $request->getSession();
        $filterForm = $this->createFilterForm();
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository('SistemaFACTURACIONBundle:ReciboVenta')
                ->createQueryBuilder('a')
                ->orderBy('a.id', 'DESC')
        ;
        // Bind values from the request
        $filterForm->handleRequest($request);
        // Reset filter
        if ($filterForm->get('reset')->isClicked()) {
            $session->remove('ReciboVentaControllerFilter');
            $filterForm = $this->createFilterForm();
        }

        // Filter action
        if ($filterForm->get('filter')->isClicked()) {
            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('ReciboVentaControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('ReciboVentaControllerFilter')) {
                $filterData = $session->get('ReciboVentaControllerFilter');           
                $filterForm = $this->createFilterForm($filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }

    /**
     * Create filter form.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createFilterForm($filterData = null) {
        $form = $this->createForm(new ReciboVentaFilterType(), $filterData, array(
            'action' => $this->generateUrl('admin_reciboventa'),
            'method' => 'GET',
        ));

        $form
                ->add('filter', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.index.filter',
                    'attr' => array('class' => 'btn btn-success col-lg-1'),
                ))
                ->add('reset', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.index.reset',
                    'attr' => array('class' => 'btn btn-danger col-lg-1 col-lg-offset-1'),
                ))
        ;

        return $form;
    }

    /**
     * Creates a new ReciboVenta entity.
     *
     * @Route("/", name="admin_reciboventa_create")
     * @Method("POST")
     * @Template("SistemaFACTURACIONBundle:ReciboVenta:new.html.twig")
     */
    public function createAction(Request $request) {
        $this->get('security_role')->controlRolesUser();
        $entity = new ReciboVenta();
        //agrego cuentas segun proceso
        $proceso = $request->request->get('sistema_facturacionbundle_reciboventa')['proceso'];
        $procesoService = $this->get('proceso');
        $entityProceso  = $procesoService->create($entity, $proceso);
        $entity->setProceso($entityProceso);
        //fin agrego
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            //MODIFICACION DE FECHA FORM
            $fecha = $entity->getFecha()->format('Y-m-d');
            $time = new \DateTime('now');
            //agrego a la fecha del form el time
            $newFecha = $fecha . ' ' . $time->format('H:i:s');
            $newDateTimeNow = new \DateTime();
            $newDateTimeNow->createFromFormat('Y-m-d H:i:s', $newFecha);
            //seteo la fecha con su time
            $entity->setFecha($newDateTimeNow);
            //FIN MODIFICACION DE FECHA FORM
            $servicioCuenta = $this->get('actualizarCuentas');
            $servicioCuenta->creaAsientos($entity, "cliente");
            //Si es ROLE_ADMIN entra y si hay mas de 1 caja abierta por usuario las trae a todas.
            if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
                $cajaAbierta = $em->getRepository('SistemaFACTURACIONBundle:Caja')
                    ->findCajaAbierta();
            } else {
                $cajaAbierta = $em->getRepository('SistemaFACTURACIONBundle:Caja')
                    ->findCajaAbierta($this->getUser()->getId());
            }
            //Si trae cajaAbierta entra
            if (!empty($cajaAbierta)) {
                $entity->setCaja($cajaAbierta[0]);
                $em->persist($entity);
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', 'flash.create.success');
                
                if ($form->get('saveAndAdd')->isClicked()) {
                    $numero = $entity->getNumero();
                    $form->getData()->setNumero($numero + 1);
                    $nextAction = $this->generateUrl('admin_reciboventa_new', array('proceso' => $entity->getProceso()->getId()));
                } else {
                    $nextAction = $this->generateUrl('admin_reciboventa_show', array('id' => $entity->getId()));
                }
                
                return $this->redirect($nextAction);
            } else {
                $this->get('session')->getFlashBag()->add('danger', 'No hay ninguna Caja Abierta');
            }
        }
        $this->get('session')->getFlashBag()->add('danger', 'Los asientos deben estar balanceados y sin valores negativos');

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a ReciboVenta entity.
     *
     * @param ReciboVenta $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ReciboVenta $entity) {
        $form = $this->createForm(new ReciboVentaType(), $entity, array(
            'action' => $this->generateUrl('admin_reciboventa_create'),
            'method' => 'POST',
        ));

        $form
            ->add('save', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label' => 'views.new.save',
                'attr' => array('class' => 'btn btn-success col-lg-2')
            ))
            ->add('saveAndAdd', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label' => 'views.new.saveAndAdd',
                'attr' => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
            ))
        ;

        return $form;
    }

    /**
     * Displays a form to create a new ReciboVenta entity.
     *
     * @Route("/new/{proceso}", name="admin_reciboventa_new", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function newAction($proceso) {
        $this->get('security_role')->controlRolesUser();
        $entity = new ReciboVenta();
        $em = $this->getDoctrine()->getManager();
        $ultimoNumero = $em->getRepository('SistemaFACTURACIONBundle:ReciboVenta')->findUltimoNumero();
        if (!empty($ultimoNumero)) {
          $entity->setNumero($ultimoNumero[0]['numero'] + 1);
        } else {
          $entity->setNumero(1);
        }
        $procesoService = $this->get('proceso');
        $entityProceso = $procesoService->create($entity, $proceso);
        $entity->setProceso($entityProceso);
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a ReciboVenta entity.
     *
     * @Route("/{id}", name="admin_reciboventa_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaFACTURACIONBundle:ReciboVenta')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ReciboVenta entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing ReciboVenta entity.
     *
     * @Route("/{id}/edit", name="admin_reciboventa_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaFACTURACIONBundle:ReciboVenta')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ReciboVenta entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a ReciboVenta entity.
     *
     * @param ReciboVenta $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(ReciboVenta $entity) {
        $form = $this->createForm(new ReciboVentaType(), $entity, array(
            'action' => $this->generateUrl('admin_reciboventa_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form
            ->add('save', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label' => 'views.new.save',
                'attr' => array('class' => 'btn btn-success col-lg-2')
            ))
            ->add('saveAndAdd', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label' => 'views.new.saveAndAdd',
                'attr' => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
            ))
        ;

        return $form;
    }

    /**
     * Edits an existing ReciboVenta entity.
     *
     * @Route("/{id}", name="admin_reciboventa_update")
     * @Method("PUT")
     * @Template("SistemaFACTURACIONBundle:ReciboVenta:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaFACTURACIONBundle:ReciboVenta')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ReciboVenta entity.');
        }
        $resgAnulado = $entity->getAnulado();
        $resgMonto = $entity->getMonto();
        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            //$servicioCuenta = $this->get('actualizarCuentas');
            //$servicioCuenta->actualizarMontos($entity, $resgAnulado, $resgMonto, "cliente");
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            if ($editForm->get('saveAndAdd')->isClicked()) {
                $numero = $entity->getNumero();
                $editForm->getData()->setNumero($numero + 1);
                $nextAction = $this->generateUrl('admin_reciboventa_new', array('proceso' => $entity->getProceso()->getId()));
            } else {
                $nextAction = $this->generateUrl('admin_reciboventa_show', array('id' => $entity->getId()));
            }

            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a ReciboVenta entity.
     *
     * @Route("/{id}", name="admin_reciboventa_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $this->get('security_role')->controlRolesUser();
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SistemaFACTURACIONBundle:ReciboVenta')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ReciboVenta entity.');
            }
            /* if (!$entity->getAnulado()) {
              $servicioCuenta = $this->get('actualizarCuentas');
              $servicioCuenta->actualizarMontosCuandoElimina($entity, "cliente");
              } */
            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.delete.success');
        }

        return $this->redirect($this->generateUrl('admin_reciboventa'));
    }

    /**
     * Creates a form to delete a ReciboVenta entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        $mensaje = $this->get('translator')->trans('views.recordactions.confirm', array(), 'MWSimpleCrudGeneratorBundle');
        $onclick = 'return confirm("' . $mensaje . '");';
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('admin_reciboventa_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array(
                            'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                            'label' => 'views.recordactions.delete',
                            'attr' => array(
                                'class' => 'btn btn-danger col-lg-11',
                                'onclick' => $onclick,
                            )
                        ))
                        ->getForm()
        ;
    }

}
