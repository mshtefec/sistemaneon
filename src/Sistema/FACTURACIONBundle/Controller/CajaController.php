<?php

namespace Sistema\FACTURACIONBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\FACTURACIONBundle\Entity\Caja;
use Sistema\FACTURACIONBundle\Form\CajaType;
use Sistema\FACTURACIONBundle\Form\CajaFilterType;

/**
 * Caja controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("admin/caja")
 */
class CajaController extends Controller
{

    /**
     * Lists all Caja entities.
     *
     * @Route("/", name="admin_caja")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->get('security_role')->controlRolesUser();
        list($filterForm, $queryBuilder) = $this->filter();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder,
            $this->get('request')->query->get('page', 1),
            (isset($this->container->parameters['knp_paginator.page_range'])) ? $this->container->parameters['knp_paginator.page_range'] : 10
        );

        return array(
            'entities'   => $pagination,
            'filterForm' => $filterForm->createView(),
        );
    }

    /**
    * Process filter request.
    *
    * @return array
    */
    protected function filter()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        $filterForm = $this->createFilterForm();
        $em = $this->getDoctrine()->getManager();

        $queryBuilder = $em->getRepository('SistemaFACTURACIONBundle:Caja')->createQueryBuilder('a');
        //Si no es ROLE_ADMIN entra
        if (!$this->get('security.context')->isGranted('ROLE_ADMIN')) {
            $userId = $this->getUser()->getId();
            $queryBuilder
                ->innerJoin('a.user', 'u')
                ->where('u.id = :id')
                ->setParameter('id', $userId)
            ;
        }
        $queryBuilder->orderBy('a.id', 'DESC');

        // Bind values from the request
        $filterForm->handleRequest($request);
        // Reset filter
        if ($filterForm->get('reset')->isClicked()) {
            $session->remove('CajaControllerFilter');
            $filterForm = $this->createFilterForm();
        }

        // Filter action
        if ($filterForm->get('filter')->isClicked()) {
            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('CajaControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('CajaControllerFilter')) {
                $filterData = $session->get('CajaControllerFilter');
                $filterForm = $this->createFilterForm($filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }
    /**
    * Create filter form.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createFilterForm($filterData = null)
    {
        $form = $this->createForm(new CajaFilterType(), $filterData, array(
            'action' => $this->generateUrl('admin_caja'),
            'method' => 'GET',
        ));

        $form
            ->add('filter', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label'              => 'views.index.filter',
                'attr'               => array('class' => 'btn btn-success col-lg-1'),
            ))
            ->add('reset', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label'              => 'views.index.reset',
                'attr'               => array('class' => 'btn btn-danger col-lg-1 col-lg-offset-1'),
            ))
        ;

        return $form;
    }

    /**
     * Displays a form to create a new Caja entity.
     *
     * @Route("/new/", name="admin_caja_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->get('security_role')->controlRolesUser();
        $entity = new Caja();
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $cajaAbierta = $em->getRepository('SistemaFACTURACIONBundle:Caja')->findCajaAbierta($user->getId());
        if (empty($cajaAbierta)){
            $entity->setUser($user);
            $entity->setFechaInicio(new \DateTime('now'));
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');
        } else {
            $this->get('session')->getFlashBag()->add('danger', 'No es posible tener dos cajas abiertas');
        }

        return $this->redirect($this->generateUrl('admin_caja'));
    }

    /**
     * Finds and displays a Caja entity.
     *
     * @Route("/{id}", name="admin_caja_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaFACTURACIONBundle:Caja')->find($id);
        $asientos = $em->getRepository('SistemaFACTURACIONBundle:Asiento')->findAsientosByCaja($id);

        $asientosAcumulados = $em->getRepository('SistemaFACTURACIONBundle:Asiento')->findAsientosByFechas($entity->getFechaInicio());
        
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Caja entity.');
        }

        return array(
            'entity'   => $entity,
            'asientos' => $asientos,
            'acumulados' => $asientosAcumulados,
        );
    }

    /**
     * Finds and close a Caja entity.
     *
     * @Route("/{id}/cerrar", name="admin_caja_cerrar")
     * @Method("GET")
     * @Template()
     */
    public function cerrarAction($id) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();
        $fechaFin = new \DateTime('now');

        $entity = $em->getRepository('SistemaFACTURACIONBundle:Caja')->find($id);

        if ($entity->getEstado() != 0) {
            $entity->setEstado(0);
            $entity->setFechaFin($fechaFin);
            $em->persist($entity);
            $em->flush();
        }else{
            $this->get('session')->getFlashBag()->add('danger', 'La Caja se encuentra Cerrada!');
        }

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Caja entity.');
        }

        return $this->redirect($this->generateUrl('admin_caja'));
    }

    /**
     * Finds and displays a Caja entity.
     *
     * @param type $id Id de la entidad
     *
     * @Route("/imprimir-caja/{id}", name="admin_caja_imprimir")
     * @Method("GET")
     * @Template()
     * @return view
     */
    public function imprimirCajapdfAction($id) {
        set_time_limit(0);
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('SistemaFACTURACIONBundle:Caja')->find($id);
        $asientos = $em->getRepository('SistemaFACTURACIONBundle:Asiento')->findAsientosByCaja($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Caja entity.');
        }

        $config = array(
            'titulo' => 'Reporte O.T. movimientos de stock',
            'PDF_HEADER_LOGO' => 'logo_neon.jpg',
            'PDF_HEADER_LOGO_WIDTH' => '55',
            'PDF_HEADER_TITLE' => '',
            'PDF_HEADER_STRING' => '
                                                                        Caja de Facturación
                                                                        M.M.O. Raúl Roberto Rojas
                                                                        Av. San Martín 2386 Resistencia - Chaco                                                                Caja Nro: ' . $entity->getId() . '            Tel: 362-4489609 / Cel: 362-4641854           Correo: cobranza@neon3r.com.ar',
            'pie' => ''
        );

        $html = $this->renderView('SistemaFACTURACIONBundle:Caja:show.pdf.twig', array('entity' => $entity, 'asientos' => $asientos));

        return $this->get('io_tcpdf_mws')->quick_pdf($html, $config);
    }
}
