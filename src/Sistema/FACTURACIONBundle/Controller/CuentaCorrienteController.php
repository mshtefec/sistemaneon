<?php

namespace Sistema\FACTURACIONBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\FACTURACIONBundle\Entity\CuentaCorriente;
use Sistema\FACTURACIONBundle\Form\CuentaCorrienteType;
use Sistema\FACTURACIONBundle\Form\CuentaCorrienteFilterType;

/**
 * CuentaCorriente controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/cuentacorriente")
 */
class CuentaCorrienteController extends Controller {
    private $estado = null;

    /**
     * Lists all CuentaCorriente entities.
     *
     * @Route("/", name="cuentacorriente")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->get('security_role')->controlRolesUser();
        $request = $this->getRequest();
        list($filterForm, $queryBuilder) = $this->filter();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder, $this->get('request')->query->get('page', 1), (isset($this->container->parameters['knp_paginator.page_range'])) ? $this->container->parameters['knp_paginator.page_range'] : 10
        );

        return array(
            'entities'   => $pagination,
            'filterForm' => $filterForm->createView(),
        );
    }

    /**
     * Lists CuentaCorriente by cliente.
     *
     * @Route("/cliente/{idCliente}/{estado}", name="cuentacorriente_cliente")
     * @Method("GET")
     * @Template()
     */
    public function indexClienteAction($idCliente, $estado) {
        $this->estado = $estado;
        list($filterForm, $queryBuilder) = $this->filter('cliente', $idCliente, $estado);
// ladybug_dump_die($queryBuilder->getQuery()->getResult());
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder, $this->get('request')->query->get('page', 1), (isset($this->container->parameters['knp_paginator.page_range'])) ? $this->container->parameters['knp_paginator.page_range'] : 10
        );


        return array(
            'idCliente'  => $idCliente,
            'entities'   => $pagination,
            'filterForm' => $filterForm->createView(),
            'estado'     => $estado,
        );
    }

    /**
     * Lists CuentaCorriente by cliente.
     *
     * @Route("/proveedor/{idProveedor}", name="cuentacorriente_proveedor")
     * @Method("GET")
     * @Template()
     */
    public function indexProveedorAction($idProveedor) {
        $this->get('security_role')->controlRolesUser();
        list($filterForm, $queryBuilder) = $this->filter('proveedor', $idProveedor);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $queryBuilder, $this->get('request')->query->get('page', 1), (isset($this->container->parameters['knp_paginator.page_range'])) ? $this->container->parameters['knp_paginator.page_range'] : 10
        );

        return array(
            'entities'   => $pagination,
            'filterForm' => $filterForm->createView(),
        );
    }

    /**
     * Process filter request.
     *
     * @return array
     */
    protected function filter($from = null, $id = null) {
        $request = $this->getRequest();
        $session = $request->getSession();
        $filterForm = $this->createFilterForm($from, $id);
        $em = $this->getDoctrine()->getManager();

        if ($from == 'cliente') {
            $queryBuilder = $em->getRepository('SistemaFACTURACIONBundle:CuentaCorriente')->findCuentaCorrienteByClienteId($id);
        } else if ($from == 'proveedor') {
            $queryBuilder = $em->getRepository('SistemaFACTURACIONBundle:CuentaCorriente')
                ->createQueryBuilder('a')
                ->select('a, pro')
                ->join('a.proveedor', 'pro')
                ->orderBy('a.monto', 'DESC');
            ;
            if (!is_null($id)) {
                $queryBuilder
                    ->leftJoin('a.asientos', 'asientos')
                    ->where('pro.id = :idProveedor')
                    ->setParameter('idProveedor', $id)
                    ->orderBy('a.monto', 'DESC');
                ;
            }
        } else {
            $queryBuilder = $em->getRepository('SistemaFACTURACIONBundle:CuentaCorriente')->findAll();
        }

        // Bind values from the request
        $filterForm->handleRequest($request);
        // Reset filter
        if ($filterForm->get('reset')->isClicked()) {
            $session->remove('CuentaCorrienteControllerFilter');
            $filterForm = $this->createFilterForm();
        }

        // Filter action
        if ($filterForm->get('filter')->isClicked()) {
            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('CuentaCorrienteControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('CuentaCorrienteControllerFilter')) {
                $filterData = $session->get('CuentaCorrienteControllerFilter');
                $filterForm = $this->createFilterForm($filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }

    /**
     * Create filter form.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createFilterForm($from, $cliente = 0, $filterData = null) {
        if ($from == 'cliente') {
            $arrayUrl = array('idCliente' => $cliente, 'estado' => $this->estado);
            $form = $this->createForm(new CuentaCorrienteFilterType(), $filterData, array(
                'action' => $this->generateUrl('cuentacorriente_cliente', $arrayUrl),
                'method' => 'GET',
            ));
        }
        if ($from == 'proveedor') {
            $form = $this->createForm(new CuentaCorrienteFilterType(), $filterData, array(
                'action' => $this->generateUrl('cuentacorriente', array('idProveedor' => $cliente)),
                'method' => 'GET',
            ));
        } else {
            $form = $this->createForm(new CuentaCorrienteFilterType(), $filterData, array(
                'action' => $this->generateUrl('cuentacorriente'),
                'method' => 'GET',
            ));
        }

        $form
            ->add('filter', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label' => 'views.index.filter',
                'attr' => array('class' => 'btn btn-success col-lg-1'),
            ))
            ->add('reset', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label' => 'views.index.reset',
                'attr' => array('class' => 'btn btn-danger col-lg-1 col-lg-offset-1'),
            ))
        ;

        return $form;
    }

    /**
     * Finds and displays a CuentaCorriente entity.
     *
     * @Route("/imprimirPdf/", name="cuentacorriente_imprimir")
     * @Method("GET")
     * @Template()
     * @return view
     */
    public function imprimirpdfAction() {
        set_time_limit(0);
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('SistemaFACTURACIONBundle:CuentaCorriente')->findAll();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Orden entity.');
        }

        $config = array(
            'titulo' => 'Impresion de Cuenta Corriente',
            'PDF_HEADER_LOGO' => 'logo_neon.jpg',
            'PDF_HEADER_LOGO_WIDTH' => '55',
            'PDF_HEADER_TITLE' => '',
            'PDF_HEADER_STRING_FACTURA' => 'X',
            'PDF_HEADER_STRING_FACTURA_DES' => 'No Valido Como Factura',
            'PDF_HEADER_STRING' => '
                            M.M.O. Raúl Roberto Rojas
                            Av. San Martín 2386 Resistencia - Chaco                                                                Cuentas Corrientes ' . '
                            Tel: 362-4489609 / Cel: 362-4641854                                                       Fecha: ' . date('d/m/Y') . '
                            Correo: gerencia@neon3r.com.ar',
            'pie' => '
                        ________________________                                                                         ____________________________
                            Firma Tecnico Instalador                                                                                  Firma Conformidad del Cliente',
        );

        $html = $this->renderView('SistemaFACTURACIONBundle:CuentaCorriente:show.pdf.twig', array(
            'entities' => $entity,
        ));

        return $this->get('io_tcpdf_mws')->quick_pdf($html, $config);
    }

    /**
     * Finds and displays a CuentaCorriente entity.
     *
     * @Route("/imprimir/cliente/pdf/{idCliente}", name="cuenta_corriente_cliente_imprimir")
     * @Method("GET")
     * @Template()
     * @return view
     */
    public function imprimirClientepdfAction($idCliente) {
        set_time_limit(0);
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaFACTURACIONBundle:CuentaCorriente')->findCuentaCorrienteByClienteId($idCliente);
        
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cuenta Corriente.');
        }

        $config = array(
            'titulo' => 'Impresion de Cuenta Corriente',
            'PDF_HEADER_LOGO' => 'logo_neon.jpg',
            'PDF_HEADER_LOGO_WIDTH' => '55',
            'PDF_HEADER_TITLE' => '',
            'PDF_HEADER_STRING' => '
                            M.M.O. Raúl Roberto Rojas
                            Av. San Martín 2386 Resistencia - Chaco                                                            Cuenta Corriente ' . '
                            Tel: 362-4489609 / Cel: 362-4641854                                                       A la fecha ' . date('d/m/Y') . '
                            Correo: gerencia@neon3r.com.ar',
            'pie' => '',
        );

        $html = $this->renderView('SistemaFACTURACIONBundle:CuentaCorriente:showCliente.pdf.twig', array(
            'entities' => $entity->getQuery()->getResult(),
        ));

        return $this->get('io_tcpdf_mws')->quick_pdf($html, $config);
    }
}
