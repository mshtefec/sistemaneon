<?php

namespace Sistema\FACTURACIONBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\FACTURACIONBundle\Entity\AnticipoVenta;
use Sistema\FACTURACIONBundle\Form\AnticipoVentaType;
use Sistema\FACTURACIONBundle\Form\AnticipoVentaFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sistema\FACTURACIONBundle\Entity\FacturaVenta;

/**
 * AnticipoVenta controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/anticipoventa")
 */
class AnticipoVentaController extends Controller {

    /**
     * Lists all AnticipoVenta entities.
     *
     * @Route("/", name="admin_anticipoventa")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->get('security_role')->controlRolesUser();
        list($filterForm, $queryBuilder) = $this->filter();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $queryBuilder, $this->get('request')->query->get('page', 1), (isset($this->container->parameters['knp_paginator.page_range'])) ? $this->container->parameters['knp_paginator.page_range'] : 10
        );

        return array(
            'entities' => $pagination,
            'filterForm' => $filterForm->createView(),
        );
    }

    /**
     * Process filter request.
     *
     * @return array
     */
    protected function filter() {
        $request = $this->getRequest();
        $session = $request->getSession();
        $filterForm = $this->createFilterForm();
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository('SistemaFACTURACIONBundle:AnticipoVenta')
                ->createQueryBuilder('a')
                ->orderBy('a.id', 'DESC')
        ;
        // Bind values from the request
        $filterForm->handleRequest($request);
        // Reset filter
        if ($filterForm->get('reset')->isClicked()) {
            $session->remove('AnticipoVentaControllerFilter');
            $filterForm = $this->createFilterForm();
        }

        // Filter action
        if ($filterForm->get('filter')->isClicked()) {
            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('AnticipoVentaControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('AnticipoVentaControllerFilter')) {
                $filterData = $session->get('AnticipoVentaControllerFilter');
                $filterForm = $this->createFilterForm($filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }

    /**
     * Create filter form.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createFilterForm($filterData = null) {
        $form = $this->createForm(new AnticipoVentaFilterType(), $filterData, array(
            'action' => $this->generateUrl('admin_anticipoventa'),
            'method' => 'GET',
        ));

        $form
                ->add('filter', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.index.filter',
                    'attr' => array('class' => 'btn btn-success col-lg-1'),
                ))
                ->add('reset', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.index.reset',
                    'attr' => array('class' => 'btn btn-danger col-lg-1 col-lg-offset-1'),
                ))
        ;

        return $form;
    }

    /**
     * Creates a new AnticipoVenta entity.
     *
     * @Route("/", name="admin_anticipoventa_create")
     * @Method("POST")
     * @Template("SistemaFACTURACIONBundle:AnticipoVenta:new.html.twig")
     */
    public function createAction(Request $request) {
        $this->get('security_role')->controlRolesUser();
        $entity = new AnticipoVenta();
        //agrego cuentas segun proceso
        $proceso = $request->request->get('sistema_facturacionbundle_anticipoventa')['proceso'];
        $procesoService = $this->get('proceso');
        $entityProceso  = $procesoService->create($entity, $proceso);
        $entity->setProceso($entityProceso);
        //fin agrego
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $servicioCuenta = $this->get('actualizarCuentas');
            $servicioCuenta->creaAsientos($entity, "cliente");
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            if ($form->get('saveAndAdd')->isClicked()) {
                    $numero = $entity->getNumero();
                    $form->getData()->setNumero($numero + 1);
                    $nextAction = $this->generateUrl('admin_anticipoventa_new', array('proceso' => $entityProceso));
                } else {
                    $nextAction = $this->generateUrl('admin_anticipoventa_show', array('id' => $entity->getId()));
            }
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a AnticipoVenta entity.
     *
     * @param AnticipoVenta $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(AnticipoVenta $entity) {
        $form = $this->createForm(new AnticipoVentaType(), $entity, array(
            'action' => $this->generateUrl('admin_anticipoventa_create'),
            'method' => 'POST',
        ));

        $form
                ->add(
                        'save', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.save',
                    'attr' => array('class' => 'btn btn-success col-lg-2')
                        )
                )
                ->add(
                        'saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
                        )
                )
        ;

        return $form;
    }

    /**
     * Displays a form to create a new AnticipoVenta entity.
     *
     * @Route("/new/{proceso}", name="admin_anticipoventa_new", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function newAction($proceso) {
        $this->get('security_role')->controlRolesUser();
        $entity = new AnticipoVenta();
        $em = $this->getDoctrine()->getManager();
        $ultimoNumero = $em->getRepository('SistemaFACTURACIONBundle:AnticipoVenta')->findUltimoNumero();
        if (!empty($ultimoNumero)) {
            $entity->setNumero($ultimoNumero[0]['numero'] + 1);
        } else {
            $entity->setNumero(1);
        }
        $entity->setFecha(new \DateTime());
        $procesoService = $this->get('proceso');
        $entityProceso = $procesoService->create($entity, $proceso);
        $entity->setProceso($entityProceso);
        $form = $this->createCreateForm($entity);
        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a AnticipoVenta entity.
     *
     * @Route("/{id}", name="admin_anticipoventa_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaFACTURACIONBundle:AnticipoVenta')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AnticipoVenta entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Finds and displays a AnticipoVenta entity.
     *
     * @Route("/generar-factura/by-cliente/{cliente}", name="admin_anticipoventa_generarFactura", defaults={"cliente" = 0}, options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function generarFacturaAction($cliente) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entitys = $em->getRepository('SistemaFACTURACIONBundle:AnticipoVenta')->findByCliente($cliente);
        $cliente = $em->getRepository('SistemaRRHHBundle:Cliente')->find($cliente);

        return array(
            'entitys' => $entitys,
            'cliente' => $cliente
        );
    }

    /**
     * Displays a form to edit an existing AnticipoVenta entity.
     *
     * @Route("/{id}/edit", name="admin_anticipoventa_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaFACTURACIONBundle:AnticipoVenta')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AnticipoVenta entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a AnticipoVenta entity.
     *
     * @param AnticipoVenta $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(AnticipoVenta $entity) {
        $form = $this->createForm(new AnticipoVentaType(), $entity, array(
            'action' => $this->generateUrl('admin_anticipoventa_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form
                ->add(
                        'save', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.save',
                    'attr' => array('class' => 'btn btn-success col-lg-2')
                        )
                )
                ->add(
                        'saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
                        )
                )
        ;

        return $form;
    }

    /**
     * Edits an existing AnticipoVenta entity.
     *
     * @Route("/{id}", name="admin_anticipoventa_update")
     * @Method("PUT")
     * @Template("SistemaFACTURACIONBundle:AnticipoVenta:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaFACTURACIONBundle:AnticipoVenta')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AnticipoVenta entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked() ? $this->generateUrl('admin_anticipoventa_new') : $this->generateUrl('admin_anticipoventa_show', array('id' => $id));
            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a AnticipoVenta entity.
     *
     * @Route("/{id}", name="admin_anticipoventa_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $this->get('security_role')->controlRolesUser();
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SistemaFACTURACIONBundle:AnticipoVenta')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find AnticipoVenta entity.');
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.delete.success');
        }

        return $this->redirect($this->generateUrl('admin_anticipoventa'));
    }

    /**
     * Creates a form to delete a AnticipoVenta entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        $mensaje = $this->get('translator')->trans('views.recordactions.confirm', array(), 'MWSimpleCrudGeneratorBundle');
        $onclick = 'return confirm("' . $mensaje . '");';
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('admin_anticipoventa_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array(
                            'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                            'label' => 'views.recordactions.delete',
                            'attr' => array(
                                'class' => 'btn btn-danger col-lg-11',
                                'onclick' => $onclick,
                            )
                        ))
                        ->getForm()
        ;
    }

    /**
     * @Route("/generar-factura-post/", name="admin_generarFactura_post")
     * @Method("POST")
     */
    public function generarFacturaPostAction() {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();

        $listaAnticipos = $request->request->get('listaAnticipos', null);
        if (!is_null($listaAnticipos)) {
            $anticipos = $em->getRepository('SistemaFACTURACIONBundle:AnticipoVenta')->findByArray($listaAnticipos);
            $seguir = $em->getRepository('SistemaFACTURACIONBundle:AnticipoVenta')->findByArrayGroupByOrden($listaAnticipos);

            if (count($seguir) == 1) {
                $ultimoNumero = $em->getRepository('SistemaFACTURACIONBundle:AnticipoVenta')->findUltimoNumero()[0]["numero"];
                $ultimoNumero = $ultimoNumero + 1;
                $facturaVenta = new FacturaVenta();
                $facturaVenta->setAnulado(false);
                foreach ($anticipos as $anticipo) {
                    foreach ($anticipo->getAsientos() as $asiento) {
                        $facturaVenta->addAsiento($asiento);
                    }
                    $facturaVenta->setMonto($facturaVenta->getMonto() + $anticipo->getMonto());
                    $anticipo->setFacturaGenerada(true);
                    $em->persist($anticipo);
                }
                $facturaVenta->setFecha(new \DateTime('now'));
                $facturaVenta->setDescripcion("Anticipos");
                $facturaVenta->setFormaPago("Indefinida");
                $facturaVenta->setOrden($anticipos[0]->getOrden());
                $facturaVenta->setNumero($ultimoNumero);
                $facturaVenta->setTipo("Indefinida");
                $em->persist($facturaVenta);

                $em->flush();
                $this->get('session')->getFlashBag()->add('success', 'Factura Generada');
            } else {
                $this->get('session')->getFlashBag()->add('warning', 'Los anticipos seleccionados debe ser de la misma orden');
            }
        }

        return $this->redirect($this->generateUrl('admin_anticipoventa_generarFactura'), 301);
    }

    /**
     * Autocomplete a Orders entity.
     *
     * @Route("/autocomplete-forms/get-cliente", name="AnticipoVenta_autocomplete_clientes")
     */
    public function getAutocompleteClientes() {
        $options = array(
            'repository' => "SistemaRRHHBundle:Cliente",
            'field' => "apellido",
        );
        $request = $this->getRequest();
        $term = $request->query->get('q', null);

        $em = $this->getDoctrine()->getManager();

        $qb = $em->getRepository($options['repository'])->createQueryBuilder('a');
        $qb
                ->add('where', "a.apellido" . " LIKE ?1 or a.nombre" . " LIKE ?1 or a.empresa" . " LIKE ?1")
                ->add('orderBy', "a." . $options['field'] . " ASC")
                ->setParameter(1, "%" . $term . "%")
        ;
        $entities = $qb->getQuery()->getResult();

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

}
