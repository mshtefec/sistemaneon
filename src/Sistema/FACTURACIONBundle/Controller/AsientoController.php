<?php

namespace Sistema\FACTURACIONBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\FACTURACIONBundle\Entity\Asiento;
use Sistema\FACTURACIONBundle\Form\AsientoType;
use Sistema\FACTURACIONBundle\Form\AsientoFilterType;
use Exporter\Source\DoctrineORMQuerySourceIterator;
use Exporter\Source\ArraySourceIterator;
use Exporter\Writer\XlsWriter;
use Exporter\Handler;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Asiento controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/asiento")
 */
class AsientoController extends Controller
{
    /**
     * Lists all Asiento entities.
     *
     * @Route("/", name="admin_asiento")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->get('security_role')->controlRolesUser();
        list($filterForm, $queryBuilder) = $this->filter();

        return array(
            'entities'   => $queryBuilder->getQuery()->getResult(),
            'filterForm' => $filterForm->createView(),
        );
    }

    /**
     * Process filter request.
     *
     * @return array
     */
    protected function filter()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        $filterForm = $this->createFilterForm();
        $em = $this->getDoctrine()->getManager();

        $queryBuilder = $em->createQueryBuilder()
            ->select('asiento', 'cuenta', 'comprobante')
            ->from('SistemaFACTURACIONBundle:Asiento', 'asiento')
            ->Join('asiento.cuenta', 'cuenta')
            ->leftJoin('asiento.comprobante', 'comprobante')
            ->orderBy('asiento.fecha', 'DESC')
        ;
        // Bind values from the request
        $filterForm->handleRequest($request);
        // Reset filter
        if ($filterForm->get('reset')->isClicked()) {
            $session->remove('AsientoControllerFilter');
            $filterForm = $this->createFilterForm();
        }

        // Filter action
        if ($filterForm->get('filter')->isClicked()) {
            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('AsientoControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('AsientoControllerFilter')) {
                $filterData = $session->get('AsientoControllerFilter');
                $filterForm = $this->createFilterForm($filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            } else {
                $now = new \DateTime('Today');
                $oneWeekBefore = new \DateTime('Today');
                $oneWeekBefore->modify('-7 day');
                $queryBuilder
                    ->andWhere("comprobante.fecha BETWEEN :fechaBefore AND :fechaNow")
                    ->setParameter('fechaNow', $now->format('Y-m-d H:i:s'))
                    ->setParameter('fechaBefore', $oneWeekBefore->format('Y-m-d H:i:s'))
                ;
                $filterForm = $this->createFilterForm();
            }
        }

        return array($filterForm, $queryBuilder);
    }

    /**
     * Create filter form.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createFilterForm($filterData = null)
    {
        $form = $this->createForm(new AsientoFilterType(), $filterData, array(
            'action' => $this->generateUrl('admin_asiento'),
            'method' => 'GET',
        ));

        $form
            ->add('filter', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label'              => 'views.index.filter',
                'attr'               => array('class' => 'btn btn-success col-lg-1'),
            ))
            ->add('reset', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label'              => 'views.index.reset',
                'attr'               => array('class' => 'btn btn-danger col-lg-1 col-lg-offset-1'),
            ))
        ;

        return $form;
    }

    /**
     * Creates a new Asiento entity.
     *
     * @Route("/", name="asiento_create")
     * @Method("POST")
     * @Template("SistemaFACTURACIONBundle:Asiento:new.html.twig")
     */
    public function createAction(Request $request) {
        $this->get('security_role')->controlRolesUser();
        $entity = new Asiento();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            //MODIFICACION DE FECHA FORM
            $fecha = $entity->getFecha()->format('Y-m-d');
            $time = new \DateTime('now');
            //agrego a la fecha del form el time
            $newFecha = $fecha . ' ' . $time->format('H:i:s');
            $newDateTimeNow = new \DateTime();
            $newDateTimeNow->createFromFormat('Y-m-d H:i:s', $newFecha);
            //seteo la fecha con su time
            $entity->setFecha($newDateTimeNow);
            //FIN MODIFICACION DE FECHA FORM
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked()
                    ? $this->generateUrl('asiento_new')
                    : $this->generateUrl('asiento_show', array('id' => $entity->getId()));
            return $this->redirect($nextAction);

        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Asiento entity.
     *
     * @param Asiento $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Asiento $entity)
    {
        $form = $this->createForm(new AsientoType(), $entity, array(
            'action' => $this->generateUrl('asiento_create'),
            'method' => 'POST',
        ));

        $form
            ->add(
                'save', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label'              => 'views.new.save',
                'attr'               => array('class' => 'btn btn-success col-lg-2')
                )
            )
            ->add(
                'saveAndAdd', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label'              => 'views.new.saveAndAdd',
                'attr'               => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
                )
            )
        ;

        return $form;
    }

    /**
     * Displays a form to create a new Asiento entity.
     *
     * @Route("/new", name="asiento_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->get('security_role')->controlRolesUser();
        $entity = new Asiento();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Asiento entity.
     *
     * @Route("/{id}", name="asiento_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaFACTURACIONBundle:Asiento')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Asiento entity.');
        }

        //$deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            //'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Finds and displays a Asientos entity.
     *
     * @param type $id Id de la entidad
     *
     * @Route("/xls-asientos", name="admin_asientos_xls")
     * @Method("GET")
     * @Template()
     * @return view
     */
    public function imprimirComprobantesxlsAction() {
        set_time_limit(0);
        $this->get('security_role')->controlRolesUser();

        list($filterForm, $queryBuilder) = $this->filter();

        $asientos = $queryBuilder->getQuery()->getResult();
        //  $format = 'xls';
        $filename = 'Asientos.xls';
        $datos = array();
        $debe  = 0;
        $haber = 0;

        foreach ($asientos as $asiento) {
            $debe  = $debe + $asiento->getDebe();
            $haber = $haber + $asiento->getHaber();
            $datos[] = array(
                'Tipo'        => $asiento->getComprobante()->getTipoDocumento(),
                'Nro'         => $asiento->getComprobante()->getNumero(),
                'Descripcion' => $asiento->getComprobante()->getDescripcion(),
                'Fecha'  => $asiento->getFecha()->format('d/m/Y'),
                'Cuenta' => $asiento->getCuenta(),
                'Debe'   => $asiento->getDebe(),
                'Haber'  => $asiento->getHaber(),
            );
        }
        $datos[] = array(
            'Tipo'        => "",
            'Nro'         => "",
            'Descripcion' => "",
            'Fecha'  => "",
            'Cuenta' => "",
            'Debe'   => "Monto Debe: " . $debe,
            'Haber'  => "Monto Haber " . $haber,
        );

        $writer      = new XlsWriter('php://output');
        $contentType = 'application/vnd.ms-excel';
        $source   = new \Exporter\Source\ArraySourceIterator($datos);
        $callback = function() use ($source, $writer) {
            $handler = \Exporter\Handler::create($source, $writer);
            $handler->export();
        };

        return new StreamedResponse($callback, 200, array(
            'Content-Type' => $contentType,
            'Content-Disposition' => sprintf('attachment; filename=%s', $filename)
        ));
    }

    /**
     * Export Csv.
     */
    public function exportCsvAction($format, $query, $filename, $campos) {
        switch ($format) {
            case 'xls':
                $content_type = 'application/vnd.ms-excel';
                break;
            case 'json':
                $content_type = 'application/json';
                break;
            case 'csv':
                $content_type = 'text/csv';
                break;
            default:
                $content_type = 'text/csv';
                break;
        }
        // Location to Export this to
        $export_to = 'php://output';
        // Data to export
        $exporter_source = new DoctrineORMQuerySourceIterator($query->getQuery(), $campos, "d-m-Y");
        // Get an Instance of the Writer
        $exporter_writer = '\Exporter\Writer\\' . ucfirst($format) . 'Writer';
        $exporter_writer = new $exporter_writer($export_to);
        // Generate response
        $response = new Response();
        // Set headers
        $response->headers->set('Cache-Control', 'must-revalidate, post-check=0, pre-check=0');
        $response->headers->set('Content-type', $content_type);
        $response->headers->set('Expires', 0);
        //$response->headers->set('Content-length', filesize($filename));
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Content-disposition', 'filename=' . $filename . ';');
        // Send headers before outputting anything
        $response->sendHeaders();
        // Export to the format
        Handler::create($exporter_source, $exporter_writer)->export();
        return $response;
    }
}
