<?php

namespace Sistema\FACTURACIONBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\FACTURACIONBundle\Entity\Anticipo;
use Sistema\FACTURACIONBundle\Form\AnticipoType;
use Sistema\FACTURACIONBundle\Form\AnticipoFilterType;

/**
 * Anticipo controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/anticipo")
 */
class AnticipoController extends Controller
{
    /**
     * Lists all Anticipo entities.
     *
     * @Route("/{id}", name="admin_anticipo")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($id)
    {
        $this->get('security_role')->controlRolesUser();
        list($filterForm, $queryBuilder) = $this->filter($id);
        $em = $this->getDoctrine()->getManager();
         $sueldo = $em->getRepository('SistemaFACTURACIONBundle:Sueldo')->find($id);
         
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder,
            $this->get('request')->query->get('page', 1),
            (isset($this->container->parameters['knp_paginator.page_range'])) ? $this->container->parameters['knp_paginator.page_range'] : 10
        );

        return array(
            'empleado' => $sueldo->getEmpleado(),
            'entities'   => $pagination,
            'filterForm' => $filterForm->createView(),
        );
    }

    /**
    * Process filter request.
    *
    * @return array
    */
    protected function filter($id)
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        $filterForm = $this->createFilterForm();
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository('SistemaFACTURACIONBundle:Anticipo')
            ->createQueryBuilder('a')
            ->select('a')
            ->where('a.sueldo = :id')
            ->setParameter('id', $id)
            ->orderBy('a.id', 'DESC')
        ;
        // Bind values from the request
        $filterForm->handleRequest($request);
        // Reset filter
        if ($filterForm->get('reset')->isClicked()) {
            $session->remove('AnticipoControllerFilter');
            $filterForm = $this->createFilterForm();
        }

        // Filter action
        if ($filterForm->get('filter')->isClicked()) {
            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('AnticipoControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('AnticipoControllerFilter')) {
                $filterData = $session->get('AnticipoControllerFilter');
                $filterForm = $this->createFilterForm($filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }
    /**
    * Create filter form.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createFilterForm($filterData = null)
    {
        $form = $this->createForm(new AnticipoFilterType(), $filterData, array(
            'action' => $this->generateUrl('admin_anticipo', array('id' => 3)),
            'method' => 'GET',
        ));

        $form
            ->add('filter', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label'              => 'views.index.filter',
                'attr'               => array('class' => 'btn btn-success col-lg-1'),
            ))
            ->add('reset', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label'              => 'views.index.reset',
                'attr'               => array('class' => 'btn btn-danger col-lg-1 col-lg-offset-1'),
            ))
        ;

        return $form;
    }
    /**
     * Creates a new Anticipo entity.
     *
     * @Route("/{id}", name="admin_anticipo_create")
     * @Method("POST")
     * @Template("SistemaFACTURACIONBundle:Anticipo:new.html.twig")
     */
    public function createAction(Request $request, $id) {
        $this->get('security_role')->controlRolesUser();
        $entity = new Anticipo();
        $form = $this->createCreateForm($entity, $id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $this->generateUrl('admin_anticipo', array('id' => $id));
            return $this->redirect($nextAction);

        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Anticipo entity.
    *
    * @param Anticipo $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Anticipo $entity, $id)
    {
        $form = $this->createForm(new AnticipoType(), $entity, array(
            'action' => $this->generateUrl('admin_anticipo_create', array('id' => $id )),
            'method' => 'POST',
        ));

        $form
            ->add(
                'save', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label'              => 'views.new.save',
                'attr'               => array('class' => 'btn btn-success col-lg-2')
                )
            )
            ->add(
                'saveAndAdd', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label'              => 'views.new.saveAndAdd',
                'attr'               => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
                )
            )
        ;

        return $form;
    }

    /**
     * Displays a form to create a new Anticipo entity.
     *
     * @Route("/new/{id}", name="admin_anticipo_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($id) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();
        $sueldo = $em->getRepository('SistemaFACTURACIONBundle:Sueldo')->find($id);
        
        $entity = new Anticipo();
        $entity->setSueldo($sueldo);
        $form   = $this->createCreateForm($entity, $id);

        return array(
            'empleado' => $sueldo->getEmpleado(),
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Anticipo entity.
     *
     * @Route("/{id}", name="admin_anticipo_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaFACTURACIONBundle:Anticipo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Anticipo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Anticipo entity.
     *
     * @Route("/{id}/edit", name="admin_anticipo_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaFACTURACIONBundle:Anticipo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Anticipo entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Anticipo entity.
    *
    * @param Anticipo $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Anticipo $entity)
    {
        $form = $this->createForm(new AnticipoType(), $entity, array(
            'action' => $this->generateUrl('admin_anticipo_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form
            ->add(
                'save', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label'              => 'views.new.save',
                'attr'               => array('class' => 'btn btn-success col-lg-2')
                )
            )
            ->add(
                'saveAndAdd', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label'              => 'views.new.saveAndAdd',
                'attr'               => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
                )
            )
        ;

        return $form;
    }
    /**
     * Edits an existing Anticipo entity.
     *
     * @Route("/{id}", name="admin_anticipo_update")
     * @Method("PUT")
     * @Template("SistemaFACTURACIONBundle:Anticipo:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaFACTURACIONBundle:Anticipo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Anticipo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $this->generateUrl('admin_anticipo_show', array('id' => $entity->getId()));
                        /*$editForm->get('saveAndAdd')->isClicked()
                        ? $this->generateUrl('admin_anticipo_new')
                        : $this->generateUrl('admin_anticipo_show', array('id' => $id));*/
            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Anticipo entity.
     *
     * @Route("/elilimar/{id}", name="admin_anticipo_delete")
     * @Method("GET")
     */
    public function deleteAction($id) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('SistemaFACTURACIONBundle:Anticipo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Anticipo entity.');
        }

        $em->remove($entity);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'flash.delete.success');

        return $this->redirect($this->generateUrl('admin_sueldo'));
    }

    /**
     * Creates a form to delete a Anticipo entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        $mensaje = $this->get('translator')->trans('views.recordactions.confirm', array(), 'MWSimpleCrudGeneratorBundle');
        $onclick = 'return confirm("'.$mensaje.'");';
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_anticipo_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label'              => 'views.recordactions.delete',
                'attr'               => array(
                    'class'   => 'btn btn-danger col-lg-11',
                    'onclick' => $onclick,
                )
            ))
            ->getForm()
        ;
    }
    
     /**
     * Finds and displays a Orden entity.
     *
     * @param type $id Id de la entidad
     *
     * @Route("/imprimir-reporte-anticipo/{id}", name="admin_imprimir_anticipo")
     * @Method("GET")
     * @Template()
     * @return view
     */
    public function imprimirReporteAnticipoPdfAction($id)
    {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();
        
            $entity = $em->getRepository('SistemaFACTURACIONBundle:Anticipo')->find($id);
            
             if (!$entity) {
                throw $this->createNotFoundException('Unable to find Orden entity.');
             }

        $config = array(
            'titulo' => 'Constancia de Anticipo',
            'PDF_HEADER_LOGO' => 'logo_neon.jpg',
            'PDF_HEADER_LOGO_WIDTH' => '55',
            'PDF_HEADER_TITLE' => '',
            'PDF_HEADER_STRING' => '                                                                        Reporte Control de Horas
                                                                 M.M.O. Raúl Roberto Rojas
                                                                 Av. San Martín 2386 Resistencia - Chaco                            
                                                                Empleado: ' . $entity->getSueldo()->getEmpleado() . '
Tel: 362-4489609 / Cel: 362-4641854             Email: cobranza@neon3r.com.ar             Fecha: ' . $entity->getFecha()->format('d-m-Y') . '
                            ',
            'pie' => ''
            
            
            
        );

        $html = $this->renderView('SistemaFACTURACIONBundle:Anticipo:show.pdf.twig', array('entity' => $entity));

        return $this->get('io_tcpdf_mws')->quick_pdf($html, $config);
    }
}