<?php

namespace Sistema\FACTURACIONBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;

/**
 * SueldoFilterType filtro.
 * @author Nombre Apellido <name@gmail.com>
 */
class SueldoFilterType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('inicio', 'filter_date',array(
                        'label_attr' => array(
                            'class' => 'col-lg-2 col-md-2 col-sm-2',
                        ),
                        'attr'=> array('class'=>'col-lg-4 col-md-4 col-sm-4')
                    ))
            ->add('fin', 'filter_date',array(
                        'label_attr' => array(
                            'class' => 'col-lg-2 col-md-2 col-sm-2',
                        ),
                        'attr'=> array('class'=>'col-lg-4 col-md-4 col-sm-4')
                    ))
            /*->add('empleado', 'filter_entity', array(
                'required' => false,
                'class' => 'SistemaRRHHBundle:Empleado',
                'attr'=> array('class'=>'col-lg-4 col-md-4 col-sm-4'),
                'label_attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                ),
            ))*/
                
            ->add('empleado', 'select2',array(
                         'class' => 'Sistema\RRHHBundle\Entity\Empleado',
                          'url'   => 'autocomplete_get_empleados',
                            'configs' => array(
                                'multiple' => false,//es requerido true o false
                            // 'width'    => 'element',
                            ),
                        'attr'=> array('class'=>'form-control')
                    ))
        ;

        $listener = function(FormEvent $event)
        {
            // Is data empty?
            foreach ((array)$event->getForm()->getData() as $data) {
                if ( is_array($data)) {
                    foreach ($data as $subData) {
                        if (!empty($subData)) {
                            return;
                        }
                    }
                } else {
                    if (!empty($data)) {
                        return;
                    }    
                }
            }
            $event->getForm()->addError(new FormError('Filter empty'));
        };
        $builder->addEventListener(FormEvents::POST_SUBMIT, $listener);
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\FACTURACIONBundle\Entity\Sueldo'
        ));
    }
    
    public function getDefaultOptions(array $options)
    {
        return array(
            'validation_groups' => array('no_validation')
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_facturacionbundle_sueldofiltertype';
    }
}
