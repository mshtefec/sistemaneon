<?php

namespace Sistema\FACTURACIONBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;

/**
 * FacturaVentaFilterType filtro.
 * @author Nombre Apellido <name@gmail.com>
 */
class FacturaVentaFilterType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('tipo', 'choice', array(
                    'choices'  => array('A' => 'A','B' => 'B'),
                    'required' => false,
                    'attr'     => array('class' => 'form-control'),
                ))
                ->add('numero', 'filter_number', array(
                    'attr' => array('class' => 'form-control')
                ))
                ->add('fecha', 'filter_text', array(
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                ))
                ->add('descripcion', 'filter_text', array(
                    'attr' => array('class' => 'form-control'),
                    'required' => false
                ))
                /* ->add('monto', 'filter_number', array(
                  'attr' => array('class' => 'form-control')
                  )) */
                ->add('anulado', null, array(
                    'required' => false
                ))
                /*
                ->add('formaPago', 'choice', array(
                    'choices' => array(
                        'Contado' => 'Contado',
                        'Cuenta Corriente' => 'Cuenta Corriente',
                        'Cheque' => 'Cheque',
                        'Transferencia' => 'Transferencia'
                    ),
                    'required' => false,
                    'attr' => array('class' => 'form-control'),
                ))
                */
        ;

        $listener = function(FormEvent $event) {
            // Is data empty?
            foreach ((array) $event->getForm()->getData() as $data) {
                if (is_array($data)) {
                    foreach ($data as $subData) {
                        if (!empty($subData)) {
                            return;
                        }
                    }
                } else {
                    if (!empty($data)) {
                        return;
                    }
                }
            }
            $event->getForm()->addError(new FormError('Filter empty'));
        };
        $builder->addEventListener(FormEvents::POST_SUBMIT, $listener);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\FACTURACIONBundle\Entity\FacturaVenta',
            'validation_groups' => false,
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_facturacionbundle_facturaventafiltertype';
    }

}
