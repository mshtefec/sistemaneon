<?php

namespace Sistema\FACTURACIONBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

/**
 * AsientoType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class AsientoType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
            /*
            ->add('debe', 'money', array(
                'divisor' => 1,
                'grouping' => false,
                'currency' => 'ARG',
            ))
            ->add('haber', 'money', array(
                'divisor' => 1,
                'grouping' => false,
                'currency' => 'ARG',
            ))
            */
            ->add('debe', null)
            ->add('haber', null)
            ->add('cuenta', 'entity', array(
                'class' => 'Sistema\PlanDeCuentasBundle\Entity\Cuenta',
                'attr' => array(
                    'class' => 'form-control'
                ),
            ))
            ->add('fecha', 'bootstrapdatetime', array(
                'label_attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                ),
                'attr' => array(
                    'class' => 'col-lg-4 col-md-4 col-sm-4',
                ),
                'data' => new \DateTime("now"),
            ))
        ;
        
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $cuenta = $event->getData()->getCuenta();
            $form = $event->getForm();
            if (!is_null($cuenta)) {
                if ($cuenta->getPermiteCheque()) {
                    $form->add('cheque', new ChequeType());
                }
            }
        });
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\FACTURACIONBundle\Entity\Asiento'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_facturacionbundle_asiento';
    }

}
