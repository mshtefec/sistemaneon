<?php

namespace Sistema\FACTURACIONBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * AnticipoType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class AnticipoType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fecha', 'bootstrapdatetime', array(
                'label_attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                ),
                'attr' => array(
                    'class' => 'col-lg-4 col-md-4 col-sm-4',
                ),
                'read_only' => true,
                'data' => new \DateTime("today"),
            ))
            ->add('monto', null, array( 
                'label_attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                ),  
                'attr' => array(
                    'class' => 'col-lg-4 col-md-4 col-sm-4',
                ),
                ))
                ->add('concepto', null, array( 
                'label_attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                ),  
                'attr' => array(
                    'class' => 'col-lg-4 col-md-4 col-sm-4',
                ),
                ))
                ->add('sueldo', null, array( 
                'attr'  =>  array('style'=>'display:none;'),
                'label' =>  false
                ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\FACTURACIONBundle\Entity\Anticipo'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_facturacionbundle_anticipo';
    }
}
