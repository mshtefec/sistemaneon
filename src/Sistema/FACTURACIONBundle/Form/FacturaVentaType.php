<?php

namespace Sistema\FACTURACIONBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

/**
 * FacturaVentaType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class FacturaVentaType extends ComprobanteType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $proceso = $options['data']->getProceso();
        parent::buildForm($builder, $options);
        if ($options['data']->getTipo() == 'A') {
            $builder
                    ->add('iva', 'choice', array(
                        'label' => 'Iva %',
                        'choices' => array('0' => 0,
                            '10.5' => (10.5),
                            '21' => 21,
                            '27' => 27),
                        'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                        'attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                    ))
            ;
        } else {
            $builder
                    ->add('iva', 'hidden', array(
                        'label' => 'Iva %',
                        'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                        'attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                    ))
            ;
        }
        $builder
                ->add('tipo', null, array(
                    'read_only' => true,
                    'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-6'),
                    'attr' => array('class' => 'col-lg-2 col-md-2 col-sm-6'),
                ))
                ->add('numero', null, array(
                    'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                    'attr' => array(
                        'class' => 'col-lg-2 col-md-2 col-sm-2'
                    ),
                ))
                ->add('fecha', 'bootstrapdatetime', array(
                    'required' => false,
                    'label' => 'Fecha',
                    'read_only' => true,
                    'label_attr' => array(
                        'class' => 'col-lg-2 col-md-2 col-sm-2',
                    ),
                    'attr' => array(
                        'class' => 'col-lg-4',
                    )
                ))
                ->add('descripcion', null, array(
                    'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                    'attr' => array(
                        'autofocus' => 'autofocus',
                        'class' => 'col-lg-8 col-md-8 col-sm-8'
                    ),
                ))
                /* ->add('monto', null, array(
                  'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                  // 'required' => false,
                  'attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                  )) */
                ->add('anulado', 'choice', array(
                    'choices' => array(0 => 'No', 1 => 'Si'),
                    'label_attr' => array('class' => 'col-lg-4 col-md-4 col-sm-6'),
                    'attr' => array('class' => 'col-lg-8 col-md-8 col-sm-6'),
                ))
                /*
                ->add('formaPago', 'choice', array(
                    'label_attr' => array('class' => 'col-lg-3 col-md-3 col-sm-3'),
                    'choices' => array(
                        'Contado' => 'Contado',
                        'Cuenta Corriente' => 'Cuenta Corriente',
                        'Cheque' => 'Cheque',
                        'Transferencia' => 'Transferencia'
                    ),
                    'required' => true,
                    'attr' => array('class' => 'col-lg-4 col-md-4 col-sm-4'),
                ))
                */
                //->add('asiento')
                ->add('orden', 'select2', array(
                    'label' => 'O.T. Nro',
                    'label_attr' => array(
                        'class' => 'col-lg-4',
                    ),
                    'attr' => array(
                        'class' => 'col-lg-8',
                    ),
                    'required' => false,
                    'class' => 'Sistema\STOCKBundle\Entity\Orden',
                    'url' => 'autocomplete_get_orden_desc',
                    'required' => true,
                    //'read_only' => false,
                    'configs' => array(
                        'multiple' => false, //es requerido true o false
                        'cantidadMaxima' => true, //es requerido true o false
                        'valCantidadMaxima' => false, //es requerido true o false
                    //'entidad' => 'articulo',//es requerido if cantidadMaxima = true
                    )
                ))
                ->add('asientos', 'collection', array(
                    'label' => false,
                    'type' => new AsientoType(),
                    // 'allow_add' => true,
                    // 'allow_delete' => true,
                    'required' => true,
                        // 'by_reference' => false,
                ))
                ->add('proceso', 'entity', array(
                    'read_only' => true,
                    'required' => true,
                    'label' => false,
                    'class' => 'Sistema\PlanDeCuentasBundle\Entity\Proceso',
                    'query_builder' => function (EntityRepository $er) use ($proceso) {
                        if (is_null($proceso)) {
                            $qb = $er->createQueryBuilder('p');
                        } else {
                            $qb = $er->createQueryBuilder('p')
                                    ->where('p.id = :idProceso')
                                    ->setParameter('idProceso', $proceso->getId())
                            ;
                        }
                        return $qb;
                    },
                    'data' => $proceso,
                    'attr' => array(
                        'style' => 'visibility:hidden',
                    ),
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\FACTURACIONBundle\Entity\FacturaVenta'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_facturacionbundle_facturaventa';
    }

}
