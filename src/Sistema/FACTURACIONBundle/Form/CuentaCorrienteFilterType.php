<?php

namespace Sistema\FACTURACIONBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;
use Sistema\FACTURACIONBundle\Form\AsientoFilterType;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;

/**
 * CuentaCorrienteFilterType filtro.
 * @author Nombre Apellido <name@gmail.com>
 */
class CuentaCorrienteFilterType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('cliente', 'select2', array(
                    'class' => 'Sistema\RRHHBundle\Entity\Cliente',
                    'url' => 'autocomplete_get_clientes',
                    'configs' => array(
                        'multiple' => false, //es requerido true o false
                    // 'width'    => 'element',
                    ),
                    'attr' => array('class' => 'form-control')
                ))
        /* ->add('asientos', 'filter_collection_adapter', array(
          'type' => new AsientoFilterType(),
          'add_shared' => function (FilterBuilderExecuterInterface $qbe) {
          $closure = function(QueryBuilder $filterBuilder, $alias, $joinAlias, Expr $expr) {
          // add the join clause to the doctrine query builder
          // the where clause for the label and color fields will be added automatically with the right alias later by the Lexik\Filter\QueryBuilderUpdater
          $filterBuilder->leftJoin($alias . '.asientos', $joinAlias);
          };

          // then use the query builder executor to define the join, the join's alias and things to do on the doctrine query builder.
          $qbe->addOnce($qbe->getAlias() . '.asientos', 'a', $closure);
          },
          )) */
        ;

        $listener = function(FormEvent $event) {
            // Is data empty?
            foreach ((array) $event->getForm()->getData() as $data) {
                if (is_array($data)) {
                    foreach ($data as $subData) {
                        if (!empty($subData)) {
                            return;
                        }
                    }
                } else {
                    if (!empty($data)) {
                        return;
                    }
                }
            }
            $event->getForm()->addError(new FormError('Filter empty'));
        };
        $builder->addEventListener(FormEvents::POST_SUBMIT, $listener);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\FACTURACIONBundle\Entity\CuentaCorriente'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_facturacionbundle_cuentacorrientefiltertype';
    }

}
