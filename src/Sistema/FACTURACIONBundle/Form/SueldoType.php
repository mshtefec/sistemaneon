<?php

namespace Sistema\FACTURACIONBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * SueldoType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class SueldoType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('empleado')
            ->add('inicio')
            ->add('fin')
            ->add('cantidadHora')
            ->add('precioHora')
            ->add('remuneracionEspecial')
            ->add('diasVacaciones')
            ->add('total')
            
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\FACTURACIONBundle\Entity\Sueldo'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_facturacionbundle_sueldo';
    }
}
