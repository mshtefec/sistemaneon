<?php

namespace Sistema\FACTURACIONBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;
use Lexik\Bundle\FormFilterBundle\Filter\Query\QueryInterface;

class AsientoFilterType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $now = new \DateTime('Today');
        $oneWeekBefore = new \DateTime('Today');
        $oneWeekBefore->modify('-7 day');

        $typeChoices = array(
            "ReciboVenta" => "Recibo venta",
            "ReciboCompra" => "Recibo compra",
            "FacturaVenta" => "Factura venta",
            "FacturaCompra" => "Factura compra"
        );

        $builder
            ->add('tipoComprobante', 'filter_choice',array(
                'label' => 'Tipo Comprobante',
                'attr'=> array('class'=>'form-control'),
                'required' => false,
                'mapped' => false,
                'choices' => $typeChoices,
                'apply_filter' => function(QueryInterface $filterQuery, $field, $values) {
                    if (!is_null($values['value'])) {
                        $filterQuery->getQueryBuilder()
                            ->andWhere('comprobante INSTANCE OF Sistema\FACTURACIONBundle\Entity\\' . $values['value'])
                        ;
                    }
                    
                    return $filterQuery;
                }
            ))
            ->add('cuenta', 'filter_entity', array(
                'class' => 'Sistema\PlanDeCuentasBundle\Entity\Cuenta',
                'mapped' => false,
                'attr' => array(
                    'class' => 'form-control'
                ),
            ))
            ->add('fecha', 'filter_date_range', array(
                'label' => 'Rango de Fecha',
                'left_date_options' =>
                array(
                    'widget' => 'single_text',
                    'attr' => array('class' => 'col-lg-6 col-md-6 col-sm-6 col-xs-6 fecha'
                    ),
                    'data' => $oneWeekBefore,
                ),
                'right_date_options' => array(
                    'widget' => 'single_text',
                    'attr' => array(
                        'class' => 'col-lg-6 col-md-6 col-sm-6 col-xs-6 fecha'
                    ),
                    'data' => $now,
                ),
            ))
            ->add('debe', 'filter_text',array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('haber', 'filter_text',array(
                'attr'=> array('class'=>'form-control')
            ))
        ;

        $listener = function(FormEvent $event)
        {
            // Is data empty?
            foreach ((array)$event->getForm()->getData() as $data) {
                if ( is_array($data)) {
                    foreach ($data as $subData) {
                        if (!empty($subData)) {
                            return;
                        }
                    }
                } else {
                    if (!empty($data)) {
                        return;
                    }    
                }
            }
            $event->getForm()->addError(new FormError('Filter empty'));
        };
        $builder->addEventListener(FormEvents::POST_SUBMIT, $listener);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\FACTURACIONBundle\Entity\Asiento'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_facturacionbundle_asiento';
    }
}
