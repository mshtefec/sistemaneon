<?php

namespace Sistema\FACTURACIONBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;
use Lexik\Bundle\FormFilterBundle\Filter\Query\QueryInterface;

/**
 * ComprobanteFilterType filtro.
 * @author Nombre Apellido <name@gmail.com>
 */
class ComprobanteFilterType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        parent::buildForm($builder, $options);
        $now = new \DateTime('Today');
        $oneWeekBefore = new \DateTime('Today');
        $oneWeekBefore->modify('-7 day');
        $typeChoices = array(
            "ReciboVenta" => "Recibo Venta",
            "ReciboCompra" => "Recibo Compra",
            "FacturaVenta" => "Factura Venta",
            "FacturaCompra" => "Factura Compra",
            "Factura" => "Facturas",
            "Recibo" => "Recibos"
        );
        $builder
                ->add('discriminador', 'filter_choice', array(
                    'label' => 'Tipo de Comprobante',
                    'choices' => $typeChoices,
                    'attr' => array('class' => 'form-control'),
                    'required' => false,
                    'apply_filter' => function(QueryInterface $filterQuery, $field, $values) {
                if (!is_null($values['value'])) {
                    if ($values['value'] == 'Factura') {
                        $filterQuery->getQueryBuilder()
                        ->add('from', 'Sistema\FACTURACIONBundle\Entity\Comprobante ' . $values['alias'])
                        ->add('where', $values['alias'] . ' INSTANCE OF Sistema\FACTURACIONBundle\Entity\FacturaCompra or ' . $values['alias'] . ' INSTANCE OF Sistema\FACTURACIONBundle\Entity\FacturaVenta')
                        ;
                    } elseif ($values['value'] == 'Recibo') {
                        $filterQuery->getQueryBuilder()
                        ->add('from', 'Sistema\FACTURACIONBundle\Entity\Comprobante ' . $values['alias'])
                        ->add('where', $values['alias'] . ' INSTANCE OF Sistema\FACTURACIONBundle\Entity\ReciboCompra or ' . $values['alias'] . ' INSTANCE OF Sistema\FACTURACIONBundle\Entity\ReciboVenta')
                        ;
                    } else {
                        $filterQuery->getQueryBuilder()
                        ->add('from', 'Sistema\FACTURACIONBundle\Entity\Comprobante ' . $values['alias'])
                        ->add('where', $values['alias'] . ' INSTANCE OF Sistema\FACTURACIONBundle\Entity\\' . $values['value'])
                        ;
                    }
                }
                return $filterQuery;
            }
                ))
                ->add('fecha', 'filter_date_range', array(
                    'label' => 'Rango de Fecha',
                    'left_date_options' =>
                    array(
                        'widget' => 'single_text',
                        'attr' => array('class' => 'col-lg-6 col-md-6 col-sm-6 col-xs-6 fecha'
                        ),
                        'data' => $oneWeekBefore,
                    ),
                    'right_date_options' => array(
                        'widget' => 'single_text',
                        'attr' => array(
                            'class' => 'col-lg-6 col-md-6 col-sm-6 col-xs-6 fecha'
                        ),
                        'data' => $now,
                    ),
                ))
        /*  ->add('descripcion', 'filter_text',array(
          'attr'=> array('class'=>'form-control')
          ))
          ->add('monto', 'filter_number_range',array(
          'attr'=> array('class'=>'form-control')
          ))
          ->add('anulado', 'filter_choice',array(
          'attr'=> array('class'=>'form-control')
          ))
          ->add('formaPago', 'filter_text',array(
          'attr'=> array('class'=>'form-control')
          )) */
        ;

        $listener = function(FormEvent $event) {
            // Is data empty?
            foreach ((array) $event->getForm()->getData() as $data) {
                if (is_array($data)) {
                    foreach ($data as $subData) {
                        if (!empty($subData)) {
                            return;
                        }
                    }
                } else {
                    if (!empty($data)) {
                        return;
                    }
                }
            }
            //$event->getForm()->addError(new FormError('Filter empty'));
        };
        $builder->addEventListener(FormEvents::POST_SUBMIT, $listener);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        parent::setDefaultOptions($resolver);
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\FACTURACIONBundle\Entity\Comprobante',
            'csrf_protection' => true,
            'validation_groups' => array('filtering')
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_facturacionbundle_comprobantefiltertype';
    }

}
