<?php

namespace Sistema\FACTURACIONBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * ChequeType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class ChequeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('banco', 'select2', array(
                'attr' => array(
                    'class' => 'col-lg-12',
                ),
                'required' => false,
                'class' => 'Sistema\FACTURACIONBundle\Entity\Banco',
                'url' => 'autocomplete_get_banco',
                'configs' => array(
                    'multiple' => false,
                )
            ))
            ->add('chequeNumero', null, array(
                'attr' => array(
                    'class' => 'col-lg-4 col-md-2 col-sm-2',
                    'placeholder' => 'Numero de Cheque',
                ),
            ))
            ->add('chequeFechaVencimiento', 'bootstrapdatetime', array(
                'required' => false,
                'label_attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                ),
                'attr' => array(
                    'class' => 'col-lg-4 col-md-12 col-sm-12',
                ),
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\FACTURACIONBundle\Entity\Cheque'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_facturacionbundle_cheque';
    }
}
