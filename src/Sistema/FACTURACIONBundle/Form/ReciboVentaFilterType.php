<?php

namespace Sistema\FACTURACIONBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;

/**
 * ReciboVentaFilterType filtro.
 * @author Nombre Apellido <name@gmail.com>
 */
class ReciboVentaFilterType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('numero', 'filter_number', array(
                    'attr' => array('class' => 'form-control')
                ))
                ->add('fecha', 'filter_date_range', array(
                    'label' => 'Rango de Fecha',
                    'left_date_options' => array('widget' => 'single_text', 'attr' => array('class' => 'col-lg-6 col-md-6 col-sm-6 col-xs-6 fecha'),),
                    'right_date_options' => array('widget' => 'single_text', 'attr' => array('class' => 'col-lg-6 col-md-6 col-sm-6 col-xs-6 fecha'),),
                ))
                ->add('descripcion', 'filter_text', array(
                    'attr' => array('class' => 'form-control')
                ))
                /*->add('monto', 'filter_number', array(
                    'attr' => array('class' => 'form-control')
                ))*/
                ->add('anulado', null, array(
                    'required' => false
                ))
                /*
                ->add('formaPago', 'choice', array(
                    'choices' => array(
                        'Contado' => 'Contado',
                        'Cuenta Corriente' => 'Cuenta Corriente',
                        'Cheque' => 'Cheque',
                        'Transferencia' => 'Transferencia'
                    ),
                    'required' => false,
                    'attr' => array('class' => 'form-control'),
                ))
                */
        ;

        $listener = function(FormEvent $event) {
            // Is data empty?
            foreach ((array) $event->getForm()->getData() as $data) {
                if (is_array($data)) {
                    foreach ($data as $subData) {
                        if (!empty($subData)) {
                            return;
                        }
                    }
                } else {
                    if (!empty($data)) {
                        return;
                    }
                }
            }
            $event->getForm()->addError(new FormError('Filter empty'));
        };
        $builder->addEventListener(FormEvents::POST_SUBMIT, $listener);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\FACTURACIONBundle\Entity\ReciboVenta',
            'validation_groups' => false
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_facturacionbundle_reciboventafiltertype';
    }

}
