<?php

namespace Sistema\FACTURACIONBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CuentaCorriente
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\FACTURACIONBundle\Entity\CuentaCorrienteRepository")
 */
class CuentaCorriente {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="Sistema\RRHHBundle\Entity\Cliente", inversedBy="cuentaCorriente")
     * @ORM\JoinColumn(name="cliente_id", referencedColumnName="id", nullable=true)
     */
    private $cliente;

    /**
     * @ORM\OneToOne(targetEntity="Sistema\RRHHBundle\Entity\Proveedor", inversedBy="cuentaCorriente")
     * @ORM\JoinColumn(name="proveedor_id", referencedColumnName="id", nullable=true)
     */
    private $proveedor;

    /**
     * @var string
     *
     * @ORM\Column(name="monto", type="float", nullable=true)
     */
    private $monto;

    /**
     * @var string
     * @ORM\OneToMany(targetEntity="Sistema\FACTURACIONBundle\Entity\Asiento", mappedBy="cuentaCorriente", cascade={"persist"}, orphanRemoval=true)
     */
    private $asientos;

    /**
     * Constructor
     */
    public function __construct() {
        $this->asientos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set monto
     *
     * @param  float           $monto
     * @return CuentaCorriente
     */
    public function setMonto($monto) {
        $this->monto = $monto;

        return $this;
    }

    /**
     * Get monto
     *
     * @return float
     */
    public function getMonto() {
        return $this->monto;
    }

    /**
     * Set cliente
     *
     * @param  \Sistema\RRHHBundle\Entity\Cliente $cliente
     * @return CuentaCorriente
     */
    public function setCliente(\Sistema\RRHHBundle\Entity\Cliente $cliente = null) {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return \Sistema\RRHHBundle\Entity\Cliente
     */
    public function getCliente() {
        return $this->cliente;
    }

    /**
     * Set proveedor
     *
     * @param  \Sistema\RRHHBundle\Entity\Proveedor $proveedor
     * @return CuentaCorriente
     */
    public function setProveedor(\Sistema\RRHHBundle\Entity\Proveedor $proveedor = null) {
        $this->proveedor = $proveedor;

        return $this;
    }

    /**
     * Get proveedor
     *
     * @return \Sistema\RRHHBundle\Entity\Proveedor
     */
    public function getProveedor() {
        return $this->proveedor;
    }

    /**
     * Add asientos
     *
     * @param  \Sistema\FACTURACIONBundle\Entity\Asiento $asientos
     * @return CuentaCorriente
     */
    public function addAsiento(\Sistema\FACTURACIONBundle\Entity\Asiento $asientos) {
        $asientos->setCuentaCorriente($this);
        $this->asientos[] = $asientos;

        return $this;
    }

    /**
     * Remove asientos
     *
     * @param \Sistema\FACTURACIONBundle\Entity\Asiento $asientos
     */
    public function removeAsiento(\Sistema\FACTURACIONBundle\Entity\Asiento $asientos) {
        $this->asientos->removeElement($asientos);
    }

    /**
     * Get asientos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAsientos() {
        return $this->asientos;
    }

}
