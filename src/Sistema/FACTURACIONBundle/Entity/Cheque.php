<?php

namespace Sistema\FACTURACIONBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Sistema\MWSCONFBundle\Entity\MWSgedmo;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Cheque
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\FACTURACIONBundle\Entity\ChequeRepository")
 */
class Cheque extends MWSgedmo {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="chequeNumero", nullable=true)
     */
    private $chequeNumero;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="chequeFechaVencimiento", type="datetime", nullable=true)
     */
    private $chequeFechaVencimiento;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\FACTURACIONBundle\Entity\Asiento", mappedBy="cheque", cascade={"persist", "remove"})
     */
    protected $asientos;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Sistema\FACTURACIONBundle\Entity\Banco", inversedBy="cheques")
     * @ORM\JoinColumn(name="banco_id", referencedColumnName="id")
     */
    private $banco;


    /**
     * Constructor
     */
    public function __construct() {
        $this->asientos = new ArrayCollection();
    }

    public function __toString() {
        return (string) $this->chequeNumero;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }


    /**
     * Set chequeNumero
     *
     * @param string $chequeNumero
     * @return Cheque
     */
    public function setChequeNumero($chequeNumero)
    {
        $this->chequeNumero = $chequeNumero;
    
        return $this;
    }

    /**
     * Get chequeNumero
     *
     * @return string 
     */
    public function getChequeNumero()
    {
        return $this->chequeNumero;
    }

    /**
     * Set chequeFechaVencimiento
     *
     * @param \DateTime $chequeFechaVencimiento
     * @return Cheque
     */
    public function setChequeFechaVencimiento($chequeFechaVencimiento)
    {
        $this->chequeFechaVencimiento = $chequeFechaVencimiento;
    
        return $this;
    }

    /**
     * Get chequeFechaVencimiento
     *
     * @return \DateTime 
     */
    public function getChequeFechaVencimiento()
    {
        return $this->chequeFechaVencimiento;
    }
    
    /**
     * Add asientos
     *
     * @param \Sistema\FACTURACIONBundle\Entity\Asiento $asientos
     * @return Cheque
     */
    public function addAsiento(\Sistema\FACTURACIONBundle\Entity\Asiento $asientos)
    {
        $this->asientos[] = $asientos;
    
        return $this;
    }

    /**
     * Remove asientos
     *
     * @param \Sistema\FACTURACIONBundle\Entity\Asiento $asientos
     */
    public function removeAsiento(\Sistema\FACTURACIONBundle\Entity\Asiento $asientos)
    {
        $this->asientos->removeElement($asientos);
    }

    /**
     * Get asientos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAsientos()
    {
        return $this->asientos;
    }

    /**
     * Set banco
     *
     * @param \Sistema\FACTURACIONBundle\Entity\Banco $banco
     * @return Cheque
     */
    public function setBanco(\Sistema\FACTURACIONBundle\Entity\Banco $banco = null)
    {
        $this->banco = $banco;
    
        return $this;
    }

    /**
     * Get banco
     *
     * @return \Sistema\FACTURACIONBundle\Entity\Banco 
     */
    public function getBanco()
    {
        return $this->banco;
    }
}