<?php

namespace Sistema\FACTURACIONBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * FacturaVenta
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\FACTURACIONBundle\Entity\FacturaVentaRepository")
 * @UniqueEntity("numero")
 */
class FacturaVenta extends Comprobante {

    protected $discriminador;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="numero", type="integer")
     * @Assert\NotNull()
     */
    protected $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=1)
     * @Assert\Choice(
     *     choices = { "A", "B" ,"I"},
     *     message = "solo factura tipo A o B."
     * )
     */
    protected $tipo;

    /**
     * Constructor
     */
    public function __construct() {
        $this->fecha = new \DateTime('now');  
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set numero
     *
     * @param  integer      $numero
     * @return FacturaVenta
     */
    public function setNumero($numero) {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer
     */
    public function getNumero() {
        return $this->numero;
    }

    /**
     * Set tipo
     *
     * @param  string      $tipo
     * @return FacturaVenta
     */
    public function setTipo($tipo) {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo() {
        return $this->tipo;
    }

    /**
     * Set fecha
     *
     * @param  \DateTime    $fecha
     * @return FacturaVenta
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha() {
        return $this->fecha;
    }

    /**
     * Set descripcion
     *
     * @param  string       $descripcion
     * @return FacturaVenta
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getOrdenDescripcion() {
        return "OT nro. " . $this->getOrden() . " | " . $this->descripcion;
    }

    /**
     * Set monto
     *
     * @param  float        $monto
     * @return FacturaVenta
     */
    public function setMonto($monto) {
        $this->monto = $monto;

        return $this;
    }

    /**
     * Get monto
     *
     * @return float
     */
    public function getMonto() {
        return $this->monto;
    }

    /**
     * Set iva
     *
     * @param  float        $iva
     * @return FacturaVenta
     */
    public function setIva($iva) {
        $this->iva = $iva;

        return $this;
    }

    /**
     * Get iva
     *
     * @return float
     */
    public function getIva() {
        return $this->iva;
    }

    /**
     * Set anulado
     *
     * @param  boolean      $anulado
     * @return FacturaVenta
     */
    public function setAnulado($anulado) {
        $this->anulado = $anulado;

        return $this;
    }

    /**
     * Get anulado
     *
     * @return boolean
     */
    public function getAnulado() {
        return $this->anulado;
    }

    /**
     * Set formaPago
     *
     * @param  string       $formaPago
     * @return FacturaVenta
     */
    public function setFormaPago($formaPago) {
        $this->formaPago = $formaPago;

        return $this;
    }

    /**
     * Get formaPago
     *
     * @return string
     */
    public function getFormaPago() {
        return $this->formaPago;
    }

    /**
     * Set orden
     *
     * @param  \Sistema\STOCKBundle\Entity\Orden $orden
     * @return FacturaVenta
     */
    public function setOrden(\Sistema\STOCKBundle\Entity\Orden $orden = null) {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return \Sistema\STOCKBundle\Entity\Orden
     */
    public function getOrden() {
        return $this->orden;
    }
}