<?php

namespace Sistema\FACTURACIONBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sueldo
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\FACTURACIONBundle\Entity\SueldoRepository")
 */
class Sueldo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="inicio", type="date")
     */
    private $inicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fin", type="date")
     */
    private $fin;

    /**
     * @var float
     *
     * @ORM\Column(name="cantidadHora", type="float")
     */
    private $cantidadHora;

    /**
     * @var float
     *
     * @ORM\Column(name="precioHora", type="float")
     */
    private $precioHora;

    /**
     * @var float
     *
     * @ORM\Column(name="remuneracionEspecial", type="float")
     */
    private $remuneracionEspecial;

    /**
     * @var integer
     *
     * @ORM\Column(name="diasVacaciones", type="integer")
     */
    private $diasVacaciones;

    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float")
     */
    private $total;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Sistema\RRHHBundle\Entity\Empleado", inversedBy="sueldo")
     * @ORM\JoinColumn(name="empleado_id", referencedColumnName="id")
     */
    protected $empleado;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\FACTURACIONBundle\Entity\Anticipo", mappedBy="sueldo", cascade={"all"})
     */
    private $anticipos;
    
    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Sistema\FACTURACIONBundle\Entity\SueldoFecha", inversedBy="sueldo")
     * @ORM\JoinColumn(name="fecha_id", referencedColumnName="id")
     */
    protected $sueldoFecha;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="empleadoAux", type="integer")
     */
    private $empleadoAux;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set inicio
     *
     * @param \DateTime $inicio
     * @return Sueldo
     */
    public function setInicio($inicio)
    {
        $this->inicio = $inicio;
    
        return $this;
    }

    /**
     * Get inicio
     *
     * @return \DateTime 
     */
    public function getInicio()
    {
        return $this->inicio;
    }

    /**
     * Set fin
     *
     * @param \DateTime $fin
     * @return Sueldo
     */
    public function setFin($fin)
    {
        $this->fin = $fin;
    
        return $this;
    }

    /**
     * Get fin
     *
     * @return \DateTime 
     */
    public function getFin()
    {
        return $this->fin;
    }

    /**
     * Set cantidadHora
     *
     * @param float $cantidadHora
     * @return Sueldo
     */
    public function setCantidadHora($cantidadHora)
    {
        $this->cantidadHora = $cantidadHora;
    
        return $this;
    }

    /**
     * Get cantidadHora
     *
     * @return float 
     */
    public function getCantidadHora()
    {
        return $this->cantidadHora;
    }

    /**
     * Set precioHora
     *
     * @param float $precioHora
     * @return Sueldo
     */
    public function setPrecioHora($precioHora)
    {
        $this->precioHora = $precioHora;
    
        return $this;
    }

    /**
     * Get precioHora
     *
     * @return float 
     */
    public function getPrecioHora()
    {
        return $this->precioHora;
    }

    /**
     * Set remuneracionEspecial
     *
     * @param float $remuneracionEspecial
     * @return Sueldo
     */
    public function setRemuneracionEspecial($remuneracionEspecial)
    {
        $this->remuneracionEspecial = $remuneracionEspecial;
    
        return $this;
    }

    /**
     * Get remuneracionEspecial
     *
     * @return float 
     */
    public function getRemuneracionEspecial()
    {
        return $this->remuneracionEspecial;
    }

    /**
     * Set diasVacaciones
     *
     * @param integer $diasVacaciones
     * @return Sueldo
     */
    public function setDiasVacaciones($diasVacaciones)
    {
        $this->diasVacaciones = $diasVacaciones;
    
        return $this;
    }

    /**
     * Get diasVacaciones
     *
     * @return integer 
     */
    public function getDiasVacaciones()
    {
        return $this->diasVacaciones;
    }

    /**
     * Set total
     *
     * @param float $total
     * @return Sueldo
     */
    public function setTotal($total)
    {
        $this->total = $total;
    
        return $this;
    }

    /**
     * Get total
     *
     * @return float 
     */
    public function getTotal()
    {
        return $this->total;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->anticipos = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set empleado
     *
     * @param \Sistema\RRHHBundle\Entity\Empleado $empleado
     * @return Sueldo
     */
    public function setEmpleado(\Sistema\RRHHBundle\Entity\Empleado $empleado = null)
    {
        $this->empleadoAux = $empleado->getId();
        $this->empleado = $empleado;
    
        return $this;
    }

    /**
     * Get empleado
     *
     * @return \Sistema\RRHHBundle\Entity\Empleado 
     */
    public function getEmpleado()
    {
        return $this->empleado;
    }

    /**
     * Add anticipos
     *
     * @param \Sistema\FACTURACIONBundle\Entity\Anticipo $anticipos
     * @return Sueldo
     */
    public function addAnticipo(\Sistema\FACTURACIONBundle\Entity\Anticipo $anticipos)
    {
        $this->anticipos[] = $anticipos;
    
        return $this;
    }

    /**
     * Remove anticipos
     *
     * @param \Sistema\FACTURACIONBundle\Entity\Anticipo $anticipos
     */
    public function removeAnticipo(\Sistema\FACTURACIONBundle\Entity\Anticipo $anticipos)
    {
        $this->anticipos->removeElement($anticipos);
    }

    /**
     * Get anticipos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAnticipos()
    {
        return $this->anticipos;
    }
    
    public function __toString() {
        return (string)$this->total;
    }

    /**
     * Set empleadoAux
     *
     * @param integer $empleadoAux
     * @return Sueldo
     */
    public function setEmpleadoAux($empleadoAux)
    {
        $this->empleadoAux = $empleadoAux;
    
        return $this;
    }

    /**
     * Get empleadoAux
     *
     * @return integer 
     */
    public function getEmpleadoAux()
    {
        return $this->empleadoAux;
    }

    /**
     * Set sueldoFecha
     *
     * @param \Sistema\FACTURACIONBundle\Entity\SueldoFecha $sueldoFecha
     * @return Sueldo
     */
    public function setSueldoFecha(\Sistema\FACTURACIONBundle\Entity\SueldoFecha $sueldoFecha = null)
    {
        $this->sueldoFecha = $sueldoFecha;
    
        return $this;
    }

    /**
     * Get sueldoFecha
     *
     * @return \Sistema\FACTURACIONBundle\Entity\SueldoFecha 
     */
    public function getSueldoFecha()
    {
        return $this->sueldoFecha;
    }
}