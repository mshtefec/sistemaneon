<?php

namespace Sistema\FACTURACIONBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CuentaEmpresa
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\FACTURACIONBundle\Entity\CuentaEmpresaRepository")
 */
class CuentaEmpresa {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="monto", type="float")
     */
    private $monto;

    /**
     * @var integer
     *
     * @ORM\OneToMany(targetEntity="Sistema\FACTURACIONBundle\Entity\Asiento", mappedBy="cuentaEmpresa", cascade={"all"}, orphanRemoval=true)
     */
    private $asientos;

    /**
     * Constructor
     */
    public function __construct() {
        $this->asientos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set monto
     *
     * @param  float         $monto
     * @return CuentaEmpresa
     */
    public function setMonto($monto) {
        $this->monto = $monto;

        return $this;
    }

    /**
     * Get monto
     *
     * @return float
     */
    public function getMonto() {
        return $this->monto;
    }

    /**
     * Add asientos
     *
     * @param  \Sistema\FACTURACIONBundle\Entity\Asiento $asientos
     * @return CuentaEmpresa
     */
    public function addAsiento(\Sistema\FACTURACIONBundle\Entity\Asiento $asientos) {
        $asientos->setCuentaEmpresa($this);
        $this->asientos[] = $asientos;

        return $this;
    }

    /**
     * Remove asientos
     *
     * @param \Sistema\FACTURACIONBundle\Entity\Asiento $asientos
     */
    public function removeAsiento(\Sistema\FACTURACIONBundle\Entity\Asiento $asientos) {
        $this->asientos->removeElement($asientos);
    }

    /**
     * Get asientos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAsientos() {
        return $this->asientos;
    }

}
