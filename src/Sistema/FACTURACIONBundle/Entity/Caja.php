<?php

namespace Sistema\FACTURACIONBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Caja
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\FACTURACIONBundle\Entity\CajaRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Caja {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $fechaInicio;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fechaFin;

    /**
     * @var boolean
     *
     * @ORM\Column(name="estado", type="boolean")
     */
    private $estado;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Sistema\UserBundle\Entity\User", inversedBy="cajas")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var integer
     *
     * @ORM\OneToMany(targetEntity="Sistema\FACTURACIONBundle\Entity\Comprobante", mappedBy="caja")
     */
    private $comprobantes;

    /**
     * Constructor
     */
    public function __construct() {
        $this->fechaInicio = new \DateTime('now');
        $this->estado = true;
        $this->comprobantes = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaInicio
     *
     * @param \DateTime $fechaInicio
     * @return Caja
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;
    
        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \DateTime 
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set fechaFin
     *
     * @param \DateTime $fechaFin
     * @return Caja
     */
    public function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;
    
        return $this;
    }

    /**
     * Get fechaFin
     *
     * @return \DateTime 
     */
    public function getFechaFin()
    {
        return $this->fechaFin;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     * @return Caja
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set user
     *
     * @param \Sistema\UserBundle\Entity\user $user
     * @return Caja
     */
    public function setUser(\Sistema\UserBundle\Entity\user $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \Sistema\UserBundle\Entity\user 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add comprobantes
     *
     * @param \Sistema\FACTURACIONBundle\Entity\Comprobante $comprobantes
     * @return Caja
     */
    public function addComprobante(\Sistema\FACTURACIONBundle\Entity\Comprobante $comprobantes)
    {
        $this->comprobantes[] = $comprobantes;
    
        return $this;
    }

    /**
     * Remove comprobantes
     *
     * @param \Sistema\FACTURACIONBundle\Entity\Comprobante $comprobantes
     */
    public function removeComprobante(\Sistema\FACTURACIONBundle\Entity\Comprobante $comprobantes)
    {
        $this->comprobantes->removeElement($comprobantes);
    }

    /**
     * Get comprobantes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getComprobantes()
    {
        return $this->comprobantes;
    }
}