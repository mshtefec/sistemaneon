<?php

namespace Sistema\FACTURACIONBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * FacturaCompra
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\FACTURACIONBundle\Entity\FacturaCompraRepository")
 * @UniqueEntity(
 *     fields={"tipo", "numero"},
 *     errorPath="numero",
 *     message="El numero para el tipo de factura ya existe."
 * )
 */
class FacturaCompra extends Comprobante {

    protected $discriminador;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="numero", type="integer")
     * @Assert\NotNull()
     */
    protected $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=1)
     * @Assert\Choice(
     *     choices = { "A", "B", "C", "E", "M", "X" },
     *     message = "solo factura tipo A, B, C, E, M, X."
     * )
     */
    protected $tipo;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Sistema\RRHHBundle\Entity\Proveedor", inversedBy="facturaCompras")
     * @ORM\JoinColumn(name="proveedor_id", referencedColumnName="id")
     * @Assert\NotNull()
     */
    protected $proveedor;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\STOCKBundle\Entity\CantAddArticulo", mappedBy="facturaCompra",cascade={"all"})
     */
    private $articulos;
   

    /**
     * Constructor
     */
    public function __construct() {
        $this->cantAddArticulos = new ArrayCollection();
        $this->asientos         = new ArrayCollection();  
        $this->fecha            = new \DateTime('now');  
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set numero
     *
     * @param  integer       $numero
     * @return FacturaCompra
     */
    public function setNumero($numero) {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer
     */
    public function getNumero() {
        return $this->numero;
    }

    /**
     * Set tipo
     *
     * @param  string      $tipo
     * @return FacturaVenta
     */
    public function setTipo($tipo) {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo() {
        return $this->tipo;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return FacturaCompra
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha() {
        return $this->fecha;
    }

    /**
     * Set descripcion
     *
     * @param  string        $descripcion
     * @return FacturaCompra
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Set monto
     *
     * @param  float         $monto
     * @return FacturaCompra
     */
    public function setMonto($monto) {
        $this->monto = $monto;

        return $this;
    }

    /**
     * Get monto
     *
     * @return float
     */
    public function getMonto() {
        return $this->monto;
    }

    /**
     * Set iva
     *
     * @param  float         $iva
     * @return FacturaCompra
     */
    public function setIva($iva) {
        $this->iva = $iva;

        return $this;
    }

    /**
     * Get iva
     *
     * @return float
     */
    public function getIva() {
        return $this->iva;
    }

    /**
     * Set anulado
     *
     * @param  boolean       $anulado
     * @return FacturaCompra
     */
    public function setAnulado($anulado) {
        $this->anulado = $anulado;

        return $this;
    }

    /**
     * Get anulado
     *
     * @return boolean
     */
    public function getAnulado() {
        return $this->anulado;
    }

    /**
     * Set formaPago
     *
     * @param  string        $formaPago
     * @return FacturaCompra
     */
    public function setFormaPago($formaPago) {
        $this->formaPago = $formaPago;

        return $this;
    }

    /**
     * Get formaPago
     *
     * @return string
     */
    public function getFormaPago() {
        return $this->formaPago;
    }

    /**
     * Set proveedor
     *
     * @param  \Sistema\RRHHBundle\Entity\Proveedor $proveedor
     * @return FacturaCompra
     */
    public function setProveedor(\Sistema\RRHHBundle\Entity\Proveedor $proveedor = null) {
        $this->proveedor = $proveedor;

        return $this;
    }

    /**
     * Get proveedor
     *
     * @return \Sistema\RRHHBundle\Entity\Proveedor
     */
    public function getProveedor() {
        return $this->proveedor;
    }

    /**
     * Set asiento
     *
     * @param  \Sistema\FACTURACIONBundle\Entity\Asiento $asiento
     * @return FacturaCompra
     */
    public function setAsiento(\Sistema\FACTURACIONBundle\Entity\Asiento $asiento = null) {
        $this->asiento = $asiento;

        return $this;
    }

    /**
     * Get asiento
     *
     * @return \Sistema\FACTURACIONBundle\Entity\Asiento
     */
    public function getAsiento() {
        return $this->asiento;
    }

    /**
     * Set orden
     *
     * @param  \Sistema\STOCKBundle\Entity\Orden $orden
     * @return FacturaCompra
     */
    public function setOrden(\Sistema\STOCKBundle\Entity\Orden $orden = null) {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return \Sistema\STOCKBundle\Entity\Orden
     */
    public function getOrden() {
        return $this->orden;
    }

    /**
     * Add articulos
     *
     * @param \Sistema\STOCKBundle\Entity\CantAddArticulo $articulos
     * @return FacturaCompra
     */
    public function addArticulo(\Sistema\STOCKBundle\Entity\CantAddArticulo $articulos) {
        $articulos->setFacturaCompra($this);
        $this->articulos[] = $articulos;

        return $this;
    }

    /**
     * Remove articulos
     *
     * @param \Sistema\STOCKBundle\Entity\CantAddArticulo $articulos
     */
    public function removeArticulo(\Sistema\STOCKBundle\Entity\CantAddArticulo $articulos) {
        $this->articulos->removeElement($articulos);
    }

    /**
     * Get articulos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getArticulos() {
        return $this->articulos;
    }

  
}