<?php

namespace Sistema\FACTURACIONBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Asiento
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\FACTURACIONBundle\Entity\AsientoRepository")
 */
class Asiento extends \Sistema\MWSCONFBundle\Entity\MWSgedmo {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="debe", type="float")
     */
    private $debe;

    /**
     * @var float
     *
     * @ORM\Column(name="haber", type="float")
     */
    private $haber;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Sistema\FACTURACIONBundle\Entity\Comprobante", inversedBy="asientos")
     * @ORM\JoinColumn(name="comprobante_id", referencedColumnName="id", nullable=true)
     */
    private $comprobante;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\FACTURACIONBundle\Entity\Cheque", inversedBy="asientos", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="cheque_id", referencedColumnName="id", nullable=true)
     */
    private $cheque;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\STOCKBundle\Entity\Orden", inversedBy="asientos")
     * @ORM\JoinColumn(name="orden_id", referencedColumnName="id")
     * */
    private $orden;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Sistema\FACTURACIONBundle\Entity\CuentaEmpresa", inversedBy="asientos")
     * @ORM\JoinColumn(name="cuentaEmpresa_id", referencedColumnName="id")
     */
    private $cuentaEmpresa;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Sistema\FACTURACIONBundle\Entity\CuentaCorriente", inversedBy="asientos")
     * @ORM\JoinColumn(name="cuentaCorriente_id", referencedColumnName="id")
     */
    private $cuentaCorriente;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\PlanDeCuentasBundle\Entity\Cuenta", inversedBy="asientos")
     * @ORM\JoinColumn(name="cuenta_id", referencedColumnName="id")
     * */
    private $cuenta;

    /**
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $tipo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    public function __construct() {
        $this->setDebe(0);
        $this->setHaber(0);
        $this->fecha = new \DateTime('now');
    }

    public function __toString() {
        return (string) $this->id;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set debe
     *
     * @param float $debe
     * @return Asiento
     */
    public function setDebe($debe) {
        $this->debe = $debe;

        return $this;
    }

    /**
     * Get debe
     *
     * @return float 
     */
    public function getDebe() {
        return $this->debe;
    }

    /**
     * Set haber
     *
     * @param float $haber
     * @return Asiento
     */
    public function setHaber($haber) {
        $this->haber = $haber;

        return $this;
    }

    /**
     * Get haber
     *
     * @return float 
     */
    public function getHaber() {
        return $this->haber;
    }

    /**
     * Set comprobante
     *
     * @param \Sistema\FACTURACIONBundle\Entity\Comprobante $comprobante
     * @return Asiento
     */
    public function setComprobante(\Sistema\FACTURACIONBundle\Entity\Comprobante $comprobante = null) {
        $this->comprobante = $comprobante;

        return $this;
    }

    /**
     * Get comprobante
     *
     * @return \Sistema\FACTURACIONBundle\Entity\Comprobante 
     */
    public function getComprobante() {
        return $this->comprobante;
    }

    /**
     * Set cheque
     *
     * @param \Sistema\FACTURACIONBundle\Entity\Cheque $cheque
     * @return Asiento
     */
    public function setCheque(\Sistema\FACTURACIONBundle\Entity\Cheque $cheque = null) {
        $this->cheque = $cheque;

        return $this;
    }

    /**
     * Get cheque
     *
     * @return \Sistema\FACTURACIONBundle\Entity\Cheque 
     */
    public function getCheque() {
        return $this->cheque;
    }

    /**
     * Set orden
     *
     * @param \Sistema\STOCKBundle\Entity\Orden $orden
     * @return Asiento
     */
    public function setOrden(\Sistema\STOCKBundle\Entity\Orden $orden = null) {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return \Sistema\STOCKBundle\Entity\Orden 
     */
    public function getOrden() {
        return $this->orden;
    }

    /**
     * Set cuentaEmpresa
     *
     * @param \Sistema\FACTURACIONBundle\Entity\CuentaEmpresa $cuentaEmpresa
     * @return Asiento
     */
    public function setCuentaEmpresa(\Sistema\FACTURACIONBundle\Entity\CuentaEmpresa $cuentaEmpresa = null) {
        $this->cuentaEmpresa = $cuentaEmpresa;

        return $this;
    }

    /**
     * Get cuentaEmpresa
     *
     * @return \Sistema\FACTURACIONBundle\Entity\CuentaEmpresa 
     */
    public function getCuentaEmpresa() {
        return $this->cuentaEmpresa;
    }

    /**
     * Set cuentaCorriente
     *
     * @param \Sistema\FACTURACIONBundle\Entity\CuentaCorriente $cuentaCorriente
     * @return Asiento
     */
    public function setCuentaCorriente(\Sistema\FACTURACIONBundle\Entity\CuentaCorriente $cuentaCorriente = null) {
        $this->cuentaCorriente = $cuentaCorriente;

        return $this;
    }

    /**
     * Get cuentaCorriente
     *
     * @return \Sistema\FACTURACIONBundle\Entity\CuentaCorriente 
     */
    public function getCuentaCorriente() {
        return $this->cuentaCorriente;
    }

    /**
     * Set cuenta
     *
     * @param \Sistema\PlanDeCuentasBundle\Entity\Cuenta $cuenta
     * @return Asiento
     */
    public function setCuenta(\Sistema\PlanDeCuentasBundle\Entity\Cuenta $cuenta = null) {
        $this->cuenta = $cuenta;

        return $this;
    }

    /**
     * Get cuenta
     *
     * @return \Sistema\PlanDeCuentasBundle\Entity\Cuenta 
     */
    public function getCuenta() {
        return $this->cuenta;
    }

    /**
     * Set tipo
     *
     * @param integer $tipo
     * @return Asiento
     */
    public function setTipo($tipo) {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return integer 
     */
    public function getTipo() {
        return $this->tipo;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Asiento
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha() {
        return $this->fecha;
    }

}
