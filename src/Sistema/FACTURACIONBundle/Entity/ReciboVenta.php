<?php

namespace Sistema\FACTURACIONBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * ReciboVenta
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\FACTURACIONBundle\Entity\ReciboVentaRepository")
 * @UniqueEntity("numero")
 */
class ReciboVenta extends Comprobante
{
    protected $discriminador;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="numero", type="integer")
     * @Assert\NotNull()
     */
    protected $numero;

    /**
     * Constructor
     */
    public function __construct() {
        $this->fecha = new \DateTime('now');  
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numero
     *
     * @param  integer     $numero
     * @return ReciboVenta
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set fecha
     *
     * @param  \DateTime   $fecha
     * @return ReciboVenta
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set descripcion
     *
     * @param  string      $descripcion
     * @return ReciboVenta
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getOrdenDescripcion()
    {
        return "OT nro. ".$this->getOrden()." | ".$this->descripcion;
    }

    /**
     * Set monto
     *
     * @param  float       $monto
     * @return ReciboVenta
     */
    public function setMonto($monto)
    {
        $this->monto = $monto;

        return $this;
    }

    /**
     * Get monto
     *
     * @return float
     */
    public function getMonto()
    {
        return $this->monto;
    }

    /**
     * Set anulado
     *
     * @param  boolean     $anulado
     * @return ReciboVenta
     */
    public function setAnulado($anulado)
    {
        $this->anulado = $anulado;

        return $this;
    }

    /**
     * Get anulado
     *
     * @return boolean
     */
    public function getAnulado()
    {
        return $this->anulado;
    }

    /**
     * Set formaPago
     *
     * @param  string      $formaPago
     * @return ReciboVenta
     */
    public function setFormaPago($formaPago)
    {
        $this->formaPago = $formaPago;

        return $this;
    }

    /**
     * Get formaPago
     *
     * @return string
     */
    public function getFormaPago()
    {
        return $this->formaPago;
    }

    /**
     * Set orden
     *
     * @param  \Sistema\STOCKBundle\Entity\Orden $orden
     * @return ReciboVenta
     */
    public function setOrden(\Sistema\STOCKBundle\Entity\Orden $orden = null)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return \Sistema\STOCKBundle\Entity\Orden
     */
    public function getOrden()
    {
        return $this->orden;
    }
    
    public function getDiscriminador() { return $this->discriminador; }
    public function setDiscriminador($discriminador) { $this->discriminador = $discriminador; }
    
}
