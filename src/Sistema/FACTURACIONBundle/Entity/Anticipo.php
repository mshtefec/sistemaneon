<?php

namespace Sistema\FACTURACIONBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Anticipo
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\FACTURACIONBundle\Entity\AnticipoRepository")
 */
class Anticipo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date")
     */
    private $fecha;

    /**
     * @var float
     *
     * @ORM\Column(name="monto", type="float")
     */
    private $monto;
    
    /**
     * @var string
     *
     * @ORM\Column(name="concepto", type="text", nullable=true)
     */
    protected $concepto;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Sistema\FACTURACIONBundle\Entity\Sueldo", inversedBy="anticipos")
     * @ORM\JoinColumn(name="sueldo_id", referencedColumnName="id")
     */
    protected $sueldo;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Anticipo
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set monto
     *
     * @param float $monto
     * @return Anticipo
     */
    public function setMonto($monto)
    {
        $this->monto = $monto;
    
        return $this;
    }

    /**
     * Get monto
     *
     * @return float 
     */
    public function getMonto()
    {
        return $this->monto;
    }

    /**
     * Set sueldo
     *
     * @param \Sistema\FACTURACIONBundle\Entity\Sueldo $sueldo
     * @return Anticipo
     */
    public function setSueldo(\Sistema\FACTURACIONBundle\Entity\Sueldo $sueldo = null)
    {
        $this->sueldo = $sueldo;
    
        return $this;
    }

    /**
     * Get sueldo
     *
     * @return \Sistema\FACTURACIONBundle\Entity\Sueldo 
     */
    public function getSueldo()
    {
        return $this->sueldo;
    }
    
    /**
     * Set concepto
     *
     * @param  string $concepto
     * @return Orden
     */
    public function setConcepto($concepto) {
        $this->concepto = $concepto;

        return $this;
    }

    /**
     * Get concepto
     *
     * @return string
     */
    public function getConcepto() {
        return $this->concepto;
    }
}