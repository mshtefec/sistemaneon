<?php

namespace Sistema\FACTURACIONBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Comprobante
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\FACTURACIONBundle\Entity\ComprobanteRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discriminador", type="string")
 * @ORM\DiscriminatorMap({
 *  "anticipoVenta" = "AnticipoVenta",
 *  "reciboVenta" = "ReciboVenta",
 *  "reciboCompra" = "ReciboCompra",
 *  "facturaVenta" = "FacturaVenta",
 *  "facturaCompra" = "FacturaCompra"
 * })
 */
class Comprobante {

    protected $discriminador;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    protected $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     * @Assert\NotBlank()
     */
    protected $descripcion;

    /**
     * @var float
     *
     * @ORM\Column(name="monto", type="float")
     */
    protected $monto;

    /**
     * @var float
     *
     * @ORM\Column(name="iva", type="float", nullable=true)
     */
    protected $iva;

    /**
     * @var integer
     * @ORM\OneToMany(targetEntity="Sistema\FACTURACIONBundle\Entity\Asiento", mappedBy="comprobante", cascade={"persist", "remove"})
     */
    protected $asientos;

    /**
     * @var boolean
     *
     * @ORM\Column(name="anulado", type="boolean", nullable=true)
     */
    protected $anulado;

    /**
     * @var string
     *
     * @ORM\Column(name="formaPago", type="string", length=255, nullable=true)
     * @Assert\Choice(choices = {"Contado", "Cuenta Corriente", "Cheque", "Transferencia"}, message = "Seleccione una Forma de pago Valida.")
     */
    protected $formaPago;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Sistema\STOCKBundle\Entity\Orden", inversedBy="comprobantes")
     * @ORM\JoinColumn(name="orden_id", referencedColumnName="id")
     */
    protected $orden;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Sistema\FACTURACIONBundle\Entity\Caja", inversedBy="comprobantes")
     * @ORM\JoinColumn(name="caja_id", referencedColumnName="id")
     */
    protected $caja;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\PlanDeCuentasBundle\Entity\Proceso", inversedBy="comprobantes")
     * @ORM\JoinColumn(name="proceso_id", referencedColumnName="id")
     */
    private $proceso;


    /**
     * Constructor
     */
    public function __construct() {
        $this->asientos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fecha    = new \DateTime('now');
    }

    /**
     * @Assert\True(message = "Los montos debe y haber deben ser iguales.")
     */
    public function isDebeIgualHaber() {
        // $DEBE = 0;
        // $HABER = 1;
        $debe = 0;
        $haber = 0;
        $contAsientos = count($this->getAsientos());
        //con esto controlo que en las cuentas el debe y el haber respeten la regla del cero.
        $flag = true;
        $return = false;

        //Comento el codigo del TIPO porque NO se esta seteando el TIPO
        //en la CUENTA al agregar al PROCESO por ende aqui no tiene TIPO
        foreach ($this->getAsientos() as $asiento) {
            $haber = $haber + round($asiento->getHaber(), 2);
            $debe = $debe + round($asiento->getDebe(), 2);
            // CONTROL PARA VALORES EN EL MISMO ASIENTO
            if ($asiento->getHaber() > 0 && $asiento->getDebe() > 0) {
                $flag = false; // es cuando se agrega debe y haber al mismo asiento
                break;
            } elseif ($asiento->getHaber() == 0 && $asiento->getDebe() == 0) {
                //$this->removeAsiento($asiento);
            }
        }

        $debe = round($debe, 2);
        $haber = round($haber, 2);
        
        if ($contAsientos > 1) {
            if ($flag == true) {
                if ($debe == $haber && $haber > 0) {
                    $this->setMonto($haber);
                    $return = true;
                }
            }
        } else {
            //Tiene 1 solo asiento
            if ($debe == 0 && $haber > 0) {
                $this->setMonto($haber);
                $return = true;
            } elseif($haber == 0 && $debe > 0) {
                $this->setMonto($debe);
                $return = true;
            }
        }

        return $return;
    }

    public function __toString() {
        if (method_exists($this, 'getNumero')) {
            return $this->getTipoDocumento() . " nro: " . $this->getNumero() . ", " . $this->descripcion;
        } else {
            return $this->getTipoDocumento() . ", " . $this->descripcion;
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param  \DateTime   $fecha
     * @return Comprobante
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha() {
        return $this->fecha;
    }

    /**
     * Set descripcion
     *
     * @param  string      $descripcion
     * @return Comprobante
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Set monto
     *
     * @param  float       $monto
     * @return Comprobante
     */
    public function setMonto($monto) {
        $this->monto = $monto;

        return $this;
    }

    /**
     * Get monto
     *
     * @return float
     */
    public function getMonto() {
        return $this->monto;
    }

    /**
     * Set iva
     *
     * @param float $iva
     * @return Comprobante
     */
    public function setIva($iva) {
        $this->iva = $iva;

        return $this;
    }

    /**
     * Get iva
     *
     * @return float
     */
    public function getIva() {
        return $this->iva;
    }

    /**
     * Set anulado
     *
     * @param  boolean     $anulado
     * @return Comprobante
     */
    public function setAnulado($anulado) {
        $this->anulado = $anulado;

        return $this;
    }

    /**
     * Get anulado
     *
     * @return boolean
     */
    public function getAnulado() {
        return $this->anulado;
    }

    /**
     * Set formaPago
     *
     * @param  string      $formaPago
     * @return Comprobante
     */
    public function setFormaPago($formaPago) {
        $this->formaPago = $formaPago;

        return $this;
    }

    /**
     * Get formaPago
     *
     * @return string
     */
    public function getFormaPago() {
        return $this->formaPago;
    }

    /**
     * Set orden
     *
     * @param  \Sistema\STOCKBundle\Entity\Orden $orden
     * @return Comprobante
     */
    public function setOrden(\Sistema\STOCKBundle\Entity\Orden $orden = null) {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return \Sistema\STOCKBundle\Entity\Orden
     */
    public function getOrden() {
        return $this->orden;
    }

    public function getDiscriminador() {
        return $this->discriminador;
    }

    public function setDiscriminador($discriminador) {
        $this->discriminador = $discriminador;
    }

    public function getTipoDocumento() {
        if ($this instanceof ReciboVenta) {
            $tipo = "Recibo venta";
        } elseif ($this instanceof FacturaVenta) {
            $tipo = "Factura venta";
        } elseif ($this instanceof ReciboCompra) {
            $tipo = "Recibo compra";
        } elseif ($this instanceof FacturaCompra) {
            $tipo = "Factura compra";
        } else {
            $tipo = null;
        }

        return $tipo;
    }

    /**
     * Set caja
     *
     * @param \Sistema\FACTURACIONBundle\Entity\Caja $caja
     * @return Comprobante
     */
    public function setCaja(\Sistema\FACTURACIONBundle\Entity\Caja $caja = null) {
        $this->caja = $caja;

        return $this;
    }

    /**
     * Get caja
     *
     * @return \Sistema\FACTURACIONBundle\Entity\Caja
     */
    public function getCaja() {
        return $this->caja;
    }

    /**
     * Set proceso
     *
     * @param \Sistema\PlanDeCuentasBundle\Entity\Proceso $proceso
     * @return Comprobante
     */
    public function setProceso(\Sistema\PlanDeCuentasBundle\Entity\Proceso $proceso = null)
    {
        $this->proceso = $proceso;

        return $this;
    }

    /**
     * Get proceso
     *
     * @return \Sistema\PlanDeCuentasBundle\Entity\Proceso
     */
    public function getProceso()
    {
        return $this->proceso;
    }

    /**
     * Add asientos
     *
     * @param \Sistema\FACTURACIONBundle\Entity\Asiento $asientos
     * @return Comprobante
     */
    public function addAsiento(\Sistema\FACTURACIONBundle\Entity\Asiento $asientos) {
        $asientos->setComprobante($this);
        $this->asientos[] = $asientos;

        return $this;
    }

    /**
     * Remove asientos
     *
     * @param \Sistema\FACTURACIONBundle\Entity\Asiento $asientos
     */
    public function removeAsiento(\Sistema\FACTURACIONBundle\Entity\Asiento $asientos) {
        $this->asientos->removeElement($asientos);
    }

    /**
     * Get asientos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAsientos() {
        return $this->asientos;
    }
}
