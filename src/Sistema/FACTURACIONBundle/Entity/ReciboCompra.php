<?php

namespace Sistema\FACTURACIONBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * ReciboCompra
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\FACTURACIONBundle\Entity\ReciboCompraRepository")
 * @UniqueEntity("numero")
 */
class ReciboCompra extends Comprobante
{
    protected $discriminador;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="numero", type="integer")
     * @Assert\NotNull()
     */
    private $numero;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Sistema\RRHHBundle\Entity\Proveedor", inversedBy="recibosCompras")
     * @ORM\JoinColumn(name="proveedor_id", referencedColumnName="id")
     */
    protected $proveedor;

    /**
     * Constructor
     */
    public function __construct() {
        $this->fecha = new \DateTime('now');  
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set numero
     *
     * @param  integer      $numero
     * @return ReciboCompra
     */
    public function setNumero($numero) {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer
     */
    public function getNumero() {
        return $this->numero;
    }

    /**
     * Set fecha
     *
     * @param  \DateTime    $fecha
     * @return ReciboCompra
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha() {
        return $this->fecha;
    }

    /**
     * Set descripcion
     *
     * @param  string       $descripcion
     * @return ReciboCompra
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Set monto
     *
     * @param  float        $monto
     * @return ReciboCompra
     */
    public function setMonto($monto) {
        $this->monto = $monto;

        return $this;
    }

    /**
     * Get monto
     *
     * @return float
     */
    public function getMonto() {
        return $this->monto;
    }

    /**
     * Set anulado
     *
     * @param  boolean      $anulado
     * @return ReciboCompra
     */
    public function setAnulado($anulado) {
        $this->anulado = $anulado;

        return $this;
    }

    /**
     * Get anulado
     *
     * @return boolean
     */
    public function getAnulado() {
        return $this->anulado;
    }

    /**
     * Set formaPago
     *
     * @param  string       $formaPago
     * @return ReciboCompra
     */
    public function setFormaPago($formaPago) {
        $this->formaPago = $formaPago;

        return $this;
    }

    /**
     * Get formaPago
     *
     * @return string
     */
    public function getFormaPago() {
        return $this->formaPago;
    }

    /**
     * Set asiento
     *
     * @param  \Sistema\FACTURACIONBundle\Entity\Asiento $asiento
     * @return ReciboCompra
     */
    public function setAsiento(\Sistema\FACTURACIONBundle\Entity\Asiento $asiento = null) {
        $this->asiento = $asiento;

        return $this;
    }

    /**
     * Get asiento
     *
     * @return \Sistema\FACTURACIONBundle\Entity\Asiento
     */
    public function getAsiento() {
        return $this->asiento;
    }

    /**
     * Set orden
     *
     * @param  \Sistema\STOCKBundle\Entity\Orden $orden
     * @return ReciboCompra
     */
    public function setOrden(\Sistema\STOCKBundle\Entity\Orden $orden = null) {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return \Sistema\STOCKBundle\Entity\Orden
     */
    public function getOrden() {
        return $this->orden;
    }

    /**
     * Set proveedor
     *
     * @param  \Sistema\RRHHBundle\Entity\Proveedor $proveedor
     * @return ReciboCompra
     */
    public function setProveedor(\Sistema\RRHHBundle\Entity\Proveedor $proveedor = null) {
        $this->proveedor = $proveedor;

        return $this;
    }

    /**
     * Get proveedor
     *
     * @return \Sistema\RRHHBundle\Entity\Proveedor
     */
    public function getProveedor() {
        return $this->proveedor;
    }

}
