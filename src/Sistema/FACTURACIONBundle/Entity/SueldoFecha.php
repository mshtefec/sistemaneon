<?php

namespace Sistema\FACTURACIONBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * SueldoFecha
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\FACTURACIONBundle\Entity\SueldoFechaRepository")
 * @UniqueEntity(
 * fields ={"inicio","fin"},
 * errorPath = "inicio",
 * message="La fecha que intenta generar ya Existe."
 * )
 */
class SueldoFecha
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="inicio", type="date", unique=true)
     */
    private $inicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fin", type="date", unique=true)
     */
    private $fin;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\FACTURACIONBundle\Entity\Sueldo", mappedBy="sueldoFecha", cascade={"all"})
     */
    private $sueldo;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set inicio
     *
     * @param \DateTime $inicio
     * @return SueldoFecha
     */
    public function setInicio($inicio)
    {
        $this->inicio = $inicio;
    
        return $this;
    }

    /**
     * Get inicio
     *
     * @return \DateTime 
     */
    public function getInicio()
    {
        return $this->inicio;
    }

    /**
     * Set fin
     *
     * @param \DateTime $fin
     * @return SueldoFecha
     */
    public function setFin($fin)
    {
        $this->fin = $fin;
    
        return $this;
    }

    /**
     * Get fin
     *
     * @return \DateTime 
     */
    public function getFin()
    {
        return $this->fin;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->sueldo = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add sueldo
     *
     * @param \Sistema\FACTURACIONBundle\Entity\Sueldo $sueldo
     * @return SueldoFecha
     */
    public function addSueldo(\Sistema\FACTURACIONBundle\Entity\Sueldo $sueldo)
    {
        $this->sueldo[] = $sueldo;
    
        return $this;
    }

    /**
     * Remove sueldo
     *
     * @param \Sistema\FACTURACIONBundle\Entity\Sueldo $sueldo
     */
    public function removeSueldo(\Sistema\FACTURACIONBundle\Entity\Sueldo $sueldo)
    {
        $this->sueldo->removeElement($sueldo);
    }

    /**
     * Get sueldo
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSueldo()
    {
        return $this->sueldo;
    }
}