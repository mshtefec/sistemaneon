<?php

namespace Sistema\ReporteBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware {

    public function reporteMenu(FactoryInterface $factory, array $options) {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav nav-pills');
        $menu->addChild('Cantidad de ordenes por cliente', array('route' => 'admin_reporte_cantidad_orden_por_cliente'));
        $menu->addChild('Articulos mas utilizados', array('route' => 'admin_reporte_articulo_mas_usados'));
        $menu->addChild('Cantidad de herramientas por empleados', array('route' => 'admin_reporte_cantidad_herramientas_por_empleado'));
        $menu->addChild('Control de cantidad articulos', array('route' => 'admin_reporte_control_articulos_cantidades'));
        $menu->addChild('Control de reservas articulos', array('route' => 'admin_reporte_control_articulos_cantidades_reserva'));
        $menu->addChild('Reporte horas trabajadas por empleado', array('route' => 'admin_reporte_horas_trabajadas_por_empleado'));
        return $menu;
    }

}
