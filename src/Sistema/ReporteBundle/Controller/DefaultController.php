<?php

namespace Sistema\ReporteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sistema\RRHHBundle\Entity\Empleado;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller {

    /**
     * @Route("/admin/reporte", name="admin_reporte")
     * @Template()
     */
    public function indexAction() {
        $this->get('security_role')->controlRolesUser();

        return array();
    }

    /**
     * @Route("/admin/reporte/articulos-mas-utilizados", name="admin_reporte_articulo_mas_usados")
     * @Template()
     */
    public function articulosMasUtilizadosAction() {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('SistemaSTOCKBundle:Articulo')->GetArticulosMasUsados();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * @Route("/admin/reporte/cantidad-orden-por-cliente", name="admin_reporte_cantidad_orden_por_cliente")
     * @Template()
     */
    public function cantidadOrdenPorClienteAction() {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('SistemaRRHHBundle:Cliente')->getClientesConMasOrdenes();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Finds and displays a Orden entity.
     *
     * @param type $id Id de la entidad
     *
     * @Route("/imprimir/cantidad-orden-por-cliente-imprimir", name="admin_cantidad_orden_por_cliente_imprimir")
     * @Method("GET")
     * @Template()
     * @return view
     */
    public function imprimircantidadordenporclientepdfAction() {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('SistemaRRHHBundle:Cliente')->getClientesConMasOrdenes();

        if (!$entities) {
            throw $this->createNotFoundException('Unable to find Orden entity.');
        }

        $config = array(
            'titulo' => 'Reporte cantidad de ordenes por cliente',
            'PDF_HEADER_LOGO' => 'logo_neon.jpg',
            'PDF_HEADER_LOGO_WIDTH' => '55',
            'PDF_HEADER_TITLE' => 'Neon 3R',
            'PDF_HEADER_STRING' => 'Reporte cantidad de ordenes por cliente',
            'pie' => '',
        );

        $html = $this->renderView('SistemaReporteBundle:Default:cantidadOrdenPorCliente.pdf.twig', array('action' => 'defuncion', 'entities' => $entities)
        );

        return $this->get('io_tcpdf_mws')->quick_pdf($html, $config);
    }

    /**
     * Finds and displays a Orden entity.
     *
     * @param type $id Id de la entidad
     *
     * @Route("/imprimir/articulos-mas-utilizados-imprimir", name="admin_articulo_mas_usados_imprimir")
     * @Method("GET")
     * @Template()
     * @return view
     */
    public function imprimirarticulomasusadospdfAction() {
        ini_set('memory_limit', '256M');
        set_time_limit(0); 
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('SistemaSTOCKBundle:Articulo')->GetArticulosMasUsados();

        if (!$entities) {
            throw $this->createNotFoundException('Unable to find Orden entity.');
        }

        $config = array(
            'titulo' => 'Reporte Articulos mas utilizados',
            'PDF_HEADER_LOGO' => 'logo_neon.jpg',
            'PDF_HEADER_LOGO_WIDTH' => '55',
            'PDF_HEADER_TITLE' => 'Neon 3R',
            'PDF_HEADER_STRING' => 'Reporte Articulos mas utilizados',
            'pie' => ''
        );

        $html = $this->renderView('SistemaReporteBundle:Default:articulosMasUtilizados.pdf.twig', array('entities' => $entities));

        return $this->get('io_tcpdf_mws')->quick_pdf($html, $config);
    }

    /**
     * @Route("/admin/reporte/cantidad-herramientas-por-empleado/", name="admin_reporte_cantidad_herramientas_por_empleado")
     * @Template()
     */
    public function cantidadHerramientasPorEmpleadoAction() {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();
        $form = $this->createSearchEmpleado();
        $request = $this->getRequest();
        $entities = null;

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $empleado = $form->get('Empleado')->getData();
                $entities = $em->getRepository('SistemaSTOCKBundle:CantEntregaHerramienta')->cantidadHerramientasPorEmpleados($empleado);
                if (empty($entities)) {
                    $this->get('session')->getFlashBag()->add('warning', 'No se ha realizado entregas de herramientas al empleado: ' . $empleado);
                }
            }
        }

        return array(
            'entities' => $entities,
            'form' => $form->createView()
        );
    }

    public function createSearchEmpleado() {
        $form = $this->createFormBuilder(
                        array(
                            'action' => $this->generateUrl('admin_reporte_cantidad_herramientas_por_empleado'),
                            'method' => 'POST')
                )
                // ->add('Empleado', 'entity')
                ->add('Empleado', 'entity', array(
                    'mapped' => false,
                    'class' => 'SistemaRRHHBundle:Empleado',
                    'label' => false,
                    'attr' => array('class' => 'col-lg-4 col-sd-4 col-md-4')
                ))
                ->getForm()
        ;

        return $form;
    }

    /**
     * Lista todos los articulos y controla que las cantidades sean correctas
     *
     * @Route("/admin/reporte/control/articulos/cantidades", name="admin_reporte_control_articulos_cantidades")
     * @Template()
     */
    public function controlCantidadesArticulosAction() {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('SistemaSTOCKBundle:Orden')->findArticulosCantRealOrdenes();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Lista todos los articulos y controla que las cantidades reservadas sean correctas
     *
     * @Route("/admin/reporte/control/articulos/cantidades-reserva", name="admin_reporte_control_articulos_cantidades_reserva")
     * @Template()
     */
    public function controlCantidadesReservaArticulosAction() {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('SistemaSTOCKBundle:Orden')->findArticulosCantReservaOrdenes();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Lista todos los articulos y controla que las cantidades reservadas sean correctas
     *
     * @Route("/admin/reporte/control/empleado/horas-trabajadas", name="admin_reporte_horas_trabajadas_por_empleado")
     * @Template()
     */
    public function reporteHorasTrabajadasPorEmpleadoAction() {
        $this->get('security_role')->controlRolesUser();
        $request = $this->getRequest();
        if ($request->getMethod() == "POST") {
            $fechaInicio = $request->request->get('fechaInicio', null);
            $fechaFin = $request->request->get('fechaFin', null);
            $tipo = $request->request->get('tipo', 'total');
            $em = $this->getDoctrine()->getManager();
            list($dia, $mes, $año) = split('[/.-]', $fechaInicio);
            $fechaInicio = $año . '-' . $mes . '-' . $dia;
            list($dia, $mes, $año) = split('[/.-]', $fechaFin);
            $fechaFin = $año . '-' . $mes . '-' . $dia;
            $fi = new \DateTime($fechaInicio);
            $ff = new \DateTime($fechaFin);
            $entities = null;
            if ($fi > $ff) {
                $this->get('session')->getFlashBag()->add('danger', 'Rango de fecha invalida');
                return array();
            } else {
                $entities = $em->getRepository('SistemaSTOCKBundle:EmpleadoHora')->obtenerHorasPorOrden($fi, $ff, $tipo);
            }

            if (!$entities) {
                throw $this->createNotFoundException('Unable to find Orden entity.');
            }
            $titulo = 'Reporte Horas Trabajadas Por Empleado';
            if (is_null($tipo)) {
                $titulo = $titulo . ' - Total';
            } elseif ($tipo == "0") {
                $titulo = $titulo . ' - Mantenimiento';
            } elseif ($tipo == "1") {
                $titulo = $titulo . ' - Obra';
            }

            $config = array(
                'titulo' => $titulo,
                'PDF_HEADER_LOGO' => 'logo_neon.jpg',
                'PDF_HEADER_LOGO_WIDTH' => '55',
                'PDF_HEADER_TITLE' => 'Neon 3R',
                'PDF_HEADER_STRING' => '                                                                    ' . $titulo . '
                                            M.M.O. Raúl Roberto Rojas
                                            Av. San Martín 2386 Resistencia - Chaco                            
                                            Tel: 362-4489609 / Cel: 362-4641854 Fecha: ' . date('d/m/Y') . '
                                            Correo: gerencia@neon3r.com.ar',
                'pie' => ''
            );

            $html = $this->renderView('SistemaReporteBundle:Default:reporteHorasTrabajadasPorEmpleado.html.pdf.twig', array('action' => 'defuncion', 'entities' => $entities)
            );

            return $this->get('io_tcpdf_mws')->quick_pdf($html, $config);
        }
        return array();
    }

}
