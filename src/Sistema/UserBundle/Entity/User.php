<?php
namespace Sistema\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sistema\MWSCONFBundle\Entity\MWSgedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;

/**
 * @ORM\Table(name="admin_user")
 * @ORM\Entity(repositoryClass="Sistema\UserBundle\Entity\UserRepository")
 */

class User extends MWSgedmo implements UserInterface, AdvancedUserInterface, \Serializable
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\Column(type="string", length=255)
    */
    protected $username;

    /**
     * @ORM\Column(name="password", type="string", length=255)
     */
    protected $password;

    /**
     * @ORM\Column(name="salt", type="string", length=255)
     */
    protected $salt;

    /**
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="users", cascade={"persist"})
     **/
    protected $user_roles;

     /**
     * @var boolean $isActive
     *
     * @ORM\Column(type="boolean", nullable=true)
     * @Assert\Type(type="boolean", message="El valor {{ value }} no es un {{ type }} valido.")
     */
    private $isActive;

    /**
     * @var integer
     *
     * @ORM\OneToMany(targetEntity="Sistema\FACTURACIONBundle\Entity\Caja", mappedBy="user")
     */
    private $cajas;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setIsActive(true);
        $this->user_roles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->cajas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set salt
     *
     * @param string $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Add user_roles
     *
     * @param  Ssitema\UserBundle\Entity\Role $userRoles
     * @return User
     */
    public function addRole(\Sistema\UserBundle\Entity\Role $userRoles)
    {
        $userRoles->addUser($this);
        $this->user_roles[] = $userRoles;

        return $this;
    }

    public function setUserRoles($roles)
    {
        $this->user_roles = $roles;
    }

    /**
     * Get user_roles
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getUserRoles()
    {
        return $this->user_roles;

    }

    /**
     * Get roles
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getRoles()
    {
        return $this->user_roles->toArray(); //IMPORTANTE: el mecanismo de seguridad de Sf2 requiere ésto como un array
    }

    /**
     * Compares this user to another to determine if they are the same.
     *
     * @param  UserInterface $user The user
     * @return boolean       True if equal, false othwerwise.
     */
    public function equals(UserInterface $user)
    {
        return md5($this->getUsername()) == md5($user->getUsername());

    }

    /**
     * Erases the user credentials.
     */
    public function eraseCredentials()
    {
    }

     /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        /*
         * ! Don't serialize $roles field !
         */

        return \serialize(array(
            $this->id,
            $this->username,
            $this->salt,
            $this->isActive,
            $this->password
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->salt,
            $this->isActive,
            $this->password
        ) = \unserialize($serialized);
    }

    /**
     * Set isActive
     *
     * @param  boolean $isActive
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Add user_roles
     *
     * @param  \Sistema\UserBundle\Entity\Role $userRoles
     * @return User
     */
    public function addUserRole(\Sistema\UserBundle\Entity\Role $userRoles)
    {
        $userRoles->addUser($this);
        $this->user_roles[] = $userRoles;

        return $this;
    }

    /**
     * Remove user_roles
     *
     * @param \Sistema\UserBundle\Entity\Role $userRoles
     */
    public function removeUserRole(\Sistema\UserBundle\Entity\Role $userRoles)
    {
        $this->user_roles->removeElement($userRoles);
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return $this->getIsActive();
    }

    /**
     * Add cajas
     *
     * @param \Sistema\FACTURACIONBundle\Entity\Caja $cajas
     * @return User
     */
    public function addCaja(\Sistema\FACTURACIONBundle\Entity\Caja $cajas)
    {
        $this->cajas[] = $cajas;
    
        return $this;
    }

    /**
     * Remove cajas
     *
     * @param \Sistema\FACTURACIONBundle\Entity\Caja $cajas
     */
    public function removeCaja(\Sistema\FACTURACIONBundle\Entity\Caja $cajas)
    {
        $this->cajas->removeElement($cajas);
    }

    /**
     * Get cajas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCajas()
    {
        return $this->cajas;
    }
}