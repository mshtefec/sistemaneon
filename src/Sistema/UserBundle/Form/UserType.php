<?php

namespace Sistema\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

/**
 * UserType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', null, array(
                'label' => 'Nombre',
                'label_attr' => array(
                    'class' => 'form-label col-lg-3',
                ),
                'attr'  => array(
                    'autofocus' => 'autofocus',
                    'class' => 'form-control'
                )
            ))
            ->add('password', 'repeated', array(
                'type'            => 'password',
                'required'        => true,
                'invalid_message' => 'Las Contraseñas deben Coincidir.',
                'options'         => array(
                    'attr' => array(
                        'class' => 'password-field'
                    )
                ),
                'first_options'  => array(
                    'label' => 'Contraseña',
                    'label_attr' => array(
                        'class' => 'form-label col-lg-3',
                    ),
                    'attr'  => array(
                        'class' => 'form-control'
                    )
                ),
                'second_options' => array(
                    'label' => 'Repetir',
                    'label_attr' => array(
                        'class' => 'form-label col-lg-3',
                    ),
                    'attr'  => array(
                        'class' => 'form-control'
                    )
                ),
            ))
            ->add('user_roles', null, array(
                'class' => 'SistemaUserBundle:Role',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('r')
                        ->where('r.acceso = true')
                        ->orderBy('r.role_name', 'ASC')
                    ;
                },
                'property' => 'roleName',
                'label'    => 'Roles',
                'multiple' => true,
                'expanded' => true,
                'label_attr'  => array(
                    'class' => 'control-label col-lg-11',
                ),
                'attr'  => array(
                    'class' => 'col-lg-4'
                ),
            ))
            ->add('isActive', null, array(
                'label' => 'Activo',
                'label_attr' => array(
                    'class' => 'form-label col-lg-11',
                ),
                'attr'  => array(
                    'class' => 'col-lg-4'
                ),
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\UserBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_userbundle_user';
    }
}
