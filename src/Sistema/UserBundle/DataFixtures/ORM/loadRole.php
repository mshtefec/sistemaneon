<?php

namespace Sistema\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Sistema\UserBundle\Entity\Role;

class loadRole extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $roles = array(
            '0'  => array('roleName' => 'ADMIN'),
            // '1'  => array('roleName' => 'VER'),
            // '2'  => array('roleName' => 'CREAR'),
            // '3'  => array('roleName' => 'EDITAR'),
            // '4'  => array('roleName' => 'ELIMINAR'),
            '5'  => array('roleName' => 'EMPLEADOS'),
            '6'  => array('roleName' => 'CLIENTES'),
            '7'  => array('roleName' => 'RUBROS'),
            '8'  => array('roleName' => 'ARTICULOS'),
            '9'  => array('roleName' => 'FACTURACION'),
            '10' => array('roleName' => 'ARTICULOS CANTIDAD'),
            '11' => array('roleName' => 'ARTICULOS PRECIO'),
            '12' => array('roleName' => 'RUBROS PRECIO'),
            '13' => array('roleName' => 'ORDEN'),
            '14' => array('roleName' => 'DEPOSITO'),
            '15' => array('roleName' => 'ORDEN SHOW PRECIO'),
            '16' => array('roleName' => 'PROVEEDOR'),
        );

        foreach ($roles as $key => $r) {
            $role = new Role();
            $role->setRoleName($r['roleName']);
            $role->setName();
            $role->serialize();
            $this->addReference($role->getName(), $role);
            $manager->persist($role);
        }

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1; // the order in which fixtures will be loaded
    }

}
