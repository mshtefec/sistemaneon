// Get the ul that holds the collection of escuelas
var collectionTelefono = jQuery('.telefono');

jQuery(document).ready(function () {
    collectionTelefono.data('index', collectionTelefono.find(':input').length);

    jQuery('.telefono').delegate('.btnRemoveTelefono', 'click', function (e) {
        // prevent the link from creating a "#" on  the URL
        e.preventDefault();
        // remove the li for the tag form
        jQuery(this).closest('.rowremove').remove();
    });

    jQuery('.ribon_dom').delegate('.add_telefono_link', 'click', function (e) {
        // prevent the link from creating a "#" on the URL                
        e.preventDefault();
        // remove the li for the tag form
        addForm(collectionTelefono, jQuery('.telefono'));
        if ($(".mws_checkbox").size()) {
            $('input[type="checkbox"].mws_checkbox').checkbox({
                buttonStyle: 'btn btn-base',
                buttonStyleChecked: 'btn btn-success',
                checkedClass: 'glyphicon glyphicon-check',
                uncheckedClass: 'glyphicon glyphicon-unchecked'
            });
        }
    });
});