// Get the ul that holds the collection of escuelas
var collectionDomicilio = jQuery('.domicilio');

jQuery(document).ready(function () {
    collectionDomicilio.data('index', collectionDomicilio.find(':input').length);

    jQuery('.domicilio').delegate('.btnRemoveDomicilio', 'click', function (e) {
        // prevent the link from creating a "#" on  the URL
        e.preventDefault();
        // remove the li for the tag form
        jQuery(this).closest('.rowremove').remove();
    });

    jQuery('.ribon_dom').delegate('.add_domicilio_link', 'click', function (e) {
        // prevent the link from creating a "#" on the URL                
        e.preventDefault();
        // remove the li for the tag form
        addForm(collectionDomicilio, jQuery('.domicilio'));
        if ($(".mws_checkbox").size()) {
            $('input[type="checkbox"].mws_checkbox').checkbox({
                buttonStyle: 'btn btn-base',
                buttonStyleChecked: 'btn btn-success',
                checkedClass: 'glyphicon glyphicon-check',
                uncheckedClass: 'glyphicon glyphicon-unchecked'
            });
        }
    });
});