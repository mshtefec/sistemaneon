<?php

namespace Sistema\RRHHBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Range;

class DomicilioType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('localidad', null, array(
                    'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                ))
                ->add('calle', null, array(
                    'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                ))
                ->add('numero', null, array(
                    'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                    'constraints' => new Range(array('min' => 1, 'max' => 999999)),
                ))
                ->add('departamento', null, array(
                    'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                    'required' => false
                ))
                ->add('edificio', null, array(
                    'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                    'required' => false
                ))
                ->add('manzana', null, array(
                    'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                    'required' => false
                ))
                ->add('entreCalles', null, array(
                    'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                    'required' => false
                ))
                ->add('tira', null, array(
                    'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                    'required' => false
                ))
                ->add('parcela', null, array(
                    'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                    'required' => false
                ))
                ->add('domicilioPrincipal', null, array(
                    'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                    'required' => false,
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\RRHHBundle\Entity\Domicilio'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_rrhhbundle_domicilio';
    }

}
