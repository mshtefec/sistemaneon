<?php

namespace Sistema\RRHHBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * EmpleadoTipoType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class EmpleadoTipoType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('calcularSueldoHsOt', null, array(
                'label_attr' => array('class' => 'col-lg-4 col-md-4 col-sm-4'),
                'label'      => 'Calcular Sueldo Hs en OT',
            ))
            ->add('nombre')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\RRHHBundle\Entity\EmpleadoTipo'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_rrhhbundle_empleadotipo';
    }
}
