<?php

namespace Sistema\RRHHBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;

/**
 * EmpleadoFilterType filtro.
 * @author Nombre Apellido <name@gmail.com>
 */
class EmpleadoFilterType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('apellido', 'filter_text_like',array(
                        'attr'=> array('class'=>'form-control')
                    ))
            ->add('nombre', 'filter_text_like',array(
                        'attr'=> array('class'=>'form-control')
                    ))
            ->add('dni', 'filter_text',array(
                        'attr'=> array('class'=>'form-control')
                    ))
            /*->add('cuitcuil', 'filter_text',array(
                        'attr'=> array('class'=>'form-control'),
                        'label'  => 'Ciut / Cuil',
                    ))
            ->add('fechanacimiento', 'filter_date_range',array(
                        'attr'=> array('class'=>'form-control'),
                        'label'  => 'Fecha Nacimiento',
                    ))*/
             //->add('activo')

        ;

        $listener = function (FormEvent $event) {
            // Is data empty?
            foreach ((array) $event->getForm()->getData() as $data) {
                if ( is_array($data)) {
                    foreach ($data as $subData) {
                        if (!empty($subData)) {
                            return;
                        }
                    }
                } else {
                    if (!empty($data)) {
                        return;
                    }
                }
            }
            $event->getForm()->addError(new FormError('Filter empty'));
        };
        $builder->addEventListener(FormEvents::POST_SUBMIT, $listener);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\RRHHBundle\Entity\Empleado',
            'csrf_protection'   => true,
            'validation_groups' => array('filtering')
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_rrhhbundle_empleadofiltertype';
    }
}
