<?php

namespace Sistema\RRHHBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * ClienteCondicionType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class ClienteCondicionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('condicion')
                ->add('iva', null, array(
                    'label' => 'Iva %',
                ))
                ->add('discriminar', null, array(
                    'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\RRHHBundle\Entity\ClienteCondicion'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_rrhhbundle_clientecondicion';
    }

}
