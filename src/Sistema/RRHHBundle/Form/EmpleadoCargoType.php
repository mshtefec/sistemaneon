<?php

namespace Sistema\RRHHBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * EmpleadoCargoType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class EmpleadoCargoType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('cargo', null, array(
                'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                'attr' => array('autofocus' => 'autofocus')
            ))
            ->add('sueldoMensual', null, array(
                'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
            ))
            ->add('costoHora', null, array(
                'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
            ))
            ->add('costoHoraCliente', null, array(
                'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                'label'      => 'Hora Cliente Obra',
            ))
            ->add('costoHoraMantenimiento', null, array(
                'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                'label'      => 'Hora Cliente Mantenimiento',
            ))
            ->add('costoHoraUrgencia', null, array(
                'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                'label'      => 'Hora Cliente Urgencia',
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\RRHHBundle\Entity\EmpleadoCargo'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_rrhhbundle_empleadocargo';
    }

}
