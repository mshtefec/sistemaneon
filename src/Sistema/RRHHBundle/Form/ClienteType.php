<?php

namespace Sistema\RRHHBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * ClienteType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class ClienteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('apellido', null, array(
                'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                'attr' => array('autofocus' => 'autofocus'),
            ))
            ->add('nombre', null, array(
                'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
            ))
            ->add('empresa', null, array(
                'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                'label' => 'Razon Social'
            ))
            ->add('dni', null, array(
                'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                'data_mask' => '99.999.999',
                'required' => false,
            ))
            ->add('cuitcuil', null, array(
                'label' => 'Cuit / Cuil',
                'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                'data_mask' => '99-99999999-9',
                'required' => false,
            ))/*
              ->add('fechanacimiento', 'bootstrapdatetime', array(
              'required'   => false,
              'label'      => 'Fecha Nacimiento',
              'read_only'  => true,
              'label_attr' => array(
              'class' => 'col-lg-2 col-md-2 col-sm-2',
              ),
              'attr'       => array(
              'class' => 'col-lg-4',
              ),
              )) */
            ->add('activo', null, array(
                'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                // 'data' => true
            ))
            ->add('email', null, array(
                'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
            ))
            ->add('condicion', null, array(
                'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                'required' => true,
            ))
            ->add('domicilios', 'collection', array(
                'type' => new DomicilioType(),
                'allow_add' => true,
                'required' => true,
                'by_reference' => false,
                'label' => false,
            ))
            ->add('telefonos', 'collection', array(
                'type' => new TelefonoType(),
                'allow_add' => true,
                'required' => true,
                'by_reference' => false,
                'label' => false,
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\RRHHBundle\Entity\Cliente'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_rrhhbundle_cliente';
    }

}
