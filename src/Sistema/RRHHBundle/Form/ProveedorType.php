<?php

namespace Sistema\RRHHBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * ProveedorType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class ProveedorType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('apellido',null, array(
                'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2')))
            ->add('nombre',null, array(
                'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2')))
            /*->add('dni', null, array(
                'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                'data_mask'  => "99.999.999",
                'required'   => true,
            ))*/
            ->add('empresa', null, array(
                'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                'label'      => 'Razon Social',
                'required'   => true,
            ))
            ->add('cuitcuil', null, array(
                'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                'label'      => 'Cuit / Cuil',
                'data_mask'  => "99-99999999-9",
                'required'   => true,
            ))
            ->add('activo',null, array(
                'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                'required'   => false))
            ->add('domicilios', 'collection', array(
                'type'          => new DomicilioType(),
                'allow_add'     => true,
                'required'      => true,
                'by_reference'  => false,
                'label'         => false,
            ))
            ->add('telefonos', 'collection', array(
                'type'          => new TelefonoType(),
                'allow_add'     => true,
                'required'      => true,
                'by_reference'  => false,
                'label'         => false,
            ))
           /* ->add('cuentaCorriente')*/
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\RRHHBundle\Entity\Proveedor'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_rrhhbundle_proveedor';
    }
}
