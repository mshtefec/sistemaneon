<?php

namespace Sistema\RRHHBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * EmpleadoType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class EmpleadoType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('apellido', null, array(
                'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                'attr'       => array('autofocus' => 'autofocus'),
                'required'   => true,
            ))
            ->add('nombre', null, array(
                'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                'required'   => true,
            ))
            ->add('dni', null, array(
                'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                'data_mask'  => "99.999.999",
                'required'   => true,
            ))
            ->add('cuitcuil', null, array(
                'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
                'label'      => 'Cuit / Cuil',
                'data_mask'  => "99-99999999-9",
                'required'   => true,
            ))
            ->add('fechanacimiento', 'bootstrapdatetime', array(
                'required'   => true,
                'label'      => 'Fecha Nacimiento',
                'read_only'  => true,
                'label_attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                ),
                'attr'       => array(
                    'class' => 'col-lg-4 col-md-4 col-sm-4',
                ),
                  'constraints' => new NotBlank(),
            ))
            ->add('tipo', null, array(
                'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
            ))
            ->add('cargo', null, array(
                'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
            ))
            ->add('activo', null, array(
                'label_attr' => array('class' => 'col-lg-2 col-md-2 col-sm-2'),
            ))
            ->add('domicilios', 'collection', array(
                'type'          => new DomicilioType(),
                'allow_add'     => true,
                'allow_delete'  => true,
                'required'      => true,
                'by_reference'  => false,
                'label'         => false,
            ))
            ->add('telefonos', 'collection', array(
                'type'          => new TelefonoType(),
                'allow_add'     => true,
                'allow_delete'  => true,
                'required'      => true,
                'by_reference'  => false,
                'label'         => false,
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\RRHHBundle\Entity\Empleado'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_rrhhbundle_empleado';
    }
}
