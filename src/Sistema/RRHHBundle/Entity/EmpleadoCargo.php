<?php

namespace Sistema\RRHHBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * EmpleadoCargo
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\RRHHBundle\Entity\EmpleadoCargoRepository")
 */
class EmpleadoCargo {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cargo", type="string", length=255)
     */
    private $cargo;

    /**
     * @var string
     *
     * @ORM\Column(name="sueldoMensual", type="float")
     * @Assert\Regex(pattern="/^[0-9(.{1})]/")
     */
    private $sueldoMensual;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\RRHHBundle\Entity\Empleado", mappedBy="cargo")
     * */
    protected $empleadoCargo;

    /**
     * @var string
     *
     * @ORM\Column(name="costoHora", type="float")
     * @Assert\Regex(pattern="/^[0-9(.{1})]/")
     */
    private $costoHora;

    /**
     * @var float
     *
     * @ORM\Column(name="costoHoraCliente", type="float")
     * @Assert\Regex(pattern="/^[0-9(.{1})]/")
     */
    private $costoHoraCliente;

    /**
     * @var float
     *
     * @ORM\Column(name="costoHoraMantenimiento", type="float")
     * @Assert\Regex(pattern="/^[0-9(.{1})]/")
     */
    private $costoHoraMantenimiento;

    /**
     * @var float
     *
     * @ORM\Column(name="costoHoraUrgencia", type="float")
     * @Assert\Regex(pattern="/^[0-9(.{1})]/")
     */
    private $costoHoraUrgencia;
    
    /**
     * @ORM\OneToMany(targetEntity="Sistema\PresupuestoBundle\Entity\HoraExtra", mappedBy="cargo")
     */
    private $horasextra;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\PresupuestoBundle\Entity\TareaCargo", mappedBy="cargo", cascade={"persist","remove"}, orphanRemoval=true)
     */
    private $tareascargos;
    
    public function __toString() {
        return $this->getCargo();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->empleadoCargo = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set cargo
     *
     * @param  string        $cargo
     * @return EmpleadoCargo
     */
    public function setCargo($cargo) {
        $this->cargo = $cargo;

        return $this;
    }

    /**
     * Get cargo
     *
     * @return string
     */
    public function getCargo() {
        return $this->cargo;
    }

    /**
     * Set sueldoMensual
     *
     * @param  float         $sueldoMensual
     * @return EmpleadoCargo
     */
    public function setSueldoMensual($sueldoMensual) {
        $this->sueldoMensual = $sueldoMensual;

        return $this;
    }

    /**
     * Get sueldoMensual
     *
     * @return float
     */
    public function getSueldoMensual() {
        return $this->sueldoMensual;
    }

    /**
     * Add empleadoCargo
     *
     * @param  \Sistema\RRHHBundle\Entity\Empleado $empleadoCargo
     * @return EmpleadoCargo
     */
    public function addEmpleadoCargo(\Sistema\RRHHBundle\Entity\Empleado $empleadoCargo) {
        $this->empleadoCargo[] = $empleadoCargo;

        return $this;
    }

    /**
     * Remove empleadoCargo
     *
     * @param \Sistema\RRHHBundle\Entity\Empleado $empleadoCargo
     */
    public function removeEmpleadoCargo(\Sistema\RRHHBundle\Entity\Empleado $empleadoCargo) {
        $this->empleadoCargo->removeElement($empleadoCargo);
    }

    /**
     * Get empleadoCargo
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmpleadoCargo() {
        return $this->empleadoCargo;
    }

    /**
     * Set costoHora
     *
     * @param  float         $costoHora
     * @return EmpleadoCargo
     */
    public function setCostoHora($costoHora) {
        $this->costoHora = $costoHora;

        return $this;
    }

    /**
     * Get costoHora
     *
     * @return float
     */
    public function getCostoHora() {
        return $this->costoHora;
    }

    /**
     * Set costoHoraCliente
     *
     * @param  float         $costoHoraCliente
     * @return EmpleadoCargo
     */
    public function setCostoHoraCliente($costoHoraCliente) {
        $this->costoHoraCliente = $costoHoraCliente;

        return $this;
    }

    /**
     * Get costoHoraCliente
     *
     * @return float
     */
    public function getCostoHoraCliente() {
        return $this->costoHoraCliente;
    }

    /**
     * Set costoHoraMantenimiento
     *
     * @param float $costoHoraMantenimiento
     * @return EmpleadoCargo
     */
    public function setCostoHoraMantenimiento($costoHoraMantenimiento) {
        $this->costoHoraMantenimiento = $costoHoraMantenimiento;

        return $this;
    }

    /**
     * Get costoHoraMantenimiento
     *
     * @return float 
     */
    public function getCostoHoraMantenimiento() {
        return $this->costoHoraMantenimiento;
    }

    /**
     * Set costoHoraUrgencia
     *
     * @param float $costoHoraUrgencia
     * @return EmpleadoCargo
     */
    public function setCostoHoraUrgencia($costoHoraUrgencia) {
        $this->costoHoraUrgencia = $costoHoraUrgencia;

        return $this;
    }

    /**
     * Get costoHoraUrgencia
     *
     * @return float 
     */
    public function getCostoHoraUrgencia() {
        return $this->costoHoraUrgencia;
    }

    /**
     * Add horasextra
     *
     * @param \Sistema\PresupuestoBundle\Entity\HoraExtra $horaextra
     * @return EmpleadoCargo
     */
    public function addHorasextra(\Sistema\PresupuestoBundle\Entity\HoraExtra $horaextra)
    {
        $this->horasextra[] = $horaextra;
    
        return $this;
    }

    /**
     * Remove horasextra
     *
     * @param \Sistema\PresupuestoBundle\Entity\HoraExtra $horaextra
     */
    public function removeHorasextra(\Sistema\PresupuestoBundle\Entity\HoraExtra $horaextra)
    {
        $this->horasextra->removeElement($horaextra);
    }

    /**
     * Get horasextra
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHorasextra()
    {
        return $this->horasextra;
    }

    /**
     * Add tareascargos
     *
     * @param \Sistema\PresupuestoBundle\Entity\TareaCargo $tareascargos
     * @return EmpleadoCargo
     */
    public function addTareascargo(\Sistema\PresupuestoBundle\Entity\TareaCargo $tareascargos)
    {
        $this->tareascargos[] = $tareascargos;
    
        return $this;
    }

    /**
     * Remove tareascargos
     *
     * @param \Sistema\PresupuestoBundle\Entity\TareaCargo $tareascargos
     */
    public function removeTareascargo(\Sistema\PresupuestoBundle\Entity\TareaCargo $tareascargos)
    {
        $this->tareascargos->removeElement($tareascargos);
    }

    /**
     * Get tareascargos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTareascargos()
    {
        return $this->tareascargos;
    }
}