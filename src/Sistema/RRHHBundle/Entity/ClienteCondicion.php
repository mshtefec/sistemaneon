<?php

namespace Sistema\RRHHBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ClienteCondicion
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class ClienteCondicion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="condicion", type="string", length=100)
     */
    private $condicion;

    /**
     * @var float
     *
     * @ORM\Column(name="iva", type="float")
     */
    private $iva;

    /**
     * @var integer
     *
     * @ORM\OneToMany(targetEntity="Sistema\RRHHBundle\Entity\Cliente", mappedBy="condicion", cascade={"persist"})
     */
    private $clientes;

    /**
     * @var boolean $discriminar
     *
     * @ORM\Column(type="boolean", nullable=true)
     * @Assert\Type(type="boolean", message="El valor {{ value }} no es un {{ type }} valido.")
     */
    protected $discriminar;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->clientes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->setIva(0);
    }

    public function __toString()
    {
        return $this->getCondicion();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set condicion
     *
     * @param  string           $condicion
     * @return ClienteCondicion
     */
    public function setCondicion($condicion)
    {
        $this->condicion = $condicion;

        return $this;
    }

    /**
     * Get condicion
     *
     * @return string
     */
    public function getCondicion()
    {
        return $this->condicion;
    }

    /**
     * Set iva
     *
     * @param  float            $iva
     * @return ClienteCondicion
     */
    public function setIva($iva)
    {
        $this->iva = $iva;

        return $this;
    }

    /**
     * Get iva
     *
     * @return float
     */
    public function getIva()
    {
        return $this->iva;
    }

    /**
     * Add clientes
     *
     * @param  \Sistema\RRHHBundle\Entity\Cliente $clientes
     * @return ClienteCondicion
     */
    public function addCliente(\Sistema\RRHHBundle\Entity\Cliente $clientes)
    {
        $this->clientes[] = $clientes;

        return $this;
    }

    /**
     * Remove clientes
     *
     * @param \Sistema\RRHHBundle\Entity\Cliente $clientes
     */
    public function removeCliente(\Sistema\RRHHBundle\Entity\Cliente $clientes)
    {
        $this->clientes->removeElement($clientes);
    }

    /**
     * Get clientes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClientes()
    {
        return $this->clientes;
    }

    /**
     * Set discriminar
     *
     * @param  boolean          $discriminar
     * @return ClienteCondicion
     */
    public function setDiscriminar($discriminar)
    {
        $this->discriminar = $discriminar;

        return $this;
    }

    /**
     * Get discriminar
     *
     * @return boolean
     */
    public function getDiscriminar()
    {
        return $this->discriminar;
    }

}
