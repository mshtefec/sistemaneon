<?php

namespace Sistema\RRHHBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Proveedor
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\RRHHBundle\Entity\ProveedorRepository")
 */
class Proveedor extends Persona {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="Sistema\FACTURACIONBundle\Entity\CuentaCorriente", mappedBy="proveedor", cascade={"persist", "remove"})
     */
    protected $cuentaCorriente;

    /**
     * @var string
     *
     * @ORM\Column(name="empresa", type="string", length=100, nullable=true)
     * @Assert\NotBlank()
     */
    protected $empresa;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\FACTURACIONBundle\Entity\FacturaCompra", mappedBy="proveedor", cascade={"all"}, orphanRemoval=true)
     */
    protected $facturaCompras;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\FACTURACIONBundle\Entity\ReciboCompra", mappedBy="proveedor", cascade={"all"}, orphanRemoval=true)
     */
    protected $recibosCompras;

    /**
     * Constructor
     */
    public function __construct() {
        $this->facturaCompras = new \Doctrine\Common\Collections\ArrayCollection();
        $this->recibosCompras = new \Doctrine\Common\Collections\ArrayCollection();
        $this->telefonos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->domicilios = new \Doctrine\Common\Collections\ArrayCollection();
        $cuentaCorriente = new \Sistema\FACTURACIONBundle\Entity\CuentaCorriente();
        $cuentaCorriente->setProveedor($this);
        $cuentaCorriente->setMonto(0);
        $this->setCuentaCorriente($cuentaCorriente);
        $this->setActivo(true);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set empresa
     *
     * @param  string    $empresa
     * @return Proveedor
     */
    public function setEmpresa($empresa) {
        $this->empresa = $empresa;

        return $this;
    }

    /**
     * Get empresa
     *
     * @return string
     */
    public function getEmpresa() {
        return $this->empresa;
    }

    /**
     * Set apellido
     *
     * @param  string    $apellido
     * @return Proveedor
     */
    public function setApellido($apellido) {
        $this->apellido = $apellido;

        return $this;
    }

    /**
     * Get apellido
     *
     * @return string
     */
    public function getApellido() {
        return $this->apellido;
    }

    /**
     * Set nombre
     *
     * @param  string    $nombre
     * @return Proveedor
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre() {
        return $this->nombre;
    }

    /**
     * Set dni
     *
     * @param  string    $dni
     * @return Proveedor
     */
    public function setDni($dni) {
        $this->dni = $dni;

        return $this;
    }

    /**
     * Get dni
     *
     * @return string
     */
    public function getDni() {
        return $this->dni;
    }

    /**
     * Set cuitcuil
     *
     * @param  string    $cuitcuil
     * @return Proveedor
     */
    public function setCuitcuil($cuitcuil) {
        $this->cuitcuil = $cuitcuil;

        return $this;
    }

    /**
     * Get cuitcuil
     *
     * @return string
     */
    public function getCuitcuil() {
        return $this->cuitcuil;
    }

    /**
     * Set fechanacimiento
     *
     * @param  \DateTime $fechanacimiento
     * @return Proveedor
     */
    public function setFechanacimiento($fechanacimiento) {
        $this->fechanacimiento = $fechanacimiento;

        return $this;
    }

    /**
     * Get fechanacimiento
     *
     * @return \DateTime
     */
    public function getFechanacimiento() {
        return $this->fechanacimiento;
    }

    /**
     * Set activo
     *
     * @param  boolean   $activo
     * @return Proveedor
     */
    public function setActivo($activo) {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean
     */
    public function getActivo() {
        return $this->activo;
    }

    /**
     * Set cuentaCorriente
     *
     * @param  \Sistema\FACTURACIONBundle\Entity\CuentaCorriente $cuentaCorriente
     * @return Proveedor
     */
    public function setCuentaCorriente(\Sistema\FACTURACIONBundle\Entity\CuentaCorriente $cuentaCorriente = null) {
        $cuentaCorriente->setProveedor($this);
        $this->cuentaCorriente = $cuentaCorriente;

        return $this;
    }

    /**
     * Get cuentaCorriente
     *
     * @return \Sistema\FACTURACIONBundle\Entity\CuentaCorriente
     */
    public function getCuentaCorriente() {
        return $this->cuentaCorriente;
    }

    /**
     * Add facturaCompras
     *
     * @param  \Sistema\FACTURACIONBundle\Entity\FacturaCompra $facturaCompras
     * @return Proveedor
     */
    public function addFacturaCompra(\Sistema\FACTURACIONBundle\Entity\FacturaCompra $facturaCompras) {
        $facturaCompras->setProveedor($this);
        $this->facturaCompras[] = $facturaCompras;

        return $this;
    }

    /**
     * Remove facturaCompras
     *
     * @param \Sistema\FACTURACIONBundle\Entity\FacturaCompra $facturaCompras
     */
    public function removeFacturaCompra(\Sistema\FACTURACIONBundle\Entity\FacturaCompra $facturaCompras) {
        $this->facturaCompras->removeElement($facturaCompras);
    }

    /**
     * Get facturaCompras
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFacturaCompras() {
        return $this->facturaCompras;
    }

    /**
     * Add telefonos
     *
     * @param  \Sistema\RRHHBundle\Entity\Telefono $telefonos
     * @return Proveedor
     */
    public function addTelefono(\Sistema\RRHHBundle\Entity\Telefono $telefonos) {
        $telefonos->setPersona($this);
        $this->telefonos[] = $telefonos;

        return $this;
    }

    /**
     * Remove telefonos
     *
     * @param \Sistema\RRHHBundle\Entity\Telefono $telefonos
     */
    public function removeTelefono(\Sistema\RRHHBundle\Entity\Telefono $telefonos) {
        $this->telefonos->removeElement($telefonos);
    }

    /**
     * Get telefonos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTelefonos() {
        return $this->telefonos;
    }

    /**
     * Add domicilios
     *
     * @param  \Sistema\RRHHBundle\Entity\Domicilio $domicilios
     * @return Proveedor
     */
    public function addDomicilio(\Sistema\RRHHBundle\Entity\Domicilio $domicilios) {
        $domicilios->setPersona($this);
        $this->domicilios[] = $domicilios;

        return $this;
    }

    /**
     * Remove domicilios
     *
     * @param \Sistema\RRHHBundle\Entity\Domicilio $domicilios
     */
    public function removeDomicilio(\Sistema\RRHHBundle\Entity\Domicilio $domicilios) {
        $this->domicilios->removeElement($domicilios);
    }

    /**
     * Get domicilios
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDomicilios() {
        return $this->domicilios;
    }

    public function __toString() {
        if (is_null($this->getEmpresa())) {
            $toString = $this->getApellido() . ' ' . $this->getNombre();
        } elseif (!is_null($this->getApellido()) || !is_null($this->getNombre())) {
            $toString = $this->getApellido() . ' ' . $this->getNombre() . ' - ' . $this->getEmpresa();
        } else {
            $toString = $this->getEmpresa();
        }
        if (!is_null($this->getDni())) {
            $toString = $toString . ' | ' . $this->getDni();
        }

        return $toString;
    }

    /**
     * Add recibosCompras
     *
     * @param \Sistema\FACTURACIONBundle\Entity\ReciboCompra $recibosCompras
     * @return Proveedor
     */
    public function addRecibosCompra(\Sistema\FACTURACIONBundle\Entity\ReciboCompra $recibosCompras) {
        $this->recibosCompras[] = $recibosCompras;

        return $this;
    }

    /**
     * Remove recibosCompras
     *
     * @param \Sistema\FACTURACIONBundle\Entity\ReciboCompra $recibosCompras
     */
    public function removeRecibosCompra(\Sistema\FACTURACIONBundle\Entity\ReciboCompra $recibosCompras) {
        $this->recibosCompras->removeElement($recibosCompras);
    }

    /**
     * Get recibosCompras
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRecibosCompras() {
        return $this->recibosCompras;
    }

}
