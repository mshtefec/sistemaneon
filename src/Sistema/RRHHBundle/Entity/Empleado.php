<?php

namespace Sistema\RRHHBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Empleado
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\RRHHBundle\Entity\EmpleadoRepository")
 */
class Empleado extends Persona {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\STOCKBundle\Entity\EmpleadoOrden", mappedBy="empleado", cascade={"all"})
     */
    protected $ordenes;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Sistema\RRHHBundle\Entity\EmpleadoCargo", inversedBy="empleadoCargo")
     * @ORM\JoinColumn(name="cargo_id", referencedColumnName="id")
     * @Assert\NotNull()
     */
    protected $cargo;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Sistema\RRHHBundle\Entity\EmpleadoTipo", inversedBy="empleados")
     * @ORM\JoinColumn(name="tipo_id", referencedColumnName="id")
     * @Assert\NotNull()
     */
    protected $tipo;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\STOCKBundle\Entity\EntregasHerramientas", mappedBy="empleado", cascade={"all"})
     */
    protected $entregasHerramienta;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\STOCKBundle\Entity\OrdenSalida", mappedBy="empleado")
     */
    private $ordenSalida;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\FACTURACIONBundle\Entity\Sueldo", mappedBy="empleado", cascade={"all"})
     */
    private $sueldo;
    
    /**
     * Constructor
     */
    public function __construct() {
        $this->ordenes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->entregasHerramienta = new \Doctrine\Common\Collections\ArrayCollection();
        $this->ordenSalida = new \Doctrine\Common\Collections\ArrayCollection();
        $this->sueldo = new \Doctrine\Common\Collections\ArrayCollection();
        $this->telefonos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->domicilios = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Add ordenes
     *
     * @param  \Sistema\STOCKBundle\Entity\EmpleadoOrden $ordenes
     * @return Empleado
     */
    public function addOrdene(\Sistema\STOCKBundle\Entity\EmpleadoOrden $ordenes) {
        $this->ordenes[] = $ordenes;

        return $this;
    }

    /**
     * Remove ordenes
     *
     * @param \Sistema\STOCKBundle\Entity\EmpleadoOrden $ordenes
     */
    public function removeOrdene(\Sistema\STOCKBundle\Entity\EmpleadoOrden $ordenes) {
        $this->ordenes->removeElement($ordenes);
    }

    /**
     * Get ordenes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrdenes() {
        return $this->ordenes;
    }

    /**
     * Set cargo
     *
     * @param  \Sistema\RRHHBundle\Entity\EmpleadoCargo $cargo
     * @return Empleado
     */
    public function setCargo(\Sistema\RRHHBundle\Entity\EmpleadoCargo $cargo = null) {
        $this->cargo = $cargo;

        return $this;
    }

    /**
     * Get cargo
     *
     * @return \Sistema\RRHHBundle\Entity\EmpleadoCargo
     */
    public function getCargo() {
        return $this->cargo;
    }

    /**
     * Set tipo
     *
     * @param  \Sistema\RRHHBundle\Entity\EmpleadoTipo $tipo
     * @return Empleado
     */
    public function setTipo(\Sistema\RRHHBundle\Entity\EmpleadoTipo $tipo = null) {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return \Sistema\RRHHBundle\Entity\EmpleadoTipo
     */
    public function getTipo() {
        return $this->tipo;
    }

    public function __toString() {
        return $this->apellido . " " . $this->nombre;
    }

    /**
     * Add entregasHerramienta
     *
     * @param  \Sistema\STOCKBundle\Entity\EntregasHerramientas $entregasHerramienta
     * @return Empleado
     */
    public function addEntregasHerramienta(\Sistema\STOCKBundle\Entity\EntregasHerramientas $entregasHerramienta) {
        $this->entregasHerramienta[] = $entregasHerramienta;

        return $this;
    }

    /**
     * Remove entregasHerramienta
     *
     * @param \Sistema\STOCKBundle\Entity\EntregasHerramientas $entregasHerramienta
     */
    public function removeEntregasHerramienta(\Sistema\STOCKBundle\Entity\EntregasHerramientas $entregasHerramienta) {
        $this->entregasHerramienta->removeElement($entregasHerramienta);
    }

    /**
     * Get entregasHerramienta
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEntregasHerramienta() {
        return $this->entregasHerramienta;
    }

    /**
     * Add ordenSalida
     *
     * @param  \Sistema\STOCKBundle\Entity\OrdenSalida $ordenSalida
     * @return Empleado
     */
    public function addOrdenSalida(\Sistema\STOCKBundle\Entity\OrdenSalida $ordenSalida) {
        $this->ordenSalida[] = $ordenSalida;

        return $this;
    }

    /**
     * Remove ordenSalida
     *
     * @param \Sistema\STOCKBundle\Entity\OrdenSalida $ordenSalida
     */
    public function removeOrdenSalida(\Sistema\STOCKBundle\Entity\OrdenSalida $ordenSalida) {
        $this->ordenSalida->removeElement($ordenSalida);
    }

    /**
     * Get ordenSalida
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrdenSalida() {
        return $this->ordenSalida;
    }

    //Fin Claseeeee

    /**
     * Add sueldo
     *
     * @param \Sistema\FACTURACIONBundle\Entity\Sueldo $sueldo
     * @return Empleado
     */
    public function addSueldo(\Sistema\FACTURACIONBundle\Entity\Sueldo $sueldo) {
        $this->sueldo[] = $sueldo;

        return $this;
    }

    /**
     * Remove sueldo
     *
     * @param \Sistema\FACTURACIONBundle\Entity\Sueldo $sueldo
     */
    public function removeSueldo(\Sistema\FACTURACIONBundle\Entity\Sueldo $sueldo) {
        $this->sueldo->removeElement($sueldo);
    }

    /**
     * Get sueldo
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSueldo() {
        return $this->sueldo;
    }

}
