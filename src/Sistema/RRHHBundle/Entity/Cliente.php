<?php

namespace Sistema\RRHHBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cliente
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\RRHHBundle\Entity\ClienteRepository")
 */
class Cliente extends Persona {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=200, nullable=true)
     * @Assert\Email(
     *     message = "El email '{{ value }}' no es un email Valido."
     * )
     */
    protected $email;

    /**
     * @var string
     *
     * @ORM\Column(name="empresa", type="string", length=100, nullable=true)
     */
    protected $empresa;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\STOCKBundle\Entity\Orden", mappedBy="cliente", cascade={"persist", "remove"})
     */
    protected $ordenes;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\PresupuestoBundle\Entity\Presupuesto", mappedBy="cliente", cascade={"persist", "remove"})
     */
    protected $presupuesto;

    /**
     * @ORM\OneToOne(targetEntity="Sistema\FACTURACIONBundle\Entity\CuentaCorriente", mappedBy="cliente", cascade={"persist", "remove"})
     */
    protected $cuentaCorriente;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Sistema\RRHHBundle\Entity\ClienteCondicion", inversedBy="clientes")
     * @ORM\JoinColumn(name="condicion_id", referencedColumnName="id")
     */
    protected $condicion;

    /**
     * Constructor
     */
    public function __construct() {
        $this->ordenes     = new ArrayCollection();
        $this->presupuesto = new ArrayCollection();
        $this->telefonos   = new ArrayCollection();
        $this->domicilios  = new ArrayCollection();
        $this->activo      = true;
    }

    public function __toString() {
        if (is_null($this->getEmpresa())) {
            $toString = $this->getApellido() . ' ' . $this->getNombre();
        } elseif (!is_null($this->getApellido()) || !is_null($this->getNombre())) {
            $toString = $this->getApellido() . ' ' . $this->getNombre() . ' - ' . $this->getEmpresa();
        } else {
            $toString = $this->getEmpresa();
        }
        if (!is_null($this->getDni())) {
            $toString = $toString . ' | ' . $this->getDni();
        }

        return $toString;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param  string  $email
     * @return Cliente
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set empresa
     *
     * @param  string  $empresa
     * @return Cliente
     */
    public function setEmpresa($empresa) {
        $this->empresa = $empresa;

        return $this;
    }

    /**
     * Get empresa
     *
     * @return string
     */
    public function getEmpresa() {
        return $this->empresa;
    }

    /**
     * Set apellido
     *
     * @param  string  $apellido
     * @return Cliente
     */
    public function setApellido($apellido) {
        $this->apellido = $apellido;

        return $this;
    }

    /**
     * Get apellido
     *
     * @return string
     */
    public function getApellido() {
        return $this->apellido;
    }

    /**
     * Set nombre
     *
     * @param  string  $nombre
     * @return Cliente
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre() {
        return $this->nombre;
    }

    /**
     * Set dni
     *
     * @param  string  $dni
     * @return Cliente
     */
    public function setDni($dni) {
        $this->dni = $dni;

        return $this;
    }

    /**
     * Get dni
     *
     * @return string
     */
    public function getDni() {
        return $this->dni;
    }

    /**
     * Set cuitcuil
     *
     * @param  string  $cuitcuil
     * @return Cliente
     */
    public function setCuitcuil($cuitcuil) {
        $this->cuitcuil = $cuitcuil;

        return $this;
    }

    /**
     * Get cuitcuil
     *
     * @return string
     */
    public function getCuitcuil() {
        return $this->cuitcuil;
    }

    /**
     * Set fechanacimiento
     *
     * @param  \DateTime $fechanacimiento
     * @return Cliente
     */
    public function setFechanacimiento($fechanacimiento) {
        $this->fechanacimiento = $fechanacimiento;

        return $this;
    }

    /**
     * Get fechanacimiento
     *
     * @return \DateTime
     */
    public function getFechanacimiento() {
        return $this->fechanacimiento;
    }

    /**
     * Set activo
     *
     * @param  boolean $activo
     * @return Cliente
     */
    public function setActivo($activo) {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean
     */
    public function getActivo() {
        return $this->activo;
    }

    /**
     * Add ordenes
     *
     * @param  \Sistema\STOCKBundle\Entity\Orden $ordenes
     * @return Cliente
     */
    public function addOrdene(\Sistema\STOCKBundle\Entity\Orden $ordenes) {
        $ordenes->setCliente($this);
        $this->ordenes[] = $ordenes;

        return $this;
    }

    /**
     * Remove ordenes
     *
     * @param \Sistema\STOCKBundle\Entity\Orden $ordenes
     */
    public function removeOrdene(\Sistema\STOCKBundle\Entity\Orden $ordenes) {
        $this->ordenes->removeElement($ordenes);
    }

    /**
     * Get ordenes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrdenes() {
        return $this->ordenes;
    }

    /**
     * Add presupuesto
     *
     * @param  \Sistema\PresupuestoBundle\Entity\Presupuesto $presupuesto
     * @return Cliente
     */
    public function addPresupuesto(\Sistema\PresupuestoBundle\Entity\Presupuesto $presupuesto) {
        $presupuesto->setCliente($this);
        $this->presupuesto[] = $presupuesto;

        return $this;
    }

    /**
     * Remove presupuesto
     *
     * @param \Sistema\PresupuestoBundle\Entity\Presupuesto $presupuesto
     */
    public function removePresupuesto(\Sistema\PresupuestoBundle\Entity\Presupuesto $presupuesto) {
        $this->presupuesto->removeElement($presupuesto);
    }

    /**
     * Get presupuesto
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPresupuesto() {
        return $this->presupuesto;
    }

    /**
     * Set cuentaCorriente
     *
     * @param  \Sistema\FACTURACIONBundle\Entity\CuentaCorriente $cuentaCorriente
     * @return Cliente
     */
    public function setCuentaCorriente(\Sistema\FACTURACIONBundle\Entity\CuentaCorriente $cuentaCorriente = null) {
        $cuentaCorriente->setCliente($this);
        $this->cuentaCorriente = $cuentaCorriente;

        return $this;
    }

    /**
     * Get cuentaCorriente
     *
     * @return \Sistema\FACTURACIONBundle\Entity\CuentaCorriente
     */
    public function getCuentaCorriente() {
        return $this->cuentaCorriente;
    }

    /**
     * Add telefonos
     *
     * @param  \Sistema\RRHHBundle\Entity\Telefono $telefonos
     * @return Cliente
     */
    public function addTelefono(\Sistema\RRHHBundle\Entity\Telefono $telefonos) {
        $telefonos->setPersona($this);
        $this->telefonos[] = $telefonos;

        return $this;
    }

    /**
     * Remove telefonos
     *
     * @param \Sistema\RRHHBundle\Entity\Telefono $telefonos
     */
    public function removeTelefono(\Sistema\RRHHBundle\Entity\Telefono $telefonos) {
        $this->telefonos->removeElement($telefonos);
    }

    /**
     * Get telefonos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTelefonos() {
        return $this->telefonos;
    }

    /**
     * Add domicilios
     *
     * @param  \Sistema\RRHHBundle\Entity\Domicilio $domicilios
     * @return Cliente
     */
    public function addDomicilio(\Sistema\RRHHBundle\Entity\Domicilio $domicilios) {
        $domicilios->setPersona($this);
        $this->domicilios[] = $domicilios;

        return $this;
    }

    /**
     * Remove domicilios
     *
     * @param \Sistema\RRHHBundle\Entity\Domicilio $domicilios
     */
    public function removeDomicilio(\Sistema\RRHHBundle\Entity\Domicilio $domicilios) {
        $this->domicilios->removeElement($domicilios);
    }

    /**
     * Get domicilios
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDomicilios() {
        return $this->domicilios;
    }

    /**
     * Set condicion
     *
     * @param  \Sistema\RRHHBundle\Entity\ClienteCondicion $condicion
     * @return Cliente
     */
    public function setCondicion(\Sistema\RRHHBundle\Entity\ClienteCondicion $condicion = null) {
        $this->condicion = $condicion;

        return $this;
    }

    /**
     * Get condicion
     *
     * @return \Sistema\RRHHBundle\Entity\ClienteCondicion
     */
    public function getCondicion() {
        return $this->condicion;
    }
}
