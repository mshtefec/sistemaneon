<?php

namespace Sistema\RRHHBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EmpleadoTipo
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\RRHHBundle\Entity\EmpleadoTipoRepository")
 */
class EmpleadoTipo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var integer
     *
     * @ORM\OneToMany(targetEntity="Sistema\RRHHBundle\Entity\Empleado", mappedBy="tipo", cascade={"persist"})
     */
    private $empleados;

    /**
     * @var boolean
     *
     * @ORM\Column(name="calcularSueldoHsOt", type="boolean")
     */
    private $calcularSueldoHsOt;


    public function __toString()
    {
        return $this->getNombre();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param  string       $nombre
     * @return EmpleadoTipo
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->empleados = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add empleados
     *
     * @param  \Sistema\RRHHBundle\Entity\Empleado $empleados
     * @return EmpleadoTipo
     */
    public function addEmpleado(\Sistema\RRHHBundle\Entity\Empleado $empleados)
    {
        $this->empleados[] = $empleados;

        return $this;
    }

    /**
     * Remove empleados
     *
     * @param \Sistema\RRHHBundle\Entity\Empleado $empleados
     */
    public function removeEmpleado(\Sistema\RRHHBundle\Entity\Empleado $empleados)
    {
        $this->empleados->removeElement($empleados);
    }

    /**
     * Get empleados
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmpleados()
    {
        return $this->empleados;
    }

    /**
     * Set calcularSueldoHsOt
     *
     * @param boolean $calcularSueldoHsOt
     * @return Caja
     */
    public function setCalcularSueldoHsOt($calcularSueldoHsOt)
    {
        $this->calcularSueldoHsOt = $calcularSueldoHsOt;
    
        return $this;
    }

    /**
     * Get calcularSueldoHsOt
     *
     * @return boolean 
     */
    public function getCalcularSueldoHsOt()
    {
        return $this->calcularSueldoHsOt;
    }
}
