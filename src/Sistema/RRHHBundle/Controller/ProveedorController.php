<?php

namespace Sistema\RRHHBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\RRHHBundle\Entity\Proveedor;
use Sistema\RRHHBundle\Form\ProveedorType;
use Sistema\RRHHBundle\Form\ProveedorFilterType;
use Sistema\FACTURACIONBundle\Entity\CuentaCorriente;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Proveedor controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/proveedor")
 */
class ProveedorController extends Controller
{
    /**
     * Lists all Proveedor entities.
     *
     * @Route("/activos", name="admin_proveedor")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->get('security_role')->controlRolesUser();
        list($filterForm, $queryBuilder) = $this->filter("admin_proveedor");

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $queryBuilder, $this->get('request')->query->get('page', 1), (isset($this->container->parameters['knp_paginator.page_range'])) ? $this->container->parameters['knp_paginator.page_range'] : 10
        );

        return array(
            'entities' => $pagination,
            'filterForm' => $filterForm->createView(),
            'estado' => "activo",
        );
    }

    /**
     * Lists all Empleado entities.
     *
     * @Route("/inactivos", name="admin_proveedor_inactivo")
     * @Method("GET")
     * @Template("SistemaRRHHBundle:Proveedor:index.html.twig")
     */
    public function indexInactivoAction()
    {
        $this->get('security_role')->controlRolesUser();
        list($filterForm, $queryBuilder) = $this->filter("admin_proveedor_inactivo");

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $queryBuilder, $this->get('request')->query->get('page', 1), 10
        );

        return array(
            'entities' => $pagination,
            'filterForm' => $filterForm->createView(),
            'estado' => "inactivo"
        );
    }

    /**
     * Process filter request.
     *
     * @return array
     */
    protected function filter($estado)
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        $filterForm = $this->createFilterForm(null, $estado);
        $em = $this->getDoctrine()->getManager();
        if ($estado == "admin_proveedor_inactivo") {
            $queryBuilder = $em->getRepository('SistemaRRHHBundle:Proveedor')
                    ->createQueryBuilder('a')
                    ->orderBy('a.id', 'DESC')
                    ->where('a.activo = false')
            ;
        } else {
            $queryBuilder = $em->getRepository('SistemaRRHHBundle:Proveedor')
                    ->createQueryBuilder('a')
                    ->orderBy('a.id', 'DESC')
                    ->where('a.activo = true')
            ;
        }

        // Bind values from the request
        $filterForm->handleRequest($request);
        // Reset filter
        if ($filterForm->get('reset')->isClicked()) {
            $session->remove($estado);
            $filterForm = $this->createFilterForm(null, $estado);
        }

        // Filter action
        if ($filterForm->get('filter')->isClicked()) {
            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set($estado, $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has($estado)) {
                $filterData = $session->get($estado);
                $filterForm = $this->createFilterForm($filterData, $estado);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }

    /**
     * Create filter form.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createFilterForm($filterData = null, $estado = null)
    {
        $form = $this->createForm(new ProveedorFilterType(), $filterData, array(
            'action' => $this->generateUrl($estado),
            'method' => 'GET',
        ));

        $form
                ->add('filter', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.index.filter',
                    'attr' => array('class' => 'btn btn-success col-lg-1 col-md-1 col-sm-1'),
                ))
                ->add('reset', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.index.reset',
                    'attr' => array('class' => 'btn btn-danger col-lg-1 col-lg-offset-1'),
                ))
        ;

        return $form;
    }

    /**
     * Creates a new Proveedor entity.
     *
     * @Route("/", name="admin_proveedor_create")
     * @Method("POST")
     * @Template("SistemaRRHHBundle:Proveedor:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $this->get('security_role')->controlRolesUser();
        $entity = new Proveedor();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            if ($entity->getEmpresa() != "" && $entity->getCuitcuil() != "") {
                $cuentaCorriente = new CuentaCorriente();
                $entity->setCuentaCorriente($cuentaCorriente);
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

                $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl('admin_proveedor_new') : $this->generateUrl('admin_proveedor_show', array('id' => $entity->getId()));

                return $this->redirect($nextAction);
            }
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Proveedor entity.
     *
     * @param Proveedor $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Proveedor $entity)
    {
        $form = $this->createForm(new ProveedorType(), $entity, array(
            'action' => $this->generateUrl('admin_proveedor_create'),
            'method' => 'POST',
        ));

        $form
                ->add(
                        'save', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.save',
                    'attr' => array('class' => 'btn btn-success col-lg-2 col-md-2 col-sm-2')
                        )
                )
                ->add(
                        'saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array('class' => 'btn btn-primary col-lg-2 col-md-2 col-sm-2 col-lg-offset-1 col-md-offset-1 col-sm-offset-1')
                        )
                )
        ;

        return $form;
    }

    /**
     * Displays a form to create a new Proveedor entity.
     *
     * @Route("/new", name="admin_proveedor_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->get('security_role')->controlRolesUser();
        $entity = new Proveedor();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Proveedor entity.
     *
     * @Route("/{id}", name="admin_proveedor_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaRRHHBundle:Proveedor')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Proveedor entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Proveedor entity.
     *
     * @Route("/{id}/edit", name="admin_proveedor_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaRRHHBundle:Proveedor')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Proveedor entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Proveedor entity.
     *
     * @param Proveedor $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Proveedor $entity)
    {
        $form = $this->createForm(new ProveedorType(), $entity, array(
            'action' => $this->generateUrl('admin_proveedor_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form
                ->add(
                        'save', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.save',
                    'attr' => array('class' => 'btn btn-success col-lg-2 col-md-2 col-sm-2')
                        )
                )
                ->add(
                        'saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array('class' => 'btn btn-primary col-lg-2 col-md-2 col-sm-2 col-lg-offset-1 col-md-offset-1 col-sm-offset-1')
                        )
                )
        ;

        return $form;
    }

    /**
     * Edits an existing Proveedor entity.
     *
     * @Route("/{id}", name="admin_proveedor_update")
     * @Method("PUT")
     * @Template("SistemaRRHHBundle:Proveedor:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $newTelefono = array();
        $newDomicilio = array();

        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaRRHHBundle:Proveedor')->findProveedor($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Proveedor entity.');
        }

        $telefonos = $entity->getTelefonos();
        $originalTelefono = $telefonos->getSnapshot();

        $domicilios = $entity->getDomicilios();
        $originalDomicilio = $domicilios->getSnapshot();

        $all = $request->request->all();

        if (isset($all['sistema_rrhhbundle_proveedor']['telefonos'])) {
            $newTelefono = $all['sistema_rrhhbundle_proveedor']['telefonos'];
        }

        if (isset($all['sistema_rrhhbundle_proveedor']['domicilios'])) {
            $newDomicilio = $all['sistema_rrhhbundle_proveedor']['domicilios'];
        }
        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            foreach ($originalTelefono as $key => $t) {
                if (!array_key_exists($key, $newTelefono)) {
                    $em->remove($t);
                }
            }
            foreach ($originalDomicilio as $key => $d) {
                if (!array_key_exists($key, $newDomicilio)) {
                    $em->remove($d);
                }
            }
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked() ? $this->generateUrl('admin_proveedor_new') : $this->generateUrl('admin_proveedor_show', array('id' => $id));

            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Proveedor entity.
     *
     * @Route("/{id}", name="admin_proveedor_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $this->get('security_role')->controlRolesUser();
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SistemaRRHHBundle:Proveedor')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Proveedor entity.');
            }

            $entity->setActivo(false);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'Proveedor desactivado satifactoriamente');
        }

        return $this->redirect($this->generateUrl('admin_proveedor'));
    }

    /**
     * Creates a form to delete a Proveedor entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        $mensaje = $this->get('translator')->trans('rrhh.mensaje.desactivar', array(), 'MWSimpleCrudGeneratorBundle');
        $onclick = 'return confirm("' . $mensaje . '");';

        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('admin_proveedor_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array(
                            'translation_domain' => 'SistemaRRHHBundle',
                            'label' => 'rrhh.boton.desactivar',
                            'attr' => array(
                                'class' => 'btn btn-danger col-lg-11',
                                'onclick' => $onclick,
                            )
                        ))
                        ->getForm()
        ;
    }
    
     /**
     * @Route("/autocomplete-forms/get-proveedor", name="autocomplete_get_proveedor")
     */
    public function getOrdenOrderDescAction(Request $request)
    {
        $term = $request->query->get('q', null);

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SistemaRRHHBundle:Proveedor')->likeNombre($term);

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

}
