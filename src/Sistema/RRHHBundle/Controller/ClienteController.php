<?php

namespace Sistema\RRHHBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\RRHHBundle\Entity\Cliente;
use Sistema\RRHHBundle\Entity\Domicilio;
use Sistema\FACTURACIONBundle\Entity\CuentaCorriente;
use Sistema\RRHHBundle\Form\ClienteType;
use Sistema\RRHHBundle\Form\ClienteFilterType;

/**
 * Cliente controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/cliente")
 */
class ClienteController extends Controller
{
    /**
     * Lists all Cliente entities.
     *
     * @Route("/activos", name="admin_cliente")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->get('security_role')->controlRolesUser();
        list($filterForm, $queryBuilder) = $this->filter("admin_cliente");

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder, $this->get('request')->query->get('page', 1), (isset($this->container->parameters['knp_paginator.page_range'])) ? $this->container->parameters['knp_paginator.page_range'] : 10
        );

        return array(
            'entities'   => $pagination,
            'filterForm' => $filterForm->createView(),
            'estado'     => "activo"
        );
    }

    /**
     * Lists all Cliente entities.
     *
     * @Route("/inactivos", name="admin_cliente_inactivo")
     * @Method("GET")
     * @Template("SistemaRRHHBundle:Cliente:index.html.twig")
     */
    public function indexInactivosAction()
    {
        $this->get('security_role')->controlRolesUser();
        list($filterForm, $queryBuilder) = $this->filter("admin_cliente_inactivo");
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder, $this->get('request')->query->get('page', 1), 10
        );

        return array(
            'entities'   => $pagination,
            'filterForm' => $filterForm->createView(),
            'estado'     => "inactivo"
        );
    }

    /**
     * Process filter request.
     *
     * @return array
     */
    protected function filter($estado)
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        $filterForm = $this->createFilterForm(null, $estado);
        $em = $this->getDoctrine()->getManager();

        if ($estado == "admin_cliente_inactivo") {
            $queryBuilder = $em->getRepository('SistemaRRHHBundle:Cliente')
                    ->createQueryBuilder('a')
                    ->where('a.activo = false')
                    ->orderBy('a.apellido', 'ASC')
            ;
        } else {
            $queryBuilder = $em->getRepository('SistemaRRHHBundle:Cliente')
                    ->createQueryBuilder('a')
                    ->where('a.activo = true')
                    ->orderBy('a.apellido', 'ASC')
            ;
        }
        // Bind values from the request
        $filterForm->handleRequest($request);

        // Reset filter
        if ($filterForm->get('reset')->isClicked()) {
            $session->remove($estado);
            $filterForm = $this->createFilterForm(null, $estado);
        }

        // Filter action
        if ($filterForm->get('filter')->isClicked()) {
            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set($estado, $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has($estado)) {
                $filterData = $session->get($estado);
                $filterForm = $this->createFilterForm($filterData, $estado);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }

    /**
     * Create filter form.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createFilterForm($filterData = null, $estado = null)
    {
        $form = $this->createForm(new ClienteFilterType(), $filterData, array(
            'action' => $this->generateUrl($estado),
            'method' => 'GET',
        ));

        $form
                ->add('filter', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.index.filter',
                    'attr' => array('class' => 'btn btn-success col-lg-1 col-md-1 col-sm-1'),
                ))
                ->add('reset', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.index.reset',
                    'attr' => array('class' => 'btn btn-danger col-lg-1 col-md-1 col-sm-1 col-lg-offset-1 col-md-offset-1 col-sm-offset-1'),
                ))
        ;

        return $form;
    }

    /**
     * Creates a new Cliente entity.
     *
     * @Route("/", name="admin_cliente_create")
     * @Method("POST")
     * @Template("SistemaRRHHBundle:Cliente:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $this->get('security_role')->controlRolesUser();
        $entity = new Cliente();

        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            if ($entity->getDomicilios()->isEmpty()) {
                $this->get('session')->getFlashBag()
                        ->add('danger', 'El Cliente Debe Poseer al Menos Un Domicilio');
            } elseif (is_null($entity->getApellido()) && is_null($entity->getNombre()) && is_null($entity->getEmpresa())) {
                $this->get('session')->getFlashBag()
                        ->add('danger', 'Al menos uno de los campos Nombre, Apellido o Razon Social no debe estar en blanco');
            } else {
                $cuentaCorriente = new CuentaCorriente();
                $entity->setCuentaCorriente($cuentaCorriente);
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

                $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl('admin_cliente_new') : $this->generateUrl('admin_cliente_show', array('id' => $entity->getId()));

                return $this->redirect($nextAction);
            }
        } else {
            $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Cliente entity.
     *
     * @param Cliente $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Cliente $entity)
    {
        $form = $this->createForm(new ClienteType(), $entity, array(
            'action' => $this->generateUrl('admin_cliente_create'),
            'method' => 'POST',
        ));

        $form
                ->add(
                        'save', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.save',
                    'attr' => array('class' => 'btn btn-success col-lg-2 col-md-2 col-sm-2')
                        )
                )
                ->add(
                        'saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array('class' => 'btn btn-primary col-lg-2 col-md-2 col-sm-2 col-lg-offset-1 col-md-offset-1 col-sm-offset-1')
                        )
                )
        ;

        return $form;
    }

    /**
     * Displays a form to create a new Cliente entity.
     *
     * @Route("/new", name="admin_cliente_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->get('security_role')->controlRolesUser();
        $entity = new Cliente();
        $domicilio = new Domicilio();
        $entity->addDomicilio($domicilio);

        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Cliente entity.
     *
     * @Route("/{id}", name="admin_cliente_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaRRHHBundle:Cliente')->findCliente($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cliente entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Cliente entity.
     *
     * @Route("/{id}/edit", name="admin_cliente_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaRRHHBundle:Cliente')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cliente entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Cliente entity.
     *
     * @param Cliente $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Cliente $entity)
    {
        $form = $this->createForm(new ClienteType(), $entity, array(
            'action' => $this->generateUrl('admin_cliente_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form
                ->add(
                        'save', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.save',
                    'attr' => array('class' => 'btn btn-success col-lg-2 col-md-2 col-sm-2')
                        )
                )
                ->add(
                        'saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array('class' => 'btn btn-primary col-lg-2 col-md-2 col-sm-2 col-lg-offset-1 col-md-offset-1 col-sm-offset-1')
                        )
                )
        ;

        return $form;
    }

    /**
     * Edits an existing Cliente entity.
     *
     * @Route("/{id}", name="admin_cliente_update")
     * @Method("PUT")
     * @Template("SistemaRRHHBundle:Cliente:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $newTelefono = array();
        $newDomicilio = array();

        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaRRHHBundle:Cliente')->findCliente($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cliente entity.');
        }

        $telefonos = $entity->getTelefonos();
        $originalTelefono = $telefonos->getSnapshot();

        $domicilios = $entity->getDomicilios();
        $originalDomicilio = $domicilios->getSnapshot();

        $all = $request->request->all();

        if (isset($all['sistema_rrhhbundle_cliente']['telefonos'])) {
            $newTelefono = $all['sistema_rrhhbundle_cliente']['telefonos'];
        }

        if (isset($all['sistema_rrhhbundle_cliente']['domicilios'])) {
            $newDomicilio = $all['sistema_rrhhbundle_cliente']['domicilios'];
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            if ($entity->getDomicilios()->isEmpty()) {
                $this->get('session')->getFlashBag()->add(
                        'danger', 'El Cliente Debe Poseer al Menos Un Domicilio'
                );

                return $this->redirect($this->generateUrl('admin_cliente_edit', array('id' => $id)));
            }

            foreach ($originalTelefono as $key => $t) {
                if (!array_key_exists($key, $newTelefono)) {
                    $em->remove($t);
                }
            }

            foreach ($originalDomicilio as $key => $d) {
                if (!array_key_exists($key, $newDomicilio)) {
                    $em->remove($d);
                }
            }
            if (empty($newDomicilio)) {
                $this->get('session')->getFlashBag()->add(
                        'danger', 'El Cliente Debe Poseer al Menos Un Domicilio'
                );

                return $this->redirect($this->generateUrl('admin_cliente_edit', array('id' => $id)));
            }

            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked() ? $this->generateUrl('admin_cliente_new') : $this->generateUrl('admin_cliente_show', array('id' => $id));

            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Cliente entity.
     *
     * @Route("/{id}", name="admin_cliente_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $this->get('security_role')->controlRolesUser();
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            // $entity = $em->getRepository('SistemaRRHHBundle:Cliente')->find($id);
            $entity = $em->getRepository('SistemaRRHHBundle:Cliente')->DesactivarCliente($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Cliente entity.');
            }

            // $em->remove($entity);
            // $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.delete.success');
        }

        return $this->redirect($this->generateUrl('admin_cliente'));
    }

    /**
     * Creates a form to delete a Cliente entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        $mensaje = $this->get('translator')->trans('rrhh.mensaje.desactivar', array(), 'SistemaRRHHBundle');
        $onclick = 'return confirm("' . $mensaje . '");';

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_cliente_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array(
                'translation_domain' => 'SistemaRRHHBundle',
                'label' => 'rrhh.boton.desactivar',
                'attr' => array(
                    'class' => 'btn btn-danger col-lg-11',
                    'onclick' => $onclick,
                )
            ))
            ->getForm()
        ;
    }

    /**
     * @Route("/autocomplete-forms/get-clientes", name="autocomplete_get_clientes")
     */
    public function getClientesAction(Request $request)
    {
        $term = $request->query->get('q', null);

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SistemaRRHHBundle:Cliente')->likeNombre($term);

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id'   => $entity->getId(),
                'text' => $entity->__toString(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }
}