<?php

namespace Sistema\RRHHBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\RRHHBundle\Entity\EmpleadoTipo;
use Sistema\RRHHBundle\Form\EmpleadoTipoType;
use Sistema\RRHHBundle\Form\EmpleadoTipoFilterType;

/**
 * EmpleadoTipo controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/empleadotipo")
 */
class EmpleadoTipoController extends Controller
{

    /**
     * Lists all EmpleadoTipo entities.
     *
     * @Route("/", name="admin_empleadotipo")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->get('security_role')->controlRolesUser();
        list($filterForm, $queryBuilder) = $this->filter();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder,
            $this->get('request')->query->get('page', 1),
            (isset($this->container->parameters['knp_paginator.page_range'])) ? $this->container->parameters['knp_paginator.page_range'] : 10
        );

        return array(
            'entities'   => $pagination,
            'filterForm' => $filterForm->createView(),
        );
    }

    /**
    * Process filter request.
    *
    * @return array
    */
    protected function filter()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        $filterForm = $this->createFilterForm();
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository('SistemaRRHHBundle:EmpleadoTipo')
            ->createQueryBuilder('a')
            ->orderBy('a.id', 'DESC')
        ;
        // Bind values from the request
        $filterForm->handleRequest($request);
        // Reset filter
        if ($filterForm->get('reset')->isClicked()) {
            $session->remove('EmpleadoTipoControllerFilter');
            $filterForm = $this->createFilterForm();
        }

        // Filter action
        if ($filterForm->get('filter')->isClicked()) {
            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('EmpleadoTipoControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('EmpleadoTipoControllerFilter')) {
                $filterData = $session->get('EmpleadoTipoControllerFilter');
                $filterForm = $this->createFilterForm($filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }
    /**
    * Create filter form.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createFilterForm($filterData = null)
    {
        $form = $this->createForm(new EmpleadoTipoFilterType(), $filterData, array(
            'action' => $this->generateUrl('admin_empleadotipo'),
            'method' => 'GET',
        ));

        $form
            ->add('filter', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label'              => 'views.index.filter',
                'attr'               => array('class' => 'btn btn-success col-lg-1'),
            ))
            ->add('reset', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label'              => 'views.index.reset',
                'attr'               => array('class' => 'btn btn-danger col-lg-1 col-lg-offset-1'),
            ))
        ;

        return $form;
    }
    /**
     * Creates a new EmpleadoTipo entity.
     *
     * @Route("/", name="admin_empleadotipo_create")
     * @Method("POST")
     * @Template("SistemaRRHHBundle:EmpleadoTipo:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $this->get('security_role')->controlRolesUser();
        $entity = new EmpleadoTipo();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked()
                    ? $this->generateUrl('admin_empleadotipo_new')
                    : $this->generateUrl('admin_empleadotipo_show', array('id' => $entity->getId()));

            return $this->redirect($nextAction);

        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a EmpleadoTipo entity.
    *
    * @param EmpleadoTipo $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(EmpleadoTipo $entity)
    {
        $form = $this->createForm(new EmpleadoTipoType(), $entity, array(
            'action' => $this->generateUrl('admin_empleadotipo_create'),
            'method' => 'POST',
        ));

        $form
            ->add(
                'save', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label'              => 'views.new.save',
                'attr'               => array('class' => 'btn btn-success col-lg-2')
                )
            )
            ->add(
                'saveAndAdd', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label'              => 'views.new.saveAndAdd',
                'attr'               => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
                )
            )
        ;

        return $form;
    }

    /**
     * Displays a form to create a new EmpleadoTipo entity.
     *
     * @Route("/new", name="admin_empleadotipo_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->get('security_role')->controlRolesUser();
        $entity = new EmpleadoTipo();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a EmpleadoTipo entity.
     *
     * @Route("/{id}", name="admin_empleadotipo_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaRRHHBundle:EmpleadoTipo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EmpleadoTipo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing EmpleadoTipo entity.
     *
     * @Route("/{id}/edit", name="admin_empleadotipo_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaRRHHBundle:EmpleadoTipo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EmpleadoTipo entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a EmpleadoTipo entity.
    *
    * @param EmpleadoTipo $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(EmpleadoTipo $entity)
    {
        $form = $this->createForm(new EmpleadoTipoType(), $entity, array(
            'action' => $this->generateUrl('admin_empleadotipo_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form
            ->add(
                'save', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label'              => 'views.new.save',
                'attr'               => array('class' => 'btn btn-success col-lg-2')
                )
            )
            ->add(
                'saveAndAdd', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label'              => 'views.new.saveAndAdd',
                'attr'               => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
                )
            )
        ;

        return $form;
    }
    /**
     * Edits an existing EmpleadoTipo entity.
     *
     * @Route("/{id}", name="admin_empleadotipo_update")
     * @Method("PUT")
     * @Template("SistemaRRHHBundle:EmpleadoTipo:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaRRHHBundle:EmpleadoTipo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EmpleadoTipo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked()
                        ? $this->generateUrl('admin_empleadotipo_new')
                        : $this->generateUrl('admin_empleadotipo_show', array('id' => $id));

            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a EmpleadoTipo entity.
     *
     * @Route("/{id}", name="admin_empleadotipo_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $this->get('security_role')->controlRolesUser();
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SistemaRRHHBundle:EmpleadoTipo')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find EmpleadoTipo entity.');
            }
            try {
                $em->remove($entity);
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', 'flash.delete.success');
            } catch (\Exception $e) {
                $this->get('session')->getFlashBag()->add('danger', 'No se puede eliminar porque esta siendo utilizado');
            }
        }

        return $this->redirect($this->generateUrl('admin_empleadotipo'));
    }

    /**
     * Creates a form to delete a EmpleadoTipo entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        $mensaje = $this->get('translator')->trans('views.recordactions.confirm', array(), 'MWSimpleCrudGeneratorBundle');
        $onclick = 'return confirm("'.$mensaje.'");';

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_empleadotipo_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label'              => 'views.recordactions.delete',
                'attr'               => array(
                    'class'   => 'btn btn-danger col-lg-11',
                    'onclick' => $onclick,
                )
            ))
            ->getForm()
        ;
    }
}
