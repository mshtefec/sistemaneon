<?php

namespace Sistema\PlanDeCuentasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * ProcesoType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class ProcesoType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nombre')
                ->add('activo', null, array(
                    'required' => false
                ))
                /*->add('tipoComprobante', 'choice', array(
                    'label' => 'Tipo de cuenta',
                    'choices' => array(0 => 'Factura Venta', 1 => 'Factura Compra'),
                    'preferred_choices' => array('Factura Venta'),
                ))*/
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\PlanDeCuentasBundle\Entity\Proceso'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_plandecuentasbundle_proceso';
    }

}
