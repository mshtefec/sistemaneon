<?php

namespace Sistema\PlanDeCuentasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * CuentaType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class CuentaType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('cuenta', null, array(
                    'label' => 'Código',
                ))
                ->add('descripcion')
               /* ->add('tipo', 'choice', array(
                    'label' => 'Tipo de cuenta',
                    'choices' => array(0 => 'Debe', 1 => 'Haber'),
                    'preferred_choices' => array('Debe'),
                ))*/
                ->add('parent', 'select2', array(
                    'label' => 'Cuenta Padre',
                    'class' => 'Sistema\PlanDeCuentasBundle\Entity\Cuenta',
                    'url' => 'autocomplete_get_cuenta',
                    'required' => false,
                    //'read_only' => false,
                    'configs' => array(
                        'multiple' => false, //es requerido true o false
                        'width' => '100%',
                    ),
                    'attr' => array(
                        'class' => 'col-lg-12 col-md-12 col-sm-12 input-sm',
                        'style' => 'min-width: 300px;',
                    ),
                ))
                ->add('imputable', null, array(
                    'required' => false,
                ))
                ->add('permiteCheque', null, array(
                    'required' => false,
                ))
                ->add('cajaOrden', null, array(
                    'attr' => array(
                        'class' => 'cajaOrden',
                    ),
                    'label' => 'Orden para visualizar en caja',
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\PlanDeCuentasBundle\Entity\Cuenta'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_plandecuentasbundle_cuenta';
    }

}
