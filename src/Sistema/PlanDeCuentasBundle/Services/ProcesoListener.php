<?php

namespace Sistema\PlanDeCuentasBundle\Services;

use Symfony\Component\Security\Core\Event\AuthenticationEvent;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
// utilizo para crear la orden de salida desde la orden
use Sistema\STOCKBundle\Entity\OrdenSalida;
use Sistema\STOCKBundle\Entity\CantOrdenSalida;
// utilizo para crear el asiento de la orden
use Sistema\FACTURACIONBundle\Entity\Asiento;

class ProcesoListener {
    /**
     * @var string
     */
    protected $em;

    /**
     * @param SecurityContext $securityContext
     * @param Router $router The router
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }

    public function create($entity, $proceso) {
        $DEBE = 0;
        $HABER = 1;
        $entityProceso = $this->em->getRepository('SistemaPlanDeCuentasBundle:Proceso')->find($proceso);

        foreach ($entityProceso->getCuentas() as $cuenta) {
            $asiento = new Asiento();
            $asiento->setCuenta($cuenta);
            $asiento->setFecha($entity->getFecha());
            $entity->addAsiento($asiento);
        }
        return $entityProceso;
    }
}