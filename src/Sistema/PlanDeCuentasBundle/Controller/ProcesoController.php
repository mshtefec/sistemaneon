<?php

namespace Sistema\PlanDeCuentasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\PlanDeCuentasBundle\Entity\Proceso;
use Sistema\PlanDeCuentasBundle\Form\ProcesoType;
use Sistema\PlanDeCuentasBundle\Form\ProcesoFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Proceso controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/proceso")
 */
class ProcesoController extends Controller {

    /**
     * Lists all Proceso entities.
     *
     * @Route("/", name="admin_Proceso")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        list($filterForm, $queryBuilder) = $this->filter();
 
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $queryBuilder, $this->get('request')->query->get('page', 1), (isset($this->container->parameters['knp_paginator.page_range'])) ? $this->container->parameters['knp_paginator.page_range'] : 10
        );

        return array(
            'entities' => $pagination,
            'filterForm' => $filterForm->createView(),
        );
    }

    /**
     * Process filter request.
     *
     * @return array
     */
    protected function filter() {
        $request = $this->getRequest();
        $session = $request->getSession();
        $filterForm = $this->createFilterForm();
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository('SistemaPlanDeCuentasBundle:Proceso')
                ->createQueryBuilder('a')
                ->orderBy('a.id', 'DESC')
        ;
        // Bind values from the request
        $filterForm->handleRequest($request);
        // Reset filter
        if ($filterForm->get('reset')->isClicked()) {
            $session->remove('ProcesoControllerFilter');
            $filterForm = $this->createFilterForm();
        }

        // Filter action
        if ($filterForm->get('filter')->isClicked()) {
            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('ProcesoControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('ProcesoControllerFilter')) {
                $filterData = $session->get('ProcesoControllerFilter');
                $filterForm = $this->createFilterForm($filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }

    /**
     * Create filter form.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createFilterForm($filterData = null) {
        $form = $this->createForm(new ProcesoFilterType(), $filterData, array(
            'action' => $this->generateUrl('admin_Proceso'),
            'method' => 'GET',
        ));

        $form
                ->add('filter', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.index.filter',
                    'attr' => array('class' => 'btn btn-success col-lg-1'),
                ))
                ->add('reset', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.index.reset',
                    'attr' => array('class' => 'btn btn-danger col-lg-1 col-lg-offset-1'),
                ))
        ;

        return $form;
    }

    /**
     * Creates a new Proceso entity.
     *
     * @Route("/", name="admin_Proceso_create")
     * @Method("POST")
     * @Template("SistemaPlanDeCuentasBundle:Proceso:new.html.twig")
     */
    public function createAction(Request $request) {
        $entity = new Proceso();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        if (isset($request->request->all()["listaCuentas"])) {
            $listaCuentas = $request->request->all()["listaCuentas"];
        }

        $em = $this->getDoctrine()->getManager();
        if ($form->isValid()) {
            if (isset($listaCuentas)) {
                $cuentas = $em->getRepository('SistemaPlanDeCuentasBundle:Cuenta')->getCuentasInmutablesByArray($listaCuentas);

                foreach ($cuentas as $cuenta) {
                    $entity->addCuenta($cuenta);
                }
            }
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl('admin_Proceso_new') : $this->generateUrl('admin_Proceso_show', array('id' => $entity->getId()));
            return $this->redirect($nextAction);
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Proceso entity.
     *
     * @param Proceso $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Proceso $entity) {
        $form = $this->createForm(new ProcesoType(), $entity, array(
            'action' => $this->generateUrl('admin_Proceso_create'),
            'method' => 'POST',
        ));

        $form
                ->add(
                        'save', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.save',
                    'attr' => array('class' => 'btn btn-success col-lg-2')
                        )
                )
                ->add(
                        'saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
                        )
                )
        ;

        return $form;
    }

    /**
     * Displays a form to create a new Proceso entity.
     *
     * @Route("/new", name="admin_Proceso_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $entity = new Proceso();
        $form = $this->createCreateForm($entity);
        $em = $this->getDoctrine()->getManager();
        $cuentas = $em->getRepository('SistemaPlanDeCuentasBundle:Cuenta')->getCuentasImputables();
        
        return array(
            'entity' => $entity,
            'form' => $form->createView(),
            'cuentas' => $cuentas
        );
    }

    /**
     * Finds and displays a Proceso entity.
     *
     * @Route("/{id}", name="admin_Proceso_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaPlanDeCuentasBundle:Proceso')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Proceso entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Proceso entity.
     *
     * @Route("/{id}/edit", name="admin_Proceso_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaPlanDeCuentasBundle:Proceso')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Proceso entity.');
        }
        $listaCuentas = array();

        foreach ($entity->getCuentas()->getValues() as $cta) {
            $listaCuentas[] = $cta->getId();
        }
        if (empty($listaCuentas)) {
            $cuentas = $em->getRepository('SistemaPlanDeCuentasBundle:Cuenta')->getCuentasImputables();
        } else {
            $cuentas = $em->getRepository('SistemaPlanDeCuentasBundle:Cuenta')->getCuentasByNotInArray($listaCuentas);
        }

        $cuentasSeleccionadas = $em->getRepository('SistemaPlanDeCuentasBundle:Cuenta')->findByProceso($id);
        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'cuentas' => $cuentas,
            'cuentasSeleccionadas' => $cuentasSeleccionadas
        );
    }

    /**
     * Creates a form to edit a Proceso entity.
     *
     * @param Proceso $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Proceso $entity) {
        $form = $this->createForm(new ProcesoType(), $entity, array(
            'action' => $this->generateUrl('admin_Proceso_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form
                ->add(
                        'save', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.save',
                    'attr' => array('class' => 'btn btn-success col-lg-2')
                        )
                )
                ->add(
                        'saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
                        )
                )
        ;

        return $form;
    }

    /**
     * Edits an existing Proceso entity.
     *
     * @Route("/{id}", name="admin_Proceso_update")
     * @Method("PUT")
     * @Template("SistemaPlanDeCuentasBundle:Proceso:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaPlanDeCuentasBundle:Proceso')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Proceso entity.');
        }

        //backup de cuentas
        $listaCuentaBackup = array();
        if (count($entity->getCuentas()->getValues()) > 0) {
            foreach ($entity->getCuentas() as $cuenta) {
                $listaCuentaBackup[] = $cuenta->getId();
            }
        }

        if (isset($request->request->all()["listaCuentas"])) {
            $listaCuentas = $request->request->all()["listaCuentas"];
        }
        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            if (isset($listaCuentas)) {
                //busco las cuentas que selecciono
                $cuentas = $em->getRepository('SistemaPlanDeCuentasBundle:Cuenta')->getCuentasInmutablesByArray($listaCuentas);
                foreach ($cuentas as $cuenta) {
                    //busca si ya tenia la cuenta asociada
                    $clave = array_search($cuenta->getId(), $listaCuentaBackup);
                    if ($clave !== false) {
                        unset($listaCuentaBackup[$clave]);
                    } else {
                        $entity->addCuenta($cuenta);
                    }
                }
            }

            if (count($listaCuentaBackup) > 0) {
                foreach ($listaCuentaBackup as $cuenta) {
                    foreach ($entity->getCuentas() as $cuentaGuar) {
                        if ($cuenta == $cuentaGuar->getId()) {
                            $entity->removeCuenta($cuentaGuar);
                        }
                    }
                }
            }

            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked() ? $this->generateUrl('admin_Proceso_new') : $this->generateUrl('admin_Proceso_show', array('id' => $id));
            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Proceso entity.
     *
     * @Route("/{id}", name="admin_Proceso_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SistemaPlanDeCuentasBundle:Proceso')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Proceso entity.');
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.delete.success');
        }

        return $this->redirect($this->generateUrl('admin_Proceso'));
    }

    /**
     * Creates a form to delete a Proceso entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        $mensaje = $this->get('translator')->trans('views.recordactions.confirm', array(), 'MWSimpleCrudGeneratorBundle');
        $onclick = 'return confirm("' . $mensaje . '");';
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('admin_Proceso_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array(
                            'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                            'label' => 'views.recordactions.delete',
                            'attr' => array(
                                'class' => 'btn btn-danger col-lg-11',
                                'onclick' => $onclick,
                            )
                        ))
                        ->getForm()
        ;
    }

    /**
     * @Route("/proceso-autocomplete/", name="admin_proceso_autocomplete", options={"expose"=true})
     * @Method("GET")
     */
    public function getProcesoAutocompleteAction() {
        $request = $this->getRequest();
        $term = $request->query->get('q', null);

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SistemaPlanDeCuentasBundle:Proceso')->getProcesoActivosByNombre($term);

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString()
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

}
