<?php

namespace Sistema\PlanDeCuentasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Proceso
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\PlanDeCuentasBundle\Entity\ProcesoRepository")
 */
class Proceso {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var integer
     *
     * @ORM\ManyToMany(targetEntity="Cuenta", inversedBy="procesos")
     * @ORM\JoinTable(name="procesos_cuentas")
     */
    private $cuentas;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\FACTURACIONBundle\Entity\Comprobante", mappedBy="proceso")
     */
    private $comprobantes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activo", type="boolean")
     */
    private $activo;


    public function __toString() {
        return $this->getNombre();
    }
    
    /**
     * Constructor
     */
    public function __construct() {
        $this->cuentas      = new \Doctrine\Common\Collections\ArrayCollection();
        $this->comprobantes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Proceso
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre() {
        return $this->nombre;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return Proceso
     */
    public function setActivo($activo) {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo() {
        return $this->activo;
    }

   

    /**
     * Add cuentas
     *
     * @param \Sistema\PlanDeCuentasBundle\Entity\Cuenta $cuentas
     * @return Proceso
     */
    public function addCuenta(\Sistema\PlanDeCuentasBundle\Entity\Cuenta $cuentas) {
        $this->cuentas[] = $cuentas;

        return $this;
    }

    /**
     * Remove cuentas
     *
     * @param \Sistema\PlanDeCuentasBundle\Entity\Cuenta $cuentas
     */
    public function removeCuenta(\Sistema\PlanDeCuentasBundle\Entity\Cuenta $cuentas) {
        $this->cuentas->removeElement($cuentas);
    }

    /**
     * Get cuentas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCuentas() {
        return $this->cuentas;
    }

    /**
     * Add comprobantes
     *
     * @param \Sistema\FACTURACIONBundle\Entity\Comprobante $comprobantes
     * @return Proceso
     */
    public function addComprobante(\Sistema\FACTURACIONBundle\Entity\Comprobante $comprobantes)
    {
        $comprobantes->setProceso($this);
        $this->comprobantes[] = $comprobantes;
    
        return $this;
    }

    /**
     * Remove comprobantes
     *
     * @param \Sistema\FACTURACIONBundle\Entity\Comprobante $comprobantes
     */
    public function removeComprobante(\Sistema\FACTURACIONBundle\Entity\Comprobante $comprobantes)
    {
        $this->comprobantes->removeElement($comprobantes);
    }

    /**
     * Get comprobantes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getComprobantes()
    {
        return $this->comprobantes;
    }
}