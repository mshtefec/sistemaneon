<?php

namespace Sistema\PlanDeCuentasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cuenta
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\PlanDeCuentasBundle\Entity\CuentaRepository")
 */
class Cuenta {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cuenta", type="string", length=255)
     */
    private $cuenta;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion;

    /**
     * @ORM\ManyToMany(targetEntity="Proceso", mappedBy="cuentas")
     * */
    private $procesos;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\FACTURACIONBundle\Entity\Asiento", mappedBy="cuenta")
     * */
    private $asientos;

    /**
     * @ORM\OneToMany(targetEntity="Cuenta", mappedBy="parent")
     * */
    private $children;

    /**
     * @ORM\ManyToOne(targetEntity="Cuenta", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     * */
    private $parent;

    /**
     * @var boolean
     *
     * @ORM\Column(name="imputable", type="boolean")
     */
    protected $imputable;

    /**
     * @var integer
     *
     * @ORM\Column(name="cajaOrden", type="integer", nullable=true)
     */
    protected $cajaOrden;

    /**
     * @var boolean
     *
     * @ORM\Column(name="permiteCheque", type="boolean")
     */
    protected $permiteCheque;

    public function __toString() {
        return $this->getDescripcion();
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->cajaOrden = 0;
        $this->procesos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->asientos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set cuenta
     *
     * @param string $cuenta
     * @return Cuenta
     */
    public function setCuenta($cuenta) {
        $this->cuenta = $cuenta;

        return $this;
    }

    /**
     * Get cuenta
     *
     * @return string 
     */
    public function getCuenta() {
        return $this->cuenta;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Cuenta
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Add procesos
     *
     * @param \Sistema\PlanDeCuentasBundle\Entity\Proceso $procesos
     * @return Cuenta
     */
    public function addProceso(\Sistema\PlanDeCuentasBundle\Entity\Proceso $procesos) {
        $this->procesos[] = $procesos;

        return $this;
    }

    /**
     * Remove procesos
     *
     * @param \Sistema\PlanDeCuentasBundle\Entity\Proceso $procesos
     */
    public function removeProceso(\Sistema\PlanDeCuentasBundle\Entity\Proceso $procesos) {
        $this->procesos->removeElement($procesos);
    }

    /**
     * Get procesos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProcesos() {
        return $this->procesos;
    }

    /**
     * Add asientos
     *
     * @param \Sistema\FACTURACIONBundle\Entity\Asiento $asientos
     * @return Cuenta
     */
    public function addAsiento(\Sistema\FACTURACIONBundle\Entity\Asiento $asientos) {
        $asientos->setCuenta($this);
        $this->asientos[] = $asientos;

        return $this;
    }

    /**
     * Remove asientos
     *
     * @param \Sistema\FACTURACIONBundle\Entity\Asiento $asientos
     */
    public function removeAsiento(\Sistema\FACTURACIONBundle\Entity\Asiento $asientos) {
        $this->asientos->removeElement($asientos);
    }

    /**
     * Get asientos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAsientos() {
        return $this->asientos;
    }

    /**
     * Add children
     *
     * @param \Sistema\PlanDeCuentasBundle\Entity\Cuenta $children
     * @return Cuenta
     */
    public function addChildren(\Sistema\PlanDeCuentasBundle\Entity\Cuenta $children) {
        $this->children[] = $children;

        return $this;
    }

    /**
     * Remove children
     *
     * @param \Sistema\PlanDeCuentasBundle\Entity\Cuenta $children
     */
    public function removeChildren(\Sistema\PlanDeCuentasBundle\Entity\Cuenta $children) {
        $this->children->removeElement($children);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChildren() {
        return $this->children;
    }

    /**
     * Set parent
     *
     * @param \Sistema\PlanDeCuentasBundle\Entity\Cuenta $parent
     * @return Cuenta
     */
    public function setParent(\Sistema\PlanDeCuentasBundle\Entity\Cuenta $parent = null) {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Sistema\PlanDeCuentasBundle\Entity\Cuenta 
     */
    public function getParent() {
        return $this->parent;
    }

    /**
     * Set imputable
     *
     * @param boolean $imputable
     * @return Cuenta
     */
    public function setImputable($imputable) {
        $this->imputable = $imputable;

        return $this;
    }

    /**
     * Get imputable
     *
     * @return boolean 
     */
    public function getImputable() {
        return $this->imputable;
    }

    /**
     * Set cajaOrden
     *
     * @param integer $cajaOrden
     * @return cajaOrden
     */
    public function setCajaOrden($cajaOrden) {
        $this->cajaOrden = $cajaOrden;

        return $this;
    }

    /**
     * Get cajaOrden
     *
     * @return integer
     */
    public function getCajaOrden() {
        return $this->cajaOrden;
    }


    /**
     * Set permiteCheque
     *
     * @param boolean $permiteCheque
     * @return Cuenta
     */
    public function setPermiteCheque($permiteCheque)
    {
        $this->permiteCheque = $permiteCheque;
    
        return $this;
    }

    /**
     * Get permiteCheque
     *
     * @return boolean 
     */
    public function getPermiteCheque()
    {
        return $this->permiteCheque;
    }
}