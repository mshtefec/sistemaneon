$("#proceso").select2({
    ajax: {
        url: Routing.generate('admin_proceso_autocomplete'),
        dataType: 'json',
        data: function (term, page) {
            return {
                q: term, //search term
                page_limit: 20, // page size
            };
        },
        results: function (data, page) {
            return {results: data};
        }
    },
    width: '100%'
});
$(".btnNew").click(function () {
    if ($("#proceso").val() === "") {
        $("#contentModal").text("Seleccione un proceso");
        $("#contentModal").addClass("alert alert-info")
        $('#modalAlert').modal('show');
    } else {
        if (typeof ($(this).data('tipo')) === "undefined") {
            window.location = Routing.generate(url, {proceso: $("#proceso").val()});
        } else {
            window.location = Routing.generate(url, {tipo: $(this).data('tipo'), proceso: $("#proceso").val()});
        }
    }
});