<?php

namespace Sistema\AuditoriaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\AuditoriaBundle\Entity\LogEntry;
use Sistema\AuditoriaBundle\Form\LogEntryFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * LogEntry controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/logentry")
 */
class LogEntryController extends Controller {

    /**
     * Lists all LogEntry entities.
     *
     * @Route("/", name="admin_logentry")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        list($filterForm, $queryBuilder) = $this->filter();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder, $this->get('request')->query->get('page', 1), (isset($this->container->parameters['knp_paginator.page_range'])) ? $this->container->parameters['knp_paginator.page_range'] : 10
        );

        return array(
            'entities' => $pagination,
            'filterForm' => $filterForm->createView(),
        );
    }

    /**
     * Process filter request.
     *
     * @return array
     */
    protected function filter($id) {
        $request = $this->getRequest();
        $session = $request->getSession();
        $filterForm = $this->createFilterForm(null, $id);
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder
            ->select('a')
            ->from('GedmoLoggable:LogEntry', 'a')
            ->where('a.objectId = :id')
            ->orderBy('a.version', 'DESC')
            ->setParameter('id', $id)
        ;
        // Bind values from the request
        $filterForm->handleRequest($request);
        // Reset filter
        if ($filterForm->get('reset')->isClicked()) {
            $session->remove('LogEntryControllerFilter');
            $filterForm = $this->createFilterForm(null, $id);
        }

        // Filter action
        if ($filterForm->get('filter')->isClicked()) {
            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('LogEntryControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('LogEntryControllerFilter')) {
                $filterData = $session->get('LogEntryControllerFilter');
                $filterForm = $this->createFilterForm($filterData, $id);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }

    /**
     * Create filter form.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createFilterForm($filterData = null, $id) {
        $form = $this->createForm(new LogEntryFilterType(), $filterData, array(
            'action' => $this->generateUrl('admin_logentry_show', array(
                'id' => $id)),
            'method' => 'GET',
        ));

        $form
            ->add('filter', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label' => 'views.index.filter',
                'attr' => array('class' => 'btn btn-success col-lg-1'),
            ))
            ->add('reset', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label' => 'views.index.reset',
                'attr' => array('class' => 'btn btn-danger col-lg-1 col-lg-offset-1'),
            ))
        ;

        return $form;
    }

    /**
     * Finds and displays a LogEntry entity.
     *
     * @Route("/{id}", name="admin_logentry_show",options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $this->get('security_role')->controlRolesUser();
        $request = $this->getRequest();
        $id = (int) $id;
        $session = $request->getSession();
        $session->remove('LogEntryControllerFilter');
        list($filterForm, $queryBuilder) = $this->filter($id);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder, $request->query->get('page', 1), 10
        );

        return array(
            'entities' => $pagination,
            'filterForm' => $filterForm->createView()
        );
    }

    /**
     * Finds and displays a LogEntry entity.
     *
     * @Route("/ver/{entity}/{id}", name="admin_log_entity_ver")
     * @Method("GET")
     * @Template()
     */
    public function verAction($entity, $id) {
        $this->get('security_role')->controlRolesUser();
        
        $em = $this->getDoctrine()->getManager();
        $objectClass = "Sistema\STOCKBundle\Entity\\".$entity;

        $qb = $em->createQueryBuilder();
        $qb
            ->select('a')
            ->from('GedmoLoggable:LogEntry', 'a')
        ;
        if ($entity == "CantOrden") {
            $qb
                ->where('a.objectClass = :class')
                ->andWhere('a.data LIKE :data')
                ->setParameter('class', $objectClass)
                ->setParameter('data', '%s:5:"orden";a:1:{s:2:"id";i:'.$id.';}%')
            ;
        } else {
            $qb
                ->where('a.objectId = :id')
                ->andWhere('a.objectClass = :class')
                ->setParameter('id', $id)
                ->setParameter('class', $objectClass)
            ;
        }
        $qb->orderBy('a.version', 'DESC');
        $entities = $qb->getQuery()->getResult();
       
        return array(
            'entities' => $entities,
        );
    }

    /**
     * @Route("/get-auditoria", name="auditoria_get")
     * @Template()
     */
    public function getAuditoriaAction() {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $term = $request->request->get('id', null);

        $entity = $em->getRepository('SistemaSTOCKBundle:Articulo')->find($term);

        $logEntity = $em->getRepository('GedmoLoggable:LogEntry')->getLogEntries($entity);

        $logArray = array();
        $logRenglonArray = array();
        foreach ($logEntity as $LEntity) {
            $logRenglonArray['fechas'] = $LEntity->getLoggedAt()->format('d-m-Y h:i');
            $logRenglonArray['usuario'] = $LEntity->getUsername();
            $datosArray = array();

            foreach ($LEntity->getData() as $key => $logData) {
                $renglonDatosArray[] = array(
                    'key' => $key,
                    'dato' => $logData,
                );
                array_push($datosArray, $renglonDatosArray);
            }
            $logRenglonArray["datos"] = $datosArray;
            array_push($logArray, $logRenglonArray);
        }

        $response = new JsonResponse();
        $response->setData($logArray);

        return $response;
    }

    /**
     * @Route("/get-entity-by-id", name="admin_logentry_get_entity_byId")
     */
    public function getEntityByIdAction() {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $id = $request->request->get('_id', null);
        $nameEntity = $request->request->get('_entity', null);
        $array = array();

        if ($nameEntity == "rubro") {
            $entity = $em->getRepository('SistemaSTOCKBundle:Rubro')->find($id);
        } else if ($nameEntity == "cliente") {
            $entity = $em->getRepository('SistemaRRHHBundle:Cliente')->find($id);
        } else if ($nameEntity == "domicilio") {
            $entity = $em->getRepository('SistemaRRHHBundle:Domicilio')->find($id);
        } else if ($nameEntity == "presupuesto") {
            $entity = $em->getRepository('SistemaPresupuestoBundle:Presupuesto')->find($id);
        } else if ($nameEntity == "articulo") {
            $entity = $em->getRepository('SistemaSTOCKBundle:Articulo')->find($id);
        }

        $array[] = array(
            'entity' => $entity->__toString(),
        );

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }
}
