<?php

namespace Sistema\AuditoriaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;

/**
 * LogEntryFilterType filtro.
 * @author Nombre Apellido <name@gmail.com>
 */
class LogEntryFilterType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('loggedAt', 'filter_date_range', array(
                    'label' => 'Fecha',
                    'left_date_options' => array('label' => 'Desde', 'widget' => 'single_text', 'attr' => array('class' => 'col-lg-6 col-md-6 col-sm-6 col-xs-6 fecha') ),
                    'right_date_options' => array('label' => 'Hasta', 'widget' => 'single_text', 'attr' => array('class' => 'col-lg-6 col-md-6 col-sm-6 col-xs-6 fecha') ),
                ))
                ->add('username', 'filter_text', array(
                    'attr' => array('class' => 'form-control')
                ))
                ->add('action', 'filter_text', array(
                    'attr' => array('class' => 'form-control')
                ))
        ;

        $listener = function(FormEvent $event) {
            // Is data empty?
            foreach ((array) $event->getForm()->getData() as $data) {
                if (is_array($data)) {
                    foreach ($data as $subData) {
                        if (!empty($subData)) {
                            return;
                        }
                    }
                } else {
                    if (!empty($data)) {
                        return;
                    }
                }
            }
            $event->getForm()->addError(new FormError('Filter empty'));
        };
        $builder->addEventListener(FormEvents::POST_SUBMIT, $listener);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_auditoriabundle_logentryfiltertype';
    }

}
