<?php
namespace Sistema\PresupuestoBundle\Services;

class Funciones
{
    private $em;

    public function __construct($em)
    {
        $this->em = $em;
    }
    // /**
    //  * Query traer valores predeterminados.
    //  */
    // public function buscoValorPredeterminadoUnico()
    // {
    //     return $this->em->getRepository('SistemaPresupuestoBundle:Valor')->findOneByPredeterminado(true);
    // }
    // /**
    //  * Controlo si existen valores predeterminados y lo devuelvo.
    //  */
    // public function controlValorPredeterminadoUnico()
    // {
    //     $valores = $this->buscoValorPredeterminadoUnico();
    //     if (!$valores) {
    //         throw $this->createNotFoundException('No existe Tarea predeterminada.');
    //     }
    //     //Antes de return setea predeterminado en FALSE.
    //     $valores->setPredeterminado(false);

    //     return $valores;
    // }
    // /**
    //  * Si existe un valor predeterminado lo devuelvo.
    //  */
    // public function actualizarValorPredeterminadoUnico()
    // {
    //     $valores = $this->buscoValorPredeterminadoUnico();
    //     if ($valores) {
    //         $valores->setPredeterminado(false);
    //     }
    //     return $valores;
    // }
    /**
     * Convierte un time en float.
     */
    public function getFloatByTime($time) {
        $date = new \DateTime("01-01-1970 00:00");
        $time = new \DateTime("01-01-1970" . $time->format('H:i'));
        $timeInterval      = $date->diff($time);
        $intervalInSeconds = (new \DateTime())->setTimeStamp(0)->add($timeInterval)->getTimeStamp();
        $intervalInMinutes = $intervalInSeconds / 60;
        $totalIntervalo    = $intervalInMinutes / 60;

        return ((float) $totalIntervalo);
    }
}