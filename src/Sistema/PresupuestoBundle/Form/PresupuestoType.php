<?php

namespace Sistema\PresupuestoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Sistema\STOCKBundle\Form\EventListener\AddClienteSubscriber;
use Sistema\STOCKBundle\Form\EventListener\AddDomicilioSubscriber;

/**
 * PresupuestoType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class PresupuestoType extends AbstractType {

    private $entityManager;
    private $MANTENIMIENTO = 0;
    private $OBRA = 1;

    public function __construct($entityManager) {
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $funcion = $builder->getData()->getFuncion();

        if ($funcion == $this->OBRA || is_null($funcion)) {
            $type = new CantPresupuestoType($this->OBRA);
        } else {
            $type = new CantPresupuestoType($this->MANTENIMIENTO);
        }

        $builder
            ->add('funcion', 'hidden')
            ->add('numero', null, array(
                'label_attr' => array(
                    'class' => 'col-lg-12 col-md-12 col-sm-12',
                ),
                'attr' => array(
                    'class' => 'col-lg-12 col-md-12 col-sm-12',
                ),
            ))
            ->add('fecha', 'bootstrapdatetime', array(
                'label_attr' => array(
                    'class' => 'col-lg-12 col-md-12 col-sm-12',
                ),
                'attr' => array(
                    'class' => 'col-lg-12 col-md-12 col-sm-12',
                ),
                'read_only' => true,
                'data' => new \DateTime("today"),
            ))
            ->add('descripcion', null, array(
                'label_attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                ),
                'attr' => array(
                    'class' => 'col-lg-10 col-md-10 col-sm-10',
                    'rows' => '10', 'cols' => '10'
                ),
                'label' => 'Descripción'
            ))
            ->add('horasextra', 'collection', array(
                'label_attr' => array(
                    'class' => 'col-lg-2',
                    'style'=>'display:none;',
                ),
                'attr' => array(
                    'class' => 'col-lg-4',
                    'style'=>'display:none;',
                ),
                'label' => false,
                'type' => new HoraExtraType(),
                'allow_add' => true,
                'allow_delete' => true,
                'required' => false,
                'by_reference' => false,
            ))
            /* ->add('funcion', 'choice', array(
              'label' => 'Función',
              'read_only' => true,
              'choices' => array(0 => 'Mantenimiento', 1 => 'Obra'),
              //'preferred_choices' => array('Mantenimiento'),
              )) */
            ->add('articulos', 'collection', array(
                'label_attr' => array(
                    'class' => 'col-lg-2',
                ),
                'attr' => array(
                    'class' => 'col-lg-4',
                ),
                'label' => false,
                'type' => new CantTareaArticuloType(),
                'allow_add' => true,
                'allow_delete' => true,
                'required' => false,
                'by_reference' => false,
            ))
            ->add('costoTotal', null, array(
                'label' => 'Total Presupuesto',
                'required' => false,
                'label_attr' => array(
                    'class' => 'col-lg-3 col-md-3 col-sm-6',
                    'style' => 'font-weight: bold; font-size: 24px;',
                ),
                'attr' => array(
                    'class' => 'col-lg-3 col-md-3 col-sm-6 input-sm',
                ),
            ))
        ;
     
        if ($funcion == $this->OBRA || is_null($funcion)) {
            //OBRA
            $builder
                ->add('tareas', 'collection', array(
                    'label_attr' => array(
                        'class' => 'col-lg-2 col-md-2 col-sm-2',
                    ),
                    'attr' => array(
                        'class' => 'col-lg-4 col-md-4 col-sm-4',
                    ),
                    'label' => false,
                    'type' => $type,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'required' => true,
                    'by_reference' => false,
                ))
                /*
                ->add('valor', new ValorType(), array(
                    'label' => false
                ))*/             
            ;
        } else {
            //MANTENIMIENTO
            $builder
                ->add('tiempoHoras', null, array(
                    'label' => 'Tiempo en Horas'
                ))
                ->add('cantOperario', null, array(
                    'label' => 'Cantidad de operarios'
                ))
                ->add('manoDeObraTotal', 'text', array(
                    'label' => 'Sub total',
                    'required' => false,
                    'read_only' => true,
                ))
            ;
        }

        $builder
            ->addEventSubscriber(
                new AddClienteSubscriber($this->entityManager)
            )
            ->addEventSubscriber(
                new AddDomicilioSubscriber($this->entityManager)
            )
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\PresupuestoBundle\Entity\Presupuesto',
            'cascade_validation' => true,
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_presupuestobundle_presupuesto';
    }
}