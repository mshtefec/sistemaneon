<?php

namespace Sistema\PresupuestoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;

/**
 * ValorFilterType filtro.
 * @author Nombre Apellido <name@gmail.com>
 */
class ValorFilterType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('predeterminado', 'filter_choice',array(
                        'attr'=> array('class'=>'form-control')
                    ))
            ->add('ayudanteCostoHora', 'filter_number_range',array(
                        'attr'=> array('class'=>'form-control')
                    ))
            ->add('oficialCostoHora', 'filter_number_range',array(
                        'attr'=> array('class'=>'form-control')
                    ))
            ->add('gastosGenerales', 'filter_number_range',array(
                        'attr'=> array('class'=>'form-control')
                    ))
            ->add('beneficio', 'filter_number_range',array(
                        'attr'=> array('class'=>'form-control')
                    ))
            ->add('impuestos', 'filter_number_range',array(
                        'attr'=> array('class'=>'form-control')
                    ))
            ->add('ivaMultiplicar', 'filter_number_range',array(
                        'attr'=> array('class'=>'form-control')
                    ))
            ->add('ivaDividir', 'filter_number_range',array(
                        'attr'=> array('class'=>'form-control')
                    ))
            ->add('ingresosBrutos', 'filter_number_range',array(
                        'attr'=> array('class'=>'form-control')
                    ))
        ;

        $listener = function(FormEvent $event)
        {
            // Is data empty?
            foreach ((array)$event->getForm()->getData() as $data) {
                if ( is_array($data)) {
                    foreach ($data as $subData) {
                        if (!empty($subData)) {
                            return;
                        }
                    }
                } else {
                    if (!empty($data)) {
                        return;
                    }    
                }
            }
            $event->getForm()->addError(new FormError('Filter empty'));
        };
        $builder->addEventListener(FormEvents::POST_SUBMIT, $listener);
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\PresupuestoBundle\Entity\Valor'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_presupuestobundle_valorfiltertype';
    }
}
