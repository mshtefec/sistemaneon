<?php

namespace Sistema\PresupuestoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CantTareaArticuloType extends AbstractType {
    private $calcularSubTotal;
    private $calcularTotal;

    public function __construct($calcularSubTotal = false, $calcularTotal = false) {
        $this->calcularSubTotal = $calcularSubTotal;
        $this->calcularTotal = $calcularTotal;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('cantidad', null, array(
                'attr' => array(
                    'class' => 'cantidadArticulo',
                ),
            ))
            ->add('articulo', 'select2articuloSubtotal', array(
                'label' => 'Articulo',
                'class' => 'Sistema\STOCKBundle\Entity\Articulo',
                'url' => 'autocomplete_get_articulos_activos_precio',
                'required' => true,
                'configs' => array(
                    'multiple' => false, //es requerido true o false
                    'width' => '100%',
                    'cantidadMaxima' => false, //es requerido true o false
                    'valCantidadMaxima' => false,
                    'entidad' => 'articulo',
                    'calcularValorEnCero' => true,
                    'calcularSubTotal' => $this->calcularSubTotal,
                    'calcularTotal' => $this->calcularTotal,
                ),
                'attr' => array(
                    'class' => 'col-lg-12 col-md-12 input-sm',
                    'style' => 'min-width: 300px;',
                ),
            ))
            ->add('precio', 'text', array(
                'label' => 'Precio',
            ))
            ->add('subTotal', 'text', array(
                'label' => 'sub total',
                'required' => false,
                'attr' => array(
                    'class' => 'articuloSubtotal',
                ),
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\PresupuestoBundle\Entity\CantTareaArticulo'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_stockbundle_canttareaarticulo';
    }
}
