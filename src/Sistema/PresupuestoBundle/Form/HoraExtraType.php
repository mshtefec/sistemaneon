<?php

namespace Sistema\PresupuestoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class HoraExtraType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('cargo', 'entity', array(
                'class' => 'SistemaRRHHBundle:EmpleadoCargo',
                'attr'=>array('style'=>'display:none;'),
                'label_attr'=>array('style'=>'display:none;'),
            ))
            ->add('canthoras')
            ->add('costocargo', null, array(
                "read_only" => true,
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\PresupuestoBundle\Entity\HoraExtra'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_stockbundle_horaextra';
    }

}
