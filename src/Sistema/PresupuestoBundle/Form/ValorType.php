<?php

namespace Sistema\PresupuestoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ValorType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            // ->add('predeterminado', null, array(
            //     'required' => false
            // ))
            ->add('ayudanteCostoHora', null, array(
                'read_only' => true
            ))
            ->add('oficialCostoHora', null, array(
                'read_only' => true
            ))
            //->add('gastosGenerales', null, array(
            //    'read_only' => true
            //))
            //->add('beneficio', null, array(
            //    'read_only' => true
            //))
            //->add('impuestos', null, array(
            //    'read_only' => true
            //))
            // ->add('ivaMultiplicar')
            // ->add('ivaDividir')
            // ->add('ingresosBrutos')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\PresupuestoBundle\Entity\Valor'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_presupuestobundle_valor';
    }

}
