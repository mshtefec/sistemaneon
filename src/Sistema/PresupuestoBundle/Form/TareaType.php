<?php

namespace Sistema\PresupuestoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * TareaType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class TareaType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $articuloCalcularSubTotal = true;
        $articuloCalcularTotal    = false;

        $builder
            ->add('nombre', null, array(
                'label_attr' => array(
                    'class' => 'col-lg-1 col-md-2',
                ),
                'attr' => array(
                    'class' => 'col-lg-7 col-md-6',
                ),
            ))
            ->add('tipo', 'choice', array(
                'label' => 'Función',
                'choices' => array(0 => 'Mantenimiento', 1 => 'Obra'),
                'preferred_choices' => array('Mantenimiento'),
                'label_attr' => array(
                    'class' => 'col-lg-1 col-md-1',
                ),
                'attr' => array(
                    'class' => 'col-lg-3 col-md-3',
                ),
            ))
            ->add('descripcion', null, array(
                'label_attr' => array(
                    'class' => 'col-lg-2 col-md-2',
                ),
                'attr' => array(
                    'class' => 'col-lg-10 col-md-10',
                    'rows' => '10', 'cols' => '10'
                ),
                'label' => 'Descripción'
            ))
            ->add('articulos', 'collection', array(
                'label_attr' => array(
                    'class' => 'col-lg-2',
                ),
                'attr' => array(
                    'class' => 'col-lg-4',
                ),
                'label' => false,
                'type' => new CantTareaArticuloType($articuloCalcularSubTotal, $articuloCalcularTotal),
                'allow_add' => true,
                'allow_delete' => true,
                'required' => true,
                'by_reference' => false,
            ))
            ->add('tareascargos', 'collection', array(
                'label_attr' => array(
                    'class' => 'col-lg-2',
                    'style' => 'display:none;',
                ),
                'attr' => array(
                    'class' => 'col-lg-4',
                    'style' => 'display:none;',
                ),
                'label' => false,
                'type' => new TareaCargoType(),
                'allow_add' => true,
                'allow_delete' => true,
                'required' => true,
                'by_reference' => false,
            ))            
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\PresupuestoBundle\Entity\Tarea'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_presupuestobundle_tarea';
    }
}
