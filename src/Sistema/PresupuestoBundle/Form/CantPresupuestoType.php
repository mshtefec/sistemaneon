<?php

namespace Sistema\PresupuestoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CantPresupuestoType extends AbstractType {

    private $MANTENIMIENTO = 0;
    private $OBRA = 1;
    private $tipo;

    public function __construct($tipo) {
        $this->tipo = $tipo;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        if ($this->tipo == $this->OBRA) {
            $url = 'autocomplete_get_tareas_obra';
        } else {
            $url = 'autocomplete_get_tareas_mantenimiento';
        }

        $builder
            ->add('tarea', 'select2tarea', array(
                'label' => 'Tarea',
                'class' => 'Sistema\PresupuestoBundle\Entity\Tarea',
                'url' => $url,
                'required' => true,
                'configs' => array(
                    'multiple' => false, //es requerido true o false
                    'width' => '100%',
                    'cantidadMaxima' => true, //es requerido true o false
                    'valCantidadMaxima' => false,
                    'entidad' => 'tarea',
                ),
                'attr' => array(
                    'class' => 'col-lg-12 col-md-12 col-sm-12 input-sm',
                    'style' => 'min-width: 300px;',
                ),
            ))
            //es Horas
            ->add('cantidadMaxima', null, array(
                'read_only' => true,
                'attr' => array(
                    'col' => 'col-lg-12 col-md-12 col-sm-12',
                    'style' => 'min-width: 60px;',
                    'class' => 'input-sm',
                ),
            ))
            ->add('cantidad', null, array(
                'label_attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                ),
                'attr' => array(
                    'class' => 'cantidadTarea',
                ),
            ))
            ->add('cantidadHoras', null, array(
                'read_only' => true,
                'attr' => array(
                    'col' => 'col-lg-12 col-md-12 col-sm-12',
                    'style' => 'min-width: 60px;',
                    'class' => 'input-sm',
                ),
            ))
            ->add('precioHoras', null, array(
                'read_only' => true,
                'attr' => array(
                    'col' => 'col-lg-12 col-md-12 col-sm-12',
                    'style' => 'min-width: 60px;',
                    'class' => 'input-sm preciosHora',
                ),
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\PresupuestoBundle\Entity\PresupuestoTarea'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_stockbundle_cantpresupuesto';
    }

}
