<?php

namespace Sistema\PresupuestoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;

/**
 * PresupuestoFilterType filtro.
 * @author Nombre Apellido <name@gmail.com>
 */
class PresupuestoFilterType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numero', 'filter_text',array(
                        'attr'=> array('class'=>'form-control')
                    ))
           ->add('cliente', 'select2',array(
                         'class' => 'Sistema\RRHHBundle\Entity\Cliente',
                          'url'   => 'autocomplete_get_clientes',
                            'configs' => array(
                                'multiple' => false,//es requerido true o false
                            // 'width'    => 'element',
                            ),
                        'attr'=> array('class'=>'form-control')
                    ))
        ;

        $listener = function (FormEvent $event) {
            // Is data empty?
            foreach ((array) $event->getForm()->getData() as $data) {
                if ( is_array($data)) {
                    foreach ($data as $subData) {
                        if (!empty($subData)) {
                            return;
                        }
                    }
                } else {
                    if (!empty($data)) {
                        return;
                    }
                }
            }
            $event->getForm()->addError(new FormError('Filter empty'));
        };
        $builder->addEventListener(FormEvents::POST_SUBMIT, $listener);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\PresupuestoBundle\Entity\Presupuesto',
            'csrf_protection'   => true,
            'validation_groups' => array('filtering')
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_presupuestobundle_presupuestofiltertype';
    }
}
