jQuery('.cliente-select').on('change', function(e){
    var id = jQuery(this).val();
    var url = Routing.generate('ajax_domicilio', { 'id': id});
    var type = 'POST';
    var $classSelect = jQuery('#sistema_presupuestobundle_presupuesto_domicilio');
    /* Está función se encuentra en ajaxElementOnSelected.js */
    ajaxElementOnSelected(url, type, $classSelect, id);
})