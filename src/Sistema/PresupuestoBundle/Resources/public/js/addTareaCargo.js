// Get the ul that holds the collection of escuelas
var collectionAddTareaCargo = jQuery('.cantTareaCargo');

jQuery(document).ready(function () {
    collectionAddTareaCargo.data('index', collectionAddTareaCargo.find(':input').length);

    jQuery('.cantTareaCargo').delegate('.btnRemoveAddTareaCargo', 'click', function (e) {
        // prevent the link from creating a "#" on  the URL
        e.preventDefault();
        // remove the li for the tag form
        jQuery(this).closest('.rowremove').remove();     
        calcularTotal();   
    });

    jQuery('.ribon_dom').delegate('.add_AddTareaCargo_link', 'click', function (e) {
        // prevent the link from creating a "#" on the URL                
        e.preventDefault();
        // remove the li for the tag form
        addForm(collectionAddTareaCargo, jQuery('.cantTareaCargo > table > tbody'));
    });
});