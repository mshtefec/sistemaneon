// Get the ul that holds the collection of escuelas
var collectionAddTarea = jQuery('.cantAddTarea');

jQuery(document).ready(function () {
    collectionAddTarea.data('index', collectionAddTarea.find(':input').length);

    jQuery('.cantAddTarea').delegate('.btnRemoveCantAddTarea', 'click', function (e) {
        // prevent the link from creating a "#" on  the URL
        e.preventDefault();
        // remove the li for the tag form
        jQuery(this).closest('.rowremove').remove();
        calcularTotal();
    });

    jQuery('.ribon_dom').delegate('.add_cantAddTarea_link', 'click', function (e) {
        // prevent the link from creating a "#" on the URL                
        e.preventDefault();
        // remove the li for the tag form
        addForm(collectionAddTarea, jQuery('.cantAddTarea > table > tbody'));
    });
});