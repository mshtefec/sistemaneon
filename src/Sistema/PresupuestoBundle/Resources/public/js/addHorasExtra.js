// Get the ul that holds the collection of escuelas
var collectionAddHorasExtra = jQuery('.cantHorasExtra');

jQuery(document).ready(function () {
    collectionAddHorasExtra.data('index', collectionAddHorasExtra.find(':input').length);

    jQuery('.cantHorasExtra').delegate('.btnRemoveAddHorasExtra', 'click', function (e) {
        // prevent the link from creating a "#" on  the URL
        e.preventDefault();
        // remove the li for the tag form
        jQuery(this).closest('.rowremove').remove();
        calcularTotal();
    });

    jQuery('.ribon_dom').delegate('.add_AddHorasExtra_link', 'click', function (e) {
        // prevent the link from creating a "#" on the URL                
        e.preventDefault();
        // remove the li for the tag form
        addForm(collectionAddHorasExtra, jQuery('.cantHorasExtra > table > tbody'));
    });
});