<?php

namespace Sistema\PresupuestoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Valor
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\PresupuestoBundle\Entity\ValorRepository")
 */
class Valor
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="predeterminado", type="boolean", nullable=true)
     */
    private $predeterminado;

    /**
     * @var float
     *
     * @ORM\Column(name="ayudanteCostoHora", type="float")
     * @Assert\Regex(pattern="/^[0-9(.{1})]/")
     */
    private $ayudanteCostoHora;

    /**
     * @var float
     *
     * @ORM\Column(name="oficialCostoHora", type="float")
     * @Assert\Regex(pattern="/^[0-9(.{1})]/")
     */
    private $oficialCostoHora;

    /**
     * @var float
     *
     * @ORM\Column(name="gastosGenerales", type="float", nullable=true)
     */
    private $gastosGenerales;

    /**
     * @var float
     *
     * @ORM\Column(name="beneficio", type="float", nullable=true)
     */
    private $beneficio;

    /**
     * @var float
     *
     * @ORM\Column(name="impuestos", type="float", nullable=true)
     */
    private $impuestos;

    /**
     * @var float
     *
     * @ORM\Column(name="ivaMultiplicar", type="float", nullable=true)
     */
    private $ivaMultiplicar;

    /**
     * @var float
     *
     * @ORM\Column(name="ivaDividir", type="float", nullable=true)
     */
    private $ivaDividir;

    /**
     * @var float
     *
     * @ORM\Column(name="ingresosBrutos", type="float", nullable=true)
     */
    private $ingresosBrutos;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set predeterminado
     *
     * @param boolean $predeterminado
     * @return Valor
     */
    public function setPredeterminado($predeterminado)
    {
        $this->predeterminado = $predeterminado;
    
        return $this;
    }

    /**
     * Get predeterminado
     *
     * @return boolean 
     */
    public function getPredeterminado()
    {
        return $this->predeterminado;
    }

    /**
     * Set ayudanteCostoHora
     *
     * @param float $ayudanteCostoHora
     * @return Valor
     */
    public function setAyudanteCostoHora($ayudanteCostoHora)
    {
        $this->ayudanteCostoHora = $ayudanteCostoHora;
    
        return $this;
    }

    /**
     * Get ayudanteCostoHora
     *
     * @return float 
     */
    public function getAyudanteCostoHora()
    {
        return $this->ayudanteCostoHora;
    }

    /**
     * Set oficialCostoHora
     *
     * @param float $oficialCostoHora
     * @return Valor
     */
    public function setOficialCostoHora($oficialCostoHora)
    {
        $this->oficialCostoHora = $oficialCostoHora;
    
        return $this;
    }

    /**
     * Get oficialCostoHora
     *
     * @return float 
     */
    public function getOficialCostoHora()
    {
        return $this->oficialCostoHora;
    }

    /**
     * Set gastosGenerales
     *
     * @param float $gastosGenerales
     * @return Valor
     */
    public function setGastosGenerales($gastosGenerales)
    {
        $this->gastosGenerales = $gastosGenerales;
    
        return $this;
    }

    /**
     * Get gastosGenerales
     *
     * @return float 
     */
    public function getGastosGenerales()
    {
        return $this->gastosGenerales;
    }

    /**
     * Set beneficio
     *
     * @param float $beneficio
     * @return Valor
     */
    public function setBeneficio($beneficio)
    {
        $this->beneficio = $beneficio;
    
        return $this;
    }

    /**
     * Get beneficio
     *
     * @return float 
     */
    public function getBeneficio()
    {
        return $this->beneficio;
    }

    /**
     * Set impuestos
     *
     * @param float $impuestos
     * @return Valor
     */
    public function setImpuestos($impuestos)
    {
        $this->impuestos = $impuestos;
    
        return $this;
    }

    /**
     * Get impuestos
     *
     * @return float 
     */
    public function getImpuestos()
    {
        return $this->impuestos;
    }

    /**
     * Set ivaMultiplicar
     *
     * @param float $ivaMultiplicar
     * @return Valor
     */
    public function setIvaMultiplicar($ivaMultiplicar)
    {
        $this->ivaMultiplicar = $ivaMultiplicar;
    
        return $this;
    }

    /**
     * Get ivaMultiplicar
     *
     * @return float 
     */
    public function getIvaMultiplicar()
    {
        return $this->ivaMultiplicar;
    }

    /**
     * Set ivaDividir
     *
     * @param float $ivaDividir
     * @return Valor
     */
    public function setIvaDividir($ivaDividir)
    {
        $this->ivaDividir = $ivaDividir;
    
        return $this;
    }

    /**
     * Get ivaDividir
     *
     * @return float 
     */
    public function getIvaDividir()
    {
        return $this->ivaDividir;
    }

    /**
     * Set ingresosBrutos
     *
     * @param float $ingresosBrutos
     * @return Valor
     */
    public function setIngresosBrutos($ingresosBrutos)
    {
        $this->ingresosBrutos = $ingresosBrutos;
    
        return $this;
    }

    /**
     * Get ingresosBrutos
     *
     * @return float 
     */
    public function getIngresosBrutos()
    {
        return $this->ingresosBrutos;
    }
}