<?php

namespace Sistema\PresupuestoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Sistema\MWSCONFBundle\Entity\MWSgedmo;

/**
 * Presupuesto
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\PresupuestoBundle\Entity\PresupuestoRepository")
 */
class Presupuesto extends MWSgedmo {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="numero", type="string", length=255)
     */
    private $numero;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date")
     */
    private $fecha;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\RRHHBundle\Entity\Cliente", inversedBy="presupuesto")
     * @ORM\JoinColumn(name="cliente_id", referencedColumnName="id")
     */
    private $cliente;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\RRHHBundle\Entity\Domicilio")
     * @ORM\JoinColumn(name="domicilio_id", referencedColumnName="id")
     * @Assert\NotNull()
     */
    private $domicilio;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\PresupuestoBundle\Entity\PresupuestoTarea", mappedBy="presupuesto", cascade={"all"})
     */
    private $tareas;

    /**
     * @var float
     *
     * @ORM\Column(name="costoTotal", type="float")
     * @Assert\Regex(pattern="/^[0-9(.{1})]/")
     */
    private $costoTotal;

    /**
     * @var float
     *
     * @ORM\Column(name="articulosTotal", type="float")
     * @Assert\Regex(pattern="/^[0-9(.{1})]/")
     */
    private $articulosTotal;

    /**
     * @var float
     *
     * @ORM\Column(name="manoDeObraTotal", type="float")
     * @Assert\Regex(pattern="/^[0-9(.{1})]/")
     */
    private $manoDeObraTotal;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\STOCKBundle\Entity\Orden", mappedBy="presupuesto")
     * @Assert\Valid
     */
    protected $ordenes;

    /**
     * @ORM\Column(type="integer")
     */
    protected $funcion;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\PresupuestoBundle\Entity\CantTareaArticulo", mappedBy="presupuesto" , cascade={"persist","remove"}, orphanRemoval=true)
     */
    private $articulos;

    /**
     * @var integer
     *
     *  @ORM\OneToOne(targetEntity="Sistema\PresupuestoBundle\Entity\Valor",cascade={"persist"})
     *  @ORM\JoinColumn(name="presupuesto_valor_id", referencedColumnName="id")
     */
    private $valor;   

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     * @Assert\NotBlank()
     */
    private $descripcion;

    /**
     * @var float
     *
     * @ORM\Column(name="tiempoHoras", type="float", nullable=true)
     * @Assert\Regex(pattern="/^[0-9(.{1})]/")
     */
    private $tiempoHoras;

    /**
     * @var integer
     *
     * @ORM\Column(name="cantOperario", type="integer", nullable=true)
     */
    private $cantOperario;
    
    /**
     * @ORM\OneToMany(targetEntity="Sistema\PresupuestoBundle\Entity\HoraExtra", mappedBy="presupuesto", cascade={"persist","remove"}, orphanRemoval=true)
     */
    private $horasextra;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->tareas = new \Doctrine\Common\Collections\ArrayCollection();
        $this->ordenes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->horasextra = new \Doctrine\Common\Collections\ArrayCollection();
        $this->setCostoTotal(0);
        $this->setCantOperario(0);
        $this->setTiempoHoras(0);        
    }

    public function getFuncionString() {
        if ($this->getFuncion() == 0) {
            return 'Mantenimiento';
        } else {
            return 'Obra';
        }
    }

    public function __toString() {
        return $this->getCostoTotal();
    }

    /**
     * Set numero
     *
     * @param  string      $numero
     * @return Presupuesto
     */
    public function setNumero($numero) {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return string
     */
    public function getNumero() {
        return $this->numero;
    }

    /**
     * Set fecha
     *
     * @param  \DateTime   $fecha
     * @return Presupuesto
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha() {
        return $this->fecha;
    }

    /**
     * Set costoTotal
     *
     * @param  float       $costoTotal
     * @return Presupuesto
     */
    public function setCostoTotal($costoTotal) {
        $this->costoTotal = $costoTotal;

        return $this;
    }

    /**
     * Get costoTotal
     *
     * @return float
     */
    public function getCostoTotal() {
        return $this->costoTotal;
    }

    /**
     * Set cliente
     *
     * @param  \Sistema\RRHHBundle\Entity\Cliente $cliente
     * @return Presupuesto
     */
    public function setCliente(\Sistema\RRHHBundle\Entity\Cliente $cliente = null) {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return \Sistema\RRHHBundle\Entity\Cliente
     */
    public function getCliente() {
        return $this->cliente;
    }

    /**
     * Add tareas
     *
     * @param  \Sistema\PresupuestoBundle\Entity\PresupuestoTarea $tareas
     * @return Presupuesto
     */
    public function addTarea(\Sistema\PresupuestoBundle\Entity\PresupuestoTarea $tareas) {
        $tareas->setPresupuesto($this);
        $this->tareas[] = $tareas;

        return $this;
    }

    /**
     * Remove tareas
     *
     * @param \Sistema\PresupuestoBundle\Entity\PresupuestoTarea $tareas
     */
    public function removeTarea(\Sistema\PresupuestoBundle\Entity\PresupuestoTarea $tareas) {
        $this->tareas->removeElement($tareas);
    }

    /**
     * Get tareas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTareas() {
        return $this->tareas;
    }

    /**
     * Set domicilio
     *
     * @param  \Sistema\RRHHBundle\Entity\Domicilio $domicilio
     * @return Orden
     */
    public function setDomicilio(\Sistema\RRHHBundle\Entity\Domicilio $domicilio = null) {
        $this->domicilio = $domicilio;

        return $this;
    }

    /**
     * Get domicilio
     *
     * @return \Sistema\RRHHBundle\Entity\Domicilio
     */
    public function getDomicilio() {
        return $this->domicilio;
    }

    /**
     * Add ordenes
     *
     * @param \Sistema\STOCKBundle\Entity\Orden $ordenes
     * @return Presupuesto
     */
    public function addOrdene(\Sistema\STOCKBundle\Entity\Orden $ordenes) {
        $ordenes->setPresupuesto($this);
        $this->ordenes[] = $ordenes;

        return $this;
    }

    /**
     * Remove ordenes
     *
     * @param \Sistema\STOCKBundle\Entity\Orden $ordenes
     */
    public function removeOrdene(\Sistema\STOCKBundle\Entity\Orden $ordenes) {
        $this->ordenes->removeElement($ordenes);
    }

    /**
     * Get ordenes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrdenes() {
        return $this->ordenes;
    }

    /**
     * Set funcion
     *
     * @param integer $funcion
     * @return Presupuesto
     */
    public function setFuncion($funcion) {
        $this->funcion = $funcion;

        return $this;
    }

    /**
     * Get funcion
     *
     * @return integer 
     */
    public function getFuncion() {
        return $this->funcion;
    }

    /**
     * Add articulos
     *
     * @param \Sistema\PresupuestoBundle\Entity\CantTareaArticulo $articulos
     * @return Presupuesto
     */
    public function addArticulo(\Sistema\PresupuestoBundle\Entity\CantTareaArticulo $articulos) {
        $articulos->setPresupuesto($this);
        $this->articulos[] = $articulos;

        return $this;
    }

    /**
     * Remove articulos
     *
     * @param \Sistema\PresupuestoBundle\Entity\CantTareaArticulo $articulos
     */
    public function removeArticulo(\Sistema\PresupuestoBundle\Entity\CantTareaArticulo $articulos) {
        $this->articulos->removeElement($articulos);
    }

    /**
     * Get articulos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getArticulos() {
        return $this->articulos;
    }

    /**
     * Set valor
     *
     * @param \Sistema\PresupuestoBundle\Entity\Valor $valor
     * @return Presupuesto
     */
    public function setValor(\Sistema\PresupuestoBundle\Entity\Valor $valor = null) {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return \Sistema\PresupuestoBundle\Entity\Valor 
     */
    public function getValor() {
        return $this->valor;
    }

    /**
     * Set articulosTotal
     *
     * @param float $articulosTotal
     * @return Presupuesto
     */
    public function setArticulosTotal($articulosTotal) {
        $this->articulosTotal = $articulosTotal;

        return $this;
    }

    /**
     * Get articulosTotal
     *
     * @return float 
     */
    public function getArticulosTotal() {
        return $this->articulosTotal;
    }

    /**
     * Set manoDeObraTotal
     *
     * @param float $manoDeObraTotal
     * @return Presupuesto
     */
    public function setManoDeObraTotal($manoDeObraTotal) {
        $this->manoDeObraTotal = $manoDeObraTotal;

        return $this;
    }

    /**
     * Get manoDeObraTotal
     *
     * @return float 
     */
    public function getManoDeObraTotal() {
        return $this->manoDeObraTotal;
    }
    
    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Presupuesto
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Set tiempoHoras
     *
     * @param float $tiempoHoras
     * @return Presupuesto
     */
    public function setTiempoHoras($tiempoHoras) {
        $this->tiempoHoras = $tiempoHoras;

        return $this;
    }

    /**
     * Get tiempoHoras
     *
     * @return float 
     */
    public function getTiempoHoras() {
        return $this->tiempoHoras;
    }

    /**
     * Set cantOperario
     *
     * @param integer $cantOperario
     * @return Presupuesto
     */
    public function setCantOperario($cantOperario) {
        $this->cantOperario = $cantOperario;

        return $this;
    }

    /**
     * Get cantOperario
     *
     * @return integer 
     */
    public function getCantOperario() {
        return $this->cantOperario;
    }


    /**
     * Add horasextra
     *
     * @param \Sistema\PresupuestoBundle\Entity\HoraExtra $horaextra
     * @return Presupuesto
     */
    public function addHorasextron(\Sistema\PresupuestoBundle\Entity\HoraExtra $horaextra)
    {
        $horaextra->setPresupuesto($this);
        $this->horasextra[] = $horaextra;
    
        return $this;
    }

    /**
     * Remove horaextra
     *
     * @param \Sistema\PresupuestoBundle\Entity\HoraExtra $horaextra
     */
    public function removeHorasextron(\Sistema\PresupuestoBundle\Entity\HoraExtra $horaextra)
    {
        $this->horasextra->removeElement($horaextra);
    }

    /**
     * Get horasextra
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHorasextra()
    {
        return $this->horasextra;
    }
}