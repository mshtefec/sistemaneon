<?php

namespace Sistema\PresupuestoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TareaCargo
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\PresupuestoBundle\Entity\TareaCargoRepository")
 */
class TareaCargo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="canthoras", type="float")
     */
    private $canthoras;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\RRHHBundle\Entity\EmpleadoCargo", inversedBy="tareascargos")
     * @ORM\JoinColumn(name="cargo_id", referencedColumnName="id")
     */
    protected $cargo;
    
    /**
     * @ORM\ManyToOne(targetEntity="Sistema\PresupuestoBundle\Entity\Tarea", inversedBy="tareascargos")
     * @ORM\JoinColumn(name="tarea_id", referencedColumnName="id")
     */
    protected $tarea;    

    /**
     * @var float
     *
     * @ORM\Column(name="costocargo", type="float")
     */
    private $costocargo;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set canthoras
     *
     * @param float $canthoras
     * @return TareaCargo
     */
    public function setCanthoras($canthoras)
    {
        $this->canthoras = $canthoras;
    
        return $this;
    }

    /**
     * Get canthoras
     *
     * @return float 
     */
    public function getCanthoras()
    {
        return $this->canthoras;
    }

    /**
     * Set cargo
     *
     * @param \Sistema\RRHHBundle\Entity\EmpleadoCargo $cargo
     * @return TareaCargo
     */
    public function setCargo(\Sistema\RRHHBundle\Entity\EmpleadoCargo $cargo = null)
    {
        $this->cargo = $cargo;
    
        return $this;
    }

    /**
     * Get cargo
     *
     * @return \Sistema\RRHHBundle\Entity\EmpleadoCargo 
     */
    public function getCargo()
    {
        return $this->cargo;
    }

    /**
     * Set tarea
     *
     * @param \Sistema\PresupuestoBundle\Entity\Tarea $tarea
     * @return TareaCargo
     */
    public function setTarea(\Sistema\PresupuestoBundle\Entity\Tarea $tarea = null)
    {
        $this->tarea = $tarea;
    
        return $this;
    }

    /**
     * Get tarea
     *
     * @return \Sistema\PresupuestoBundle\Entity\Tarea 
     */
    public function getTarea()
    {
        return $this->tarea;
    } 

    /**
     * Set costocargo
     *
     * @param float $costocargo
     * @return HoraExtra
     */
    public function setCostocargo($costocargo)
    {
        $this->costocargo = $costocargo;
    
        return $this;
    }

    /**
     * Get costocargo
     *
     * @return float 
     */
    public function getCostocargo()
    {
        return $this->costocargo;
    }   
}