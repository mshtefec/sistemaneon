<?php

namespace Sistema\PresupuestoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HoraExtra
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\PresupuestoBundle\Entity\HoraExtraRepository")
 */
class HoraExtra
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="canthoras", type="float")
     */
    private $canthoras;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\RRHHBundle\Entity\EmpleadoCargo", inversedBy="horasextra")
     * @ORM\JoinColumn(name="cargo_id", referencedColumnName="id")
     */
    protected $cargo;
    
    /**
     * @ORM\ManyToOne(targetEntity="Sistema\PresupuestoBundle\Entity\Presupuesto", inversedBy="horasextra")
     * @ORM\JoinColumn(name="presupuesto_id", referencedColumnName="id")
     */
    protected $presupuesto;
    
    /**
     * @var float
     *
     * @ORM\Column(name="costocargo", type="float")
     */
    private $costocargo;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set canthoras
     *
     * @param float $canthoras
     * @return HoraExtra
     */
    public function setCanthoras($canthoras)
    {
        $this->canthoras = $canthoras;
    
        return $this;
    }

    /**
     * Get canthoras
     *
     * @return float 
     */
    public function getCanthoras()
    {
        return $this->canthoras;
    }

    /**
     * Set cargo
     *
     * @param \Sistema\RRHHBundle\Entity\EmpleadoCargo $cargo
     * @return HoraExtra
     */
    public function setCargo(\Sistema\RRHHBundle\Entity\EmpleadoCargo $cargo = null)
    {
        $this->cargo = $cargo;
    
        return $this;
    }

    /**
     * Get cargo
     *
     * @return \Sistema\RRHHBundle\Entity\EmpleadoCargo 
     */
    public function getCargo()
    {
        return $this->cargo;
    }

    /**
     * Set presupuesto
     *
     * @param \Sistema\PresupuestoBundle\Entity\Presupuesto $presupuesto
     * @return HoraExtra
     */
    public function setPresupuesto(\Sistema\PresupuestoBundle\Entity\Presupuesto $presupuesto = null)
    {
        $this->presupuesto = $presupuesto;
    
        return $this;
    }

    /**
     * Get presupuesto
     *
     * @return \Sistema\PresupuestoBundle\Entity\Presupuesto 
     */
    public function getPresupuesto()
    {
        return $this->presupuesto;
    }

    /**
     * Set costocargo
     *
     * @param float $costocargo
     * @return HoraExtra
     */
    public function setCostocargo($costocargo)
    {
        $this->costocargo = $costocargo;
    
        return $this;
    }

    /**
     * Get costocargo
     *
     * @return float 
     */
    public function getCostocargo()
    {
        return $this->costocargo;
    }
}