<?php

namespace Sistema\PresupuestoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CantTareaArticulo
 *
 * @ORM\Table()
 * @ORM\Entity()
 */
class CantTareaArticulo {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Sistema\STOCKBundle\Entity\Articulo")
     * @ORM\JoinColumn(name="articulo_id", referencedColumnName="id")
     *
     */
    private $articulo;

    /**
     * @var integer
     *
     * @ORM\Column(name="cantidad", type="integer")
     */
    private $cantidad;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\PresupuestoBundle\Entity\Tarea", inversedBy="articulos")
     * @ORM\JoinColumn(name="tarea_id", referencedColumnName="id")
     */
    private $tarea;

    /**
     * @ORM\ManyToOne(targetEntity="Presupuesto", inversedBy="articulos")
     * @ORM\JoinColumn(name="presupuesto_id", referencedColumnName="id")
     * */
    private $presupuesto;

    /**
     * @var integer
     *
     * @ORM\Column(name="precio", type="decimal", scale=2)
     */
    private $precio;

    /**
     * @var integer
     *
     * @ORM\Column(name="subTotal", type="decimal", scale=2)
     */
    private $subTotal;

    public function __construct() {
        $this->precio = 0;
        $this->subTotal = 0;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set cantidad
     *
     * @param integer $cantidad
     * @return CantTareaArticulo
     */
    public function setCantidad($cantidad) {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer 
     */
    public function getCantidad() {
        return $this->cantidad;
    }

    /**
     * Set articulo
     *
     * @param \Sistema\STOCKBundle\Entity\Articulo $articulo
     * @return CantTareaArticulo
     */
    public function setArticulo(\Sistema\STOCKBundle\Entity\Articulo $articulo = null) {
        $this->articulo = $articulo;

        return $this;
    }

    /**
     * Get articulo
     *
     * @return \Sistema\STOCKBundle\Entity\Articulo 
     */
    public function getArticulo() {
        return $this->articulo;
    }

    /**
     * Set tarea
     *
     * @param \Sistema\PresupuestoBundle\Entity\Tarea $tarea
     * @return CantTareaArticulo
     */
    public function setTarea(\Sistema\PresupuestoBundle\Entity\Tarea $tarea = null) {
        $this->tarea = $tarea;

        return $this;
    }

    /**
     * Get tarea
     *
     * @return \Sistema\PresupuestoBundle\Entity\Tarea 
     */
    public function getTarea() {
        return $this->tarea;
    }

    /**
     * Set presupuesto
     *
     * @param \Sistema\PresupuestoBundle\Entity\Presupuesto $presupuesto
     * @return CantTareaArticulo
     */
    public function setPresupuesto(\Sistema\PresupuestoBundle\Entity\Presupuesto $presupuesto = null) {
        $this->presupuesto = $presupuesto;

        return $this;
    }

    /**
     * Get presupuesto
     *
     * @return \Sistema\PresupuestoBundle\Entity\Presupuesto 
     */
    public function getPresupuesto() {
        return $this->presupuesto;
    }

    /* Retorno el monto del articulo multiplicado por la cantidad */

    public function getMontoArticulosTarea() {
        return ($this->getArticulo()->getPrecio() * $this->getCantidad());
    }

    /**
     * Set precio
     *
     * @param string $precio
     * @return CantTareaArticulo
     */
    public function setPrecio($precio) {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return string 
     */
    public function getPrecio() {
        return $this->precio;
    }

    /**
     * Set subtotal
     *
     * @param string $subTotal
     * @return CantTareaArticulo
     */
    public function setSubTotal($subTotal) {
        $this->subTotal = $subTotal;

        return $this;
    }

    /**
     * Get subtotal
     *
     * @return string 
     */
    public function getSubTotal() {
        $this->subTotal=  $this->precio* $this->cantidad;
        return $this->subTotal;
    }

}
