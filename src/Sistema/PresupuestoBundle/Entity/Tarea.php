<?php

namespace Sistema\PresupuestoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Tarea
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\PresupuestoBundle\Entity\TareaRepository")
 */
class Tarea
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\PresupuestoBundle\Entity\CantTareaArticulo", mappedBy="tarea" , cascade={"persist","remove"}, orphanRemoval=true)
     */
    private $articulos;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\PresupuestoBundle\Entity\PresupuestoTarea", mappedBy="tarea" , cascade={"persist","remove"}, orphanRemoval=true)
     */
    private $presupuesto;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;

    /**
     * @ORM\Column(type="integer")
     */
    protected $tipo;    

    /**
     * @ORM\OneToMany(targetEntity="Sistema\PresupuestoBundle\Entity\TareaCargo", mappedBy="tarea", cascade={"persist","remove"}, orphanRemoval=true)
     */
    private $tareascargos;
    
    /**
     * Constructor
     */
    public function __construct() {
        $this->articulos   = new \Doctrine\Common\Collections\ArrayCollection();
        $this->presupuesto = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tareascargos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString() {
        return $this->nombre;
    }

    // /**
    //  * Get cantidad el metodo se llama asi porque lo utiliza en articulo
    //  */
    // public function getCantidad()
    // {
    //     return $this->ayudanteHora;
    // }
    /**
     * Get cantidadReal el metodo se llama asi porque lo utiliza en articulo
     * Tambien lo uso en OrdenController en avanceAction() para sumar las hs
     */
    public function getCantidadReal()
    {
        //Antes con ayudanteHora y oficialHora
        /*
        return ($this->ayudanteHora + $this->oficialHora);
        */

        //Ahora recorro tareasCargo y realizo una suma de todos los cargos agregados
        $horas = 0;
        foreach ($this->tareascargos as $tareaCargo) {
            $horas = $horas + $tareaCargo->getCanthoras();
        }
        return $horas;
    }

    public function getCostoTotalHsPorCargoObra()
    {
        $total = 0;
        $costoHoraObra = 0;
        $horas = 0;
        foreach ($this->tareascargos as $tareaCargo) {
            //Va a traer el costo de hora para Obra 
            $costoHoraObra = $tareaCargo->getCargo()->getCostoHoraCliente();
            //La cantidad de horas hombre de la tareaCargo
            $horas = $tareaCargo->getCanthoras();
            $total = $total + ($costoHoraObra * $horas);
        }

        return $total;
    }

    public function getCostoTotalTarea()
    {
        $total = 0;

        $totalHorasCargo = 0;
        $costoHoraObra = 0;
        $horas = 0;
        foreach ($this->tareascargos as $tareaCargo) {
            //Va a traer el costo de hora para Obra 
            $costoHoraObra = $tareaCargo->getCargo()->getCostoHoraCliente();
            //La cantidad de horas hombre de la tareaCargo
            $horas = $tareaCargo->getCanthoras();
            $totalHorasCargo = $totalHorasCargo + ($costoHoraObra * $horas);
        }

        $totalArticulos = 0;
        foreach ($this->articulos as $cantTareaArticulo) {
            //Voy sumando el costo total de todos los articulos 
            $totalArticulos = $totalArticulos + $cantTareaArticulo->getSubTotal();
        }

        $total = $totalHorasCargo + $totalArticulos;

        return $total;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set funcion
     *
     * @param integer $tipo
     * @return Presupuesto
     */
    public function setTipo($tipo) {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get funcion
     *
     * @return integer 
     */
    public function getTipo() {
        return $this->tipo;
    }

    /**
     * Set nombre
     *
     * @param  string $nombre
     * @return Tarea
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre() {
        return $this->nombre;
    }

    /**
     * Add articulos
     *
     * @param  \Sistema\PresupuestoBundle\Entity\CantTareaArticulo $articulos
     * @return Tarea
     */
    public function addArticulo(\Sistema\PresupuestoBundle\Entity\CantTareaArticulo $articulos) {
        $articulos->setTarea($this);
        $this->articulos[] = $articulos;

        return $this;
    }

    /**
     * Remove articulos
     *
     * @param \Sistema\PresupuestoBundle\Entity\CantTareaArticulo $articulos
     */
    public function removeArticulo(\Sistema\PresupuestoBundle\Entity\CantTareaArticulo $articulos) {
        $this->articulos->removeElement($articulos);
    }

    /**
     * Get articulos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticulos() {
        return $this->articulos;
    }

    /**
     * Add presupuesto
     *
     * @param  \Sistema\PresupuestoBundle\Entity\PresupuestoTarea $presupuesto
     * @return Tarea
     */
    public function addPresupuesto(\Sistema\PresupuestoBundle\Entity\PresupuestoTarea $presupuesto) {
        $this->presupuesto[] = $presupuesto;

        return $this;
    }

    /**
     * Remove presupuesto
     *
     * @param \Sistema\PresupuestoBundle\Entity\PresupuestoTarea $presupuesto
     */
    public function removePresupuesto(\Sistema\PresupuestoBundle\Entity\PresupuestoTarea $presupuesto) {
        $this->presupuesto->removeElement($presupuesto);
    }

    /**
     * Get presupuesto
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPresupuesto() {
        return $this->presupuesto;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Tarea
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion() {
        return $this->descripcion;
    }    

    /**
     * Add tareascargos
     *
     * @param \Sistema\PresupuestoBundle\Entity\TareaCargo $tareascargos
     * @return Tarea
     */
    public function addTareascargo(\Sistema\PresupuestoBundle\Entity\TareaCargo $tareascargos)
    {
        $tareascargos->setTarea($this);
        $this->tareascargos[] = $tareascargos;
    
        return $this;
    }

    /**
     * Remove tareascargos
     *
     * @param \Sistema\PresupuestoBundle\Entity\TareaCargo $tareascargos
     */
    public function removeTareascargo(\Sistema\PresupuestoBundle\Entity\TareaCargo $tareascargos)
    {
        $this->tareascargos->removeElement($tareascargos);
    }

    /**
     * Get tareascargos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTareascargos()
    {
        return $this->tareascargos;
    }
}