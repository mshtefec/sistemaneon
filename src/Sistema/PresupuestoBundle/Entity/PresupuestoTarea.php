<?php

namespace Sistema\PresupuestoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PresupuestoTarea
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\PresupuestoBundle\Entity\PresupuestoTareaRepository")
 */
class PresupuestoTarea
{
    //private $cantidadMaxima;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     /**
     * @ORM\ManyToOne(targetEntity="Sistema\PresupuestoBundle\Entity\Presupuesto", inversedBy="tareas")
     * @ORM\JoinColumn(name="presupuesto_id", referencedColumnName="id")
     */
    private $presupuesto;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\PresupuestoBundle\Entity\Tarea", inversedBy="presupuesto")
     * @ORM\JoinColumn(name="tarea_id", referencedColumnName="id")
     */
    private $tarea;

    /**
     * @var float
     *
     * @ORM\Column(name="precio_articulos", type="float")
     * @Assert\Regex(pattern="/^[0-9(.{1})]/")
     */
    private $precioArticulos;

    /**
     * @var float
     *
     * @ORM\Column(name="precio_horas", type="float")
     * @Assert\Regex(pattern="/^[0-9(.{1})]/")
     */
    private $precioHoras;

    /**
     * @var integer
     *
     * @ORM\Column(name="cantidad", type="integer")
     * @Assert\Regex(pattern="/^[1-9]/")
     */
    private $cantidad;

    /**
     * @var float
     *
     * @ORM\Column(name="cantidad_horas", type="float")
     * @Assert\Regex(pattern="/^[0-9(.{1})]/")
     */
    private $cantidadHoras;


    /**
     * Set cantidad maxima utilizo cantidadHoras
     */
    public function setCantidadMaxima($cantidadMaxima) {
        $this->cantidadHoras = $cantidadMaxima;
    }

    /**
     * Get cantidad maxima utilizo cantidadHoras
     */
    public function getCantidadMaxima() {
        return $this->cantidadHoras;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set presupuesto
     *
     * @param  \Sistema\PresupuestoBundle\Entity\Presupuesto $presupuesto
     * @return PresupuestoTarea
     */
    public function setPresupuesto(\Sistema\PresupuestoBundle\Entity\Presupuesto $presupuesto = null)
    {
        $this->presupuesto = $presupuesto;

        return $this;
    }

    /**
     * Get presupuesto
     *
     * @return \Sistema\PresupuestoBundle\Entity\Presupuesto
     */
    public function getPresupuesto()
    {
        return $this->presupuesto;
    }

    /**
     * Set tarea
     *
     * @param  \Sistema\PresupuestoBundle\Entity\Tarea $tarea
     * @return PresupuestoTarea
     */
    public function setTarea(\Sistema\PresupuestoBundle\Entity\Tarea $tarea = null)
    {
        $this->tarea = $tarea;

        return $this;
    }

    /**
     * Get tarea
     *
     * @return \Sistema\PresupuestoBundle\Entity\Tarea
     */
    public function getTarea()
    {
        return $this->tarea;
    }

    /**
     * Set cantidad
     *
     * @param  integer           $cantidad
     * @return CantTareaArticulo
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set precioArticulos
     *
     * @param float $precioArticulos
     * @return PresupuestoTarea
     */
    public function setPrecioArticulos($precioArticulos)
    {
        $this->precioArticulos = $precioArticulos;
    
        return $this;
    }

    /**
     * Get precioArticulos
     *
     * @return float 
     */
    public function getPrecioArticulos()
    {
        return $this->precioArticulos;
    }

    /**
     * Set precioHoras
     *
     * @param float $precioHoras
     * @return PresupuestoTarea
     */
    public function setPrecioHoras($precioHoras)
    {
        $this->precioHoras = $precioHoras;
    
        return $this;
    }

    /**
     * Get precioHoras
     *
     * @return float 
     */
    public function getPrecioHoras()
    {
        return $this->precioHoras;
    }

    /**
     * Set cantidadHoras
     *
     * @param float $cantidadHoras
     * @return PresupuestoTarea
     */
    public function setCantidadHoras($cantidadHoras)
    {
        $this->cantidadHoras = $cantidadHoras;
    
        return $this;
    }

    /**
     * Get cantidadHoras
     *
     * @return float 
     */
    public function getCantidadHoras()
    {
        return $this->cantidadHoras;
    }
}