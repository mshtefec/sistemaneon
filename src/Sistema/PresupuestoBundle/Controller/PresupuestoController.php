<?php

namespace Sistema\PresupuestoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\PresupuestoBundle\Entity\Presupuesto;
use Sistema\STOCKBundle\Entity\Orden;
use Sistema\PresupuestoBundle\Form\PresupuestoType;
use Sistema\PresupuestoBundle\Form\PresupuestoFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sistema\STOCKBundle\Entity\CantOrden;
use Sistema\STOCKBundle\Entity\Articulo;
use Sistema\PresupuestoBundle\Entity\CantTareaArticulo;
use Sistema\PresupuestoBundle\Entity\Tarea;
use Sistema\PresupuestoBundle\Entity\Valor;
use Sistema\FACTURACIONBundle\Entity\Asiento;
use Sistema\STOCKBundle\Controller\OrdenController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Presupuesto controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/presupuesto")
 */
class PresupuestoController extends Controller {

    private $MANTENIMIENTO = 0;
    private $OBRA = 1;

    /**
     * Lists all Presupuesto entities.
     *
     * @Route("/", name="admin_presupuesto")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        list($filterForm, $queryBuilder) = $this->filter();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $queryBuilder, $this->get('request')->query->get('page', 1), (isset($this->container->parameters['knp_paginator.page_range'])) ? $this->container->parameters['knp_paginator.page_range'] : 10
        );

        return array(
            'entities' => $pagination,
            'filterForm' => $filterForm->createView(),
        );
    }

    /**
     * Process filter request.
     *
     * @return array
     */
    protected function filter() {
        $request = $this->getRequest();
        $session = $request->getSession();
        $filterForm = $this->createFilterForm();
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository('SistemaPresupuestoBundle:Presupuesto')
                ->createQueryBuilder('a')
                ->join('a.cliente', 'cli')
                ->orderBy('a.id', 'DESC')
        ;
        // Bind values from the request
        $filterForm->handleRequest($request);
        // Reset filter
        if ($filterForm->get('reset')->isClicked()) {
            $session->remove('PresupuestoControllerFilter');
            $filterForm = $this->createFilterForm();
        }

        // Filter action
        if ($filterForm->get('filter')->isClicked()) {
            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('PresupuestoControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('PresupuestoControllerFilter')) {
                $filterData = $session->get('PresupuestoControllerFilter');
                $filterForm = $this->createFilterForm($filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }

    /**
     * Create filter form.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createFilterForm($filterData = null) {
        $form = $this->createForm(new PresupuestoFilterType(), $filterData, array(
            'action' => $this->generateUrl('admin_presupuesto'),
            'method' => 'GET',
        ));

        $form
                ->add('filter', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.index.filter',
                    'attr' => array('class' => 'btn btn-success col-lg-1'),
                ))
                ->add('reset', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.index.reset',
                    'attr' => array('class' => 'btn btn-danger col-lg-1 col-lg-offset-1'),
                ))
        ;

        return $form;
    }

    /**
     * Creates a new Presupuesto entity.
     *
     * @Route("/", name="admin_presupuesto_create")
     * @Method("POST")
     */
    public function createAction(Request $request) {

        $funcion = (int) $request->request->get('sistema_presupuestobundle_presupuesto')["funcion"];
        $entity = new Presupuesto();
        $entity->setFuncion($funcion);
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        $precioHora = current($em->getRepository('SistemaMWSCONFBundle:Setting')->findByClave("pre_mantenimiento"))->getValor();
        if ($form->isValid()) {

            $this->setCostoTotal($entity);
            if ($entity->getFuncion() == $this->MANTENIMIENTO) {
                $entity->setValor(null);
            }
            // elseif ($entity->getValor()->getPredeterminado() == true) {
            //     //ES OBRA Y VALOR PREDETERMINADO TRUE
            //     $valores = $this->get('sistema_presupuesto_funciones')->actualizarValorPredeterminadoUnico();
            //     if ($valores) {
            //         $em->persist($valores);
            //     }
            // }
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');
            if ($entity->getFuncion() == $this->MANTENIMIENTO) {
                $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl('admin_presupuesto_mant_new', array('precioHora' => $precioHora)) : $this->generateUrl('admin_presupuesto_show', array('id' => $entity->getId()));
            } elseif ($entity->getFuncion() == $this->OBRA) {
                $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl('admin_presupuesto_obra_new') : $this->generateUrl('admin_presupuesto_show', array('id' => $entity->getId()));
            }
            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');
        if ($entity->getFuncion() == $this->MANTENIMIENTO) {
            $view = 'SistemaPresupuestoBundle:Presupuesto:newMantenimiento.html.twig';
        } elseif ($entity->getFuncion() == $this->OBRA) {
            $view = 'SistemaPresupuestoBundle:Presupuesto:newObra.html.twig';
        }

        return $this->render($view, array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'precioHora' => $precioHora,
        ));
    }

    private function setCostoTotal($entity) {
        $em = $this->getDoctrine()->getManager();
        $precioHora = current($em->getRepository('SistemaMWSCONFBundle:Setting')->findByClave("pre_mantenimiento"))->getValor();
        $costoTotalMateriales = 0; //Es el costo total de los materiales de todas las tareas
        $costoTotalPresupuesto = 0;
        //articulos
        if (!is_null($entity->getArticulos())) {
            foreach ($entity->getArticulos() as $articulo) {
                $costoTotalMateriales += ($articulo->getPrecio() * $articulo->getCantidad());
            }
        }


        if ($entity->getFuncion() == $this->OBRA) {
            //$costoHoraAyudante = $entity->getValor()->getAyudanteCostoHora();
            //$costoHoraOficial = $entity->getValor()->getOficialCostoHora();

            $costoMateriales = 0; //Es el costo total de los materiales de una tarea
            $costoHoras = 0; //Es el costo total de las horas
            $costoTotalHoras = 0; //Es el costo total de las horas de todas las tareas
            // $sistema_presupuesto_funciones = $this->get('sistema_presupuesto_funciones');
            //tareas
            if (!is_null($entity->getTareas()->getValues())) {
                //como ya estan filtradas las tareas por funcion segun el tipo de presupuesto a crear, 
                //no tengo que preocuparme de preguntar por el tipo de tarea.
                //recorro todas las tareas y sumo los totales.
                foreach ($entity->getTareas() as $presupuestoTarea) {

                    $costoHoras = $presupuestoTarea->getTarea()->getCostoTotalHsPorCargoObra();

                    //$horaAyudante = $presupuestoTarea->getTarea()->getAyudanteHora();
                    //$horaOficial = $presupuestoTarea->getTarea()->getOficialHora();

                    /*
                    $costoHoras +=
                            (
                            ($horaAyudante * $costoHoraAyudante) +
                            ($horaOficial * $costoHoraOficial)
                            ) * $presupuestoTarea->getCantidad()
                    ;*/

                    if (!is_null($presupuestoTarea->getTarea()->getArticulos()->getValues())) {
                        foreach ($presupuestoTarea->getTarea()->getArticulos() as $cantTareaArticulo) {
                            $costoMateriales += $cantTareaArticulo->getMontoArticulosTarea() * $presupuestoTarea->getCantidad();
                        }
                    }
                    //Guardo los valores y sumo totales
                    $presupuestoTarea->setPrecioArticulos($costoMateriales);
                    $presupuestoTarea->setPrecioHoras($costoHoras);
                    $costoTotalHoras += $costoHoras;
                    $costoTotalMateriales += $costoMateriales;
                    $costoHoras = 0;
                    $costoMateriales = 0;
                }
            }

            //horasextra
            if (!is_null($entity->getHorasExtra())) {                
                //recorro todas las Horas extra y sumo los totales.
                foreach ($entity->getHorasExtra() as $horaExtra) {                    
                    
                    $costoHoras +=                            
                            $horaExtra->getCostocargo() * $horaExtra->getCanthoras();
                    ;                    
                    
                    $costoTotalHoras += $costoHoras;                    
                    $costoHoras = 0;
                    if ($horaExtra->getCanthoras() == 0){
                        $entity->removeHorasextron($horaExtra);//elimino el elemento para que no me cree un registro en 0
                    }
                }
            }
            //horas           

            /*
            $costoHoras +=
                    ($horaAyudante * $costoHoraAyudante) +
                    ($horaOficial * $costoHoraOficial)
            ;
            $costoTotalHoras += $costoHoras;
            */
            //Sumo total presupuesto
            $costoTotalPresupuesto = $costoTotalHoras + $costoTotalMateriales;
            //Seteo los valores totales por separado
            $entity->setManoDeObraTotal($costoTotalHoras);
            $entity->setArticulosTotal($costoTotalMateriales);
            $entity->setCostoTotal($costoTotalPresupuesto);
        } else if ($entity->getFuncion() == $this->MANTENIMIENTO) {
            $horasExtras = $precioHora * $entity->getTiempoHoras() * $entity->getCantOperario();
            $costoTotalPresupuesto = $costoTotalMateriales + $horasExtras;
            $entity->setManoDeObraTotal($horasExtras);
            $entity->setArticulosTotal($costoTotalMateriales);
            $entity->setCostoTotal($costoTotalPresupuesto);
        }
    }

    /**
     * Creates a form to create a Presupuesto entity.
     *
     * @param Presupuesto $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Presupuesto $entity) {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new PresupuestoType($em), $entity, array(
            'action' => $this->generateUrl('admin_presupuesto_create'),
            'method' => 'POST',
        ));

        $form
                ->add('save', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.save',
                    'attr' => array('class' => 'btn btn-success col-lg-2')
                ))
                ->add('saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
                ))
        ;

        return $form;
    }

    /**
     * Displays a form to create a new Presupuesto entity.
     *
     * @Route("/new/presupuesto-obra", name="admin_presupuesto_obra_new")
     * @Method("GET")
     * @Template()
     */
    public function newObraAction() {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();
        $ultimoNumeroPresupuesto = $em->getRepository('SistemaPresupuestoBundle:Presupuesto')
                ->findUltimoNumeroPresupuesto();

        $entity = new Presupuesto();
        //obtengo los cargos que tienen un costo por hora
        $cargos = $em->getRepository('SistemaRRHHBundle:EmpleadoCargo')->findAll();        
        foreach ($cargos as $cargo){
            $horaextra = new \Sistema\PresupuestoBundle\Entity\HoraExtra();
            $horaextra->setCargo($cargo);
            $horaextra->setCanthoras(0);
            $horaextra->setCostocargo($cargo->getCostoHoraCliente());
            $entity->addHorasextron($horaextra);
        }
        
        //obtengo los valores con predeterminado true
        // $valores = $this->get('sistema_presupuesto_funciones')->controlValorPredeterminadoUnico();
        $valor = new Valor();
        $arrayCostosHora = $em->getRepository('SistemaMWSCONFBundle:Setting')
                ->getArrayByClaves(array(
            "pre_costoHoraAyudante", "pre_costoHoraOficial",
            "pre_gastosGenerales", "pre_beneficio", "pre_impuestos"
                )
        );

        $valor->setAyudanteCostoHora($arrayCostosHora['pre_costoHoraAyudante']);
        $valor->setOficialCostoHora($arrayCostosHora['pre_costoHoraOficial']);
        $valor->setGastosGenerales($arrayCostosHora['pre_gastosGenerales']);
        $valor->setBeneficio($arrayCostosHora['pre_beneficio']);
        $valor->setImpuestos($arrayCostosHora['pre_impuestos']);

        $entity->setValor($valor);

        if (!empty($ultimoNumeroPresupuesto)) {
            $entity->setNumero($ultimoNumeroPresupuesto[0]['numero'] + 1);
        }

        $entity->setFuncion(1);

        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Presupuesto entity.
     *
     * @Route("/new/presupuesto-mantenimiento", name="admin_presupuesto_mant_new")
     * @Method("GET")
     * @Template()
     */
    public function newMantenimientoAction() {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();
        $ultimoNumeroPresupuesto = $em->getRepository('SistemaPresupuestoBundle:Presupuesto')->findUltimoNumeroPresupuesto();
        $precioHora = current($em->getRepository('SistemaMWSCONFBundle:Setting')->findByClave("pre_mantenimiento"))->getValor();
        $entity = new Presupuesto();

        if (!empty($ultimoNumeroPresupuesto)) {
            $entity->setNumero($ultimoNumeroPresupuesto[0]['numero'] + 1);
        }

        $entity->setFuncion(0);

        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
            'precioHora' => $precioHora,
        );
    }

    /**
     * Finds and displays a Presupuesto entity.
     *
     * @Route("/{id}", name="admin_presupuesto_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaPresupuestoBundle:Presupuesto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Presupuesto entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        if ($entity->getFuncion() == $this->MANTENIMIENTO) {
            $template = 'SistemaPresupuestoBundle:Presupuesto:showMantenimiento.html.twig';
        } else if ($entity->getFuncion() == $this->OBRA) {
            $template = 'SistemaPresupuestoBundle:Presupuesto:showObra.html.twig';
        }

        $content = $this->renderView(
                $template, array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
                )
        );
        return new Response($content);
    }

    /**
     * Displays a form to edit an existing Presupuesto entity.
     *
     * @Route("/{id}/edit", name="admin_presupuesto_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaPresupuestoBundle:Presupuesto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Presupuesto entity.');
        }
        
        //obtengo los cargos que tienen un costo por hora
        $cargos = $em->getRepository('SistemaRRHHBundle:EmpleadoCargo')->findAll();
        //recorro los cargos y agrego los que no existen
        foreach ($cargos as $cargo){
            $agregar = true;
            foreach ($entity->getHorasextra() as $presuHoraextra){
                if ($presuHoraextra->getCargo() == $cargo){                    
                    $agregar = false;
                }
            }
            if ($agregar){
            $horaextra = new \Sistema\PresupuestoBundle\Entity\HoraExtra();
                $horaextra->setCargo($cargo);
                $horaextra->setCanthoras(0);
                $horaextra->setCostocargo($cargo->getCostoHoraCliente());
                $entity->addHorasextron($horaextra);
            }    
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        if ($entity->getFuncion() == 1) {
            
            $content = $this->renderView(
                    "SistemaPresupuestoBundle:Presupuesto:editObra.html.twig", array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
                    )
            );
        } else {
            $precioHora = current($em->getRepository('SistemaMWSCONFBundle:Setting')->findByClave("pre_mantenimiento"))->getValor();
            $content = $this->renderView(
                    "SistemaPresupuestoBundle:Presupuesto:editMantenimiento.html.twig", array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
                'precioHora' => $precioHora,
                    )
            );
        }


        return new Response($content);
    }

    /**
     * Creates a form to edit a Presupuesto entity.
     *
     * @param Presupuesto $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Presupuesto $entity) {

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new PresupuestoType($em), $entity, array(
            'action' => $this->generateUrl('admin_presupuesto_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form
                ->add('save', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.save',
                    'attr' => array('class' => 'btn btn-success col-lg-2')
                ))
                ->add('saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
                ))
        ;

        return $form;
    }

    /**
     * Edits an existing Presupuesto entity.
     *
     * @Route("/{id}", name="admin_presupuesto_update")
     * @Method("PUT")
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaPresupuestoBundle:Presupuesto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Presupuesto entity.');
        }

        $originalTarea = new \Doctrine\Common\Collections\ArrayCollection();

        // Create an ArrayCollection of the current Tag objects in the database
        foreach ($entity->getTareas() as $tarea) {
            $originalTarea->add($tarea);
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            foreach ($originalTarea as $tarea) {
                if (false === $entity->getTareas()->contains($tarea)) {
                    $entity->removeTarea($tarea);
                    $em->remove($tarea);
                }
            }

            $this->setCostoTotal($entity);

            if ($entity->getFuncion() == $this->OBRA) {
                if ($entity->getValor()) {
                    if ($entity->getValor()->getPredeterminado() == true) {
                        //ES OBRA Y VALOR PREDETERMINADO TRUE
                        $valores = $this->get('sistema_presupuesto_funciones')->actualizarValorPredeterminadoUnico();
                        if ($valores) {
                            $em->persist($valores);
                        }
                    }   
                }
            }

            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            if ($entity->getFuncion() == $this->MANTENIMIENTO) {
                $nextAction = $editForm->get('saveAndAdd')->isClicked() ? $this->generateUrl('admin_presupuesto_mant_new') : $this->generateUrl('admin_presupuesto_show', array('id' => $id));
            } elseif ($entity->getFuncion() == $this->OBRA) {
                $nextAction = $editForm->get('saveAndAdd')->isClicked() ? $this->generateUrl('admin_presupuesto_obra_new') : $this->generateUrl('admin_presupuesto_show', array('id' => $id));
            }
            $nextAction = $editForm->get('saveAndAdd')->isClicked() ? $this->generateUrl('admin_presupuesto_new') : $this->generateUrl('admin_presupuesto_show', array('id' => $id));

            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        if ($entity->getFuncion() == 1) {
            $content = $this->renderView(
                    "SistemaPresupuestoBundle:Presupuesto:editObra.html.twig", array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
                    )
            );
        } else {
            $precioHora = current($em->getRepository('SistemaMWSCONFBundle:Setting')->findByClave("pre_mantenimiento"))->getValor();
            $content = $this->renderView(
                    "SistemaPresupuestoBundle:Presupuesto:editMantenimiento.html.twig", array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
                'precioHora' => $precioHora,
                    )
            );
        }
        return new Response($content);
    }

    /**
     * Deletes a Presupuesto entity.
     *
     * @Route("/{id}", name="admin_presupuesto_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SistemaPresupuestoBundle:Presupuesto')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Presupuesto entity.');
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.delete.success');
        }

        return $this->redirect($this->generateUrl('admin_presupuesto'));
    }

    /**
     * Creates a form to delete a Presupuesto entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        $mensaje = $this->get('translator')->trans('views.recordactions.confirm', array(), 'MWSimpleCrudGeneratorBundle');
        $onclick = 'return confirm("' . $mensaje . '");';

        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('admin_presupuesto_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array(
                            'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                            'label' => 'views.recordactions.delete',
                            'attr' => array(
                                'class' => 'btn btn-danger col-lg-11',
                                'onclick' => $onclick,
                            )
                        ))
                        ->getForm()
        ;
    }

    /**
     * Finds and displays a Orden entity.
     *
     * @param type $id Id de la entidad
     *
     * @Route("/imprimir-presupuesto/{id}", name="admin_presupuesto_imprimir")
     * @Method("GET")
     * @Template()
     * @return view
     */
    public function imprimirPresupuestoAction($id) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('SistemaPresupuestoBundle:Presupuesto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Orden entity.');
        }

        $config = array(
            'titulo' => 'Presupuesto',
            'PDF_HEADER_LOGO' => '',
            'PDF_HEADER_LOGO_WIDTH' => '',
            'PDF_HEADER_TITLE' => '',
            'PDF_HEADER_STRING' => '',
            'pie' => '',
            'REMOVE_HEADER' => false
        );

        $html = $this->renderView('SistemaPresupuestoBundle:Presupuesto:show_presupuesto.pdf.twig', array('entity' => $entity));

        return $this->get('io_tcpdf_mws')->quick_pdf($html, $config);
    }

    /**
     * @Route("/generar/presupuesto-orden/", name="admin_generar_presupuesto_orden")
     * @Method("POST")
     * @Template()
     */
    public function generarPresupuestoOrdenAction() {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();
        $id = $this->getRequest()->get('id');
        $cantidadHoras = 0;
        $totalhoras = 0;
        $costoHora = 0;
        $totalArticulo = 0;

        $entity = $em->getRepository('SistemaPresupuestoBundle:Presupuesto')->getPresupuestoJoinTarea($id);

        $orden = new Orden();

        $ultimoNumeroOrden = $em->getRepository('SistemaSTOCKBundle:Orden')->findUltimoNumeroOrden();

        $orden->setNumero($ultimoNumeroOrden[0]['numero'] + 1);
        $orden->setFecha(new \DateTime("now"));
        $orden->setCliente($entity->getCliente());
        $orden->setDomicilio($entity->getDomicilio());
        $orden->setPresupuesto($entity);
        $orden->setFuncion($entity->getFuncion());
        $arrayArticulos = array();
        $seguir = true;

        $articulosSinStock = array();
        $arrayArticulos = array();
        //0 = Mantenimiento; 
        //1 = Obra;
        if ($entity->getFuncion() == 1) {
            //obtengo todos los articulos de las tareass  del presupuesto
            foreach ($entity->getTareas() as $presupuestoTarea) {
                foreach ($presupuestoTarea->getTarea()->getArticulos() as $cantTareaArticulo) {
                    //pregunta si ahi articulos necesarios para crear la orden
                    $arrayArticulos[$cantTareaArticulo->getArticulo()->getId()] = $cantTareaArticulo->getCantidad() * $presupuestoTarea->getCantidad();
                }
            }

            
            //pregunto si los articulos se pueden cubrir y creo
            foreach ($entity->getTareas() as $presupuestoTarea) {
                foreach ($presupuestoTarea->getTarea()->getArticulos() as $cantTareaArticulo) {
                    //pregunta si ahi articulos necesarios para crear la orden
                    if ($cantTareaArticulo->getArticulo()->getCantidadReal() < $arrayArticulos[$cantTareaArticulo->getArticulo()->getId()]) {
                        $articulosSinStock[] = $cantTareaArticulo->getArticulo()->getDescripcion();
                        $seguir = false;
                    }
                    if ($seguir) {
                        $nuevoaAticulo = new CantOrden();
                        $nuevoaAticulo->setArticulo($cantTareaArticulo->getArticulo());
                        $nuevoaAticulo->setPrecio($cantTareaArticulo->getArticulo()->getPrecio());
                        $nuevoaAticulo->setCantidad($cantTareaArticulo->getCantidad() * $presupuestoTarea->getCantidad());
                        $orden->addArticulo($nuevoaAticulo);
                    }
                }
            }
        }

            //obtengo todos los articulos del presupuesto
            foreach ($entity->getArticulos() as $articulos) {
                $arrayArticulos[$articulos->getArticulo()->getId()] = $articulos->getCantidad();
            }
            //pregunto por los articulos del presupuesto si pueden cubrir y creo
            foreach ($entity->getArticulos() as $articulos) {
                if ($articulos->getArticulo()->getCantidadReal() < $arrayArticulos[$articulos->getArticulo()->getId()]) {
                    $articulosSinStock[] = $articulos->getArticulo()->getDescripcion();
                    $seguir = false;
                }
                if ($seguir) {
                    $nuevoaAticulo = new CantOrden();
                    $nuevoaAticulo->setArticulo($articulos->getArticulo());
                    $nuevoaAticulo->setPrecio($articulos->getArticulo()->getPrecio());
                    $nuevoaAticulo->setCantidad($articulos->getCantidad());
                    $orden->addArticulo($nuevoaAticulo);
                }
            }

        if ($seguir) {
            $ordenService = $this->get('orden');
            $arrayResultado = $ordenService->create($orden);
            // $arrayResultado["resultado"] = true;
        }
        if (!isset($arrayResultado)) {
            $arrayResultado["resultado"] = false;
        }
        //ladybug_dump_die($arrayResultado);
        /* $objeto = array(
          'estado' => ''
          ); */
        $array = array(
            "resultado" => $arrayResultado["resultado"],
            "articulosSinStock" => $articulosSinStock
        );
        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

}
