<?php

namespace Sistema\PresupuestoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\PresupuestoBundle\Entity\Tarea;
use Sistema\PresupuestoBundle\Form\TareaType;
use Sistema\PresupuestoBundle\Form\TareaFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sistema\PresupuestoBundle\Entity\Valor;

/**
 * Tarea controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/tarea")
 */
class TareaController extends Controller {

    /**
     * Lists all Tarea entities.
     *
     * @Route("/", name="tarea")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        list($filterForm, $queryBuilder) = $this->filter();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $queryBuilder, $this->get('request')->query->get('page', 1), (isset($this->container->parameters['knp_paginator.page_range'])) ? $this->container->parameters['knp_paginator.page_range'] : 10
        );

        return array(
            'entities' => $pagination,
            'filterForm' => $filterForm->createView(),
        );
    }

    /**
     * Process filter request.
     *
     * @return array
     */
    protected function filter() {
        $request = $this->getRequest();
        $session = $request->getSession();
        $filterForm = $this->createFilterForm();
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository('SistemaPresupuestoBundle:Tarea')
                ->createQueryBuilder('a')
                ->orderBy('a.id', 'DESC')
        ;
        // Bind values from the request
        $filterForm->handleRequest($request);
        // Reset filter
        if ($filterForm->get('reset')->isClicked()) {
            $session->remove('TareaControllerFilter');
            $filterForm = $this->createFilterForm();
        }

        // Filter action
        if ($filterForm->get('filter')->isClicked()) {
            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('TareaControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('TareaControllerFilter')) {
                $filterData = $session->get('TareaControllerFilter');
                $filterForm = $this->createFilterForm($filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }

    /**
     * Create filter form.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createFilterForm($filterData = null) {
        $form = $this->createForm(new TareaFilterType(), $filterData, array(
            'action' => $this->generateUrl('tarea'),
            'method' => 'GET',
        ));

        $form
                ->add('filter', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.index.filter',
                    'attr' => array('class' => 'btn btn-success col-lg-1'),
                ))
                ->add('reset', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.index.reset',
                    'attr' => array('class' => 'btn btn-danger col-lg-1 col-lg-offset-1'),
                ))
        ;

        return $form;
    }

    /**
     * Creates a new Tarea entity.
     *
     * @Route("/", name="tarea_create")
     * @Method("POST")
     * @Template("SistemaPresupuestoBundle:Tarea:new.html.twig")
     */
    public function createAction(Request $request) {
        $entity = new Tarea();

        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            foreach ($entity->getArticulos()->getValues() as $cantArticulo) {
                $entity->addArticulo($cantArticulo);
                $cantArticulo->setTarea($entity);
                $em->persist($cantArticulo);
            }
            
            //tareascargos
            if (!is_null($entity->getTareascargos())) {        
                foreach ($entity->getTareascargos() as $tareaCargo) {             
                    if ($tareaCargo->getCanthoras() == 0){
                        $entity->removeTareascargo($tareaCargo);//elimino el elemento para que no me cree un registro en 0
                    }
                }
            }
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl('tarea_new') : $this->generateUrl('tarea_show', array('id' => $entity->getId()));

            return $this->redirect($nextAction);
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Tarea entity.
     *
     * @param Tarea $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Tarea $entity) {
        $form = $this->createForm(new TareaType(), $entity, array(
            'action' => $this->generateUrl('tarea_create'),
            'method' => 'POST',
        ));

        $form
                ->add(
                        'save', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.save',
                    'attr' => array('class' => 'btn btn-success col-lg-2')
                        )
                )
                ->add(
                        'saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
                        )
                )
        ;

        return $form;
    }

    /**
     * Displays a form to create a new Tarea entity.
     *
     * @Route("/new", name="tarea_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $em = $this->getDoctrine()->getManager();
        $entity = new Tarea();

        //Ahora todos los Empleados Cargo
        $cargos = $em->getRepository('SistemaRRHHBundle:EmpleadoCargo')->findAll();
        foreach ($cargos as $cargo){
            $tareacargo = new \Sistema\PresupuestoBundle\Entity\TareaCargo();
            $tareacargo->setCargo($cargo);
            $tareacargo->setCanthoras(0);  
            $tareacargo->setCostocargo($cargo->getCostoHoraCliente());          
            $entity->addTareascargo($tareacargo);
        }
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Tarea entity.
     *
     * @Route("/{id}", name="tarea_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaPresupuestoBundle:Tarea')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tarea entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Tarea entity.
     *
     * @Route("/{id}/edit", name="tarea_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaPresupuestoBundle:Tarea')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tarea entity.');
        }

        //Ahora todos los Empleados Cargo
        $cargos = $em->getRepository('SistemaRRHHBundle:EmpleadoCargo')->findAll();
        //recorro los cargos y agrego los que no existen
        foreach ($cargos as $cargo){
            $agregar = true;
            foreach ($entity->getTareascargos() as $tareacargo){
                if ($tareacargo->getCargo() == $cargo){                    
                    $agregar = false;
                }
            }
            if ($agregar){
            $tcargo = new \Sistema\PresupuestoBundle\Entity\TareaCargo();
                $tcargo->setCargo($cargo);
                $tcargo->setCanthoras(0); 
                $tcargo->setCostocargo($cargo->getCostoHoraCliente());               
                $entity->addTareascargo($tcargo);
            }
        }

        
        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Tarea entity.
     *
     * @param Tarea $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Tarea $entity) {
        $form = $this->createForm(new TareaType(), $entity, array(
            'action' => $this->generateUrl('tarea_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form
                ->add(
                        'save', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.save',
                    'attr' => array('class' => 'btn btn-success col-lg-2')
                        )
                )
                ->add(
                        'saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
                        )
                )
        ;

        return $form;
    }

    /**
     * Edits an existing Tarea entity.
     *
     * @Route("/{id}", name="tarea_update")
     * @Method("PUT")
     * @Template("SistemaPresupuestoBundle:Tarea:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaPresupuestoBundle:Tarea')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tarea entity.');
        }
        
        $originalTareas = new \Doctrine\Common\Collections\ArrayCollection();

        // Create an ArrayCollection of the current Tag objects in the database
        foreach ($entity->getArticulos() as $cantArticulo) {
            $originalTareas->add($cantArticulo);
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            foreach ($originalTareas as $cantArticulo) {
                if (false === $entity->getArticulos()->contains($cantArticulo)) {
                    $cantArticulo->setTarea(null);
                    $em->remove($cantArticulo);
                }
            }
            
            //tareascargos
            if (!is_null($entity->getTareascargos())) {               
                foreach ($entity->getTareascargos() as $tareaCargo) {             
                    if ($tareaCargo->getCanthoras() == 0){
                        $entity->removeTareascargo($tareaCargo);//elimino el elemento para que no me cree un registro en 0
                    }
                }
            }
            
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked() ? $this->generateUrl('tarea_new') : $this->generateUrl('tarea_show', array('id' => $id));

            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Tarea entity.
     *
     * @Route("/{id}", name="tarea_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SistemaPresupuestoBundle:Tarea')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Tarea entity.');
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.delete.success');
        }

        return $this->redirect($this->generateUrl('tarea'));
    }

    /**
     * Creates a form to delete a Tarea entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        $mensaje = $this->get('translator')->trans('views.recordactions.confirm', array(), 'MWSimpleCrudGeneratorBundle');
        $onclick = 'return confirm("' . $mensaje . '");';

        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('tarea_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array(
                            'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                            'label' => 'views.recordactions.delete',
                            'attr' => array(
                                'class' => 'btn btn-danger col-lg-11',
                                'onclick' => $onclick,
                            )
                        ))
                        ->getForm()
        ;
    }

    /**
     * @Route("/autocomplete-forms/get-tareas-obra", name="autocomplete_get_tareas_obra")
     */
    public function getTareasbyObraAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $term = $request->query->get('q', null);
        
        $entities = $em->getRepository('SistemaPresupuestoBundle:Tarea')->tareaByObra($term);

        $array = array();

        foreach ($entities as $entity) {
            //Utilizo la función que me devuelve la suma de los cargos en la tarea
            $totalHorasTareaCargo = $entity->getCantidadReal();
            $totalTarea = $entity->getCostoTotalTarea();
            $array[] = array(
                'id'             => $entity->getId(),
                'text'           => $entity->getNombre(),                
                'cantidadMaxima' => $totalHorasTareaCargo,
                'totalTarea'     => $totalTarea,
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * @Route("/autocomplete-forms/get-tareas-mantenimiento", name="autocomplete_get_tareas_mantenimiento")
     */
    public function getTareasbyManteninmientoAction(Request $request) {
        //$this->get('security_role')->controlRolesUser();
        $term = $request->query->get('q', null);

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SistemaPresupuestoBundle:Tarea')->tareaByMantenimiento($term);

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->getNombre(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

}
