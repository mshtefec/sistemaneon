<?php

namespace Sistema\MWSCONFBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Configuracion
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\MWSCONFBundle\Entity\ConfiguracionRepository")
 */
class Configuracion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="empresa", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $empresa;

    /**
     * @var string
     *
     * @ORM\Column(name="homepage", type="text")
     */
    private $homepage;

    /**
     * @var integer
     *
     * @ORM\OneToOne(targetEntity="ImagenLogo", mappedBy="configuracion", cascade={"all"}, orphanRemoval=true)
     */
    private $imagenLogo;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param  string        $titulo
     * @return Configuracion
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set empresa
     *
     * @param  string        $empresa
     * @return Configuracion
     */
    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;

        return $this;
    }

    /**
     * Get empresa
     *
     * @return string
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * Set homepage
     *
     * @param  string        $homepage
     * @return Configuracion
     */
    public function setHomepage($homepage)
    {
        $this->homepage = $homepage;

        return $this;
    }

    /**
     * Get homepage
     *
     * @return string
     */
    public function getHomepage()
    {
        return $this->homepage;
    }

    /**
     * Set imagenLogo
     *
     * @param  \Sistema\MWSCONFBundle\Entity\ImagenLogo $imagenLogo
     * @return Configuracion
     */
    public function setImagenLogo(\Sistema\MWSCONFBundle\Entity\ImagenLogo $imagenLogo = null)
    {
        $imagenLogo->setConfiguracion($this);
        $this->imagenLogo = $imagenLogo;

        return $this;
    }

    /**
     * Get imagenLogo
     *
     * @return \Sistema\MWSCONFBundle\Entity\ImagenLogo
     */
    public function getImagenLogo()
    {
        return $this->imagenLogo;
    }
}
