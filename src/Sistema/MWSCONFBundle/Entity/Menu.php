<?php

namespace Sistema\MWSCONFBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Menu
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\MWSCONFBundle\Entity\MenuRepository")
 */
class Menu
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $url;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activo", type="boolean")
     */
    private $activo;

    /**
     * @var string
     *
     * @ORM\Column(name="orden", type="integer", length=20)
     */
    private $orden;

    /**
     * @var boolean
     *
     * @ORM\Column(name="acceso", type="boolean", nullable=true)
     */
    private $acceso;  

    /**
     * @ORM\Column(type="json_array")
     */
    private $roles = array();

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\MWSCONFBundle\Entity\MenuPadre", inversedBy="menus")
     * @ORM\JoinColumn(name="menu_id", referencedColumnName="id")
     */
    private $menupadre;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setActivo(true);
    }

    public function __toString() {
        return $this->getTitulo();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param  string $titulo
     * @return Menu
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set url
     *
     * @param  string $url
     * @return Menu
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set activo
     *
     * @param  boolean $activo
     * @return Menu
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set orden
     *
     * @param  integer $orden
     * @return Menu
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return integer
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set acceso
     *
     * @param boolean $acceso
     * @return Caja
     */
    public function setAcceso($acceso)
    {
        $this->acceso = $acceso;
    
        return $this;
    }

    /**
     * Get acceso
     *
     * @return boolean 
     */
    public function getAcceso()
    {
        return $this->acceso;
    }

    /**
     * Set roles
     *
     * @param boolean $acceso
     * @return Caja
     */
    public function addMenuRole($roles)
    {
        $this->roles = $roles;

        // allows for chaining
        return $this;
    }

    /**
     * Get roles
     *
     * @return array 
     */
    public function getMenuRole()
    {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * Set roles
     *
     * @param array $roles
     * @return Menu
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;
    
        return $this;
    }

    /**
     * Get roles
     *
     * @return array 
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Set menupadre
     *
     * @param \Sistema\MWSCONFBundle\Entity\MenuPadre $menupadre
     * @return Menu
     */
    public function setMenupadre(\Sistema\MWSCONFBundle\Entity\MenuPadre $menupadre = null)
    {
        $this->menupadre = $menupadre;
    
        return $this;
    }

    /**
     * Get menupadre
     *
     * @return \Sistema\MWSCONFBundle\Entity\MenuPadre 
     */
    public function getMenupadre()
    {
        return $this->menupadre;
    }
}