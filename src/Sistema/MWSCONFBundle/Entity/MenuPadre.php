<?php

namespace Sistema\MWSCONFBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * MenuPadre
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\MWSCONFBundle\Entity\MenuPadreRepository")
 */
class MenuPadre
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $titulo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activo", type="boolean")
     */
    private $activo;

    /**
     * @var string
     *
     * @ORM\Column(name="orden", type="integer", length=20)
     */
    private $orden;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\MWSCONFBundle\Entity\Menu", mappedBy="menupadre", cascade={"persist"})
     */
    private $menus;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setActivo(true);
    }

    public function __toString() {
        return $this->getTitulo();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param  string $titulo
     * @return MenuPadre
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set activo
     *
     * @param  boolean $activo
     * @return MenuPadre
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set orden
     *
     * @param  integer $orden
     * @return MenuPadre
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return integer
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Add menus
     *
     * @param \Sistema\MWSCONFBundle\Entity\Menu $menus
     * @return MenuPadre
     */
    public function addMenu(\Sistema\MWSCONFBundle\Entity\Menu $menus)
    {
        $menus->setMenupadre($this);
        $this->menus[] = $menus;
    
        return $this;
    }

    /**
     * Remove menus
     *
     * @param \Sistema\MWSCONFBundle\Entity\Menu $menus
     */
    public function removeMenu(\Sistema\MWSCONFBundle\Entity\Menu $menus)
    {
        $this->menus->removeElement($menus);
    }

    /**
     * Get menus
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMenus()
    {
        return $this->menus;
    }
}