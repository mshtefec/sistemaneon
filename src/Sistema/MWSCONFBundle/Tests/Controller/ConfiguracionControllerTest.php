<?php

namespace Sistema\MWSCONFBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class ConfiguracionControllerTest extends WebTestCase
{
    public function testCompleteScenario()
    {
        // login user client
        $client = $this->logIn();

        // Create a new entry in the database
        $crawler = $client->request('GET', '/superadmin/configuracion/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET /superadmin/configuracion/");
        $crawler = $client->click($crawler->selectLink('Crear Configuracion')->link());

        // Fill in the form and submit it
        $form = $crawler->selectButton('Guardar')->form(array(
            'sistema_mwsconfbundle_configuracion[titulo]'   => 'Test',
            'sistema_mwsconfbundle_configuracion[empresa]'  => 'Test',
            'sistema_mwsconfbundle_configuracion[homepage]' => 'Test',
            // ... other fields to fill
        ));

        $client->submit($form);
        $crawler = $client->followRedirect();

        // Check data in the show view
        $this->assertGreaterThan(0, $crawler->filter('span:contains("Test")')->count(), 'Missing element span:contains("Test")');

        // Edit the entity
        $crawler = $client->click($crawler->selectLink('Editar')->link());

        $form = $crawler->selectButton('Guardar')->form(array(
            'sistema_mwsconfbundle_configuracion[titulo]'   => 'Update',
            'sistema_mwsconfbundle_configuracion[empresa]'  => 'Update',
            'sistema_mwsconfbundle_configuracion[homepage]' => 'Update',
            // ... other fields to fill
        ));

        $client->submit($form);
        $crawler = $client->followRedirect();

        // Check the element contains an attribute with value equals "Update"
        $this->assertGreaterThan(0, $crawler->filter('span:contains("Update")')->count(), 'Missing element span:contains("Update")');

        // Delete the entity
        $client->submit($crawler->selectButton('Eliminar')->form());
        $crawler = $client->followRedirect();

        // Check the entity has been delete on the list
        $this->assertNotRegExp('/Update/', $client->getResponse()->getContent());
    }

    private function logIn()
    {
        // Create a new client to browse the application
        $client = static::createClient();

        $session = $client->getContainer()->get('session');

        $firewall = 'secured_area';
        $token = new UsernamePasswordToken('superadmin', null, $firewall, array('ROLE_SUPER_ADMIN'));
        $session->set('_security_'.$firewall, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $client->getCookieJar()->set($cookie);

        return $client;
    }
}
