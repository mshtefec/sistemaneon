<?php

namespace Sistema\MWSCONFBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Template()
     */
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        if ($session->get('logoWebPath')) {
            $session->set('logoWebPath', null);
        }
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('SistemaMWSCONFBundle:Configuracion')->findAll();

        if (empty($entity)) {
            $entity[0]['conf'] = "Configurar sistema";
        }

        return array(
            'entity' => $entity[0]
        );
    }

    public function logoAction()
    {
        $session = $this->getRequest()->getSession();

        if (!$session->get('logoWebPath')) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SistemaMWSCONFBundle:Configuracion')->findAll();

            if (empty($entity)) {
                $session->set('logoWebPath', 'bundles/sistemamwsconf/images/logo.jpg');
            } else {
                if ($entity[0]->getImagenlogo() != null) {
                    $session->set('logoWebPath', $entity[0]->getImagenlogo()->getWebPath());
                } else {
                    $session->set('logoWebPath', 'bundles/sistemamwsconf/images/logo.jpg');
                }
            }
        }

        return $this->render(
            'SistemaMWSCONFBundle:Default:logo.html.twig',
            array(
                'logoWebPath' => $session->get('logoWebPath')
            )
        );
    }
}
