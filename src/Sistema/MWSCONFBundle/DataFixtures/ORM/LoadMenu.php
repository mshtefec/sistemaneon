<?php

namespace Sistema\MWSCONFBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Sistema\MWSCONFBundle\Entity\Menu;

class LoadMenu extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $menues = array(
            '0' => array(
                'titulo' => 'Configuracion',
                'activo' => true,
                'url' => 'superadmin_configuracion',
                'orden' => 1,
                'roles' => null,
                'acceso' => false,
            ),
            '1' => array(
                'titulo' => 'Menu',
                'activo' => true,
                'url' => 'superadmin_menu',
                'orden' => 2,
                'roles' => null,
                'acceso' => false,
            ),
            '2' => array(
                'titulo' => 'Roles',
                'activo' => true,
                'url' => 'role',
                'orden' => 3,
                'roles' => null,
                'acceso' => false,
            ),
            '3' => array(
                'titulo' => 'Usuarios',
                'activo' => true,
                'url' => 'user',
                'orden' => 4,
                'roles' => array($this->getReference('ROLE_ADMIN')),
                'acceso' => false,
            ),
            '4' => array(
                'titulo' => 'Clientes',
                'activo' => true,
                'url' => 'admin_cliente',
                'orden' => 5,
                'roles' => array($this->getReference('ROLE_ADMIN'), $this->getReference('ROLE_CLIENTES')),
                'acceso' => false,
            ),
            '5' => array(
                'titulo' => 'Empleados',
                'activo' => true,
                'url' => 'admin_empleado',
                'orden' => 6,
                'roles' => array($this->getReference('ROLE_ADMIN'), $this->getReference('ROLE_EMPLEADOS')),
                'acceso' => false,
            ),
            '6' => array(
                'titulo' => 'Rubros',
                'activo' => true,
                'url' => 'admin_rubro',
                'orden' => 9,
                'roles' => array($this->getReference('ROLE_ADMIN'), $this->getReference('ROLE_RUBROS')),
                'acceso' => false,
            ),
            '7' => array(
                'titulo' => 'Articulos',
                'activo' => true,
                'url' => 'admin_articulo',
                'orden' => 10,
                'roles' => array($this->getReference('ROLE_ADMIN'), $this->getReference('ROLE_ARTICULOS')),
                'acceso' => false,
            ),
            '8' => array(
                'titulo' => 'Cargo de Empleado',
                'activo' => true,
                'url' => 'admin_empleadocargo',
                'orden' => 7,
                'roles' => array($this->getReference('ROLE_ADMIN'), $this->getReference('ROLE_EMPLEADOS')),
                'acceso' => false,
            ),
            '9' => array(
                'titulo' => 'Tipo de Empleado',
                'activo' => true,
                'url' => 'admin_empleadotipo',
                'orden' => 8,
                'roles' => array($this->getReference('ROLE_ADMIN'), $this->getReference('ROLE_EMPLEADOS')),
                'acceso' => false,
            ),
            '10' => array(
                'titulo' => 'Medidas para Articulos',
                'activo' => true,
                'url' => 'admin_medida',
                'orden' => 12,
                'roles' => array($this->getReference('ROLE_ADMIN'), $this->getReference('ROLE_ARTICULOS')),
                'acceso' => false,
            ),
            '11' => array(
                'titulo' => 'Orden',
                'activo' => true,
                'url' => 'admin_orden',
                'orden' => 15,
                'roles' => array($this->getReference('ROLE_ADMIN'), $this->getReference('ROLE_ORDEN')),
                'acceso' => false,
            ),
            '12' => array(
                'titulo' => 'Tipos de Articulos',
                'activo' => true,
                'url' => 'admin_articulotipo',
                'orden' => 11,
                'roles' => array($this->getReference('ROLE_ADMIN'), $this->getReference('ROLE_ARTICULOS')),
                'acceso' => false,
            ),
            '13' => array(
                'titulo' => 'Herramientas',
                'activo' => true,
                'url' => 'admin_herramienta',
                'orden' => 13,
                'roles' => array($this->getReference('ROLE_ADMIN'), $this->getReference('ROLE_DEPOSITO')),
                'acceso' => false,
            ),
            '14' => array(
                'titulo' => 'Entregas de Herramientas',
                'activo' => true,
                'url' => 'admin_entregasherramientas',
                'orden' => 14,
                'roles' => array($this->getReference('ROLE_ADMIN'), $this->getReference('ROLE_DEPOSITO')),
                'acceso' => false,
            ),
            '15' => array(
                'titulo' => 'Reportes',
                'activo' => true,
                'url' => 'admin_reporte',
                'orden' => 17,
                'roles' => array($this->getReference('ROLE_ADMIN')),
                'acceso' => false,
            ),
            '16' => array(
                'titulo' => 'Reporte cantidad orden por cliente',
                'activo' => true,
                'url' => 'admin_reporte_cantidad_orden_por_cliente',
                'orden' => 18,
                'roles' => array($this->getReference('ROLE_ADMIN')),
                'acceso' => true,
            ),
            '17' => array(
                'titulo' => 'Reporte articulos mas usados',
                'activo' => true,
                'url' => 'admin_reporte_articulo_mas_usados',
                'orden' => 19,
                'roles' => array($this->getReference('ROLE_ADMIN')),
                'acceso' => true,
            ),
            '18' => array(
                'titulo' => 'Proveedor',
                'activo' => true,
                'url' => 'admin_proveedor',
                'orden' => 20,
                'roles' => array($this->getReference('ROLE_ADMIN'), $this->getReference('ROLE_PROVEEDOR')),
                'acceso' => false,
            ),
            '19' => array(
                'titulo' => 'Comprobante',
                'activo' => true,
                'url' => 'admin_comprobante',
                'orden' => 21,
                'roles' => array($this->getReference('ROLE_ADMIN')),
                'acceso' => false,
            ),
            '20' => array(
                'titulo' => 'Cuenta Corriente',
                'activo' => true,
                'url' => 'cuentacorriente',
                'orden' => 22,
                'roles' => array($this->getReference('ROLE_ADMIN')),
                'acceso' => false,
            ),
            '21' => array(
                'titulo' => 'Factura Compra',
                'activo' => true,
                'url' => 'admin_facturacompra',
                'orden' => 23,
                'roles' => array($this->getReference('ROLE_ADMIN')),
                'acceso' => false,
            ),
            '22' => array(
                'titulo' => 'Factura Venta',
                'activo' => true,
                'url' => 'admin_facturaventa',
                'orden' => 24,
                'roles' => array($this->getReference('ROLE_ADMIN')),
                'acceso' => false,
            ),
            '23' => array(
                'titulo' => 'Recibo Compra',
                'activo' => true,
                'url' => 'admin_recibocompra',
                'orden' => 25,
                'roles' => array($this->getReference('ROLE_ADMIN')),
                'acceso' => false,
            ),
            '24' => array(
                'titulo' => 'Recibo Venta',
                'activo' => true,
                'url' => 'admin_reciboventa',
                'orden' => 26,
                'roles' => array($this->getReference('ROLE_ADMIN')),
                'acceso' => false,
            ),
            '25' => array(
                'titulo' => 'Sueldos',
                'activo' => true,
                'url' => 'admin_sueldo_generar',
                'orden' => 27,
                'roles' => array($this->getReference('ROLE_ADMIN')),
                'acceso' => false,
            ),
            '26' => array(
                'titulo' => 'Tarea',
                'activo' => true,
                'url' => 'tarea',
                'orden' => 28,
                'roles' => array($this->getReference('ROLE_ADMIN')),
                'acceso' => false,
            ),
            '27' => array(
                'titulo' => 'Presupuesto',
                'activo' => true,
                'url' => 'admin_presupuesto',
                'orden' => 29,
                'roles' => array($this->getReference('ROLE_ADMIN')),
                'acceso' => false,
            ),
        );

        foreach ($menues as $key => $m) {
            $menu = new Menu();
            $menu->setTitulo($m['titulo']);
            $menu->setActivo($m['activo']);
            $menu->setUrl($m['url']);
            if (!is_null($m['roles'])) {
                foreach ($m['roles'] as $r) {
                    $menu->addMenuRole($r);
                }
            }
            $menu->setOrden($m['orden']);
            $menu->setAcceso($m['acceso']);
            $manager->persist($menu);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 3;
    }

    public function setContainer(ContainerInterface $container = null)
    {
    }

}
