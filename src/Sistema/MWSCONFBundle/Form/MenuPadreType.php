<?php

namespace Sistema\MWSCONFBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * MenuPadreType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class MenuPadreType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titulo')
            ->add('activo')
            ->add('orden')
            ->add('menus', 'select2', array(
                'attr' => array(
                    'class' => 'col-lg-12',
                ),
                'required' => false,
                'class' => 'Sistema\MWSCONFBundle\Entity\Menu',
                'url' => 'autocomplete_get_menu',
                'configs' => array(
                    'multiple' => true,
                )
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\MWSCONFBundle\Entity\MenuPadre'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_mwsconfbundle_menupadre';
    }
}
