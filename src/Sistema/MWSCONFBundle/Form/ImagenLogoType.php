<?php

namespace Sistema\MWSCONFBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ImagenLogoType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'file', 'document_file', array(
                    'required'  => false,
                    'file_path' => 'webPath',
                    'label'     => 'Logo'
                )
            )
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\MWSCONFBundle\Entity\ImagenLogo'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_mwsconfbundle_imagenlogo';
    }
}
