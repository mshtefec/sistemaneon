<?php

namespace Sistema\MWSCONFBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Sistema\FACTURACIONBundle\Entity\ReciboVenta;
use Sistema\FACTURACIONBundle\Entity\FacturaVenta;
use Sistema\FACTURACIONBundle\Entity\ReciboCompra;
use Sistema\FACTURACIONBundle\Entity\FacturaCompra;
use Sistema\STOCKBundle\Entity\Rubro;

/**
 * Twig Extension.
 *
 * @author Gonzalo Alonso <gonkpo@gmail.com>
 */
class Twig extends \Twig_Extension {

    protected $container;

    public function __construct(ContainerInterface $container = null) {
        $this->container = $container;
    }

    public function getFunctions() {
        return array(
            'isActive' => new \Twig_Function_Method($this, 'isActive', array(
                'is_safe' => array('html')
                    )),
            'dateDiff' => new \Twig_Function_Method($this, 'dateDiff', array(
                'is_safe' => array('html')
                    )),
            'segunComprobante' => new \Twig_Function_Method($this, 'segunComprobante', array(
                'is_safe' => array('html')
                    )),
            'getFormat' => new \Twig_Function_Method($this, 'getFormat', array(
                'is_safe' => array('html')
                    )),
            'getDecimal' => new \Twig_Function_Method($this, 'getDecimal', array(
                'is_safe' => array('html')
                    )),
        );
    }

    public function dateDiff($desde, $hasta) {
        $timeInterval = $desde->diff($hasta);
        $intervalInSeconds = (new \DateTime())->setTimeStamp(0)->add($timeInterval)->getTimeStamp();
        $intervalInMinutes = $intervalInSeconds / 60;
        $totalIntervalo = $intervalInMinutes / 60;
        return ((float) $totalIntervalo);
    }

    public function isActive($active, $title_true = null, $title_false = null) {
        if ($active) {
            if ($title_true) {
                $res = '<i style="color: green;" class="glyphicon glyphicon-ok-circle tooltips" rel="tooltip" title="' . $title_true . '"></i>';
            } else {
                $res = '<i style="color: green;" class="glyphicon glyphicon-ok-circle"></i>';
            }
        } else {
            if ($title_false) {
                $res = '<i style="color: darkred;" class="glyphicon glyphicon-ban-circle tooltips" rel="tooltip" title="' . $title_false . '"></i>';
            } else {
                $res = '<i style="color: darkred;" class="glyphicon glyphicon-ban-circle"></i>';
            }
        }

        return $res;
    }

    public function getDecimal($time) {
        $date = new \DateTime("01-01-1970".$time->format('H:i'));

        $intervalInSeconds = $date->getTimeStamp();
        $intervalInMinutes = $intervalInSeconds / 60;
        $totalIntervalo = $intervalInMinutes / 60;
        $cantidadHoras = ((float) $totalIntervalo);
        $decimal = $cantidadHoras - 3;
        return $decimal;
    }

    public function getFormat($key, $dato) {
        if ($dato instanceof \DateTime) {
            $valor = date_format($dato, 'Y-m-d');
        } else if (is_array($dato)) {
            foreach ($dato as $d) {
                $valor = "<a class='glyphicon glyphicon-search tooltips verDetalle'   href='javascript:;' data-entity='" . $key . "' data-id='" . $d . "'"
                        . "title='' rel='tooltip' data-original-title='Ver detalle'></a>";
            }
        } else if (is_bool($dato)) {
            $valor = $this->isActive($dato);
        } else {
            $valor = $dato;
        }
        return $valor;
    }

    public function segunComprobante($comprobante) {
        if ($comprobante instanceof ReciboVenta) {
            $href = "reciboventa";
        } elseif ($comprobante instanceof FacturaVenta) {
            $href = "facturaventa";
        } elseif ($comprobante instanceof ReciboCompra) {
            $href = "recibocompra";
        } elseif ($comprobante instanceof FacturaCompra) {
            $href = "facturacompra";
        } else {
            $href = null;
        }

        if (is_null($href)) {
            $res = null;
        } else {
            $ruta = 'admin_' . $href . '_show';
            $url = $this->container->get('router')->generate(
                    $ruta, array('id' => $comprobante->getId())
            );
            $res = "<a class='glyphicon glyphicon-search tooltips' href='" . $url . "' title='' rel='tooltip' data-original-title='Ver' target='_blank'></a> OT nro. " . $comprobante->getOrden() . " | " . $comprobante;
        }

        return $res;
    }

    public function getName() {
        return 'twig.extension';
    }

}
