<?php

namespace Sistema\STOCKBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Sistema\STOCKBundle\Entity\Medida;

class loadArticuloMedida extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
     /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $values = array (
            '0' => array('nombre' => 'UNIDAD'),
            '1' => array('nombre' => 'METROS'),
        );

        foreach ($values as $key => $value) {
            $medida = new Medida();
            $medida->setNombre($value['nombre']);
            $manager->persist($medida);
        }

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 4; // the order in which fixtures will be loaded
    }

}
