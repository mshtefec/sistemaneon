<?php

namespace Sistema\STOCKBundle\EventListener;

use Symfony\Component\Security\Core\Event\AuthenticationEvent;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
// utilizo para crear la orden de salida desde la orden
use Sistema\STOCKBundle\Entity\OrdenSalida;
use Sistema\STOCKBundle\Entity\CantOrdenSalida;
// utilizo para crear el asiento de la orden
use Sistema\FACTURACIONBundle\Entity\Asiento;
use Sistema\STOCKBundle\Entity\CantAddArticulo;
use Sistema\STOCKBundle\Entity\CantOrden;

class OrdenListener {

    /**
     * @var string
     */
    protected $em;

    /**
     * @param SecurityContext $securityContext
     * @param Router $router The router
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }

    public function create($entity, $form = null) {
        $cantidadHoras = 0;
        $totalhoras    = 0;
        $costoHora     = 0;
        $totalArticulo = 0;
        $seguir        = true;
        $articulos       = array();
        $articulos_count = array();
        $arrayResultado  = array();
        $arraySession    = array();
        //TRY By Rodri
        try {
            //revisa si el articulo tiene precio
            $arrayResultado["resul"] = 'saveAndAdd';
            $arrayResultado["reedirigir"] = 'admin_orden_create';
            foreach ($entity->getArticulos()->getValues() as $articulo) {
                if ($articulo->getArticulo()->getPrecio() == 0) {
                    $arraySession[] = array(
                        "tipo"      => "danger",
                        "contenido" => "El articulo " . $articulo->getArticulo() . ' no posee Precio !!'
                    );
                    $arrayResultado["reedirigir"] = 'admin_orden_new';
                    $arrayResultado["resultado"] = false;
                    $seguir = false;
                    break;
                }
                $articulos[] = $articulo->getArticulo()->getRubro() . $articulo->getArticulo()->getTipo() . $articulo->getArticulo()->getDescripcion();
                $articulo->setPrecio($articulo->getArticulo()->getPrecio());
            }
            if ($seguir) {
                //pone como keys los valores y cuenta cuantas veces se repiten en el array
                //controla que no haya articulos repetidos
                $articulos_count = array_count_values($articulos);

                foreach ($articulos_count as $key => $articulo) {
                    if ($articulo > 1) {
                        // return $this->redirect($this->generateUrl('admin_orden_new'));
                        $arraySession[] = array(
                            "tipo"      => "danger",
                            "contenido" => 'El articulo ' . $key . ' esta repetido por favor solo ingrese un articulo'
                        );
                        $arrayResultado["reedirigir"] = 'admin_orden_new';
                        $seguir = false;
                        $arrayResultado["resultado"] = false;
                        break;
                    }
                }
                if ($seguir) {
                    //calcula el costo del empleado
                    foreach ($entity->getEmpleados() as $empleado) {
                        $costoHora = $empleado->getEmpleado()->getCargo()->getCostoHora();
                        //verifico el tipo de funcion
                        $funcion = $entity->getFuncion();
                        
                        //si funcion ==  
                        //  0 MANTENIMIENTO
                        //  1 OBRA
                        //  2 URGENCIA
                        if ($funcion == 0){
                            $costoHoraCliente = $empleado->getEmpleado()->getCargo()->getCostoHoraMantenimiento();
                        } elseif ($funcion == 1) {
                            $costoHoraCliente = $empleado->getEmpleado()->getCargo()->getCostoHoraCliente();
                        } else {                
                            $costoHoraCliente = $empleado->getEmpleado()->getCargo()->getCostoHoraUrgencia();
                        }

                        $empleado->setCostoHoraCliente($costoHoraCliente);
                        foreach ($empleado->getEmpleadoHoras() as $horas) {
                            //Si es nulo el costoHora entro y seteo
                            if (is_null($horas->getCostoHora())) {
                                $horas->setCostoHora($costoHora);
                            }
                            //Si desde o hasta es null ERROR
                            if (is_null($horas->getDesde()) || is_null($horas->getHasta())) {
                                $arraySession[] = array(
                                    "tipo"      => "danger",
                                    "contenido" => 'Error el rango de horarios en el empleado ' . $empleado->getEmpleado()->getApellido() . ' ' . $empleado->getEmpleado()->getNombre() . ' es incorrecto.'
                                );
                                $arrayResultado["reedirigir"] = 'admin_orden_create';
                                $seguir = false;
                                $arrayResultado["resultado"] = false;
                                break;
                            } else {
                                //Si existe empleadoHora con la fecha y horario ERROR
                                $fechaFechaHoraEmpleado = $this->existeFechaHoraEmpleado($horas);
                                if ($fechaFechaHoraEmpleado) {
                                    $arraySession[] = array(
                                        "tipo" => "danger",
                                        "contenido" => 'Error el rango de fecha ' . $fechaFechaHoraEmpleado . ' en el empleado ' . $empleado->getEmpleado()->getApellido() . ' ' . $empleado->getEmpleado()->getNombre() . ' es repetido.'
                                    );
                                    $arrayResultado["reedirigir"] = 'admin_orden_create';
                                    $seguir = false;
                                    $arrayResultado["resultado"] = false;
                                    break;
                                } else {
                                    //Si todo esta bien calculo
                                    $timeInterval = $horas->getDesde()->diff($horas->getHasta());
                                    $intervalInSeconds = (new \DateTime())->setTimeStamp(0)->add($timeInterval)->getTimeStamp();
                                    $intervalInMinutes = $intervalInSeconds / 60;
                                    $totalIntervalo = $intervalInMinutes / 60;
                                    $cantidadHoras += ((float) $totalIntervalo);
                                }
                            }
                        }
                        if ($seguir) {
                            $totalhoras += ($empleado->getCostoHoraCliente() * $cantidadHoras);
                            $cantidadHoras = 0;
                        }
                    }

                    if ($seguir) {
                        foreach ($entity->getArticulos() as $articulo) {
                            $totalArticulo = ($articulo->getCantidad() * $articulo->getPrecio()) + $totalArticulo;
                            if ($entity->getReservaMateriales()) {
                                $articulo->getArticulo()->setCantidadReservada($articulo->getCantidad() + $articulo->getArticulo()->getCantidadReservada());
                                $articulo->setCantidadReservada($articulo->getCantidad());
                            }
                        }

                        $entity->setMontoArticulos($totalArticulo);
                        $entity->setMontoEmpleados($totalhoras);
                        $entity->setMontoOrden($totalhoras + $totalArticulo);

                        //obtengo cuenta corriente del cliente
                        $cuentaCorriente = $entity->getCliente()->getCuentaCorriente();
                        //obtengo las cuenta settings
                        $valor = current($this->em->getRepository('SistemaMWSCONFBundle:Setting')->findByClave("CuentaOrden"))->getValor();
                        $valorManoDeObra = current($this->em->getRepository('SistemaMWSCONFBundle:Setting')->findByClave("cuentaManoDeObra"))->getValor();
                        //obtengo las cuentas
                        $cuenta = $this->em->getRepository('SistemaPlanDeCuentasBundle:Cuenta')->find($valor);
                        $cuentaManoDeObra = $this->em->getRepository('SistemaPlanDeCuentasBundle:cuenta')->find($valorManoDeObra);
                        //seteo montoOrden con descuento
                        $montoOrden = $entity->getMontoOrden() - ($entity->getMontoOrden() / 100) * $entity->getDescuentoOrden();
                        //descuento por separado
                        $descuento = ($entity->getMontoOrden() / 100) * $entity->getDescuentoOrden();
                        
                        $montoHoras = 0;
                        $montoArticulo = 0;
                        //Control del aplique de descuento
                        if ($totalhoras > 0) {
                            if ($totalhoras >= $descuento) {
                                $montoHoras = $totalhoras - $descuento;    
                            } else {
                                $descuento = $descuento - $totalhoras;
                                $montoHoras = 0;    
                            }
                        } 
                        if ($totalArticulo > 0 && $descuento > 0) {
                            $montoArticulo = $totalArticulo - $descuento;
                        } else {
                            $montoArticulo = $totalArticulo;
                        }//fin del control descuento

                        //creo asiento debe
                        $asiento = new Asiento();
                        $asiento->setCuenta($cuenta);
                        $asiento->setFecha($entity->getFecha());
                        $asiento->setHaber($montoOrden);
                        $entity->addAsiento($asiento);
                        $cuentaCorriente->addAsiento($asiento);
                        //vinculo con cuenta empresa
                        $cuentaEmpresa = $this->em->getRepository('SistemaFACTURACIONBundle:CuentaEmpresa')->find(1);
                        
                        if ($cuentaEmpresa) {
                            //creo asiento para montoArticulo
                            $asiento2 = new Asiento();
                            //creo asiento para montoHoras
                            $asiento22 = new Asiento();
                            $asiento2->setCuenta($cuenta);
                            $asiento22->setCuenta($cuentaManoDeObra);
                            $asiento2->setDebe($montoArticulo);
                            $asiento22->setDebe($montoHoras);
                            $asiento2->setFecha($entity->getFecha());
                            $asiento22->setFecha($entity->getFecha());
                            $entity->addAsiento($asiento2);
                            $entity->addAsiento($asiento22);
                            $cuentaEmpresa->addAsiento($asiento2);
                            $cuentaEmpresa->addAsiento($asiento22);

                            //$this->get('session')->getFlashBag()->add('success', 'flash.create.success');
                            //si clickeo generar orden de salida entra.
                            if (!is_null($form)) {
                                if ($form->get('saveAndGenerarSalida')->isClicked()) {
                                    $cantOrden = $entity->getArticulos()->getValues();
                                    if (is_null($form->get('empleado_show')->getData())) {
                                        $this->get('session')->getFlashBag()->add('danger', 'La orden no posee responsable, no se pudo completar la salida');
                                    } elseif (empty($cantOrden)) {
                                        $this->get('session')->getFlashBag()->add('danger', 'La Orden no posee articulos, no se puede realizar una Salida');
                                    } else {
                                        //creo orden de salida.
                                        $ordenSalida = new OrdenSalida();
                                        $ordenSalida->setEmpleado($form->get('empleado_show')->getData());
                                        //recorro los articulos de la orden que serian las cantOrden y creo las CantOrdenSalida.
                                        foreach ($cantOrden as $co) {
                                            $articulo = $co->getArticulo();
                                            $cantOrdenSalida = new CantOrdenSalida();
                                            $cantOrdenSalida->setCantidad($co->getCantidad());
                                            $cantOrdenSalida->setArticulo($articulo);
                                            $ordenSalida->addArticulo($cantOrdenSalida);
                                            //actualizo la cantidad utilizada del articulo.
                                            $articulo->setCantidadUtilizada($articulo->getCantidadUtilizada() + $co->getCantidad());
                                            if ($entity->getReservaMateriales()) {
                                                $co->setCantidadReservada($co->getCantidad());
                                                $articulo->setCantidadReservada($co->getCantidad());
                                            }
                                            $this->em->persist($articulo);
                                        }
                                        //$this->get('session')->getFlashBag()->add('info', 'Se genero la orden de salida con todos los articulos.');
                                        // guardo la orden de salida generada.
                                        $entity->addOrdenSalida($ordenSalida);
                                    }
                                    $arrayResultado["resul"] = 'saveAndGenerarSalida';
                                }
                            }
                            $this->em->persist($entity);
                            $this->em->flush();
                            $arrayResultado["resultado"] = true;
                        } else {
                            $arraySession[] = array(
                                "tipo" => "danger",
                                "contenido" => 'Error Usted debe crear la Cuenta Empresa'
                            );
                            $arrayResultado["resultado"] = false;
                        }
                    }
                }
            }
            $arrayResultado["session"] = $arraySession;
        } catch (Exception $e) {
            $arrayResultado["resultado"] = false;
        }
        return $arrayResultado;
    }

    /* Si retorna FECHA es porque encontro una fecha y hora repetida ERROR */
    public function existeFechaHoraEmpleado($horas) {
        $fechaHoraEmpleado = $this->em->getRepository('SistemaSTOCKBundle:EmpleadoHora')
            ->obtenerFechaHoraEmpleado($horas);

        return $fechaHoraEmpleado;
    }

    public function actualizarArticulos($entity) {
        $existe = false;
        if (!is_null($entity->getArticulos())) {
            //recorro cant add articulo
            foreach ($entity->getArticulos() as $articulo) {
                if (!is_null($entity->getOrden())) { // si setea una orden entra.
                    foreach ($entity->getOrden()->getArticulos()->getValues() as $cantOrden) {
                        // si existe el articulo en la orden entra y actualizo la cantidad de la orden.
                        if ($cantOrden->getArticulo()->getId() == $articulo->getArticulo()->getId()) {
                            $cantOrden->setCantidad($cantOrden->getCantidad() + $articulo->getCantidad());

                            if ($entity->getOrden()->getReservaMateriales()) {
                                $cantOrden->setCantidadReservada($articulo->getCantidad() + $cantOrden->getCantidadReservada());
                                $articulo->getArticulo()->setCantidadReservada($articulo->getArticulo()->getCantidadReservada() + $articulo->getCantidad());
                            }
                            $this->em->persist($cantOrden);
                            $existe = true;
                            break;
                            // no actualizo la cantidad utilizada porque deberia ser una salida de materiales.
                            // $articulo->getArticulo()->setCantidadUtilizada($articulo->getArticulo()->getCantidadUtilizada() + $articulo->getCantidad());
                        }
                    }
                    if (!$existe) {
                        $cantOrden = new CantOrden();
                        $cantOrden->setArticulo($articulo->getArticulo());
                        $cantOrden->setCantidad($articulo->getCantidad());
                        $cantOrden->setPrecio($articulo->getArticulo()->getPrecio());
                        $cantOrden->setOrden($entity->getOrden());
                        $entity->getOrden()->addArticulo($cantOrden);
                    }
                }
                // actualizo la cantidad del articulo.

                $articulo->getArticulo()->setCantidad($articulo->getCantidad() + $articulo->getArticulo()->getCantidad());
                $this->em->persist($articulo);
            }
        }
    }
}
