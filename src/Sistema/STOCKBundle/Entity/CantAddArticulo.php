<?php

namespace Sistema\STOCKBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CantAddArticulo
 *
 * @ORM\Table()
 * @ORM\Entity()
 */
class CantAddArticulo {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Sistema\STOCKBundle\Entity\Articulo")
     * @ORM\JoinColumn(name="articulo_id", referencedColumnName="id")
     *
     */
    private $articulo;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Sistema\STOCKBundle\Entity\AddArticulos", inversedBy="articulos")
     * @ORM\JoinColumn(name="addArticulo_id", referencedColumnName="id")
     *
     */
    private $addArticulo;

    /**
     * @var integer
     *
     * @ORM\Column(name="cantidad", type="integer")
     */
    private $cantidad;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\FACTURACIONBundle\Entity\FacturaCompra", inversedBy="articulos")
     * @ORM\JoinColumn(name="facturaCompra_id", referencedColumnName="id")
     * */
    private $facturaCompra;

    /**
     * @var double
     *
     * @ORM\Column(name="precio", type="float")
     */
    private $precio;

    public function __construct() {
        $this->precio = 0;
        $this->cantidad = 0;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set cantidad
     *
     * @param  integer         $cantidad
     * @return CantAddArticulo
     */
    public function setCantidad($cantidad) {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer
     */
    public function getCantidad() {
        return $this->cantidad;
    }

    /**
     * Set articulo
     *
     * @param  \Sistema\STOCKBundle\Entity\Articulo $articulo
     * @return CantAddArticulo
     */
    public function setArticulo(\Sistema\STOCKBundle\Entity\Articulo $articulo = null) {
        $this->articulo = $articulo;

        return $this;
    }

    /**
     * Get articulo
     *
     * @return \Sistema\STOCKBundle\Entity\Articulo
     */
    public function getArticulo() {
        return $this->articulo;
    }

    /**
     * Set addArticulo
     *
     * @param  \Sistema\STOCKBundle\Entity\AddArticulos $addArticulo
     * @return CantAddArticulo
     */
    public function setAddArticulo(\Sistema\STOCKBundle\Entity\AddArticulos $addArticulo = null) {
        $this->addArticulo = $addArticulo;

        return $this;
    }

    /**
     * Get addArticulo
     *
     * @return \Sistema\STOCKBundle\Entity\AddArticulos
     */
    public function getAddArticulo() {
        return $this->addArticulo;
    }

    /**
     * Set facturaCompra
     *
     * @param \Sistema\FACTURACIONBundle\Entity\FacturaCompra $facturaCompra
     * @return CantAddArticulo
     */
    public function setFacturaCompra(\Sistema\FACTURACIONBundle\Entity\FacturaCompra $facturaCompra = null) {
        $this->facturaCompra = $facturaCompra;

        return $this;
    }

    /**
     * Get facturaCompra
     *
     * @return \Sistema\FACTURACIONBundle\Entity\FacturaCompra 
     */
    public function getFacturaCompra() {
        return $this->facturaCompra;
    }

    /**
     * Set precio
     *
     * @param float $precio
     * @return CantAddArticulo
     */
    public function setPrecio($precio) {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return float 
     */
    public function getPrecio() {
        return $this->precio;
    }

}
