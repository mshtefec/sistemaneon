<?php

namespace Sistema\STOCKBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CantOrdenSalida
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\STOCKBundle\Entity\CantOrdenSalidaRepository")
 */
class CantOrdenSalida
{
    private $cantidadMaxima;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cantidad", type="integer")
     * @Assert\Type(type="integer", message="Solo permite numeros")
     * @Assert\Regex(pattern="/^[0-9]\d*$/")
     */
    private $cantidad;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Sistema\STOCKBundle\Entity\OrdenSalida", inversedBy="articulos")
     * @ORM\JoinColumn(name="orden_salida_id", referencedColumnName="id")
     *
     */
    private $ordenSalida;

    /**
     * @var integer
     *
     *  @ORM\ManyToOne(targetEntity="Sistema\STOCKBundle\Entity\Articulo")
     *  @ORM\JoinColumn(name="articulo_id", referencedColumnName="id")
     */
    private $articulo;

    /**
     * @Assert\True(message = "La cantidad debe ser menor a la cantidad maxima.")
     */
    public function isCantidades()
    {
        // return true or false
        if ($this->getCantidad() != 0) {
            $total = $this->getCantidadMaxima() - $this->getCantidad();
            if ($total < 0) {
                return false;
            }
        }

        return true;
    }

    /**
     * Set cantidad maxima
     */
    public function setCantidadMaxima($cantidadMaxima)
    {
        $this->cantidadMaxima = $cantidadMaxima;
    }

    /**
     * Get cantidad maxima
     */
    public function getCantidadMaxima()
    {
        return $this->cantidadMaxima;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cantidad
     *
     * @param  integer         $cantidad
     * @return CantOrdenSalida
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set ordenSalida
     *
     * @param  \Sistema\STOCKBundle\Entity\OrdenSalida $ordenSalida
     * @return CantOrdenSalida
     */
    public function setOrdenSalida(\Sistema\STOCKBundle\Entity\OrdenSalida $ordenSalida = null)
    {
        $this->ordenSalida = $ordenSalida;

        return $this;
    }

    /**
     * Get ordenSalida
     *
     * @return \Sistema\STOCKBundle\Entity\OrdenSalida
     */
    public function getOrdenSalida()
    {
        return $this->ordenSalida;
    }

    /**
     * Set articulo
     *
     * @param  \Sistema\STOCKBundle\Entity\Articulo $articulo
     * @return CantOrdenSalida
     */
    public function setArticulo(\Sistema\STOCKBundle\Entity\Articulo $articulo = null)
    {
        $this->articulo = $articulo;

        return $this;
    }

    /**
     * Get articulo
     *
     * @return \Sistema\STOCKBundle\Entity\Articulo
     */
    public function getArticulo()
    {
        return $this->articulo;
    }
}
