<?php

namespace Sistema\STOCKBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Sistema\MWSCONFBundle\Entity\MWSgedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Herramienta
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\STOCKBundle\Entity\HerramientaRepository")
 * @UniqueEntity("codigo")
 * @Gedmo\Loggable
 */
class Herramienta extends MWSgedmo {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="integer", unique=true, nullable=true)
     * @Assert\Type(type="integer", message="Solo permite numeros")
     * @Gedmo\Versioned
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     * @Assert\NotBlank()
     * @Gedmo\Versioned
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="cantidad", type="integer")
     * @Assert\Type(type="integer", message="Solo permite numeros")
     * @Gedmo\Versioned
     */
    private $cantidad;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activo", type="boolean", nullable=true)
     * @Gedmo\Versioned
     */
    private $activo;

    /**
     *
     * @ORM\Column(name="cantidadUtilizada", type="integer")
     * @Gedmo\Versioned
     */
    private $cantidadUtilizada;
    private $cantidadReal;

    /**
     * Constructor
     */
    public function __construct() {
        $this->entregasHerramienta = new \Doctrine\Common\Collections\ArrayCollection();
        $this->setActivo(true);
        $this->setCantidadUtilizada(0);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param  integer     $codigo
     * @return Herramienta
     */
    public function setCodigo($codigo) {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return integer
     */
    public function getCodigo() {
        return $this->codigo;
    }

    /**
     * Set descripcion
     *
     * @param  string      $descripcion
     * @return Herramienta
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Set cantidad
     *
     * @param  integer     $cantidad
     * @return Herramienta
     */
    public function setCantidad($cantidad) {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer
     */
    public function getCantidad() {
        return $this->cantidad;
    }

    /**
     * Set activo
     *
     * @param  boolean     $activo
     * @return Herramienta
     */
    public function setActivo($activo) {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean
     */
    public function getActivo() {
        return $this->activo;
    }

    public function __toString() {
        return $this->descripcion;
    }

    /**
     * Set cantidadUtilizada
     *
     * @param  integer     $cantidadUtilizada
     * @return Herramienta
     */
    public function setCantidadUtilizada($cantidadUtilizada) {
        $this->cantidadUtilizada = $cantidadUtilizada;

        return $this;
    }

    /**
     * Get cantidadUtilizada
     *
     * @return integer
     */
    public function getCantidadUtilizada() {
        return $this->cantidadUtilizada;
    }

    /**
     * Get cantidadReal
     */
    public function getCantidadReal() {
        return $this->cantidad - $this->cantidadUtilizada;
    }
}
