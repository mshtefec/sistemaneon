<?php

namespace Sistema\STOCKBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class ArticuloPrecio
{
    private $idRubro;

    private $idTipo;

    private $porcentaje;

    /**
     * @Assert\Choice(choices = {"Aumentar", "Disminuir"}, message = "Seleccione un Tipo Valido.")
     */
     private $opciones;

    public function getIdRubro()
    {
        return $this->idRubro;
    }

    public function setIdRubro($idRubro)
    {
        $this->idRubro = $idRubro;
    }

    public function getIdTipo()
    {
        return $this->idTipo;
    }

    public function setIdTipo($idTipo)
    {
        $this->idTipo = $idTipo;
    }

    public function getPorcentaje()
    {
        return $this->porcentaje;
    }

    public function setPorcentaje($porcentaje)
    {
        $this->porcentaje = $porcentaje;
    }

    public function getOpciones()
    {
        return $this->opciones;
    }

    public function setOpciones($opciones)
    {
        $this->opciones = $opciones;
    }
}
