<?php

namespace Sistema\STOCKBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CantOrdenEntrada
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\STOCKBundle\Entity\CantOrdenEntradaRepository")
 */
class CantOrdenEntrada
{
    private $cantidadMaxima;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cantidad", type="integer")
     * @Assert\Type(type="integer", message="Solo permite numeros")
     * @Assert\Regex(pattern="/^[0-9]\d*$/")
     */
    private $cantidad;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Sistema\STOCKBundle\Entity\OrdenEntrada", inversedBy="articulos")
     * @ORM\JoinColumn(name="orden_entrada_id", referencedColumnName="id")
     *
     */
    private $ordenEntrada;

    /**
     * @var integer
     *
     *  @ORM\ManyToOne(targetEntity="Sistema\STOCKBundle\Entity\Articulo")
     *  @ORM\JoinColumn(name="articulo_id", referencedColumnName="id")
     */
    private $articulo;

    public function __construct()
    {
        $this->setCantidad(0);
    }

    /**
     * Set cantidad maxima
     */
    public function setCantidadMaxima($cantidadMaxima)
    {
        $this->cantidadMaxima = $cantidadMaxima;
    }

    /**
     * Get cantidad maxima
     */
    public function getCantidadMaxima()
    {
        return $this->cantidadMaxima;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cantidad
     *
     * @param  integer          $cantidad
     * @return CantOrdenEntrada
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set ordenEntrada
     *
     * @param  \Sistema\STOCKBundle\Entity\OrdenEntrada $ordenEntrada
     * @return CantOrdenEntrada
     */
    public function setOrdenEntrada(\Sistema\STOCKBundle\Entity\OrdenEntrada $ordenEntrada = null)
    {
        $this->ordenEntrada = $ordenEntrada;

        return $this;
    }

    /**
     * Get ordenEntrada
     *
     * @return \Sistema\STOCKBundle\Entity\OrdenEntrada
     */
    public function getOrdenEntrada()
    {
        return $this->ordenEntrada;
    }

    /**
     * Set articulo
     *
     * @param  \Sistema\STOCKBundle\Entity\Articulo $articulo
     * @return CantOrdenEntrada
     */
    public function setArticulo(\Sistema\STOCKBundle\Entity\Articulo $articulo = null)
    {
        $this->articulo = $articulo;

        return $this;
    }

    /**
     * Get articulo
     *
     * @return \Sistema\STOCKBundle\Entity\Articulo
     */
    public function getArticulo()
    {
        return $this->articulo;
    }
}
