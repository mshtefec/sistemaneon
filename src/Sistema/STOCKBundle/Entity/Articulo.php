<?php

namespace Sistema\STOCKBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Gedmo\Mapping\Annotation as Gedmo;
use Sistema\MWSCONFBundle\Entity\MWSgedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Articulo
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\STOCKBundle\Entity\ArticuloRepository")
 * @Gedmo\Loggable
 * @UniqueEntity("codigo")
 * @UniqueEntity(
 *     fields={"rubro", "tipo", "descripcion"},
 *     errorPath="descripcion",
 *     message="El articulo ya existe, verificar Rubro, Tipo y Descripcion."
 * )
 */
class Articulo extends MWSgedmo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="integer", unique=true, nullable=true)
     * @Assert\Type(type="integer", message="Solo permite numeros")
     * @Gedmo\Versioned
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     * @Assert\NotBlank()
     * @Gedmo\Versioned
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="cantidad", type="integer")
     * @Assert\Type(type="integer", message="Solo permite numeros")
     * @Gedmo\Versioned
     * @Assert\Regex(pattern="/^[0-9]\d*$/")
     */
    private $cantidad;

    /**
     * @var string
     *
     * @ORM\Column(name="cantidadReservada", type="integer")
     * @Assert\Type(type="integer", message="Solo permite numeros")
     * @Gedmo\Versioned
     */
    private $cantidadReservada;

    /**
     * @var string
     *
     * @ORM\Column(name="precio", type="float")
     * @Assert\Regex(pattern="/^[0-9(.{1})]/")
     * @Gedmo\Versioned
     */
    private $precio;

   /**
    *
    * @ORM\ManyToOne(targetEntity="Sistema\STOCKBundle\Entity\Rubro", inversedBy="articulos")
    * @ORM\JoinColumn(name="rubro_id", referencedColumnName="id")
    * @Assert\NotBlank()
    * @Gedmo\Versioned
    */
    private $rubro;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activo", type="boolean")
     * @Gedmo\Versioned
     */
    private $activo;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Sistema\STOCKBundle\Entity\Medida", inversedBy="articulos")
     * @ORM\JoinColumn(name="medida_id", referencedColumnName="id")
     * @Assert\NotNull()
     */
    private $medida;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Sistema\STOCKBundle\Entity\ArticuloTipo", inversedBy="articulos")
     * @ORM\JoinColumn(name="articulo_tipo_id", referencedColumnName="id")
     * @Assert\NotNull(message="No ha seleccionado un valor para el campo Tipo.")
     */
    private $tipo;

    /**
     * @ORM\Column(name="cantidadUtilizada", type="integer")
     */
    private $cantidadUtilizada;

    private $cantidadReal;

    public function __construct()
    {
        $this->setActivo(true);
        $this->setCantidadUtilizada(0);
        $this->setCantidadReservada(0);
    }

    public function __toString()
    {
        return $this->getRubro().' '.$this->getTipo().' '.$this->getDescripcion().' $'.$this->getPrecio();
    }

    public function getArticuloSinPrecio()
    {
        return $this->getRubro().' '.$this->getTipo().' '.$this->getDescripcion();
    }
    
    /**
     * Set cantidadReal
     */
    public function setCantidadReal($cantidadReal)
    {
        $this->cantidadReal = $cantidadReal;
    }

    /**
     * Get cantidadReal
     */
    public function getCantidadReal()
    {
        return $this->cantidad - $this->cantidadUtilizada;
    }

    /**
     * Get cantidadRealReservada
     */
    public function getCantidadRealReservada()
    {
        return ($this->cantidad - $this->cantidadUtilizada) - $this->getCantidadReservada();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param  integer  $codigo
     * @return Articulo
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return integer
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set descripcion
     *
     * @param  string   $descripcion
     * @return Articulo
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set cantidad
     *
     * @param  integer  $cantidad
     * @return Articulo
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidadReservada
     *
     * @return integer
     */
    public function getCantidadReservada()
    {
        return $this->cantidadReservada;
    }

    /**
     * Set cantidadReservada
     *
     * @param  integer  $cantidadReservada
     * @return Articulo
     */
    public function setCantidadReservada($cantidadReservada)
    {
        $this->cantidadReservada = $cantidadReservada;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set precio
     *
     * @param  float    $precio
     * @return Articulo
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return float
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set activo
     *
     * @param  boolean  $activo
     * @return Articulo
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set cantidadUtilizada
     *
     * @param  integer  $cantidadUtilizada
     * @return Articulo
     */
    public function setCantidadUtilizada($cantidadUtilizada)
    {
        $this->cantidadUtilizada = $cantidadUtilizada;

        return $this;
    }

    /**
     * Get cantidadUtilizada
     *
     * @return integer
     */
    public function getCantidadUtilizada()
    {
        return $this->cantidadUtilizada;
    }

    /**
     * Set rubro
     *
     * @param  \Sistema\STOCKBundle\Entity\Rubro $rubro
     * @return Articulo
     */
    public function setRubro(\Sistema\STOCKBundle\Entity\Rubro $rubro = null)
    {
        $this->rubro = $rubro;

        return $this;
    }

    /**
     * Get rubro
     *
     * @return \Sistema\STOCKBundle\Entity\Rubro
     */
    public function getRubro()
    {
        return $this->rubro;
    }

    /**
     * Set medida
     *
     * @param  \Sistema\STOCKBundle\Entity\Medida $medida
     * @return Articulo
     */
    public function setMedida(\Sistema\STOCKBundle\Entity\Medida $medida)
    {
        $this->medida = $medida;

        return $this;
    }

    /**
     * Get medida
     *
     * @return \Sistema\STOCKBundle\Entity\Medida
     */
    public function getMedida()
    {
        return $this->medida;
    }

    /**
     * Set tipo
     *
     * @param  \Sistema\STOCKBundle\Entity\ArticuloTipo $tipo
     * @return Articulo
     */
    public function setTipo(\Sistema\STOCKBundle\Entity\ArticuloTipo $tipo = null)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return \Sistema\STOCKBundle\Entity\ArticuloTipo
     */
    public function getTipo()
    {
        return $this->tipo;
    }
}
