<?php

namespace Sistema\STOCKBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
// DON'T forget this use statement!!!
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * EmpleadoHora
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\STOCKBundle\Entity\EmpleadoHoraRepository")
 * @UniqueEntity(
 *     fields={"empleadoOrden", "fecha", "desde"},
 *     errorPath="empleadoOrden",
 *     message="El empleado trabajo en ese horario.",
 *     groups={"controlHoraEmpleado"}
 * )
 * @UniqueEntity(
 *     fields={"empleadoOrden", "fecha", "hasta"},
 *     errorPath="empleadoOrden",
 *     message="El empleado trabajo en ese horario.",
 *     groups={"controlHoraEmpleado"}
 * )
 */
class EmpleadoHora {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="desde", type="time")
     * @Assert\NotNull()
     */
    private $desde;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="hasta", type="time")
     * @Assert\NotNull()
     */
    private $hasta;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date")
     * @Assert\NotNull()
     */
    private $fecha;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\STOCKBundle\Entity\EmpleadoOrden", inversedBy="empleadoHoras" ,cascade={"persist"})
     * @ORM\JoinColumn(name="empleadoOrden_id", referencedColumnName="id")
     */
    private $empleadoOrden;

    /**
     * @var float
     *
     * @ORM\Column(name="costoHora", type="float")
     */
    private $costoHora;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set desde
     *
     * @param \DateTime $desde
     * @return EmpleadoHora
     */
    public function setDesde($desde) {
        $this->desde = $desde;

        return $this;
    }

    /**
     * Get desde
     *
     * @return \DateTime 
     */
    public function getDesde() {
        return $this->desde;
    }

    /**
     * Set hasta
     *
     * @param \DateTime $hasta
     * @return EmpleadoHora
     */
    public function setHasta($hasta) {
        $this->hasta = $hasta;

        return $this;
    }

    /**
     * Get hasta
     *
     * @return \DateTime 
     */
    public function getHasta() {
        return $this->hasta;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return EmpleadoHora
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha() {
        return $this->fecha;
    }

    /**
     * Set empleadoOrden
     *
     * @param \Sistema\STOCKBundle\Entity\EmpleadoOrden $empleadoOrden
     * @return EmpleadoHora
     */
    public function setEmpleadoOrden(\Sistema\STOCKBundle\Entity\EmpleadoOrden $empleadoOrden = null) {
        $this->empleadoOrden = $empleadoOrden;

        return $this;
    }

    /**
     * Get empleadoOrden
     *
     * @return \Sistema\STOCKBundle\Entity\EmpleadoOrden 
     */
    public function getEmpleadoOrden() {
        return $this->empleadoOrden;
    }

    /**
     * Set costoHora
     *
     * @param float $costoHora
     * @return EmpleadoHora
     */
    public function setCostoHora($costoHora) {
        $this->costoHora = $costoHora;

        return $this;
    }

    /**
     * Get costoHora
     *
     * @return float 
     */
    public function getCostoHora() {
        return $this->costoHora;
    }

}
