<?php

namespace Sistema\STOCKBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * ArticuloRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ArticuloRepository extends EntityRepository {

    /**
     * Busca por Rubro
     */
    public function GetArticuloByStockRubro() {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
                ->select('a', 'r')
                ->from('SistemaSTOCKBundle:Articulo', 'a')
                ->Join('a.rubro', 'r')
                ->where('a.activo = true')
                ->having('(a.cantidad - a.cantidadUtilizada) < r.cantidadMinima')
                ->orderBy('r.nombre', 'ASC')
                ->addorderBy('a.descripcion', 'asc')
                ->setMaxResults(50)
        ;

        return $qb
                        ->getQuery()
                        ->getResult()
        ;
    }

    /**
     * Busca por Tipo
     */
    public function GetArticuloByStockTipo() {
        $qb = $this->QueryArticuloByStockTipo();

        return $qb
                        ->setMaxResults(50)
                        ->getQuery()
                        ->getResult()
        ;
    }

    /**
     * Busca por Tipo
     */
    public function QueryArticuloByStockTipo() {
        $qb = $this->getEntityManager()->createQueryBuilder();
        return $qb
                        ->select('a', 't', 'r')
                        ->addSelect('t.cantidadMinima-(a.cantidad - a.cantidadUtilizada)')
                        ->from('SistemaSTOCKBundle:Articulo', 'a')
                        ->Join('a.rubro', 'r')
                        ->Join('a.tipo', 't')
                        ->where('a.activo = true')
                        ->having('(a.cantidad - a.cantidadUtilizada) < t.cantidadMinima')
                        ->orderBy('t.nombre', 'ASC')
                        ->addorderBy('a.descripcion', 'asc')
        ;
    }

    // Busca todos los articulos por like tipo y descripcion.
    public function likeDescripcion($descripcion) {
        $qb = $this->createQueryBuilder('a');
        $qb
                ->leftJoin('a.tipo', 't')
                ->where(
                        $qb->expr()->like("CONCAT(t.nombre, CONCAT(' ', a.descripcion))", ':descripcion')
                )
                ->orderBy('t.nombre', 'ASC')
                ->orderBy('a.descripcion', 'ASC')
                ->setParameter('descripcion', '%' . $descripcion . '%')
        ;

        return $qb
                        ->getQuery()
                        ->getResult()
        ;
    }

    // Busca los articulos activos por like tipo y descripcion.
    public function likeDescripcionActivos($descripcion) {
        $qb = $this->createQueryBuilder('a');
        $qb
                ->leftJoin('a.tipo', 't')
                ->where('a.activo = true')
                ->andWhere(
                        $qb->expr()->like("CONCAT(t.nombre, CONCAT(' ', a.descripcion))", ':descripcion')
                )
                ->orderBy('t.nombre', 'ASC')
                ->orderBy('a.descripcion', 'ASC')
                ->setParameter('descripcion', '%' . $descripcion . '%')
        ;

        return $qb
                        ->getQuery()
                        ->getResult()
        ;
    }

    public function GetArticulosMasUsados() {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
                ->select('a', 'r','t')
                ->from('SistemaSTOCKBundle:Articulo', 'a')
                ->Join('a.rubro', 'r')
                ->Join('a.tipo', 't')
                ->where('a.activo = true')
                ->orderBy('a.cantidadUtilizada', 'DESC')
                ->addorderBy('r.nombre', 'ASC')

        ;
        return $qb
                        ->getQuery()
                        ->getResult()
        ;
    }

}
