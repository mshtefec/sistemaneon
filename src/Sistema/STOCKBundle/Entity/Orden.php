<?php

namespace Sistema\STOCKBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Sistema\MWSCONFBundle\Entity\MWSgedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Orden
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\STOCKBundle\Entity\OrdenRepository")
 * @UniqueEntity("numero")
 * @Gedmo\Loggable
 */
class Orden extends MWSgedmo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date")
     * @Gedmo\Versioned
     */
    protected $fecha;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\RRHHBundle\Entity\Cliente", inversedBy="ordenes")
     * @ORM\JoinColumn(name="cliente_id", referencedColumnName="id")
     * @Assert\NotNull()
     * @Gedmo\Versioned
     */
    protected $cliente;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\RRHHBundle\Entity\Domicilio")
     * @ORM\JoinColumn(name="domicilio_id", referencedColumnName="id")
     * @Assert\NotNull()
     * @Gedmo\Versioned
     */
    protected $domicilio;

    /**
     * @var string
     *
     * @ORM\Column(name="trabajosRealizados", type="text", nullable=true)
     * @Gedmo\Versioned
     */
    protected $trabajosRealizados;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\STOCKBundle\Entity\EmpleadoOrden", mappedBy="orden", cascade={"persist"}, orphanRemoval=true)
     */
    protected $empleados;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\STOCKBundle\Entity\CantOrden", mappedBy="orden", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $articulos;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\STOCKBundle\Entity\OrdenSalida", mappedBy="orden", cascade={"persist", "remove"})
     */
    protected $ordenSalida;

    /**
     * @var integer
     *
     * @ORM\Column(name="numero", type="integer", unique=true)
     * @Assert\NotNull()
     * @Assert\Regex(pattern="/\d+/")
     * @Gedmo\Versioned
     */
    protected $numero;

    /**
     * @var boolean $cerrada
     *
     * @ORM\Column(type="boolean")
     * @Assert\Type(type="boolean", message="El valor {{ value }} no es un {{ type }} valido.")
     * @Gedmo\Versioned
     */
    protected $cerrada;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\STOCKBundle\Entity\EntregasHerramientas", mappedBy="orden", cascade={"all"})
     */
    protected $herramientaOrdenes;

    /**
     * @var string
     *
     * @ORM\Column(name="observaciones", type="text", nullable=true)
     * @Gedmo\Versioned
     */
    protected $observaciones;

    /**
     * @var float
     *
     * @ORM\Column(name="montoArticulos", type="float")
     * @Gedmo\Versioned
     */
    protected $montoArticulos;

    /**
     * @var float
     *
     * @ORM\Column(name="montoEmpleado", type="float")
     * @Gedmo\Versioned
     */
    protected $montoEmpleados;

    /**
     * @var float
     *
     * @ORM\Column(name="montoVarios", type="float")
     * @Gedmo\Versioned
     */
    protected $montoVarios;

    /**
     * @var float
     *
     * @ORM\Column(name="montoOrden", type="float")
     * @Gedmo\Versioned
     */
    protected $montoOrden;

    /**
     * @var float
     *
     * @ORM\Column(name="montoDescuento", type="float", nullable=true)
     * @Gedmo\Versioned
     */
    protected $montoDescuento;

    /**
     * @var float
     *
     * @ORM\Column(name="descuentoOrden", type="float", nullable=true)
     * @Gedmo\Versioned
     */
    protected $descuentoOrden;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\FACTURACIONBundle\Entity\Comprobante", mappedBy="orden", cascade={"persist"}, orphanRemoval=true)
     */
    protected $comprobantes;

    /**
     * @var boolean $reservaMateriales
     *
     * @ORM\Column(type="boolean")
     * @Assert\Type(type="boolean", message="El valor {{ value }} no es un {{ type }} valido.")
     * @Gedmo\Versioned
     */
    protected $reservaMateriales;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\FACTURACIONBundle\Entity\Asiento", mappedBy="orden", cascade={"persist", "remove"})
     */
    protected $asientos;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\PresupuestoBundle\Entity\Presupuesto", inversedBy="ordenes")
     * @ORM\JoinColumn(name="presupuesto_id", referencedColumnName="id", nullable=true)
     * @Gedmo\Versioned
     */
    protected $presupuesto;

    /**
     * 0 => 'Mantenimiento', 1 => 'Obra
     *
     * @ORM\Column(type="integer")
     */
    protected $funcion;

    /**
     * @var float
     *
     * @ORM\Column(name="horasPresupuesto", type="float", nullable=true)
     * @Gedmo\Versioned
     */
    protected $horasPresupuesto;


    /**
     * Constructor
     */
    public function __construct() {
        $this->empleados          = new ArrayCollection();
        $this->articulos          = new ArrayCollection();
        $this->ordenSalida        = new ArrayCollection();
        $this->herramientaOrdenes = new ArrayCollection();
        $this->comprobantes       = new ArrayCollection();
        // $this->presupuesto        = new ArrayCollection();
        $this->setMontoArticulos(0);
        $this->setMontoEmpleados(0);
        $this->setMontoOrden(0);
        $this->setMontoVarios(0);
        $this->setMontoDescuento(0);
        $this->setCerrada(false);
        $this->setDescuentoOrden(0);
        $this->setReservaMateriales(false);
        $this->setHorasPresupuesto(0);
    }

    /**
     * @Assert\True(message = "Solo se debe dar un descuento por monto o por orden.")
     */
    public function isDescuentoOrdenIgualDescuentoMonto() {
        $return = true;
        
        if ($this->getDescuentoOrden() > 0 xor $this->getMontoDescuento() > 0) {
            $return = true;
        } elseif ($this->getDescuentoOrden() == 0 && $this->getMontoDescuento() == 0) {
            $return = true;
        } else {
            $return = false;
        }

        return $return;
    }

    public function __toString() {
        return (string) $this->getNumero();
    }

    /**
     * Add asientos
     *
     * @param \Sistema\FACTURACIONBundle\Entity\Asiento $asientos
     * @return Orden
     */
    public function addAsiento(\Sistema\FACTURACIONBundle\Entity\Asiento $asientos) {
        $asientos->setOrden($this);
        $this->asientos[] = $asientos;

        return $this;
    }

    /**
     * Remove asientos
     *
     * @param \Sistema\FACTURACIONBundle\Entity\Asiento $asientos
     */
    public function removeAsiento(\Sistema\FACTURACIONBundle\Entity\Asiento $asientos) {
        $this->asientos->removeElement($asientos);
    }

    /**
     * Get asientos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAsientos() {
        return $this->asientos;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param  \DateTime $fecha
     * @return Orden
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha() {
        return $this->fecha;
    }

    /**
     * Set trabajosRealizados
     *
     * @param  string $trabajosRealizados
     * @return Orden
     */
    public function setTrabajosRealizados($trabajosRealizados) {
        $this->trabajosRealizados = $trabajosRealizados;

        return $this;
    }

    /**
     * Get trabajosRealizados
     *
     * @return string
     */
    public function getTrabajosRealizados() {
        return $this->trabajosRealizados;
    }

    /**
     * Set numero
     *
     * @param  integer $numero
     * @return Orden
     */
    public function setNumero($numero) {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer
     */
    public function getNumero() {
        return $this->numero;
    }

    /**
     * Set cerrada
     *
     * @param  boolean $cerrada
     * @return Orden
     */
    public function setCerrada($cerrada) {
        $this->cerrada = $cerrada;

        return $this;
    }

    /**
     * Get cerrada
     *
     * @return boolean
     */
    public function getCerrada() {
        return $this->cerrada;
    }

    /**
     * Set observaciones
     *
     * @param  string $observaciones
     * @return Orden
     */
    public function setObservaciones($observaciones) {
        $this->observaciones = $observaciones;

        return $this;
    }

    /**
     * Get observaciones
     *
     * @return string
     */
    public function getObservaciones() {
        return $this->observaciones;
    }

    /**
     * Set montoArticulos
     *
     * @param  float $montoArticulos
     * @return Orden
     */
    public function setMontoArticulos($montoArticulos) {
        $this->montoArticulos = $montoArticulos;

        return $this;
    }

    /**
     * Get montoArticulos
     *
     * @return float
     */
    public function getMontoArticulos() {
        return $this->montoArticulos;
    }

    /**
     * Set montoEmpleados
     *
     * @param  float $montoEmpleados
     * @return Orden
     */
    public function setMontoEmpleados($montoEmpleados) {
        $this->montoEmpleados = $montoEmpleados;

        return $this;
    }

    /**
     * Get montoEmpleados
     *
     * @return float
     */
    public function getMontoEmpleados() {
        return $this->montoEmpleados;
    }

    /**
     * Set montoVarios
     *
     * @param  float $montoVarios
     * @return Orden
     */
    public function setMontoVarios($montoVarios) {
        $this->montoVarios = $montoVarios;

        return $this;
    }

    /**
     * Get montoVarios
     *
     * @return float
     */
    public function getMontoVarios() {
        return $this->montoVarios;
    }

    /**
     * Set montoOrden
     *
     * @param  float $montoOrden
     * @return Orden
     */
    public function setMontoOrden($montoOrden) {
        $this->montoOrden = $montoOrden;

        return $this;
    }

    /**
     * Get montoOrden
     *
     * @return float
     */
    public function getMontoOrden() {
        return $this->montoOrden;
    }

    /**
     * Set montoDescuento
     *
     * @param  float $montoDescuento
     * @return Orden
     */
    public function setMontoDescuento($montoDescuento) {
        $this->montoDescuento = $montoDescuento;

        return $this;
    }

    /**
     * Get montoDescuento
     *
     * @return float
     */
    public function getMontoDescuento() {
        return $this->montoDescuento;
    }

    /**
     * Set descuentoOrden
     *
     * @param  float $descuentoOrden
     * @return Orden
     */
    public function setDescuentoOrden($descuentoOrden) {
        $this->descuentoOrden = $descuentoOrden;

        return $this;
    }

    /**
     * Get descuentoOrden
     *
     * @return float
     */
    public function getDescuentoOrden() {
        return $this->descuentoOrden;
    }

    /**
     * Set cliente
     *
     * @param  \Sistema\RRHHBundle\Entity\Cliente $cliente
     * @return Orden
     */
    public function setCliente(\Sistema\RRHHBundle\Entity\Cliente $cliente = null) {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return \Sistema\RRHHBundle\Entity\Cliente
     */
    public function getCliente() {
        return $this->cliente;
    }

    /**
     * Set domicilio
     *
     * @param  \Sistema\RRHHBundle\Entity\Domicilio $domicilio
     * @return Orden
     */
    public function setDomicilio(\Sistema\RRHHBundle\Entity\Domicilio $domicilio = null) {
        $this->domicilio = $domicilio;

        return $this;
    }

    /**
     * Get domicilio
     *
     * @return \Sistema\RRHHBundle\Entity\Domicilio
     */
    public function getDomicilio() {
        return $this->domicilio;
    }

    /**
     * Add empleados
     *
     * @param  \Sistema\STOCKBundle\Entity\EmpleadoOrden $empleados
     * @return Orden
     */
    public function addEmpleado(\Sistema\STOCKBundle\Entity\EmpleadoOrden $empleados) {
        $empleados->setOrden($this);
        $this->empleados[] = $empleados;

        return $this;
    }

    /**
     * Remove empleados
     *
     * @param \Sistema\STOCKBundle\Entity\EmpleadoOrden $empleados
     */
    public function removeEmpleado(\Sistema\STOCKBundle\Entity\EmpleadoOrden $empleados) {
        $this->empleados->removeElement($empleados);
    }

    /**
     * Get empleados
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmpleados() {
        return $this->empleados;
    }

    /**
     * Add articulos
     *
     * @param  \Sistema\STOCKBundle\Entity\CantOrden $articulos
     * @return Orden
     */
    public function addArticulo(\Sistema\STOCKBundle\Entity\CantOrden $articulos) {
        $articulos->setOrden($this);
        $this->articulos[] = $articulos;

        return $this;
    }

    /**
     * Remove articulos
     *
     * @param \Sistema\STOCKBundle\Entity\CantOrden $articulos
     */
    public function removeArticulo(\Sistema\STOCKBundle\Entity\CantOrden $articulos) {
        $articulos->setOrden($this);
        $this->articulos->removeElement($articulos);
    }

    /**
     * Get articulos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticulos() {
        return $this->articulos;
    }

    /**
     * Add ordenSalida
     *
     * @param  \Sistema\STOCKBundle\Entity\OrdenSalida $ordenSalida
     * @return Orden
     */
    public function addOrdenSalida(\Sistema\STOCKBundle\Entity\OrdenSalida $ordenSalida) {
        $ordenSalida->setOrden($this);
        $this->ordenSalida[] = $ordenSalida;

        return $this;
    }

    /**
     * Remove ordenSalida
     *
     * @param \Sistema\STOCKBundle\Entity\OrdenSalida $ordenSalida
     */
    public function removeOrdenSalida(\Sistema\STOCKBundle\Entity\OrdenSalida $ordenSalida) {
        $this->ordenSalida->removeElement($ordenSalida);
    }

    /**
     * Get ordenSalida
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrdenSalida() {
        return $this->ordenSalida;
    }

    /**
     * Add herramientaOrdenes
     *
     * @param  \Sistema\STOCKBundle\Entity\EntregasHerramientas $herramientaOrdenes
     * @return Orden
     */
    public function addHerramientaOrdene(\Sistema\STOCKBundle\Entity\EntregasHerramientas $herramientaOrdenes) {
        $herramientaOrdenes->setOrden($this);
        $this->herramientaOrdenes[] = $herramientaOrdenes;

        return $this;
    }

    /**
     * Remove herramientaOrdenes
     *
     * @param \Sistema\STOCKBundle\Entity\EntregasHerramientas $herramientaOrdenes
     */
    public function removeHerramientaOrdene(\Sistema\STOCKBundle\Entity\EntregasHerramientas $herramientaOrdenes) {
        $this->herramientaOrdenes->removeElement($herramientaOrdenes);
    }

    /**
     * Get herramientaOrdenes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHerramientaOrdenes() {
        return $this->herramientaOrdenes;
    }

    /**
     * Add comprobantes
     *
     * @param  \Sistema\FACTURACIONBundle\Entity\Comprobante $comprobantes
     * @return Orden
     */
    public function addComprobante(\Sistema\FACTURACIONBundle\Entity\Comprobante $comprobantes) {
        $comprobantes->setOrden($this);
        $this->comprobantes[] = $comprobantes;

        return $this;
    }

    /**
     * Remove comprobantes
     *
     * @param \Sistema\FACTURACIONBundle\Entity\Comprobante $comprobantes
     */
    public function removeComprobante(\Sistema\FACTURACIONBundle\Entity\Comprobante $comprobantes) {
        $this->comprobantes->removeElement($comprobantes);
    }

    /**
     * Get comprobantes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComprobantes() {
        return $this->comprobantes;
    }

    /**
     * Set reservaMateriales
     *
     * @param  boolean $reservaMateriales
     * @return Orden
     */
    public function setReservaMateriales($reservaMateriales) {
        $this->reservaMateriales = $reservaMateriales;

        return $this;
    }

    /**
     * Get reservaMateriales
     *
     * @return boolean
     */
    public function getReservaMateriales() {
        return $this->reservaMateriales;
    }

    /**
     * Set presupuesto
     *
     * @param \Sistema\PresupuestoBundle\Entity\Presupuesto $presupuesto
     * @return Orden
     */
    public function setPresupuesto(\Sistema\PresupuestoBundle\Entity\Presupuesto $presupuesto = null) {
        $this->presupuesto = $presupuesto;

        return $this;
    }

    /**
     * Get presupuesto
     *
     * @return \Sistema\PresupuestoBundle\Entity\Presupuesto 
     */
    public function getPresupuesto() {
        return $this->presupuesto;
    }

    public function __sleep() {
        return array('id');
    }

    /**
     * Set funcion
     *
     * @param integer $funcion
     * @return Orden
     */
    public function setFuncion($funcion) {
        $this->funcion = $funcion;

        return $this;
    }

    /**
     * Get funcion
     *
     * @return integer 
     */
    public function getFuncion() {
        return $this->funcion;
    }

    /**
     * Set horasPresupuesto
     *
     * @param  float $horasPresupuesto
     * @return Orden
     */
    public function setHorasPresupuesto($horasPresupuesto) {
        $this->horasPresupuesto = $horasPresupuesto;

        return $this;
    }

    /**
     * Get horasPresupuesto
     *
     * @return float
     */
    public function getHorasPresupuesto() {
        return $this->horasPresupuesto;
    }
}