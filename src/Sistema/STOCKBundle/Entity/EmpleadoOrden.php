<?php

namespace Sistema\STOCKBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * EmpleadoOrden
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\STOCKBundle\Entity\EmpleadoOrdenRepository")
 */
class EmpleadoOrden
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\RRHHBundle\Entity\Empleado", inversedBy="ordenes")
     * @ORM\JoinColumn(name="empleado_id", referencedColumnName="id")
     * @Assert\NotNull()
     */
    private $empleado;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Sistema\STOCKBundle\Entity\Orden", inversedBy="empleados")
     * @ORM\JoinColumn(name="orden_id", referencedColumnName="id")
     * @Assert\NotNull()
     */
    private $orden;

    /**
     * @var integer
     *
     * @ORM\OneToMany(targetEntity="Sistema\STOCKBundle\Entity\EmpleadoHora", mappedBy="empleadoOrden", cascade={"all"}, orphanRemoval=true)
     * @Assert\Valid
     * @Assert\NotNull()
     * @ORM\OrderBy({"fecha" = "ASC"})
     */
    private $empleadoHoras;

    /**
     * @var float
     *
     * @ORM\Column(name="costoHoraCliente", type="float")
     */
    private $costoHoraCliente;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->empleadoHoras = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return "".$this->getId();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set empleado
     *
     * @param  integer       $empleado
     * @return EmpleadoOrden
     */
    public function setEmpleado($empleado)
    {
        $this->empleado = $empleado;

        return $this;
    }

    /**
     * Get empleado
     *
     * @return integer
     */
    public function getEmpleado()
    {
        return $this->empleado;
    }

    /**
     * Set orden
     *
     * @param  integer       $orden
     * @return EmpleadoOrden
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return integer
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set empleadoHoras
     *
     * @param  integer       $empleadoHoras
     * @return EmpleadoOrden
     */
    public function setEmpleadoHoras($empleadoHoras)
    {
        $this->empleadoHoras = $empleadoHoras;

        return $this;
    }

    /**
     * Get empleadoHoras
     *
     * @return integer
     */
    public function getEmpleadoHoras()
    {
        return $this->empleadoHoras;
    }

    /**
     * Add empleadoHoras
     *
     * @param  \Sistema\STOCKBundle\Entity\EmpleadoHora $empleadoHoras
     * @return EmpleadoOrden
     */
    public function addEmpleadoHora(\Sistema\STOCKBundle\Entity\EmpleadoHora $empleadoHoras)
    {
        $empleadoHoras->setEmpleadoOrden($this);
        $this->empleadoHoras[] = $empleadoHoras;

        return $this;
    }

    /**
     * Remove empleadoHoras
     *
     * @param \Sistema\STOCKBundle\Entity\EmpleadoHora $empleadoHoras
     */
    public function removeEmpleadoHora(\Sistema\STOCKBundle\Entity\EmpleadoHora $empleadoHoras)
    {
        $this->empleadoHoras->removeElement($empleadoHoras);
    }

    /**
     * Set costoHoraCliente
     *
     * @param  float         $costoHoraCliente
     * @return EmpleadoOrden
     */
    public function setCostoHoraCliente($costoHoraCliente)
    {
        $this->costoHoraCliente = $costoHoraCliente;

        return $this;
    }

    /**
     * Get costoHoraCliente
     *
     * @return float
     */
    public function getCostoHoraCliente()
    {
        return $this->costoHoraCliente;
    }
}