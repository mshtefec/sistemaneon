<?php

namespace Sistema\STOCKBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * CantOrden
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\STOCKBundle\Entity\CantOrdenRepository")
 * @Gedmo\Loggable
 */
class CantOrden {

    private $cantidadMaxima;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="cantidad", type="integer")
     * @Assert\Type(type="integer", message="Solo permite numeros")
     * @Assert\Regex(pattern="/^[0-9]\d*$/")
     * @Gedmo\Versioned
     */
    private $cantidad;

    /**
     * @var string
     *
     * @ORM\Column(name="cantidadReservada", type="integer", nullable=true)
     * @Assert\Type(type="integer", message="Solo permite numeros")
     * @Gedmo\Versioned
     */
    private $cantidadReservada;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Sistema\STOCKBundle\Entity\Orden", inversedBy="articulos")
     * @ORM\JoinColumn(name="orden_id", referencedColumnName="id")
     * @Gedmo\Versioned
     */
    private $orden;

    /**
     * @var integer
     *
     *  @ORM\ManyToOne(targetEntity="Sistema\STOCKBundle\Entity\Articulo")
     *  @ORM\JoinColumn(name="articulo_id", referencedColumnName="id")
     *  @Gedmo\Versioned
     */
    private $articulo;

    /**
     * @var float
     *
     * @ORM\Column(name="precio", type="float")
     * @Gedmo\Versioned
     */
    private $precio;
    
    private $obtenerId;


    /**
     * @Assert\True(message = "La cantidad debe ser menor a la cantidad maxima.")
     */
    public function isCantidades() {
        // return true or false
        if ($this->getCantidad() != 0) {
            $total = $this->getCantidadMaxima() - $this->getCantidad();
            if ($total < 0) {
                return false;
            }
        }

        return true;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->setCantidadReservada(0);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set cantidad
     *
     * @param  integer   $cantidad
     * @return CantOrden
     */
    public function setCantidad($cantidad) {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer
     */
    public function getCantidad() {
        return $this->cantidad;
    }

    /**
     * Set cantidad maxima
     */
    public function setCantidadMaxima($cantidadMaxima) {
        $this->cantidadMaxima = $cantidadMaxima;
    }

    /**
     * Get cantidad maxima
     */
    public function getCantidadMaxima() {
        return $this->cantidadMaxima;
    }

    /**
     * Get cantidadReservada
     *
     * @return integer
     */
    public function getCantidadReservada() {
        return $this->cantidadReservada;
    }

    /**
     * Set cantidadReservada
     *
     * @param  integer  $cantidadReservada
     * @return Articulo
     */
    public function setCantidadReservada($cantidadReservada) {
        $this->cantidadReservada = $cantidadReservada;

        return $this;
    }

    /**
     * Set orden
     *
     * @param  \Sistema\STOCKBundle\Entity\Orden $orden
     * @return CantOrden
     */
    public function setOrden(\Sistema\STOCKBundle\Entity\Orden $orden = null) {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return \Sistema\STOCKBundle\Entity\Orden
     */
    public function getOrden() {
        return $this->orden;
    }

    /**
     * Set articulo
     *
     * @param  \Sistema\STOCKBundle\Entity\Articulo $articulo
     * @return CantOrden
     */
    public function setArticulo(\Sistema\STOCKBundle\Entity\Articulo $articulo = null) {
        $this->articulo = $articulo;

        return $this;
    }

    /**
     * Get articulo
     *
     * @return \Sistema\STOCKBundle\Entity\Articulo
     */
    public function getArticulo() {
        return $this->articulo;
    }

    /**
     * Set precio
     *
     * @param  float     $precio
     * @return CantOrden
     */
    public function setPrecio($precio) {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return float
     */
    public function getPrecio() {
        return $this->precio;
    }
    
    public function getObtenerId(){
        return $this->id;
    }
    
    public function setObtenerId($id){
        $this->obtenerId = $id;

        return $this;
    }

}
