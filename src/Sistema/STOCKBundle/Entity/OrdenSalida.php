<?php

namespace Sistema\STOCKBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Sistema\MWSCONFBundle\Entity\MWSgedmo;

/**
 * OrdenSalida
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\STOCKBundle\Entity\OrdenSalidaRepository")
 */
class OrdenSalida extends MWSgedmo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Sistema\STOCKBundle\Entity\Orden", inversedBy="ordenSalida")
     * @ORM\JoinColumn(name="Orden_id", referencedColumnName="id")
     * @Assert\NotNull()
     */
    private $orden;

    /**
     * @var integer
     *
     *  @ORM\OneToOne(targetEntity="Sistema\STOCKBundle\Entity\OrdenEntrada")
     *  @ORM\JoinColumn(name="orden_entrada_id", referencedColumnName="id")
     */
    private $ordenEntrada;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\STOCKBundle\Entity\CantOrdenSalida", mappedBy="ordenSalida", cascade={"all"}, orphanRemoval=true)
     */
    private $articulos;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Sistema\RRHHBundle\Entity\Empleado", inversedBy="ordenSalida")
     * @ORM\JoinColumn(name="empleado_id", referencedColumnName="id")
     * @Assert\NotNull()
     */
    private $empleado;

    private $obtenerSalidaId;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->articulos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return (string) $this->getId();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set orden
     *
     * @param  \Sistema\STOCKBundle\Entity\Orden $orden
     * @return OrdenSalida
     */
    public function setOrden(\Sistema\STOCKBundle\Entity\Orden $orden = null)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return \Sistema\STOCKBundle\Entity\Orden
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set ordenEntrada
     *
     * @param  \Sistema\STOCKBundle\Entity\OrdenEntrada $ordenEntrada
     * @return OrdenSalida
     */
    public function setOrdenEntrada(\Sistema\STOCKBundle\Entity\OrdenEntrada $ordenEntrada = null)
    {
        $this->ordenEntrada = $ordenEntrada;

        return $this;
    }

    /**
     * Get ordenEntrada
     *
     * @return \Sistema\STOCKBundle\Entity\OrdenEntrada
     */
    public function getOrdenEntrada()
    {
        return $this->ordenEntrada;
    }

    /**
     * Add articulos
     *
     * @param  \Sistema\STOCKBundle\Entity\CantOrdenSalida $articulos
     * @return OrdenSalida
     */
    public function addArticulo(\Sistema\STOCKBundle\Entity\CantOrdenSalida $articulos)
    {
        $articulos->setOrdenSalida($this);
        $this->articulos[] = $articulos;

        return $this;
    }

    /**
     * Remove articulos
     *
     * @param \Sistema\STOCKBundle\Entity\CantOrdenSalida $articulos
     */
    public function removeArticulo(\Sistema\STOCKBundle\Entity\CantOrdenSalida $articulos)
    {
        $this->articulos->removeElement($articulos);
    }

    /**
     * Get articulos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticulos()
    {
        return $this->articulos;
    }
    //Fin de Clase

    /**
     * Set empleado
     *
     * @param  \Sistema\RRHHBundle\Entity\Empleado $empleado
     * @return OrdenSalida
     */
    public function setEmpleado(\Sistema\RRHHBundle\Entity\Empleado $empleado = null)
    {
        $this->empleado = $empleado;

        return $this;
    }

    /**
     * Get empleado
     *
     * @return \Sistema\RRHHBundle\Entity\Empleado
     */
    public function getEmpleado()
    {
        return $this->empleado;
    }
    
    public function getObtenerSalidaId(){
        return $this->id;
    }
    
    public function setObtenerSalidaId($id){
        $this->obtenerSalidaId = $id;

        return $this;
    }
}
