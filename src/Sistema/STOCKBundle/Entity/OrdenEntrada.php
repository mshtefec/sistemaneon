<?php

namespace Sistema\STOCKBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sistema\MWSCONFBundle\Entity\MWSgedmo;

/**
 * OrdenEntrada
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\STOCKBundle\Entity\OrdenEntradaRepository")
 */
class OrdenEntrada  extends MWSgedmo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     *  @ORM\OneToOne(targetEntity="Sistema\STOCKBundle\Entity\OrdenSalida")
     *  @ORM\JoinColumn(name="orden_salida_id", referencedColumnName="id")
     */
    private $ordenSalida;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\STOCKBundle\Entity\CantOrdenEntrada"
     * ,mappedBy="ordenEntrada", cascade={"all"}, orphanRemoval=true)
     */
    private $articulos;

    private $obtenerEntradaId;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->articulos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add articulos
     *
     * @param  \Sistema\STOCKBundle\Entity\CantOrdenEntrada $articulos
     * @return OrdenEntrada
     */
    public function addArticulo(\Sistema\STOCKBundle\Entity\CantOrdenEntrada $articulos)
    {
        $articulos->setOrdenEntrada($this);
        $this->articulos[] = $articulos;

        return $this;
    }

    /**
     * Remove articulos
     *
     * @param \Sistema\STOCKBundle\Entity\CantOrdenEntrada $articulos
     */
    public function removeArticulo(\Sistema\STOCKBundle\Entity\CantOrdenEntrada $articulos)
    {
        $this->articulos->removeElement($articulos);
    }

    /**
     * Get articulos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticulos()
    {
        return $this->articulos;
    }

    /**
     * Set ordenSalida
     *
     * @param  \Sistema\STOCKBundle\Entity\OrdenSalida $ordenSalida
     * @return OrdenEntrada
     */
    public function setOrdenSalida(\Sistema\STOCKBundle\Entity\OrdenSalida $ordenSalida = null)
    {
        $ordenSalida->setOrdenEntrada($this);
        $this->ordenSalida = $ordenSalida;

        return $this;
    }

    /**
     * Get ordenSalida
     *
     * @return \Sistema\STOCKBundle\Entity\OrdenSalida
     */
    public function getOrdenSalida()
    {
        return $this->ordenSalida;
    }
    
    public function getObtenerEntradaId(){
        return $this->id;
    }
    
    public function setObtenerEntradaId($id){
        $this->obtenerEntradaId = $id;

        return $this;
    }
}
