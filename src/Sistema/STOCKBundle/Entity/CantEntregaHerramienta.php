<?php

namespace Sistema\STOCKBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CantEntregaHerramienta
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\STOCKBundle\Entity\CantEntregaHerramientaRepository")
 */
class CantEntregaHerramienta
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Sistema\STOCKBundle\Entity\Herramienta")
     * @ORM\JoinColumn(name="herramienta_id", referencedColumnName="id")
     *
     */
    private $herramienta;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Sistema\STOCKBundle\Entity\EntregasHerramientas", inversedBy="herramientas")
     * @ORM\JoinColumn(name="entregaHerramienta_id", referencedColumnName="id")
     *
     */
    private $entregaHerramienta;

    /**
     * @var integer
     *
     * @ORM\Column(name="cantidad", type="integer")
     */
    private $cantidad;

    private $cantidadMaxima;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set herramienta
     *
     * @param  string                 $herramienta
     * @return CantEntregaHerramienta
     */
    public function setHerramienta($herramienta)
    {
        $this->herramienta = $herramienta;

        return $this;
    }

    /**
     * Get herramienta
     *
     * @return string
     */
    public function getHerramienta()
    {
        return $this->herramienta;
    }

    /**
     * Set entregaHerramienta
     *
     * @param  string                 $entregaHerramienta
     * @return CantEntregaHerramienta
     */
    public function setEntregaHerramienta($entregaHerramienta)
    {
        $this->entregaHerramienta = $entregaHerramienta;

        return $this;
    }

    /**
     * Get entregaHerramienta
     *
     * @return string
     */
    public function getEntregaHerramienta()
    {
        return $this->entregaHerramienta;
    }

    /**
     * Set cantidad
     *
     * @param  integer                $cantidad
     * @return CantEntregaHerramienta
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    public function getCantidadMaxima()
    {
        return $this->cantidadMaxima;
    }

    public function setCantidadMaxima($cantidadMaxima)
    {
        $this->cantidadMaxima = $cantidadMaxima;
    }

}
