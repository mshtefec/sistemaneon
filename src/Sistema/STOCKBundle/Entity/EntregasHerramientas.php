<?php

namespace Sistema\STOCKBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Sistema\MWSCONFBundle\Entity\MWSgedmo;

/**
 * EntregasHerramientas
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\STOCKBundle\Entity\EntregasHerramientasRepository")
 */
class EntregasHerramientas extends MWSgedmo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\STOCKBundle\Entity\CantEntregaHerramienta", mappedBy="entregaHerramienta", cascade={"all"}, orphanRemoval=true)
     */
    private $herramientas;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\RRHHBundle\Entity\Empleado", inversedBy="entregasHerramienta")
     * @ORM\JoinColumn(name="empleado_id", referencedColumnName="id")
     * @Assert\NotNull()
     */
    private $empleado;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\STOCKBundle\Entity\Orden", inversedBy="herramientaOrdenes")
     * @ORM\JoinColumn(name="orden_id", referencedColumnName="id", nullable=true)
     */
    private $orden;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->herramientas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add herramientas
     *
     * @param  \Sistema\STOCKBundle\Entity\Herramienta $herramientas
     * @return EntregasHerramientas
     */
    public function addHerramienta(\Sistema\STOCKBundle\Entity\CantEntregaHerramienta $herramientas)
    {
        $herramientas->setEntregaHerramienta($this);
        $this->herramientas[] = $herramientas;

        return $this;
    }

    /**
     * Remove herramientas
     *
     * @param \Sistema\STOCKBundle\Entity\Herramienta $herramientas
     */
    public function removeHerramienta(\Sistema\STOCKBundle\Entity\CantEntregaHerramienta $herramientas)
    {
        $this->herramientas->removeElement($herramientas);
    }

    /**
     * Get herramientas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHerramientas()
    {
        return $this->herramientas;
    }

    /**
     * Set empleado
     *
     * @param  \Sistema\RRHHBundle\Entity\Empleado $empleado
     * @return EntregasHerramientas
     */
    public function setEmpleado(\Sistema\RRHHBundle\Entity\Empleado $empleado)
    {
        $this->empleado = $empleado;

        return $this;
    }

    /**
     * Get empleado
     *
     * @return \Sistema\RRHHBundle\Entity\Empleado
     */
    public function getEmpleado()
    {
        return $this->empleado;
    }

    /**
     * Set orden
     *
     * @param  \Sistema\STOCKBundle\Entity\Orden $orden
     * @return EntregasHerramientas
     */
    public function setOrden(\Sistema\STOCKBundle\Entity\Orden $orden = null)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return \Sistema\STOCKBundle\Entity\Orden
     */
    public function getOrden()
    {
        return $this->orden;
    }

}
