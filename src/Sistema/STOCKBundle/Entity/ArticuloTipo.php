<?php

namespace Sistema\STOCKBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ArticuloTipo
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\STOCKBundle\Entity\ArticuloTipoRepository")
 * @ORM\HasLifecycleCallbacks
 */
class ArticuloTipo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var integer
     *
     * @ORM\OneToMany(targetEntity="Sistema\STOCKBundle\Entity\Articulo"
     * , mappedBy="tipo", cascade={"persist"})
     */
    private $articulos;

    /**
     * @var decimal
     *
     * @ORM\Column(name="cantidadMinima", type="decimal")
     * @Assert\Type(type="integer", message="Solo permite numeros")
     */
    private $cantidadMinima;

    public function __toString()
    {
        return $this->getNombre();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param  string       $nombre
     * @return ArticuloTipo
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->articulos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->setCantidadMinima(0);
    }

    /**
     * Add articulos
     *
     * @param  \Sistema\STOCKBundle\Entity\Articulo $articulos
     * @return ArticuloTipo
     */
    public function addArticulo(\Sistema\STOCKBundle\Entity\Articulo $articulos)
    {
        $this->articulos[] = $articulos;

        return $this;
    }

    /**
     * Remove articulos
     *
     * @param \Sistema\STOCKBundle\Entity\Articulo $articulos
     */
    public function removeArticulo(\Sistema\STOCKBundle\Entity\Articulo $articulos)
    {
        $this->articulos->removeElement($articulos);
    }

    /**
     * Get articulos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticulos()
    {
        return $this->articulos;
    }

    /**
     * Set cantidadMinima
     *
     * @param  string $cantidadMinima
     * @return Tipo
     */
    public function setCantidadMinima($cantidadMinima)
    {
        $this->cantidadMinima = $cantidadMinima;

        return $this;
    }

    /**
     * Get cantidadMinima
     *
     * @return string
     */
    public function getCantidadMinima()
    {
        return $this->cantidadMinima;
    }

    /**
     * @ORM\PrePersist
     */
    public function cantidadMinimaisNull()
    {
        if ($this->getCantidadMinima()==null) {
            $this->setCantidadMinima(0);
        }

    }
}
