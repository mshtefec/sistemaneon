<?php

namespace Sistema\STOCKBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sistema\MWSCONFBundle\Entity\MWSgedmo;

/**
 * AddArticulos
 *
 * @ORM\Table()
 * @ORM\Entity()
 */
class AddArticulos extends MWSgedmo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\STOCKBundle\Entity\CantAddArticulo", mappedBy="addArticulo", cascade={"all"}, orphanRemoval=true)
     */
    private $articulos;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\STOCKBundle\Entity\Orden")
     * @ORM\JoinColumn(name="orden_id", referencedColumnName="id", nullable=true)
     */
    private $orden;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->articulos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add articulos
     *
     * @param  \Sistema\STOCKBundle\Entity\CantAddArticulo $articulos
     * @return AddArticulos
     */
    public function addArticulo(\Sistema\STOCKBundle\Entity\CantAddArticulo $articulos)
    {
        $this->articulos[] = $articulos;

        return $this;
    }

    /**
     * Remove articulos
     *
     * @param \Sistema\STOCKBundle\Entity\CantAddArticulo $articulos
     */
    public function removeArticulo(\Sistema\STOCKBundle\Entity\CantAddArticulo $articulos)
    {
        $this->articulos->removeElement($articulos);
    }

    /**
     * Get articulos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticulos()
    {
        return $this->articulos;
    }

    /**
     * Set orden
     *
     * @param  \Sistema\STOCKBundle\Entity\Orden $orden
     * @return AddArticulos
     */
    public function setOrden(\Sistema\STOCKBundle\Entity\Orden $orden = null)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return \Sistema\STOCKBundle\Entity\Orden
     */
    public function getOrden()
    {
        return $this->orden;
    }
}
