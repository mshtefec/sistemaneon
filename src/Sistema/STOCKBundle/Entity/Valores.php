<?php

namespace Sistema\STOCKBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Valores
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\STOCKBundle\Entity\ValoresRepository")
 */
class Valores
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="valorHoraOrden", type="float")
     */
    private $valorHoraOrden;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set valorHoraOrden
     *
     * @param  float   $valorHoraOrden
     * @return Valores
     */
    public function setValorHoraOrden($valorHoraOrden)
    {
        $this->valorHoraOrden = $valorHoraOrden;

        return $this;
    }

    /**
     * Get valorHoraOrden
     *
     * @return float
     */
    public function getValorHoraOrden()
    {
        return $this->valorHoraOrden;
    }
}
