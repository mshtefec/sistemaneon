<?php

namespace Sistema\STOCKBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Sistema\STOCKBundle\Form\EventListener\AddClienteSubscriber;
use Sistema\STOCKBundle\Form\EventListener\AddDomicilioSubscriber;

/**
 * OrdenType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class OrdenType extends AbstractType {

    private $entityManager;
    // contiene opciones en un array.
    private $opciones;

    /**
     * Constructor
     *
     * @param EntityManger $entityManager EntityManger
     *
     * @return void;
     */
    public function __construct($entityManager, $opciones) {
        $this->entityManager = $entityManager;
        $this->opciones = $opciones;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('orden_id', 'hidden', array(
                'label' => false,
                'required' => false,
                'mapped' => false,
                'data' => $this->opciones['id']
            ))
            ->add('numero')
        ;
        if ($options['data']->getId()) {
            $builder
                ->add('fecha', 'bootstrapdatetime', array(
                    'label_attr' => array(
                        'class' => 'col-lg-12 col-md-12 col-sm-12',
                    ),
                    'attr' => array(
                        'class' => 'col-lg-12 col-md-12 col-sm-12',
                    ),
                    'read_only' => true,
                    'data' => $options['data']->getFecha(),
                ))
            ;
        } else {
            $builder
                ->add('fecha', 'bootstrapdatetime', array(
                    'label_attr' => array(
                        'class' => 'col-lg-12 col-md-12 col-sm-12',
                    ),
                    'attr' => array(
                        'class' => 'col-lg-12 col-md-12 col-sm-12',
                    ),
                    'read_only' => true,
                    'data' => new \DateTime("today"),
                ))
            ;
        }
        $builder
            ->add('trabajosRealizados', null, array(
                'label_attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                ),
                'attr' => array(
                    'class' => 'col-lg-10 col-md-10 col-sm-10',
                    'rows' => '10', 'cols' => '10'
                ),
                'label' => 'Trabajos Realizados',
            ))
            ->add('cerrada', null, array(
                'label_attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                ),
                'attr' => array(
                    'class' => 'col-lg-4 col-md-4 col-sm-4',
                ),
                'label' => 'Cerrar O.T.',
                'required' => false,
            ))
            ->add('articulos', 'collection', array(
                'label_attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                ),
                'attr' => array(
                    'class' => 'col-lg-4 col-md-4 col-sm-4',
                ),
                'label' => false,
                'type' => new CantOrdenType($this->opciones),
                'allow_add' => true,
                'allow_delete' => true,
                'required' => true,
                'by_reference' => false,
            ))
            ->add('empleados', 'collection', array(
                'label_attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                ),
                'attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                ),
                'label' => false,
                'type' => new EmpleadoOrdenType($this->opciones),
                'allow_add' => true,
                'allow_delete' => true,
                'required' => true,
                'by_reference' => false,
                'prototype' => true,
                'prototype_name' => '__name__',
                'validation_groups' => array('controlHoraEmpleado'),
            ))
            ->add('observaciones', null, array(
                'label_attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                ),
                'attr' => array(
                    'class' => 'col-lg-10 col-md-10 col-sm-10',
                    'rows' => '10', 'cols' => '10'
                ),
                'label' => 'Observaciones',
            ))
            ->add('montoDescuento', null, array(
                'label_attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                ),
                'attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                ),
            ))
            ->add('descuentoOrden', null, array(
                'label_attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                ),
                'attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                ),
            ))
            ->add('empleado', 'entity', array(
                'mapped' => false,
                'class' => 'SistemaRRHHBundle:Empleado',
                'label' => false,
                'attr' => array('class' => 'col-lg-4 col-md-4 col-sm-4')
            ))
            ->add('empleado_show', 'entity', array(
                'mapped' => false,
                'class' => 'SistemaRRHHBundle:Empleado',
                'label' => false,
                'attr' => array('style' => 'display:none;'),
            ))
            ->add('funcion', 'choice', array(
                'label' => 'Función',
                'choices' => array(0 => 'Mantenimiento', 1 => 'Obra', 2 => 'Urgencia'),
                'preferred_choices' => array('Mantenimiento'),
            ))
            ->add('horasPresupuesto', null, array(
                'label_attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                ),
                'attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                ),
            ))
        ;
        if (is_null($this->opciones)) {
            $builder->add('reservaMateriales', null, array(
                'label' => 'Reservar materiales',
                'label_attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                ),
                'required' => false,
                'attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                ),
            ));
        }
        $builder
            ->addEventSubscriber(
                new AddClienteSubscriber($this->entityManager)
            )
            ->addEventSubscriber(
                new AddDomicilioSubscriber($this->entityManager)
            )
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\STOCKBundle\Entity\Orden',
            'cascade_validation' => true,
                // 'validation_group'   => array('controlHoraEmpleado'),
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_stockbundle_orden';
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options) {
        if (!empty($this->opciones['ordenContieneOrdenSalida'])) {
            $view->vars['hideButton'] = true;
        } else {
            $view->vars['hideButton'] = false;
        }
        if (is_null($this->opciones)) {
            $view->vars['isNew'] = true;
        }
    }
}