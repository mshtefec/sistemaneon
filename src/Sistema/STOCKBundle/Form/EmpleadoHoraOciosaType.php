<?php

namespace Sistema\STOCKBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EmpleadoHoraOciosaType extends AbstractType
{
    private $opciones;
    private $empleado;

    public function __construct($em,$opciones, $empleado=null) {
        $this->opciones = $opciones;
        $this->empleado = $empleado;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        if (is_null($builder->getData()->getId())) {
            $isNew = true;
        } else {
            $isNew = false;
        }
        if ($this->opciones == true) {
            $builder->add('fecha', 'date', array(
                'attr' => array(
                    'data-mask' => '99/99/9999',
                ),
                // 'read_only' => true,
                'required' => true,
                'widget' => "single_text",
                'format' => 'dd/MM/yyyy',
            ));
        } else {
            $builder->add('fecha', 'date', array(
                'attr' => array(
                    'data-mask' => '99/99/9999',
                ),
                // 'read_only' => true,
                'data' => new \DateTime("today"),
                'widget' => "single_text",
                'format' => 'dd/MM/yyyy',
            ));
        }
        $builder
            ->add('desde', 'time', array(
                'attr' => array(
                    'data-mask' => '99:99',
                ),
                'widget' => "single_text",
            ))
            ->add('hasta', 'time', array(
                'attr' => array(
                    'data-mask' => '99:99',
                ),
                'widget' => "single_text",
            ))
            ->add('costoHora', null, array(
                'read_only' => true,
            ))
        ;
        //es edit

        if ($isNew) {
            $builder->add('empleado', 'hidden', array(
                'mapped' => false,
                'data' => $this->empleado,
                'required' => false,
            ));
        }
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\STOCKBundle\Entity\EmpleadoHora'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_stockbundle_empleadohora';
    }
}