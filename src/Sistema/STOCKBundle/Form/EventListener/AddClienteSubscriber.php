<?php

namespace Sistema\STOCKBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Sistema\RRHHBundle\Entity\Cliente;

/**
 * AddClienteSubscriber
 *
 * @author Gonzalo Alonso <gonkpo@gmail.com>
 */
class AddClienteSubscriber implements EventSubscriberInterface
{
    private $entityManager;

    /**
    * Constructor
    *
    * @param EntityManger $entityManager EntityManger
    *
    * @return void;
    */
    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_SUBMIT   => 'preSubmit'
        );
    }

    private function addClienteForm($form, $cliente = null)
    {
        $formOptions = array(
            'label' => 'Cliente',
            // 'empty_value' => 'Cliente',
            'class' => 'Sistema\RRHHBundle\Entity\Cliente',
            'url'   => 'autocomplete_get_clientes',
            'required' => true,
            'configs' => array(
                'multiple' => false,//es requerido true o false
                'width'    => 'element',
            ),
            'label_attr' => array(
                'class' => 'col-lg-2 col-md-2 col-sm-2',
            ),
            'attr' => array(
                'class' => 'col-lg-4 col-md-4 col-sm-4 cliente-select',
            ),
        );

        if ($cliente) {
            $formOptions['data'] = $cliente->getId();
        }

        $form->add('cliente', 'select2', $formOptions);
    }

    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();
        if (null === $data) {
            return;
        }

        $accessor = PropertyAccess::getPropertyAccessor();

        $cliente = $accessor->getValue($data, 'cliente');
        $clienteId = ($cliente) ? $cliente->getId() : null;

        $this->addClienteForm($form, $cliente);
    }

    public function preSubmit(FormEvent $event)
    {
        $form = $event->getForm();
        $data = $event->getData();

        if (array_key_exists('cliente', $data)) {
            $cliente = json_decode($data['cliente']);
            if (is_object($cliente)) {
                $cliente = get_object_vars($cliente);
                $clienteId = $cliente['id'];
            } elseif (is_array($cliente)) {
                $clienteId = $cliente[0]->id;
            } else {
                $clienteId = null;
            }
        } else {
            $clienteId = null;
        }

        $em = $this->entityManager;
        $cliente = $em->getRepository('SistemaRRHHBundle:Cliente')->find($clienteId);

        $this->addClienteForm($form, $cliente);
    }
}
