<?php

namespace Sistema\STOCKBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Doctrine\ORM\EntityRepository;
use Sistema\RRHHBundle\Entity\Domicilio;

/**
 * AddDomicilioSubscriber
 *
 * @author Gonzalo Alonso <gonkpo@gmail.com>
 */
class AddDomicilioSubscriber implements EventSubscriberInterface
{
    private $entityManager;

    /**
    * Constructor
    *
    * @param EntityManger $entityManager EntityManger
    *
    * @return void;
    */
    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_SUBMIT   => 'preSubmit'
        );
    }

    private function addDomicilioForm($form, $clienteId, $domicilio = null)
    {
        $formOptions = array(
            'class'         => 'SistemaRRHHBundle:Domicilio',
            'empty_value'   => 'Domicilio',
            'label'         => 'Domicilio',
            'label_attr' => array(
                'class' => 'col-lg-2 col-md-2 col-sm-2',
            ),
            'attr' => array(
                'class' => 'col-lg-4 col-md-4 col-sm-4',
            ),
            'query_builder' => function (EntityRepository $repository) use ($clienteId) {
                $qb = $repository->createQueryBuilder('d')
                    ->innerJoin('d.persona', 'p')
                    ->where('p.id = :clienteId')
                    ->setParameter('clienteId', $clienteId)
                ;

                return $qb;
            }
        );

        if ($domicilio) {
            $formOptions['data'] = $domicilio;
        }

        $form->add('domicilio', null, $formOptions);
    }

    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if (null === $data) {
            return;
        }

        $accessor = PropertyAccess::getPropertyAccessor();

        $cliente = $accessor->getValue($data, 'cliente');
        $clienteId = ($cliente) ? $cliente->getId() : null;
        $domicilio = $accessor->getValue($data, 'domicilio');

        $this->addDomicilioForm($form, $clienteId, $domicilio);
    }

    public function preSubmit(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if (array_key_exists('cliente', $data)) {
            $cliente = json_decode($data['cliente']);
            if (is_object($cliente)) {
                $cliente = get_object_vars($cliente);
                $clienteId = $cliente['id'];
            } elseif (is_array($cliente)) {
                $clienteId = $cliente[0]->id;
            } else {
                $clienteId = null;
            }
        } else {
            $clienteId = null;
        }

        if (array_key_exists('domicilio', $data)) {
            $domicilio = json_decode($data['domicilio']);
        } else {
            $domicilio = null;
        }

        $this->addDomicilioForm($form, $clienteId, $domicilio);
    }
}
