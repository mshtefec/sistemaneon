<?php

namespace Sistema\STOCKBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * MedidaType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class MedidaType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\STOCKBundle\Entity\Medida'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_stockbundle_medida';
    }
}
