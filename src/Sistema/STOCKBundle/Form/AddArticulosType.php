<?php

namespace Sistema\STOCKBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * AddArticulosType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class AddArticulosType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('orden', 'select2', array(
                'label' => 'O.T. Nro',
                'class' => 'Sistema\STOCKBundle\Entity\Orden',
                'url'   => 'autocomplete_get_orden',
                'required' => false,
                'configs' => array(
                    'multiple' => false,//es requerido true o false
                    'width'    => 'element',
                ),
                'label_attr' => array(
                    'class' => 'col-lg-2',
                ),
                'attr' => array(
                    'class' => 'col-lg-4',
                )
            ))
            ->add('articulos', 'collection', array(
                'label_attr' => array(
                    'class' => 'col-lg-2',
                ),
                'attr' => array(
                    'class' => 'col-lg-4',
                ),
                'label' => false,
                'type' => new CantAddArticuloType(),
                'allow_add' => true,
                'allow_delete' => true,
                'required' => true,
                'by_reference' => false,
            ));
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\STOCKBundle\Entity\AddArticulos',
            'cascade_validation' => true,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_stockbundle_addarticulos';
    }
}
