<?php

namespace Sistema\STOCKBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CantAddArticuloType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cantidad')
            ->add('articulo', 'select2articulo', array(
                'label' => null,
                'class' => 'Sistema\STOCKBundle\Entity\Articulo',
                'url'   => 'autocomplete_get_articulos_activos',
                'required' => true,
                'configs' => array(
                    'multiple' => false, //es requerido true o false
                    'width'    => '100%',
                    'cantidadMaxima'    => false, //es requerido true o false
                    'valCantidadMaxima' => false,
                    'entidad' => 'articulo',
                ),
                'attr' => array(
                    'class' => 'col-lg-12 col-md-12 col-sm-12 input-sm',
                    'style' => 'min-width: 300px;',
                ),
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\STOCKBundle\Entity\CantAddArticulo'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_stockbundle_cantaddarticulo';
    }

}
