<?php

namespace Sistema\STOCKBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CantAddArticuloPrecioSubTotalType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('cantidad')
                ->add('articulo', 'select2articuloSubtotal', array(
                    'label' => null,
                    'class' => 'Sistema\STOCKBundle\Entity\Articulo',
                    'url' => 'autocomplete_get_articulos_activos_precio',
                    'required' => true,
                    'configs' => array(
                        'multiple' => false, //es requerido true o false
                        'width' => '100%',
                        'cantidadMaxima' => false, //es requerido true o false
                        'valCantidadMaxima' => false,
                        'entidad' => 'articulo',
                        'calcularValorEnCero' => true,
                        'calcularSubTotal' => true,
                        'calcularTotal' => false,
                    ),
                    'attr' => array(
                        'class' => 'col-lg-12 col-md-12 col-sm-12 input-sm',
                        'style' => 'min-width: 300px;',
                    ),
                ))
                ->add('precio', 'text', array(
                    'label' => 'Precio',
                    'attr' => array(
                        'class' => 'txtPrecio'
                    ),
                ))
                ->add('subTotal', 'text', array(
                    'label' => 'sub total',
                    'required' => false,
                    'mapped' => false,
                    'read_only' => true,
                    'attr' => array(
                        'class' => 'articuloSubtotal',
                    ),
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\STOCKBundle\Entity\CantAddArticulo'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_stockbundle_cantaddarticulo';
    }

}
