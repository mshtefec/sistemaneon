<?php

namespace Sistema\STOCKBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * EntregasHerramientasType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class EntregasHerramientasType extends AbstractType
{
     // contiene opciones en un array.
    private $opciones;

    /**
     * Constructor
     *
     * @param EntityManger $entityManager EntityManger
     *
     * @return void;
     */
    public function __construct($opciones)
    {
        $this->opciones = $opciones;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('empleado', null, array(
                    'label_attr' => array(
                        'class' => 'col-lg-2',
                    ),
                    'attr' => array(
                        'class' => 'col-lg-4',
                    ),
                ))
                ->add('orden', 'select2', array(
                    'label' => 'O.T. Nro',
                    'label_attr' => array(
                        'class' => 'col-lg-2',
                    ),
                    'attr' => array(
                        'class' => 'col-lg-4',
                    ),
                    'required' => false,
                    'class' => 'Sistema\STOCKBundle\Entity\Orden',
                    'url'   => 'autocomplete_get_orden_desc',
                    'required'  => true,
                    //'read_only' => false,
                    'configs' => array(
                        'multiple' => false,//es requerido true o false
                        'cantidadMaxima' => true,//es requerido true o false
                        'valCantidadMaxima' => false,//es requerido true o false
                        //'entidad' => 'articulo',//es requerido if cantidadMaxima = true
                    )))
                ->add('herramientas', 'collection', array(
                    'label_attr' => array(
                        'class' => 'col-lg-2',
                    ),
                    'attr' => array(
                        'class' => 'col-lg-4',
                    ),
                    'label' => false,
                    'type' => new CantEntregaHerramientaType($this->opciones),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'required' => true,
                    'by_reference' => false,
                ));
        /* ->add('herramientas', 'select2', array(
          'label' => 'Herramienta',
          'class' => 'Sistema\STOCKBundle\Entity\Herramienta',
          'url'   => 'autocomplete_get_herramientas',
          'required' => true,
          'configs' => array(
          'multiple' => true,//es requerido true o false
          'width'    => 'element',
          ),
          'label_attr' => array(
          'class' => 'col-lg-2',
          ),
          'attr' => array(
          'class' => 'col-lg-10',
          ),
          )) */
        /* ->add('herramienta', null, array(
          'label_attr' => array(
          'class' => 'col-lg-2',
          ),
          'attr' => array(
          'class' => 'col-lg-4',
          ),
          )) */
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\STOCKBundle\Entity\EntregasHerramientas',
            'cascade_validation' => true,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_stockbundle_entregasherramientas';
    }
}
