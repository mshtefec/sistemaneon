<?php

namespace Sistema\STOCKBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

/**
 * OrdenSalidaType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class OrdenSalidaType extends AbstractType
{
    // contiene opciones en un array.
    private $opciones;

    /**
     * Constructor
     *
     * @param EntityManger $entityManager EntityManger
     *
     * @return void;
     */
    public function __construct($opciones)
    {
        $this->opciones = $opciones;
        
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // ->add('created')
            // ->add('updated')
            // ->add('ordenEntrada')
            ->add('obtenerSalidaId', 'hidden')
            ->add('orden', 'select2', array(
                'label' => 'O.T. Nro:',
                'class' => 'Sistema\STOCKBundle\Entity\Orden',
                'url'   => 'autocomplete_get_orden',
                'required'  => true,
                'read_only' => true,
                'configs' => array(
                    'multiple' => false,//es requerido true o false
                    // 'width'    => 'element',
                ),
                'label_attr' => array(
                    'class' => 'col-lg-2 control-label',
                ),
                'attr' => array(
                    'class' => 'col-lg-2',
                ),
            ))
            ->add('articulos', 'collection', array(
                'label_attr' => array(
                    'class' => 'col-lg-2',
                ),
                'attr' => array(
                    'class' => 'col-lg-4',
                ),
                'label'        => false,
                'type'         => new CantOrdenSalidaType($this->opciones),
                'allow_add'    => true,
                'allow_delete' => true,
                'required'     => true,
                'by_reference' => false,
            ))
            ->add('empleado', 'entity', array(
                'class' => 'SistemaRRHHBundle:Empleado',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->where('e.activo = 1')
                        ->orderBy('e.apellido', 'ASC');
                },
                'attr' => array(
                    'class' => 'col-lg-4',
                ),
                'label_attr' => array(
                    'class' => 'col-lg-3',
                ),
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\STOCKBundle\Entity\OrdenSalida',
            'cascade_validation' => true,
            'hideButton' => true
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_stockbundle_ordensalida';
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['hideButton'] = 1;
    }
}
