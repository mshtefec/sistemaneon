<?php

namespace Sistema\STOCKBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CantOrdenSalidaType extends AbstractType
{
    // contiene opciones en un array.
    private $opciones;

    /**
     * Constructor
     */
    public function __construct($opciones)
    {
        // si contiene valCantidadMaxima se utiliza el valor de cantidad maxima.
        if (!empty($opciones['valCantidadMaxima'])) {
            $this->opciones['valCantidadMaxima'] = true;
        } else {
            $this->opciones['valCantidadMaxima'] = false;
        }
        
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            /*->add('carry_id', 'hidden', array(
                'label' => false,
                'required' => false,
                'mapped' => false,
                'data' => $this->id
                ))*/
            ->add('articulo', 'select2articulo', array(
                'label' => 'Articulo',
                'class' => 'Sistema\STOCKBundle\Entity\Articulo',
                'url'   => 'autocomplete_get_articulos',
                'required' => true,
                'read_only' => true,
                'configs' => array(
                    'multiple' => false,//es requerido true o false
                    'width'    => '100%',//es requerido
                    'cantidadMaxima' => true,//es requerido true o false
                    'valCantidadMaxima' => $this->opciones['valCantidadMaxima'],//es requerido true o false
                    'entidad' => 'articulo',//es requerido if cantidadMaxima = true
                ),
                // 'label_attr' => array(
                //     'class' => 'col-lg-1',
                //     'style' => 'width: 10%;',
                // ),
                'attr' => array(
                    'class' => 'col-lg-12 col-md-12 col-sm-12 input-sm',
                    'style' => 'min-width: 300px;',
                ),
            ))
            ->add('cantidadMaxima', null, array(
                'read_only' => true,
                // 'label_attr' => array(
                //     'class' => 'col-lg-1',
                //     'style' => 'width: 10%;',
                // ),
                'attr' => array(
                    'col'   => 'col-lg-12 col-md-12 col-sm-12',
                    'style' => 'min-width: 60px;',
                    'class' => 'input-sm',

                ),
            ))
            ->add('cantidad', null, array(
                // 'label_attr' => array(
                //     'class' => 'col-lg-1',
                //     'style' => 'width: 10%;',
                // ),
                'attr' => array(
                    'col'   => 'col-lg-12 col-md-12 col-sm-12',
                    'style' => 'min-width: 60px;',
                    'class' => 'input-sm',
                    'onchange' => 'validacion(this);',
                ),
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\STOCKBundle\Entity\CantOrdenSalida'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_stockbundle_cantordensalida';
    }
}
