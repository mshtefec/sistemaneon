<?php

namespace Sistema\STOCKBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;

/**
 * OrdenFilterType filtro.
 * @author Nombre Apellido <name@gmail.com>
 */
class OrdenFilterType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            /*->add('created', 'filter_date_range',array(
                        'attr'=> array('class'=>'form-control'),
                        'label' => 'Creada',
                    ))
            ->add('updated', 'filter_date_range',array(
                        'attr'=> array('class'=>'form-control'),
                        'label' => 'Actualizada',
                    ))
            ->add('fecha', 'filter_date_range',array(
                        'attr'=> array('class'=>'form-control')
                    ))*/
             ->add('numero', 'filter_text',array(
                        'attr'=> array('class'=>'form-control')
                    ))
             ->add('cliente', 'select2',array(
                         'class' => 'Sistema\RRHHBundle\Entity\Cliente',
                          'url'   => 'autocomplete_get_clientes',
                            'configs' => array(
                                'multiple' => false,//es requerido true o false
                            // 'width'    => 'element',
                            ),
                        'attr'=> array('class'=>'form-control')
                    ))
           /* ->add('domicilio', 'filer_entity',array(
                        'class' => 'Sistema\RRHHBundle\Entity\Domicilio',
                        'attr'=> array('class'=>'form-control')
                    ))

            ->add('trabajosRealizados', 'filter_text',array(
                        'attr'=> array('class'=>'form-control'),
                        'label' => 'Trabajos Realizados'
                    ))
            ->add('observaciones', 'filter_text',array(
                        'attr'=> array('class'=>'form-control')
                    ))*/

            /*->add('montoOrden', 'filter_number',array(
                        'attr'=> array('class'=>'form-control')
                    ))*/
            ->add('cerrada', 'filter_boolean',array(
                        'attr'=> array('class'=>'form-control')
                    ))
        ;

        $listener = function (FormEvent $event) {
            // Is data empty?
            foreach ((array) $event->getForm()->getData() as $data) {
                if ( is_array($data)) {
                    foreach ($data as $subData) {
                        if (!empty($subData)) {
                            return;
                        }
                    }
                } else {
                    if (!empty($data)) {
                        return;
                    }
                }
            }
            $event->getForm()->addError(new FormError('Filter empty'));
        };
        $builder->addEventListener(FormEvents::POST_SUBMIT, $listener);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\STOCKBundle\Entity\Orden',
            'csrf_protection'   => true,
            'validation_groups' => array('filtering')
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_stockbundle_ordenfiltertype';
    }
}
