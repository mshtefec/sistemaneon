<?php

namespace Sistema\STOCKBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class EmpleadoOrdenType extends AbstractType
{
    private $opciones;

    public function __construct($opciones)
    {
        $this->opciones = $opciones;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('id','hidden')
            ->add('empleado', 'entity', array(
                'class' => 'SistemaRRHHBundle:Empleado',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->where('e.activo = 1')
                        ->orderBy('e.apellido', 'ASC');
                },
                'empty_value' => 'Seleccione empleado',
                'label_attr' =>  array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                ),
                'attr' => array(
                    'class' => 'col-lg-3 col-md-3 col-sm-3 input-sm',
                ),
            ))
            ->add('empleadoHoras', 'collection', array(
                'label'          => false,
                'type'           => new EmpleadoHoraType($this->opciones),
                'allow_add'      => true,
                'allow_delete'   => true,
                'required'       => true,
                'by_reference'   => false,
                'prototype_name' => '__nameembed__',
                'validation_groups' => array('controlHoraEmpleado'),
            ))
            //->add('orden')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\STOCKBundle\Entity\EmpleadoOrden',
            'cascade_validation' => true,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_stockbundle_empleadoorden';
    }
}
