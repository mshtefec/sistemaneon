<?php

namespace Sistema\STOCKBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CantEntregaHerramientaType extends AbstractType
{
     // contiene opciones en un array.
    private $opciones;

    /**
     * Constructor
     */
    public function __construct($opciones)
    {
        // si contiene valCantidadMaxima se utiliza el valor de cantidad maxima.
        if (!empty($opciones['valCantidadMaxima'])) {
            $this->opciones['valCantidadMaxima'] = true;
        } else {
            $this->opciones['valCantidadMaxima'] = false;
        }
    }
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('cantidad',null, array(
                'attr' => array(
                    'onchange'=>'validacion(this);',
                ),
                ))
                /* ->add('herramientas', 'select2', array(
                  'label' => 'Herramienta',
                  'class' => 'Sistema\STOCKBundle\Entity\Herramienta',
                  'url' => 'autocomplete_get_herramientas',
                  'required' => true,
                  'configs' => array(
                  'multiple' => true, //es requerido true o false
                  'width' => 'element',
                  ),
                  'label_attr' => array(
                  'class' => 'col-lg-2',
                  ),
                  'attr' => array(
                  'class' => 'col-lg-10',
                  ),
                  )) */
                ->add('cantidadMaxima', null, array(
                'read_only' => true,
                // 'label_attr' => array(
                //     'class' => 'col-lg-1',
                //     'style' => 'width: 10%;',
                // ),
                'attr' => array(
                    'col'   => 'col-lg-6',
                    'class' => 'input-sm',
                ),
            ))
                ->add('herramienta', 'select2articulo', array(
                    'label' => 'Herramientas',
                    'class' => 'Sistema\STOCKBundle\Entity\Herramienta',
                    'url' => 'autocomplete_get_herramientas',
                    'required' => true,
                    'configs' => array(
                        'multiple' => false, //es requerido true o false
                        'width' => '100%',
                        'cantidadMaxima' => true, //es requerido true o false
                        'valCantidadMaxima' =>  $this->opciones['valCantidadMaxima'],
                        'entidad'=>'herramienta',
            )))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\STOCKBundle\Entity\CantEntregaHerramienta'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_stockbundle_cantentregaherramienta';
    }

}
