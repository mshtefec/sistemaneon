<?php

namespace Sistema\STOCKBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

/**
 * ArticuloType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class ArticuloType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //compruebo si el formulario es new o edit
        if (is_null($builder->getData()->getId())) {
            $isNew = true;
        } else {
            $isNew = false;
        }
        //fin compruebo si el formulario es new o edit
        $builder
            ->add('codigo', null, array(
                'label' => 'Código',
                'label_attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                ),
            ))
            ->add('cantidad', null, array(
                'attr' => array(
                    'onchange' => 'validacionArt(this);'
                    ),
                'label_attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                    // 'style' => 'width: 10%;'
                ),
            ))
        ;
        if (!$isNew) {
        $builder
            ->add('cantidadUtilizada', null, array(
                'label' => 'Utilizados',
                'read_only' => true,

                'label_attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                    // 'style' => 'width: 10%;'
                ),
            ))
        ;
        }
        $builder
            ->add('medida', null, array(
                'label_attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                    // 'style' => 'width: 10%;'
                ),
            ))
            // ->add('tipo', null, array(
            //     'label_attr' => array(
            //         'class' => 'col-lg-2 control-label',
            //         // 'style' => 'width: 10%;'
            //     ),
            // ))
            ->add('tipo', 'select2', array(
                'label' => 'Tipo',
                'class' => 'Sistema\STOCKBundle\Entity\ArticuloTipo',
                'url'   => 'autocomplete_get_articulo_tipos',
                'required' => false,
                'configs' => array(
                    'multiple' => false,//es requerido true o false
                    // 'width'    => 'element',
                ),
                'label_attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                ),
                'attr' => array(
                    'class' => 'col-lg-4 col-md-4 col-sm-4',
                ),
            ))
            ->add('descripcion', null, array(
                'label' => 'Descripción',
                'label_attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                    // 'style' => 'width: 12%;'
                ),
            ))
            ->add('rubro', 'entity', array(
                'class' => 'SistemaSTOCKBundle:Rubro',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('r')
                        ->orderBy('r.nombre', 'ASC');
                },
                'label_attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                ),
            ))
            ->add('precio', null, array(
                'label_attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                ),
            ))
            ->add('activo', null, array(
                'label_attr' => array(
                    'class' => 'col-lg-1 col-md-1 col-sm-1',
                ),
                'required' => false
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\STOCKBundle\Entity\Articulo'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_stockbundle_articulo';
    }

}
