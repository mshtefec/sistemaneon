<?php

namespace Sistema\STOCKBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CantOrdenType extends AbstractType
{
    // contiene opciones en un array.
    private $opciones;

    /**
     * Constructor
     */
    public function __construct($opciones)
    {
        // si contiene ordenContieneOrdenSalida articulos no se pueden cambiar.
        if (!empty($opciones['ordenContieneOrdenSalida'])) {
            $this->opciones['read_only'] = true;
        } else {
            $this->opciones['read_only'] = false;
        }
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // si read_only es true articulos no se pueden cambiar.
        if ($this->opciones['read_only']) {
            $builder
                ->add('articulo', 'select2articulo', array(
                    'label' => 'Articulo',
                    'class' => 'Sistema\STOCKBundle\Entity\Articulo',
                    'url'   => 'autocomplete_get_articulos_activos',
                    'required'  => true,
                    //'read_only' => false,
                    'configs' => array(
                        'multiple' => false,//es requerido true o false
                        'width'    => '100%',
                        'cantidadMaxima' => true,//es requerido true o false
                        'valCantidadMaxima' => false,//es requerido true o false
                        'entidad' => 'articulo',//es requerido if cantidadMaxima = true
                    ),
                    'attr' => array(
                        'class' => 'col-lg-12 col-md-12 col-sm-12 input-sm',
                        'style' => 'min-width: 300px;',
                    ),
                ))
            ;
        } else {
            $builder
                ->add('articulo', 'select2articulo', array(
                    'label' => 'Articulo',
                    'class' => 'Sistema\STOCKBundle\Entity\Articulo',
                    'url'   => 'autocomplete_get_articulos_activos',
                    'required' => true,
                    'configs' => array(
                        'multiple' => false,//es requerido true o false
                        'width'    => '100%',
                        'cantidadMaxima' => true,//es requerido true o false
                        'valCantidadMaxima' => false,//es requerido true o false
                        'entidad' => 'articulo',//es requerido if cantidadMaxima = true
                    ),
                    'attr' => array(
                        'class' => 'col-lg-12 col-md-12 col-sm-12 input-sm',
                        'style' => 'min-width: 300px;',
                    ),
                ))
            ;
        }
        $builder
            ->add('cantidadMaxima', null, array(
                'read_only' => true,
                // 'label_attr' => array(
                //     'class' => 'col-lg-1',
                //     'style' => 'width: 10%;',
                // ),
                'attr' => array(
                    'col'   => 'col-lg-12 col-md-12 col-sm-12',
                    'style' => 'min-width: 60px;',
                    'class' => 'input-sm',
                ),
            ))
            ->add('cantidad', null, array(
                // 'label_attr' => array(
                //     'class' => 'col-lg-1',
                //     'style' => 'width: 10%;',
                // ),
                'attr' => array(
                    'col'   => 'col-lg-12 col-md-12 col-sm-12',
                    'class' => 'input-sm',
                    'style' => 'min-width: 60px;',
                    'onchange' => 'validacion(this);',
                ),
            ))
            /*->add('carry_id', 'hidden', array(
                'label' => false,
                'required' => false,
                'mapped' => false,
                'data' => $this->id
                ))*/
                ->add('obtenerId', 'hidden')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\STOCKBundle\Entity\CantOrden'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_stockbundle_cantorden';
    }
}
