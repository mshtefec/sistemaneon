<?php

namespace Sistema\STOCKBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;
use Doctrine\ORM\EntityRepository;

/**
 * ArticuloFilterType filtro.
 * @author Nombre Apellido <name@gmail.com>
 */
class ArticuloFilterType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('codigo', 'filter_text',array(
                'attr' => array(
                    'class' => 'form-control',
                )
            ))
            ->add('rubro', 'filter_entity',array(
                'attr' => array(
                    'class'=>'form-control',
                ),
                'class' => 'Sistema\STOCKBundle\Entity\Rubro',
                'query_builder' => function (EntityRepository $repository) {
                    $qb = $repository->createQueryBuilder('r')
                        ->orderBy('r.nombre', 'ASC')
                    ;

                    return $qb;
                }
            ))
            ->add('tipo', 'filter_entity',array(
                'attr'=> array('class'=>'form-control'),
                'class' => 'Sistema\STOCKBundle\Entity\ArticuloTipo',
                'query_builder' => function (EntityRepository $repository) {
                    $qb = $repository->createQueryBuilder('at')
                        ->orderBy('at.nombre', 'ASC')
                    ;

                    return $qb;
                }
            ))
            ->add('descripcion', 'filter_text_like',array(
                'attr'=> array(
                    'class' => 'form-control',
                    'label' => 'Descripción',
                )
            ))
/*            ->add('precio', 'filter_number_range',array(
                        'attr'=> array('class'=>'form-control')
                    ))
            ->add('activo', 'filter_boolean')*/
        ;

        $listener = function (FormEvent $event) {
            // Is data empty?
            foreach ((array) $event->getForm()->getData() as $data) {
                if ( is_array($data)) {
                    foreach ($data as $subData) {
                        if (!empty($subData)) {
                            return;
                        }
                    }
                } else {
                    if (!empty($data)) {
                        return;
                    }
                }
            }
            $event->getForm()->addError(new FormError('Filter empty'));
        };
        $builder->addEventListener(FormEvents::POST_SUBMIT, $listener);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\STOCKBundle\Entity\Articulo',
            'csrf_protection'   => true,
            'validation_groups' => array('filtering')
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_stockbundle_articulofiltertype';
    }
}
