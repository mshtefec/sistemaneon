<?php

namespace Sistema\STOCKBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * OrdenEntradaType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class OrdenEntradaType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('created')
            //->add('updated')
            ->add('ordenSalida', 'select2', array(
                'label' => 'Entrega de Materiales Nro:',
                'class' => 'Sistema\STOCKBundle\Entity\OrdenSalida',
                'url'   => 'autocomplete_get_ordensalida',
                'required' => true,
                'read_only' => true,
                'configs' => array(
                    'multiple' => false,//es requerido true o false
                    // 'width'    => 'element',
                ),
                'label_attr' => array(
                    'class' => 'col-lg-2 control-label',
                ),
                'attr' => array(
                    'class' => 'col-lg-2',
                ),
            ))
             ->add('articulos', 'collection', array(
                'label_attr' => array(
                    'class' => 'col-lg-2',
                ),
                'attr' => array(
                    'class' => 'col-lg-4',
                ),
                'label'        => false,
                'type'         => new CantOrdenEntradaType(),
                'allow_add'    => true,
                'allow_delete' => true,
                'required'     => true,
                'by_reference' => false,
            ))
            ->add('obtenerEntradaId', 'hidden')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\STOCKBundle\Entity\OrdenEntrada',
            'cascade_validation' => true,
            'hideButton' => true
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_stockbundle_ordenentrada';
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['hideButton'] = 1;
    }

}
