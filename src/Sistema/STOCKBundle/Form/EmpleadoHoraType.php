<?php

namespace Sistema\STOCKBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EmpleadoHoraType extends AbstractType
{
    private $opciones;

    public function __construct($opciones)
    {
        $this->opciones = $opciones;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($this->opciones == true) {
            $builder->add('fecha', 'date', array(
                'label' => false,
                // 'label_attr' => array(
                //     'class' => 'col-lg-1 col-md-1 col-sm-1',
                // ),
                'attr' => array(
                    'class' => 'col-lg-3 col-md-3 input-sm',
                    'data-mask' => '99/99/9999',
                ),
                'required' => true,
                'widget' => "single_text",
                'format' => 'dd/MM/yyyy',
            ));
        } else {
            $builder->add('fecha', 'date', array(
                'label' => false,
                // 'label_attr' => array(
                //     'class' => 'col-lg-1 col-md-1 col-sm-1',
                // ),
                'attr' => array(
                    'class' => 'col-lg-3 col-md-3 input-sm',
                    'data-mask' => '99/99/9999',
                ),
                'data' => new \DateTime("today"),
                'widget' => "single_text",
                'format' => 'dd/MM/yyyy',
            ));
        }
        $builder
            ->add('desde','time', array(
                'label' => false,
                // 'label_attr' => array(
                //     'class' => 'col-lg-1 col-md-1 col-sm-1',
                // ),
                'attr' => array(
                    'class' => 'col-lg-2 col-md-3 input-sm',
                    'data-mask' => '99:99',
                ),
                'widget' => "single_text",
            ))
            ->add('hasta','time',array(
                'label' => false,
                // 'label_attr' => array(
                //     'class' => 'col-lg-1 col-md-1 col-sm-1',
                // ),
                'attr' => array(
                    'class' => 'col-lg-2 col-md-3 input-sm',
                    'data-mask' => '99:99',
                ),
                'widget' => "single_text",
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\STOCKBundle\Entity\EmpleadoHora'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_stockbundle_empleadohora';
    }
}