<?php

namespace Sistema\STOCKBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CantOrdenEntradaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // ->add('ordenEntrada')
            ->add('articulo', 'select2articulo', array(
                'label' => 'Articulo',
                'class' => 'Sistema\STOCKBundle\Entity\Articulo',
                'url'   => 'autocomplete_get_articulos',
                'required' => true,
                'read_only' => true,
                'configs' => array(
                    'multiple' => false,//es requerido true o false
                    'width'    => '100%',
                    'cantidadMaxima' => false,//es requerido true o false
                    'valCantidadMaxima' => false,//es requerido true o false
                ),
                // 'label_attr' => array(
                //     'class' => 'col-lg-1',
                //     'style' => 'width: 10%;',
                // ),
                'attr' => array(
                    'class' => 'col-lg-12 col-md-12 col-sm-12 input-sm',
                    'style' => 'min-width: 300px;',
                ),
            ))
            ->add('cantidadMaxima', null, array(
                'read_only' => true,
                // 'label_attr' => array(
                //     'class' => 'col-lg-1',
                //     'style' => 'width: 10%;',
                // ),
                'attr' => array(
                    'col'   => 'col-lg-12 col-md-12 col-sm-12',
                    'style' => 'min-width: 60px;',
                    'class' => 'input-sm',
                ),
            ))
            ->add('cantidad', null, array(
                // 'label_attr' => array(
                //     'class' => 'col-lg-1',
                //     'style' => 'width: 10%;',
                // ),
                'attr' => array(
                    'col'   => 'col-lg-12 col-md-12 col-sm-12',
                    'style' => 'min-width: 60px;',
                    'class' => 'input-sm',
                    'onchange' => 'validacion(this);',
                ),
            ))
        ;

    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\STOCKBundle\Entity\CantOrdenEntrada'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_stockbundle_cantordenentrada';
    }
}
