<?php

namespace Sistema\STOCKBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\STOCKBundle\Entity\EmpleadoHora;
use Sistema\STOCKBundle\Entity\EmpleadoOrden;
use Sistema\STOCKBundle\Form\EmpleadoHoraOciosaType;
use Sistema\STOCKBundle\Form\EmpleadoHoraFilterType;

/**
 * EmpleadoHora controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/empleadohora")
 */
class EmpleadoHoraController extends Controller {

    /**
     * Lists all EmpleadoHora entities.
     *
     * @Route("/", name="admin_empleadohora")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        list($filterForm, $queryBuilder) = $this->filter(false);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $queryBuilder, $this->get('request')->query->get('page', 1), (isset($this->container->parameters['knp_paginator.page_range'])) ? $this->container->parameters['knp_paginator.page_range'] : 10
        );

        return array(
            'entities' => $pagination,
            'filterForm' => $filterForm->createView(),
        );
    }

    /**
     * Lists all EmpleadoHora entities.
     *
     * @Route("/horas-ociosas/{empleado}", name="admin_empleadohora_horasOciosas")
     * @Method("GET")
     * @Template()
     */
    public function horasOciosasAction($empleado) {
        $em = $this->getDoctrine()->getManager();
        list($filterForm, $queryBuilder) = $this->filter(true, $empleado);
        $entity = $em->getRepository('SistemaRRHHBundle:Empleado')->find($empleado);
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $queryBuilder, $this->get('request')->query->get('page', 1), (isset($this->container->parameters['knp_paginator.page_range'])) ? $this->container->parameters['knp_paginator.page_range'] : 10
        );

        return array(
            'entities' => $pagination,
            'filterForm' => $filterForm->createView(),
            'empleado' => $empleado,
            'entity' => $entity
        );
    }

    /**
     * Process filter request.
     *
     * @return array
     */
    protected function filter($horasOciosas = false, $empleado = 0) {
        $request = $this->getRequest();
        $session = $request->getSession();
        $filterForm = $this->createFilterForm();
        $em = $this->getDoctrine()->getManager();
        if ($horasOciosas) {
            $queryBuilder = $em->getRepository('SistemaSTOCKBundle:EmpleadoHora')
                    ->createQueryBuilder('a')
                    ->leftJoin('a.empleadoOrden', 'eo')
                    ->where('eo.orden is null')
                    ->andWhere('eo.empleado = :emp')
                    ->orderBy('a.id', 'DESC')
                    ->setParameter('emp', $empleado)
            ;
        } else {
            $queryBuilder = $em->getRepository('SistemaSTOCKBundle:EmpleadoHora')
                    ->createQueryBuilder('a')
                    ->orderBy('a.id', 'DESC')
            ;
        }
        // Bind values from the request
        $filterForm->handleRequest($request);
        // Reset filter
        if ($filterForm->get('reset')->isClicked()) {
            $session->remove('EmpleadoHoraControllerFilter');
            $filterForm = $this->createFilterForm();
        }

        // Filter action
        if ($filterForm->get('filter')->isClicked()) {
            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('EmpleadoHoraControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('EmpleadoHoraControllerFilter')) {
                $filterData = $session->get('EmpleadoHoraControllerFilter');
                $filterForm = $this->createFilterForm($filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }

    /**
     * Create filter form.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createFilterForm($filterData = null) {
        $form = $this->createForm(new EmpleadoHoraFilterType(), $filterData, array(
            'action' => $this->generateUrl('admin_empleadohora'),
            'method' => 'GET',
        ));

        $form
                ->add('filter', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.index.filter',
                    'attr' => array('class' => 'btn btn-success col-lg-1'),
                ))
                ->add('reset', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.index.reset',
                    'attr' => array('class' => 'btn btn-danger col-lg-1 col-lg-offset-1'),
                ))
        ;

        return $form;
    }

    /**
     * Creates a new EmpleadoHora entity.
     *
     * @Route("/", name="admin_empleadohora_create")
     * @Method("POST")
     * @Template("SistemaSTOCKBundle:EmpleadoHora:new.html.twig")
     */
    public function createAction(Request $request) {
        $entity = new EmpleadoHora();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $request = $this->getRequest();

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $idEmpleado = $form->get('empleado')->getData();
            $empleado = $em->getRepository('SistemaRRHHBundle:Empleado')->find($idEmpleado);

            if (!is_null($empleado)) {
                $empleadoOrden = new EmpleadoOrden();
                $empleadoOrden->setEmpleado($empleado);
                $empleadoOrden->setCostoHoraCliente(0);
                $empleadoOrden->addEmpleadoHora($entity);
            }

            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl('admin_empleadohora_new') : $this->generateUrl('admin_empleadohora_show', array('id' => $entity->getId()));
            return $this->redirect($nextAction);
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
            'empleado' => $idEmpleado
        );
    }

    /**
     * Creates a form to create a EmpleadoHora entity.
     *
     * @param EmpleadoHora $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(EmpleadoHora $entity, $empleado = null, array $opciones = null) {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(new EmpleadoHoraOciosaType($em, $opciones, $empleado), $entity, array(
            'action' => $this->generateUrl('admin_empleadohora_create'),
            'method' => 'POST',
        ));

        $form
                ->add(
                        'save', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.save',
                    'attr' => array('class' => 'btn btn-success col-lg-2')
                        )
                )
                ->add(
                        'saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
                        )
                )
        ;

        return $form;
    }

    /**
     * Displays a form to create a new EmpleadoHora entity.
     *
     * @Route("/new/{empleado}", name="admin_empleadohora_new", defaults={"empleado" = 0})
     * @Method("GET")
     * @Template()
     */
    public function newAction($empleado) {
        $em = $this->getDoctrine()->getManager();
        $entity = new EmpleadoHora();
        $empleadoDb = $em->getRepository('SistemaRRHHBundle:Empleado')->find($empleado);
        $entity->setCostoHora($empleadoDb->getCargo()->getCostoHora());
        $form = $this->createCreateForm($entity, $empleado);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a EmpleadoHora entity.
     *
     * @Route("/{id}", name="admin_empleadohora_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaSTOCKBundle:EmpleadoHora')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EmpleadoHora entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing EmpleadoHora entity.
     *
     * @Route("/{id}/edit", name="admin_empleadohora_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaSTOCKBundle:EmpleadoHora')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EmpleadoHora entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a EmpleadoHora entity.
     *
     * @param EmpleadoHora $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(EmpleadoHora $entity, array $opciones = null) {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new EmpleadoHoraOciosaType($em, $opciones), $entity, array(
            'action' => $this->generateUrl('admin_empleadohora_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form
                ->add(
                        'save', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.save',
                    'attr' => array('class' => 'btn btn-success col-lg-2')
                        )
                )
                ->add(
                        'saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
                        )
                )
        ;

        return $form;
    }

    /**
     * Edits an existing EmpleadoHora entity.
     *
     * @Route("/{id}", name="admin_empleadohora_update")
     * @Method("PUT")
     * @Template("SistemaSTOCKBundle:EmpleadoHora:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaSTOCKBundle:EmpleadoHora')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EmpleadoHora entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked() ? $this->generateUrl('admin_empleadohora_new') : $this->generateUrl('admin_empleadohora_show', array('id' => $id));
            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a EmpleadoHora entity.
     *
     * @Route("/{id}", name="admin_empleadohora_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SistemaSTOCKBundle:EmpleadoHora')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find EmpleadoHora entity.');
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.delete.success');
        }

        return $this->redirect($this->generateUrl('admin_empleadohora'));
    }

    /**
     * Creates a form to delete a EmpleadoHora entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        $mensaje = $this->get('translator')->trans('views.recordactions.confirm', array(), 'MWSimpleCrudGeneratorBundle');
        $onclick = 'return confirm("' . $mensaje . '");';
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('admin_empleadohora_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array(
                            'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                            'label' => 'views.recordactions.delete',
                            'attr' => array(
                                'class' => 'btn btn-danger col-lg-11',
                                'onclick' => $onclick,
                            )
                        ))
                        ->getForm()
        ;
    }

}
