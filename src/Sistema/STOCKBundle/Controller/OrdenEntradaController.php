<?php

namespace Sistema\STOCKBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\STOCKBundle\Entity\OrdenEntrada;
use Sistema\STOCKBundle\Form\OrdenEntradaType;
use Sistema\STOCKBundle\Form\OrdenEntradaFilterType;
use Sistema\STOCKBundle\Entity\CantOrdenEntrada;

/**
 * OrdenEntrada controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/ordenentrada")
 */
class OrdenEntradaController extends Controller
{
    /**
     * Lists all OrdenEntrada entities.
     *
     * @Route("/{id}", name="admin_ordenentrada")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($id)
    {
        $this->get('security_role')->controlRolesUser();
        list($filterForm, $queryBuilder) = $this->filter($id);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $queryBuilder, $this->get('request')->query->get('page', 1), (isset($this->container->parameters['knp_paginator.page_range'])) ? $this->container->parameters['knp_paginator.page_range'] : 10
        );

        return array(
            'entities' => $pagination,
            'idOrden' => $id,
            'filterForm' => $filterForm->createView(),
        );
    }

    /**
     * Process filter request.
     *
     * @return array
     */
    protected function filter($id)
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        $filterForm = $this->createFilterForm($id, null);
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository('SistemaSTOCKBundle:OrdenEntrada')
                ->createQueryBuilder('a')
                ->select('a, o')
                ->join('a.ordenSalida', 'o')
                ->orderBy('o.id', 'DESC')
                ->where('o.id = :id')
                ->setParameter('id', $id)
        ;
        // Bind values from the request
        $filterForm->handleRequest($request);
        // Reset filter
        if ($filterForm->get('reset')->isClicked()) {
            $session->remove('OrdenEntradaControllerFilter');
            $filterForm = $this->createFilterForm($id, null);
        }

        // Filter action
        if ($filterForm->get('filter')->isClicked()) {
            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('OrdenEntradaControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('OrdenEntradaControllerFilter')) {
                $filterData = $session->get('OrdenEntradaControllerFilter');
                $filterForm = $this->createFilterForm($id, $filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }

    /**
     * Create filter form.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createFilterForm($id, $filterData = null)
    {
        $form = $this->createForm(new OrdenEntradaFilterType(), $filterData, array(
            'action' => $this->generateUrl('admin_ordenentrada', array('id' => $id)),
            'method' => 'GET',
        ));

        $form
                ->add('filter', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.index.filter',
                    'attr' => array('class' => 'btn btn-success col-lg-1'),
                ))
                ->add('reset', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.index.reset',
                    'attr' => array('class' => 'btn btn-danger col-lg-1 col-lg-offset-1'),
                ))
        ;

        return $form;
    }

    /**
     * Creates a new OrdenEntrada entity.
     *
     * @Route("/create/{idOrden}", name="admin_ordenentrada_create",  defaults={"idOrden" = null})
     * @Method("POST")
     * @Template("SistemaSTOCKBundle:OrdenEntrada:new.html.twig")
     */
    public function createAction($idOrden, Request $request)
    {
        $this->get('security_role')->controlRolesUser();
        $entity = new OrdenEntrada();
        $form = $this->createCreateForm($idOrden, $entity);
        $form->handleRequest($request);
        $errorCantidad = false;

        if ($form->isValid()) {
            foreach ($entity->getArticulos() as $cantEntrada) {
                if ($cantEntrada->getCantidad() > $cantEntrada->getCantidadMaxima()) {
                    $errorCantidad = true;
                }
            }
            if ($errorCantidad == false) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);

                foreach ($entity->getArticulos() as $art) {

                    if ($art->getOrdenEntrada()->getOrdenSalida()->getOrden()->getReservaMateriales()) {
                        foreach ($art->getOrdenEntrada()->getOrdenSalida()->getOrden()->getArticulos()->getValues() as $cantOrden ) {
                            if ($cantOrden->getArticulo()->getId() == $art->getArticulo()->getId() ) {
                                $cantOrden->setCantidadReservada($cantOrden->getCantidadReservada() + $art->getCantidad());
                                break;
                            }
                        }
                    }

                    $articulo = $em->getRepository('SistemaSTOCKBundle:Articulo')->find($art->getArticulo()->getId());
                    $articulo->setCantidadUtilizada($articulo->getCantidadUtilizada() - $art->getCantidad());

                    if ($art->getOrdenEntrada()->getOrdenSalida()->getOrden()->getReservaMateriales()) {
                        $articulo->setCantidadReservada( $articulo->getCantidadReservada() + $art->getCantidad());
                    }
                    $em->persist($articulo);
                }

                $em->flush();
                $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

                $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl('admin_ordenentrada_new') : $this->generateUrl('admin_ordenentrada_show', array('id' => $entity->getId()));

                return $this->redirect($nextAction);
            }
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
            'idOrden' => $entity->getOrdenSalida()->getOrden()->getId(),
        );
    }

    /**
     * Creates a form to create a OrdenEntrada entity.
     *
     * @param OrdenEntrada $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm($idOrden, OrdenEntrada $entity)
    {
        $form = $this->createForm(new OrdenEntradaType(), $entity, array(
            'action' => $this->generateUrl('admin_ordenentrada_create', array('id' => $idOrden)),
            'method' => 'POST',
        ));

        $form
                ->add(
                        'save', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.save',
                    'attr' => array('class' => 'btn btn-success col-lg-2')
                        )
                )
                ->add(
                        'saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
                        )
                )
        ;

        return $form;
    }

    /**
     * Displays a form to create a new OrdenEntrada entity.
     *
     * @Route("/new/{idOrdenSalida}/{idOrden}", name="admin_ordenentrada_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($idOrdenSalida, $idOrden)
    {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();
        $ordenSalida = $em->getRepository('SistemaSTOCKBundle:OrdenSalida')->findOrdenSalida($idOrdenSalida);

        $entity = new OrdenEntrada();
        $entity->setOrdenSalida($ordenSalida);
        $cantOrden = $ordenSalida->getArticulos();

        foreach ($cantOrden as $co) {
            $CantOrdenEntrada = new CantOrdenEntrada();
            $CantOrdenEntrada->setArticulo($co->getArticulo());
            $CantOrdenEntrada->setCantidadMaxima($co->getCantidad());
            $entity->addArticulo($CantOrdenEntrada);
        }

        $form = $this->createCreateForm($idOrden, $entity);

        return array(
            'entity' => $entity,
            'idOrden' => $idOrden,
            'hideButton' => true,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a OrdenEntrada entity.
     *
     * @Route("/show/{id}", name="admin_ordenentrada_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaSTOCKBundle:OrdenEntrada')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find OrdenEntrada entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'idOrden' => $entity->getOrdenSalida()->getOrden()->getId(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing OrdenEntrada entity.
     *
     * @Route("/{id}/edit", name="admin_ordenentrada_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaSTOCKBundle:OrdenEntrada')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find OrdenEntrada entity.');
        }

        $c = 0;
        foreach ($entity->getArticulos() as $cantEntrada) {
            $cantEntrada->setCantidadMaxima($entity->getOrdenSalida()->getArticulos()->get($c)->getCantidad());
            $c++;
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'idOrden' => $entity->getOrdenSalida()->getOrden()->getId(),
            'hideButton' => true,
        );
    }

    /**
     * Creates a form to edit a OrdenEntrada entity.
     *
     * @param OrdenEntrada $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(OrdenEntrada $entity)
    {
        $form = $this->createForm(new OrdenEntradaType(), $entity, array(
            'action' => $this->generateUrl('admin_ordenentrada_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form
                ->add(
                        'save', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.save',
                    'attr' => array('class' => 'btn btn-success col-lg-2')
                        )
                )
                ->add(
                        'saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
                        )
                )
        ;

        return $form;
    }

    /**
     * Edits an existing OrdenEntrada entity.
     *
     * @Route("/{id}", name="admin_ordenentrada_update")
     * @Method("PUT")
     * @Template("SistemaSTOCKBundle:OrdenEntrada:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaSTOCKBundle:OrdenEntrada')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find OrdenEntrada entity.');
        }

        $oldCantidad[] = array();
        $oldCantidadReservada[] = array();

        foreach ($entity->getArticulos()->getValues() as $key => $cantidad) {
            $oldCantidad[$key] = $cantidad->getCantidad();
        }
        // ladybug_dump_die($oldCantidad);
        // if ($entity->getOrdenSalida()->getOrden()->getReservaMateriales()) {
        //     foreach ($entity->getOrdenSalida()->getOrden()->getArticulos()->getValues() as $key => $cantidadReservada) {
        //         $oldCantidadReservada[$key] = $cantidadReservada->getCantidadReservada();
        //     }
        // }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
        $errorCantidad = false;
        if ($editForm->isValid()) {
            foreach ($entity->getArticulos() as $cantEntrada) {
                if ($cantEntrada->getCantidad() > $cantEntrada->getCantidadMaxima()) {
                    $errorCantidad = true;
                }
            }
            if ($errorCantidad == false) {
                $em->persist($entity);

                foreach ($entity->getArticulos() as $key => $coe) {
                    $articulo = $coe->getArticulo();
                    $articulo->setCantidadUtilizada($articulo->getCantidadUtilizada() + $oldCantidad[$key]);
                    $articulo->setCantidadUtilizada($articulo->getCantidadUtilizada() - $coe->getCantidad());
                    if ($entity->getOrdenSalida()->getOrden()->getReservaMateriales()) {
                        foreach ($entity->getOrdenSalida()->getOrden()->getArticulos()->getValues() as $cantOrden ) {
                            if ($cantOrden->getArticulo()->getId() == $coe->getArticulo()->getId()) {
                                // echo $oldCantidad[$key]."-".$coe->getCantidad()."<br>";
                                // echo $articulo->getCantidadReservada()."<br>";
                                if ($oldCantidad[$key] != $coe->getCantidad()) {
                                    $cantidadReservada = $coe->getCantidad() - $oldCantidad[$key];
                                    $cantOrden->setCantidadReservada($cantOrden->getCantidadReservada() + $cantidadReservada);
                                    $articulo->setCantidadReservada($articulo->getCantidadReservada() + $cantidadReservada);
                                }
                                // echo $articulo->getCantidadReservada()."<br>";
                                // echo $articulo->getCantidadReservada()."-".$oldCantidadReservada[$key]."+".$coe->getCantidad()."<br>";
                                // $cantidadReservada = (int)($articulo->getCantidadReservada() - $oldCantidadReservada[$key]) + $coe->getCantidad();
                                // echo $cantOrden->getCantidadReservada()."-".$oldCantidadReservada[$key]."+".$coe->getCantidad()."<br>";
                                // $cantOrden->setCantidadReservada(($cantOrden->getCantidadReservada() - $oldCantidadReservada[$key]) + $coe->getCantidad());
                                // // $cantOrden->setCantidadReservada($cantOrden->getCantidadReservada() + $coe->getCantidad());
                                // echo $cantOrden->getCantidadReservada()."<br>";
                                // $articulo->setCantidadReservada($cantidadReservada);
                                // echo $articulo->getCantidadReservada();
                                break;
                            }
                        }
                    }
                    // $articulo = $em->getRepository('SistemaSTOCKBundle:Articulo')->find($coe->getArticulo()->getId());
                    // $articulo->setCantidadUtilizada($articulo->getCantidadUtilizada() + $oldCantidad[$key]);
                    // $articulo->setCantidadUtilizada($articulo->getCantidadUtilizada() - $coe->getCantidad());

                    // if ($coe->getOrdenEntrada()->getOrdenSalida()->getOrden()->getReservaMateriales()) {
                    //     $articulo->setCantidadReservada($articulo->getCantidadReservada() - $oldCantidadReservada[$key]);
                    //     $articulo->setCantidadReservada($articulo->getCantidadReservada() + $coe->getCantidad());
                    // }
                    $em->persist($articulo);
                }
                // die;
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

                $nextAction = $editForm->get('saveAndAdd')->isClicked() ? $this->generateUrl('admin_ordenentrada_new') : $this->generateUrl('admin_ordenentrada_show', array('id' => $id));

                return $this->redirect($nextAction);
            }
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'idOrden' => $entity->getOrdenSalida()->getOrden()->getId(),
            'hideButton' => true,
        );
    }

    /**
     * Deletes a OrdenEntrada entity.
     *
     * @Route("/{id}", name="admin_ordenentrada_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $this->get('security_role')->controlRolesUser();
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SistemaSTOCKBundle:OrdenEntrada')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find OrdenEntrada entity.');
            }

            if (false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
                $this->get('session')->getFlashBag()->add(
                        'danger', 'No es posible Eliminar una Devolucion de Articulos, por favor consulte al ADMINISTRADOR para Eliminarla'
                );

                return $this->redirect($this->generateUrl('admin_ordensalida', array('idOrden' => $entity->getOrdenSalida()->getId())));
            }

            foreach ($entity->getArticulos() as $art) {

                if ($art->getOrdenEntrada()->getOrdenSalida()->getOrden()->getReservaMateriales()) {
                    foreach ($art->getOrdenEntrada()->getOrdenSalida()->getOrden()->getArticulos()->getValues() as $cantOrden ) {
                            if ($cantOrden->getArticulo()->getId() == $art->getArticulo()->getId() ) {
                                $cantOrden->setCantidadReservada($cantOrden->getCantidadReservada() - $art->getCantidad());
                                break;
                            }
                    }
                }

                $articulo = $em->getRepository('SistemaSTOCKBundle:Articulo')->find($art->getArticulo()->getId());
                $articulo->setCantidadUtilizada($articulo->getCantidadUtilizada() + $art->getCantidad());

                if ($art->getOrdenEntrada()->getOrdenSalida()->getOrden()->getReservaMateriales()) {
                    $articulo->setCantidadReservada($articulo->getCantidadReservada() - $art->getCantidad());
                }
                $em->persist($articulo);
            }
            $entity->getOrdenSalida()->setOrdenEntrada(null);
            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.delete.success');
        }

        return $this->redirect($this->generateUrl('admin_ordensalida', array('idOrden' => $entity->getOrdenSalida()->getOrden()->getId())));
        //return $this->redirect($this->generateUrl('admin_ordenentrada'));
    }

    /**
     * Creates a form to delete a OrdenEntrada entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        $mensaje = $this->get('translator')->trans('views.recordactions.confirm', array(), 'MWSimpleCrudGeneratorBundle');
        $onclick = 'return confirm("' . $mensaje . '");';

        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('admin_ordenentrada_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array(
                            'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                            'label' => 'views.recordactions.delete',
                            'attr' => array(
                                'class' => 'btn btn-danger col-lg-11',
                                'onclick' => $onclick,
                            )
                        ))
                        ->getForm()
        ;
    }

    /**
     *
     * @Route("/get/entrada", name="get_entrada_update")
     * @Method("GET")
     */
    public function getEntradaAction()
    {
        $id = $this->getRequest()->get('id');
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaSTOCKBundle:OrdenEntrada')->findOneByOrdenSalida($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Entrega entity.');
        }

        $objeto = array(
            'id' => $entity->getId(),
            'updated' => $entity->getUpdated()->format('d/m/Y, H:i'),
            'created' => $entity->getCreated()->format('d/m/Y, H:i'),
        );

        $response = new JsonResponse();
        $response->setData($objeto);

        return $response;
    }

    /**
     * Finds and displays a Orden entity.
     *
     * @param type $id Id de la entidad
     *
     * @Route("/imprimir/{id}", name="admin_ordenentrada_imprimir")
     * @Method("GET")
     * @Template()
     * @return view
     */
    public function imprimirpdfAction($id)
    {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('SistemaSTOCKBundle:OrdenEntrada')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Orden entity.');
        }

        $config = array(
            'pie' => '
                                ________________________
                                    Firma Encargado',
            'titulo' => 'Impresion Devolucion de Materiales',
            'PDF_HEADER_LOGO' => 'logo_neon.jpg',
            'PDF_HEADER_LOGO_WIDTH' => '55',
            'PDF_HEADER_TITLE' => '',
            'PDF_HEADER_STRING_FACTURA' => 'X',
            'PDF_HEADER_STRING_FACTURA_DES' => 'Comprobante Devolucion',
            'PDF_HEADER_STRING' => '
                                                          M.M.O. Raúl Roberto                                                    Fecha: ' . $entity->getCreated()->format('Y-m-d') . '
                                                                             Rojas                                                                   O.T. Nro: ' . $entity->getOrdenSalida()->getOrden()->getNumero() . '
                    Av. San Martín 2386 Resistencia - Chaco                                                         Entrega Mat. Nro: ' . $entity->getOrdenSalida()->getId() . '
Tel: 362-4489609/362-4641854     gerencia@neon3r.com.ar                                              Devolucion Mat. Nro: ' . $entity->getId(),
        );

        $html = $this->renderView('SistemaSTOCKBundle:OrdenEntrada:show.pdf.twig', array('entity' => $entity));

        return $this->get('io_tcpdf_mws')->quick_pdf($html, $config);
    }

}
