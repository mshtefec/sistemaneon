<?php

namespace Sistema\STOCKBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\STOCKBundle\Entity\Rubro;
use Sistema\STOCKBundle\Form\RubroType;
use Sistema\STOCKBundle\Form\RubroFilterType;
use Sistema\STOCKBundle\Entity\ArticuloPrecio;

/**
 * Rubro controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/rubro")
 */
class RubroController extends Controller
{
    /**
     * Lists all Rubro entities.
     *
     * @Route("/", name="admin_rubro")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $this->get('security_role')->controlRolesUser();
        list($filterForm, $queryBuilder) = $this->filter();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder, $this->get('request')->query->get('page', 1),
            (isset($this->container->parameters['knp_paginator.page_range'])) ? $this->container->parameters['knp_paginator.page_range'] : 10
        );
        $articuloPrecio = new ArticuloPrecio();
        $form = $this->crearFormularioModal($articuloPrecio);

        return array(
            'entities' => $pagination,
            'filterForm' => $filterForm->createView(),
            'formModal' => $form->createView(),
        );
    }

    /**
     * Process filter request.
     *
     * @return array
     */
    protected function filter()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        $filterForm = $this->createFilterForm();
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository('SistemaSTOCKBundle:Rubro')
                ->createQueryBuilder('a')
                ->orderBy('a.nombre', 'ASC')
        ;
        // Bind values from the request
        $filterForm->handleRequest($request);
        // Reset filter
        if ($filterForm->get('reset')->isClicked()) {
            $session->remove('RubroControllerFilter');
            $filterForm = $this->createFilterForm();
        }

        // Filter action
        if ($filterForm->get('filter')->isClicked()) {
            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('RubroControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('RubroControllerFilter')) {
                $filterData = $session->get('RubroControllerFilter');
                $filterForm = $this->createFilterForm($filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }

    /**
     * Create filter form.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createFilterForm($filterData = null)
    {
        $form = $this->createForm(new RubroFilterType(), $filterData, array(
            'action' => $this->generateUrl('admin_rubro'),
            'method' => 'GET',
        ));

        $form
                ->add('filter', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.index.filter',
                    'attr' => array('class' => 'btn btn-success col-lg-1'),
                ))
                ->add('reset', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.index.reset',
                    'attr' => array('class' => 'btn btn-danger col-lg-1 col-lg-offset-1'),
                ))
        ;

        return $form;
    }

    /**
     * Creates a new Rubro entity.
     *
     * @Route("/", name="admin_rubro_create")
     * @Method("POST")
     * @Template("SistemaSTOCKBundle:Rubro:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $this->get('security_role')->controlRolesUser();
        $entity = new Rubro();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl('admin_rubro_new') : $this->generateUrl('admin_rubro_show', array('id' => $entity->getId()));

            return $this->redirect($nextAction);
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Rubro entity.
     *
     * @param Rubro $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Rubro $entity)
    {
        $form = $this->createForm(new RubroType(), $entity, array(
            'action' => $this->generateUrl('admin_rubro_create'),
            'method' => 'POST',
        ));

        $form
                ->add(
                        'save', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.save',
                    'attr' => array('class' => 'btn btn-success col-lg-2')
                        )
                )
                ->add(
                        'saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
                        )
                )
        ;

        return $form;
    }

    /**
     * Displays a form to create a new Rubro entity.
     *
     * @Route("/new", name="admin_rubro_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->get('security_role')->controlRolesUser();
        $entity = new Rubro();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Rubro entity.
     *
     * @Route("/{id}", name="admin_rubro_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaSTOCKBundle:Rubro')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Rubro entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Rubro entity.
     *
     * @Route("/{id}/edit", name="admin_rubro_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaSTOCKBundle:Rubro')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Rubro entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Rubro entity.
     *
     * @param Rubro $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Rubro $entity)
    {
        $form = $this->createForm(new RubroType(), $entity, array(
            'action' => $this->generateUrl('admin_rubro_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form
                ->add(
                        'save', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.save',
                    'attr' => array('class' => 'btn btn-success col-lg-2')
                        )
                )
                ->add(
                        'saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
                        )
                )
        ;

        return $form;
    }

    /**
     * Edits an existing Rubro entity.
     *
     * @Route("/{id}", name="admin_rubro_update")
     * @Method("PUT")
     * @Template("SistemaSTOCKBundle:Rubro:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaSTOCKBundle:Rubro')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Rubro entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked() ? $this->generateUrl('admin_rubro_new') : $this->generateUrl('admin_rubro_show', array('id' => $id));

            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Rubro entity.
     *
     * @Route("/{id}", name="admin_rubro_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $this->get('security_role')->controlRolesUser();
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SistemaSTOCKBundle:Rubro')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Rubro entity.');
            }
            try {
                $em->remove($entity);
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', 'flash.delete.success');
            } catch (\Exception $e) {
                $this->get('session')->getFlashBag()->add('danger', 'No se puede eliminar porque esta siendo utilizado');
            }
        }

        return $this->redirect($this->generateUrl('admin_rubro'));
    }

    /**
     * Creates a form to delete a Rubro entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        $mensaje = $this->get('translator')->trans('views.recordactions.confirm', array(), 'MWSimpleCrudGeneratorBundle');
        $onclick = 'return confirm("' . $mensaje . '");';

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_rubro_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label' => 'views.recordactions.delete',
                'attr' => array(
                    'class' => 'btn btn-danger col-lg-11',
                    'onclick' => $onclick,
                )
            ))
            ->getForm()
        ;
    }

    /**
     * @Route("/autocomplete-forms/get-rubros", name="autocomplete_get_rubros")
     */
    public function getRubrosAction(Request $request)
    {
        $term = $request->query->get('q', null);

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SistemaSTOCKBundle:Rubro')->likeNombre($term);

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * Creates a new Rubro entity.
     *
     * @Route("/actualizarPrecio", name="unico_rubro_precio_producto")
     * @Method("POST")
     */
    public function actualizarPrecioProductoPorRubro(Request $request)
    {
        $this->get('security_role')->controlRolesUser();
        $entity = new ArticuloPrecio();
        $form = $this->crearFormularioModal($entity);
        $form->handleRequest($request);
        if ($form->isValid()) {
            //Busco los articulos por rubro
            $repository = $this->getDoctrine()
                    ->getRepository('SistemaSTOCKBundle:Articulo')
            ;

            $query = $repository->createQueryBuilder('a')
                    ->where('a.rubro = :rubro')
                    ->setParameter('rubro', $entity->getIdRubro())
                    ->getQuery()
            ;

            $articulos = $query->getResult();

            if ($articulos) {
                //pregunto por la opcion si aumenta o disminuye el precio
                $em = $this->getDoctrine()->getManager();
                if ($entity->getOpciones() == "Aumentar") {
                    foreach ($articulos as $articulo) {
                        $articulo->setPrecio(($articulo->getPrecio() * ($entity->getPorcentaje() / 100)) + ($articulo->getPrecio()));
                        $em->persist($articulo);
                    }
                } elseif ($entity->getOpciones() == "Disminuir") {
                    foreach ($articulos as $articulo) {
                        $articulo->setPrecio(($articulo->getPrecio() * ($entity->getPorcentaje() / 100)) - ($articulo->getPrecio()));
                        $em->persist($articulo);
                    }
                }
                //actualizo con el nuevo precio
                $em->flush($articulos);
                $this->get('session')->getFlashBag()->add(
                        'success', 'Todos los Articulos del Rubro se han Actualizado Correctamente'
                        );
            }
        } else {
                $this->get('session')->getFlashBag()->add(
                        'danger', 'Los Articulos del Rubro no se han Actualizado'
                        );
        }

        return $this->redirect($this->generateUrl('admin_rubro'));
    }

    public function crearFormularioModal(ArticuloPrecio $articuloPrecio)
    {
        return $this->createFormBuilder($articuloPrecio)
            ->setAction($this->generateUrl('unico_rubro_precio_producto'))
            ->setMethod('POST')
            ->add('idRubro', 'hidden')
            ->add('porcentaje', 'number', array(
                'label_attr' => array(
                    'class' => 'col-lg-3 control-label',
                ),
                'attr' => array('class' => 'col-lg-2')
            ))
            ->add('opciones', 'choice', array(
                'attr' => array('class' => 'col-lg-3'),
                'label_attr' => array(
                    'class' => 'col-lg-3 control-label',
                ),
                'choices' => array(
                    'Aumentar' => 'Aumentar',
                    'Disminuir' => 'Disminuir',
                ),
            ))
            ->add('save', 'submit', array(
                'label' => 'Guardar',
                'attr' => array(
                    'class' => 'btn btn-success'
                )
            ))
            ->getForm()
        ;
    }

}
