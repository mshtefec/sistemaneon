<?php

namespace Sistema\STOCKBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\STOCKBundle\Entity\EntregasHerramientas;
use Sistema\STOCKBundle\Form\EntregasHerramientasType;
use Sistema\STOCKBundle\Form\EntregasHerramientasFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * EntregasHerramientas controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/entregasherramientas")
 */
class EntregasHerramientasController extends Controller
{
    /**
     * Lists all EntregasHerramientas entities.
     *
     * @Route("/", name="admin_entregasherramientas")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->get('security_role')->controlRolesUser();
        list($filterForm, $queryBuilder) = $this->filter();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $queryBuilder, $this->get('request')->query->get('page', 1), (isset($this->container->parameters['knp_paginator.page_range'])) ? $this->container->parameters['knp_paginator.page_range'] : 10
        );

        return array(
            'entities' => $pagination,
            'filterForm' => $filterForm->createView(),
        );
    }

    /**
     * Process filter request.
     *
     * @return array
     */
    protected function filter()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        $filterForm = $this->createFilterForm();
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository('SistemaSTOCKBundle:EntregasHerramientas')
                ->createQueryBuilder('a')
                ->join('a.empleado', 'e')
                ->orderBy('a.id', 'DESC')
        ;
        // Bind values from the request
        $filterForm->handleRequest($request);
        // Reset filter
        if ($filterForm->get('reset')->isClicked()) {
            $session->remove('EntregasHerramientasControllerFilter');
            $filterForm = $this->createFilterForm();
        }

        // Filter action
        if ($filterForm->get('filter')->isClicked()) {
            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('EntregasHerramientasControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('EntregasHerramientasControllerFilter')) {
                $filterData = $session->get('EntregasHerramientasControllerFilter');
                $filterForm = $this->createFilterForm($filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }

    /**
     * Create filter form.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createFilterForm($filterData = null)
    {
        $form = $this->createForm(new EntregasHerramientasFilterType(), $filterData, array(
            'action' => $this->generateUrl('admin_entregasherramientas'),
            'method' => 'GET',
        ));

        $form
                ->add('filter', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.index.filter',
                    'attr' => array('class' => 'btn btn-success col-lg-1'),
                ))
                ->add('reset', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.index.reset',
                    'attr' => array('class' => 'btn btn-danger col-lg-1 col-lg-offset-1'),
                ))
        ;

        return $form;
    }

    /**
     * Creates a new EntregasHerramientas entity.
     *
     * @Route("/", name="admin_entregasherramientas_create")
     * @Method("POST")
     * @Template("SistemaSTOCKBundle:EntregasHerramientas:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $this->get('security_role')->controlRolesUser();
        $entity = new EntregasHerramientas();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        //pregunto si algun articulo del arreglo es mayor la cantidad maxima
        $estado = false;
        foreach ($entity->getHerramientas()->getValues() as $herramienta) {
            if ($herramienta->getHerramienta()->getCantidadReal() < $herramienta->getCantidad()) {
                $estado = true;
                break;
            }
        }
        if ($estado == true) {
            $this->get('session')->getFlashBag()->add(
                    'danger', 'Error: El campo cantidad debe ser menor o igual al campo cantidad maxima'
            );
        } else {

            if ($form->isValid()) {
                $herramientas = array();
                $herramientas_count = array();

                foreach ($entity->getHerramientas()->getValues() as $herramienta) {
                    $herramientas[] = $herramienta->getHerramienta()->getDescripcion();
                }

                $herramientas_count = array_count_values($herramientas);

                foreach ($herramientas_count as $key => $herramienta) {
                    if ($herramienta > 1) {
                        $this->get('session')->getFlashBag()->add(
                                'danger', 'La Herramienta ' . $key . ' esta repetido por favor solo ingrese una herramienta'
                        );

                        return $this->redirect($this->generateUrl('admin_entregasherramientas_new'));
                    }
                }

                foreach ($entity->getHerramientas()->getValues() as $cantHerram) {

                    $cantHerram->getHerramienta()->setCantidadUtilizada($cantHerram->getHerramienta()->getCantidadUtilizada() + $cantHerram->getCantidad());
                }

                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

                $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl('admin_entregasherramientas_new') : $this->generateUrl('admin_entregasherramientas_show', array('id' => $entity->getId()));

                return $this->redirect($nextAction);
            }
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a EntregasHerramientas entity.
     *
     * @param EntregasHerramientas $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(EntregasHerramientas $entity)
    {
        $form = $this->createForm(new EntregasHerramientasType(null), $entity, array(
            'action' => $this->generateUrl('admin_entregasherramientas_create'),
            'method' => 'POST',
        ));

        $form
                ->add(
                        'save', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.save',
                    'attr' => array('class' => 'btn btn-success col-lg-2')
                        )
                )
                ->add(
                        'saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
                        )
                )
        ;

        return $form;
    }

    /**
     * Displays a form to create a new EntregasHerramientas entity.
     *
     * @Route("/new", name="admin_entregasherramientas_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->get('security_role')->controlRolesUser();
        $entity = new EntregasHerramientas();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a EntregasHerramientas entity.
     *
     * @Route("/{id}", name="admin_entregasherramientas_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaSTOCKBundle:EntregasHerramientas')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EntregasHerramientas entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing EntregasHerramientas entity.
     *
     * @Route("/{id}/edit", name="admin_entregasherramientas_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaSTOCKBundle:EntregasHerramientas')->find($id);
        //ladybug_dump_die($entity->getHerramientas()->getHerramienta()->);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EntregasHerramientas entity.');
        }
        $herramientas = array();
        $herramientas_count = array();

        foreach ($entity->getHerramientas() as $herramienta) {
            //ladybug_dump_die($herramienta->getHerramienta()->getCantidadUtilizada());
            $herramienta->getHerramienta()->setCantidad($herramienta->getCantidad() + $herramienta->getHerramienta()->getCantidadReal());
            //$herramienta->setCantidadMaxima($herramienta->getCantidad() + $herramienta->getHerramienta()->getCantidadReal());
            //ladybug_dump_die($herramienta->getCantidadMaxima());
        }
        $opciones['valCantidadMaxima'] = true;
        $editForm = $this->createEditForm($entity, $opciones);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a EntregasHerramientas entity.
     *
     * @param EntregasHerramientas $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(EntregasHerramientas $entity, $opciones = null)
    {
        $form = $this->createForm(new EntregasHerramientasType($opciones), $entity, array(
            'action' => $this->generateUrl('admin_entregasherramientas_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form
                ->add(
                        'save', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.save',
                    'attr' => array('class' => 'btn btn-success col-lg-2')
                        )
                )
                ->add(
                        'saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
                        )
                )
        ;

        return $form;
    }

    /**
     * Edits an existing EntregasHerramientas entity.
     *
     * @Route("/{id}", name="admin_entregasherramientas_update")
     * @Method("PUT")
     * @Template("SistemaSTOCKBundle:EntregasHerramientas:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaSTOCKBundle:EntregasHerramientas')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EntregasHerramientas entity.');
        }
        $herramientasAux = array();
        $herramientasCantUtilizadaAux = array();
        foreach ($entity->getHerramientas()->getValues() as $herramienta) {
            $herramientasAux[$herramienta->getHerramienta()->getId()] = $herramienta->getCantidad();
            $herramientasCantUtilizadaAux[$herramienta->getHerramienta()->getId()] = $herramienta->getHerramienta()->getCantidadUtilizada();
        }

        //ladybug_dump_die($herramientasCantUtilizadaAux);
        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);

        $editForm->handleRequest($request);
        if ($editForm->isValid()) {
            $herramientas = array();
            $herramientas_count = array();

            $herramientas_count = array_count_values($herramientas);

            foreach ($herramientas_count as $key => $herramienta) {
                if ($herramienta > 1) {
                    $this->get('session')->getFlashBag()->add(
                            'danger', 'La Herramienta ' . $key . ' esta repetido por favor solo ingrese una herramienta'
                    );

                    return $this->redirect($this->generateUrl('admin_entregasherramientas_edit', array('id' => $id)));
                }
            }
            foreach ($entity->getHerramientas()->getValues() as $herramienta) {
                $herramientas[] = $herramienta->getHerramienta()->getDescripcion();
            }
            $estado = false;
            foreach ($entity->getHerramientas()->getValues() as $herramienta) {

                if (!empty($herramientasAux[$herramienta->getHerramienta()->getId()])) {
                    $cantidadReal = $herramienta->getHerramienta()->getCantidadReal() + $herramientasAux[$herramienta->getHerramienta()->getId()];

                    if ($cantidadReal < $herramienta->getCantidad()) {
                        $estado = true;
                        break;
                    }
                } else {
                    if ($herramienta->getHerramienta()->getCantidadReal() <= $herramienta->getCantidad()) {
                        $estado = true;
                        break;
                    }
                }
            }
            if ($estado == true) {
                $this->get('session')->getFlashBag()->add(
                        'danger', 'Error: El campo cantidad debe ser menor o igual al campo cantidad maxima'
                );

                return $this->redirect($this->generateUrl('admin_entregasherramientas_edit', array('id' => $id)));
            }

            foreach ($entity->getHerramientas()->getValues() as $cantHerram) {
                if (!empty($herramientasAux[$herramienta->getHerramienta()->getId()])) {
                    // $total = ($cantHerram->getHerramienta()->getCantidadUtilizada() - $herramientasAux[$herramienta->getHerramienta()->getId()]) + $cantHerram->getCantidad();
                    $total = ($herramientasCantUtilizadaAux[$herramienta->getHerramienta()->getId()] - $herramientasAux[$herramienta->getHerramienta()->getId()]) + $cantHerram->getCantidad();

                    $cantHerram->getHerramienta()->setCantidadUtilizada($total);
                } else {
                    $cantHerram->getHerramienta()->setCantidadUtilizada($cantHerram->getHerramienta()->getCantidadUtilizada() + $cantHerram->getCantidad());
                }
            }
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked() ? $this->generateUrl('admin_entregasherramientas_new') : $this->generateUrl('admin_entregasherramientas_show', array('id' => $id));

            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a EntregasHerramientas entity.
     *
     * @Route("/{id}", name="admin_entregasherramientas_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $this->get('security_role')->controlRolesUser();
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SistemaSTOCKBundle:EntregasHerramientas')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find EntregasHerramientas entity.');
            }
            foreach ($entity->getHerramientas()->getValues() as $cantHerram) {
                $cantHerram->getHerramienta()->setCantidadUtilizada($cantHerram->getHerramienta()->getCantidadUtilizada() - $cantHerram->getCantidad());
                $em->persist($cantHerram);
            }
            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.delete.success');
        }

        return $this->redirect($this->generateUrl('admin_entregasherramientas'));
    }

    /**
     * Creates a form to delete a EntregasHerramientas entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        $mensaje = $this->get('translator')->trans('views.recordactions.confirm', array(), 'MWSimpleCrudGeneratorBundle');
        $onclick = 'return confirm("' . $mensaje . '");';

        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('admin_entregasherramientas_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array(
                            'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                            'label' => 'views.recordactions.delete',
                            'attr' => array(
                                'class' => 'btn btn-danger col-lg-11',
                                'onclick' => $onclick,
                            )
                        ))
                        ->getForm()
        ;
    }

    /**
     * @Route("/autocomplete-forms/get-herramientas", name="autocomplete_get_herramientas")
     */
    public function getHerramientasAction(Request $request)
    {
        $term = $request->query->get('q', null);

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SistemaSTOCKBundle:EntregasHerramientas')->likeNombre($term);

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

}
