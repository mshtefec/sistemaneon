<?php

namespace Sistema\STOCKBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\STOCKBundle\Entity\Articulo;
use Sistema\STOCKBundle\Form\ArticuloType;
use Sistema\STOCKBundle\Form\AddArticuloType;
use Sistema\STOCKBundle\Form\AddPrecioArticuloType;
use Sistema\STOCKBundle\Form\ArticuloFilterType;
use Sistema\STOCKBundle\Entity\AddArticulos;
use Sistema\STOCKBundle\Entity\CantAddArticulo;
use Sistema\STOCKBundle\Form\AddArticulosType;
use Exporter\Source\DoctrineORMQuerySourceIterator;
use Exporter\Source\ArraySourceIterator;
use Exporter\Writer\XlsWriter;
use Exporter\Handler;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Articulo controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/articulo")
 */
class ArticuloController extends Controller {

    /**
     * Lists all Cliente entities.
     *
     * @Route("/activos", name="admin_articulo")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->get('security_role')->controlRolesUser();
        list($filterForm, $queryBuilder) = $this->filter("admin_articulo");

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $queryBuilder, $this->get('request')->query->get('page', 1), (isset($this->container->parameters['knp_paginator.page_range'])) ? $this->container->parameters['knp_paginator.page_range'] : 10
        );

        return array(
            'entities' => $pagination,
            'filterForm' => $filterForm->createView(),
            'estado' => "activo",
        );
    }

    /**
     * Lists all Cliente entities.
     *
     * @Route("/inactivos", name="admin_articulo_inactivo")
     * @Method("GET")
     * @Template("SistemaSTOCKBundle:Articulo:index.html.twig")
     */
    public function indexInactivosAction() {
        $this->get('security_role')->controlRolesUser();
        list($filterForm, $queryBuilder) = $this->filter("admin_articulo_inactivo");
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $queryBuilder, $this->get('request')->query->get('page', 1), 10
        );

        return array(
            'entities' => $pagination,
            'filterForm' => $filterForm->createView(),
            'estado' => "inactivo"
        );
    }

    /**
     * Lists all Articulos sin Stock entities.
     *
     * @Route("/articulos-sin-stock", name="admin_articulo_sin_stock")
     * @Method("GET")
     * @Template("SistemaSTOCKBundle:Articulo:articulosSinStock.html.twig")
     */
    public function articulosSinStockAction() {
        $em = $this->getDoctrine()->getManager();
        $articulosSinStock = $em->getRepository('SistemaSTOCKBundle:Articulo')->GetArticuloByStockTipo();

        return $this->render(
                        'SistemaSTOCKBundle:Articulo:articulosSinStock.html.twig', array(
                    'articulosSinStock' => $articulosSinStock,
                        )
        );
    }

    /**
     * Finds and displays a Articulos entity.
     *
     * @param type $id Id de la entidad
     *
     * @Route("/imprimir-articulosSinStock/", name="admin_articulosSinStock_imprimir")
     * @Method("GET")
     * @Template()
     * @return view
     */
    public function imprimirArticulosSinStockpdfAction() {
        set_time_limit(0);
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();
        $articulosSinStock = $em->getRepository('SistemaSTOCKBundle:Articulo')->GetArticuloByStockTipo();

        if (!$articulosSinStock) {
            throw $this->createNotFoundException('Unable to find Articulos sin Stock entity.');
        }

        $config = array(
            'titulo' => 'Reporte O.T. Articulos Sin Stock',
            'PDF_HEADER_LOGO' => 'logo_neon.jpg',
            'PDF_HEADER_LOGO_WIDTH' => '55',
            'PDF_HEADER_TITLE' => '',
            'PDF_HEADER_STRING' => '                                                                        M.M.O. Raúl Roberto Rojas
                                                                        Av. San Martín 2386 Resistencia - Chaco
                                                                        Tel: 362-4489609 / Cel: 362-4641854           
                                                                        Correo: cobranza@neon3r.com.ar',
            'pie' => ''
        );

        $html = $this->renderView('SistemaSTOCKBundle:Articulo:articulosSinStock.pdf.twig', array('entities' => $articulosSinStock));

        return $this->get('io_tcpdf_mws')->quick_pdf($html, $config);
    }

    /**
     * Finds and displays a Articulos entity.
     *
     * @param type $id Id de la entidad
     *
     * @Route("/xls-articulosSinStock/", name="admin_articulosSinStock_xls")
     * @Method("GET")
     * @Template()
     * @return view
     */
    public function imprimirArticulosSinStockxlsAction() {
        set_time_limit(0);
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();
        $articulosSinStock = $em->getRepository('SistemaSTOCKBundle:Articulo')->QueryArticuloByStockTipo();

        if (!$articulosSinStock) {
            throw $this->createNotFoundException('Unable to find Articulos sin Stock entity.');
        }
        $entities = $articulosSinStock
                ->getQuery()
                ->getArrayResult();
        $format = 'xls';
        $filename = 'Articulos_sin_stock.xls';
        $content_type = 'application/vnd.ms-excel';
        $datos = array();

        foreach ($entities as $entity) {
            $datos[] = array(
                'Codigo' => $entity[0]['codigo'],
                'Rubro' => $entity[0]['rubro']['nombre'],
                'Tipo' => $entity[0]['rubro']['nombre'],
                'Descripcion' => $entity[0]['descripcion'],
                'Cantidad' => $entity[0]['cantidad'],
                'Cant. Min. Necesaria' => $entity[1],
                'Precio' => $entity[0]['precio'],
            );
        }

        $writer = new XlsWriter('php://output');
        $contentType = 'application/vnd.ms-excel';
        $source = new \Exporter\Source\ArraySourceIterator($datos);
        $callback = function() use ($source, $writer) {
            $handler = \Exporter\Handler::create($source, $writer);
            $handler->export();
        };

        return new StreamedResponse($callback, 200, array(
            'Content-Type' => $contentType,
            'Content-Disposition' => sprintf('attachment; filename=%s', $filename)
        ));
        //return $this->exportCsvAction('xls', $articulosSinStock, 'Articulos_sin_stock.xls', $datos);
    }

    /**
     * Export Csv.
     */
    public function exportCsvAction($format, $query, $filename, $campos) {
        switch ($format) {
            case 'xls':
                $content_type = 'application/vnd.ms-excel';
                break;
            case 'json':
                $content_type = 'application/json';
                break;
            case 'csv':
                $content_type = 'text/csv';
                break;
            default:
                $content_type = 'text/csv';
                break;
        }
        // Location to Export this to
        $export_to = 'php://output';
        // Data to export
        $exporter_source = new DoctrineORMQuerySourceIterator($query->getQuery(), $campos, "Y-m-d H:i:s");
        // Get an Instance of the Writer
        $exporter_writer = '\Exporter\Writer\\' . ucfirst($format) . 'Writer';
        $exporter_writer = new $exporter_writer($export_to);
        // Generate response
        $response = new Response();
        // Set headers
        $response->headers->set('Cache-Control', 'must-revalidate, post-check=0, pre-check=0');
        $response->headers->set('Content-type', $content_type);
        $response->headers->set('Expires', 0);
        //$response->headers->set('Content-length', filesize($filename));
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Content-disposition', 'filename=' . $filename . ';');
        // Send headers before outputting anything
        $response->sendHeaders();
        // Export to the format
        Handler::create($exporter_source, $exporter_writer)->export();
        return $response;
    }

    /**
     * Process filter request.
     *
     * @return array
     */
    protected function filter($estado) {
        $request = $this->getRequest();
        $session = $request->getSession();
        $filterForm = $this->createFilterForm(null, $estado);
        $em = $this->getDoctrine()->getManager();

        if ($estado == "admin_articulo_inactivo") {
            $queryBuilder = $em->getRepository('SistemaSTOCKBundle:Articulo')
                    ->createQueryBuilder('a')
                    ->join('a.rubro', 'r')
                    ->leftJoin('a.tipo', 't')
                    ->where('a.activo = false')
                    ->orderBy('r.nombre', 'ASC')
                    ->addorderBy('t.nombre', 'ASC')
                    ->addorderBy('a.descripcion', 'ASC')
            ;
        } else {
            $queryBuilder = $em->getRepository('SistemaSTOCKBundle:Articulo')
                    ->createQueryBuilder('a')
                    ->join('a.rubro', 'r')
                    ->leftJoin('a.tipo', 't')
                    ->where('a.activo = true')
                    ->orderBy('r.nombre', 'ASC')
                    ->addorderBy('t.nombre', 'ASC')
                    ->addorderBy('a.descripcion', 'ASC')
            ;
        }
        // Bind values from the request
        $filterForm->handleRequest($request);

        // Reset filter
        if ($filterForm->get('reset')->isClicked()) {
            $session->remove($estado);
            $filterForm = $this->createFilterForm(null, $estado);
        }

        // Filter action
        if ($filterForm->get('filter')->isClicked()) {
            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set($estado, $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has($estado)) {
                $filterData = $session->get($estado);

                if ($filterForm->isSubmitted()) {
                    $filterForm = $this->createFilterForm($filterData, $estado);
                } else {
                    $filterForm = $this->createFilterForm(null, $estado);
                }

                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }

    /**
     * Create filter form.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createFilterForm($filterData = null, $estado = null) {
        $form = $this->createForm(new ArticuloFilterType(), $filterData, array(
            'action' => $this->generateUrl($estado),
            'method' => 'GET',
        ));

        $form
                ->add('filter', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.index.filter',
                    'attr' => array('class' => 'btn btn-success col-lg-1'),
                ))
                ->add('reset', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.index.reset',
                    'attr' => array('class' => 'btn btn-danger col-lg-1 col-lg-offset-1'),
                ))
        ;

        return $form;
    }

    /**
     * Creates a new Articulo entity.
     *
     * @Route("/", name="admin_articulo_create")
     * @Method("POST")
     * @Template("SistemaSTOCKBundle:Articulo:new.html.twig")
     */
    public function createAction(Request $request) {
        $this->get('security_role')->controlRolesUser();
        $entity = new Articulo();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');
            if (is_null($entity->getCodigo())) {
                $entity->setCodigo($entity->getId());
            }
            $em->persist($entity);
            try {
                $em->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {
                $this->get('session')->getFlashBag()->add('info', 'El código ya existe se guardo como nulo.');
            }

            $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl('admin_articulo_new') : $this->generateUrl('admin_articulo_show', array('id' => $entity->getId()));

            return $this->redirect($nextAction);
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Articulo entity.
     *
     * @param Articulo $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Articulo $entity) {
        $form = $this->createForm(new ArticuloType(), $entity, array(
            'action' => $this->generateUrl('admin_articulo_create'),
            'method' => 'POST',
        ));

        $form
                ->add(
                        'save', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.save',
                    'attr' => array('class' => 'btn btn-success col-lg-2')
                        )
                )
                ->add(
                        'saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
                        )
                )
        ;

        return $form;
    }

    /**
     * Displays a form to create a new Articulo entity.
     *
     * @Route("/new", name="admin_articulo_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->get('security_role')->controlRolesUser();
        $entity = new Articulo();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Articulo entity.
     *
     * @Route("/{id}", name="admin_articulo_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaSTOCKBundle:Articulo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Articulo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Articulo entity.
     *
     * @Route("/{id}/edit", name="admin_articulo_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaSTOCKBundle:Articulo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Articulo entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Articulo entity.
     *
     * @param Articulo $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Articulo $entity) {
        $form = $this->createForm(new ArticuloType(), $entity, array(
            'action' => $this->generateUrl('admin_articulo_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form
                ->add(
                        'save', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.save',
                    'attr' => array('class' => 'btn btn-success col-lg-2')
                        )
                )
                ->add(
                        'saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
                        )
                )
        ;

        return $form;
    }

    /**
     * Edits an existing Articulo entity.
     *
     * @Route("/{id}", name="admin_articulo_update")
     * @Method("PUT")
     * @Template("SistemaSTOCKBundle:Articulo:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaSTOCKBundle:Articulo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Articulo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');
            if (is_null($entity->getCodigo())) {
                $entity->setCodigo($entity->getId());
            }
            $em->persist($entity);
            try {
                $em->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {
                $this->get('session')->getFlashBag()->add('info', 'El código ya existe se guardo como nulo.');
            }

            $nextAction = $editForm->get('saveAndAdd')->isClicked() ? $this->generateUrl('admin_articulo_new') : $this->generateUrl('admin_articulo_show', array('id' => $id));

            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Articulo entity.
     *
     * @Route("/{id}", name="admin_articulo_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $this->get('security_role')->controlRolesUser();
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SistemaSTOCKBundle:Articulo')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Articulo entity.');
            }

            //$em->remove($entity);
            $entity->setActivo(false);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'Elemento desactivado satisfactoriamente.');
        }

        return $this->redirect($this->generateUrl('admin_articulo'));
    }

    /**
     * Creates a form to delete a Articulo entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        $mensaje = "Desactivar el elemento?";
        $onclick = 'return confirm("' . $mensaje . '");';

        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('admin_articulo_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array(
                            'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                            'label' => 'Desactivar',
                            'attr' => array(
                                'class' => 'btn btn-danger col-lg-11',
                                'onclick' => $onclick,
                            )
                        ))
                        ->getForm()
        ;
    }

    /**
     * @Route("/autocomplete-forms/get-articulos", name="autocomplete_get_articulos")
     */
    public function getArticulosAction(Request $request) {
        $this->get('security_role')->controlRolesUser();
        $term = $request->query->get('q', null);

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SistemaSTOCKBundle:Articulo')->likeDescripcion($term);

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
                'cantidadMaxima' => $entity->getCantidadReal(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * @Route("/autocomplete-forms/get-articulos-activos", name="autocomplete_get_articulos_activos")
     */
    public function getArticulosActivosAction(Request $request) {
        //$this->get('security_role')->controlRolesUser();
        $term = $request->query->get('q', null);

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SistemaSTOCKBundle:Articulo')->likeDescripcionActivos($term);

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
                'cantidadMaxima' => $entity->getCantidadRealReservada(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * @Route("/autocomplete-forms/get-articulos-activos-precio", name="autocomplete_get_articulos_activos_precio")
     */
    public function getArticulosActivosPrecioAction(Request $request) {
        //$this->get('security_role')->controlRolesUser();
        $term = $request->query->get('q', null);

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SistemaSTOCKBundle:Articulo')->likeDescripcionActivos($term);

        $array = array();
   
        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
                'cantidadMaxima' => $entity->getPrecio(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * Displays a form to edit an existing Articulo entity.
     *
     * @Route("/agregar/cantidad/edit", name="admin_articulo_agregar_cantidad", options={"expose"=true})
     * @Method("POST")
     * @Template()
     */
    public function editAddAction() {
        $this->get('security_role')->controlRolesUser();
        $id = $this->getRequest()->get('id');
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaSTOCKBundle:Articulo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Articulo entity.');
        }

        $entity->setCantidad(0);
        $editForm = $this->createAddcantidadForm($entity);
        $viewForm = $this->renderView('SistemaSTOCKBundle:Articulo:editAdd.html.twig', array(
            'form' => $editForm->createView()
        ));

        $objeto = array(
            'entity' => $entity,
            'form' => $viewForm,
        );

        $response = new JsonResponse();
        $response->setData($objeto);

        return $response;
    }

    /**
     * Creates a form to edit a Articulo entity.
     *
     * @param Articulo $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createAddcantidadForm(Articulo $entity) {
        $form = $this->createForm(new AddArticuloType(), $entity, array(
            'action' => $this->generateUrl('unico_articulo_cantidad_update', array('id' => $entity->getId())),
            'method' => 'POST',
            'attr' => array(
                'id' => 'formCantidad'
            ),
        ));

        $form
                ->add('save', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.save',
                    'attr' => array(
                        'class' => 'btn btn-success'
                    )
                ))
        ;

        return $form;
    }

    /**
     * Edits an existing Articulo entity.
     *
     * @Route("/agregar/cantidad/update/{id}", name="unico_articulo_cantidad_update")
     * @Method("POST")
     */
    public function AgregarCantidadAction(Request $request, $id) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaSTOCKBundle:Articulo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Articulo entity.');
        }

        $form = $request->get('formulario');
        parse_str($form, $datos);
        $cantidad = $datos['sistema_stockbundle_articulo_cantidad']['cantidad'];

        $entity->setCantidad($cantidad + $entity->getCantidad());

        $em->flush();

        $articulosSinStock = $em->getRepository('SistemaSTOCKBundle:Articulo')->GetArticuloByStockRubro();
        $articulosSinStock = $this->renderView(
                'SistemaSTOCKBundle:Articulo:articulosSinStock.html.twig', array(
            'articulosSinStock' => $articulosSinStock
                )
        );

        $objeto = array(
            'id' => $entity->getId(),
            'cantidad' => $entity->getCantidadReal(),
            'articulosSinStock' => $articulosSinStock,
        );

        $response = new JsonResponse();
        $response->setData($objeto);

        return $response;
    }

    // Add Precio Articulo

    /**
     * Displays a form to edit an existing Articulo entity.
     *
     * @Route("/agregar/precio/edit", name="admin_articulo_agregar_precio", options={"expose"=true})
     * @Method("POST")
     * @Template()
     */
    public function editAddPrecioAction() {
        $this->get('security_role')->controlRolesUser();
        $id = $this->getRequest()->get('id');
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaSTOCKBundle:Articulo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Articulo entity.');
        }

        $entity->setPrecio(0);
        $editForm = $this->createAddprecioForm($entity);
        $viewForm = $this->renderView('SistemaSTOCKBundle:Articulo:editAddPrecio.html.twig', array(
            'form' => $editForm->createView()
        ));

        $objeto = array(
            'entity' => $entity,
            'form' => $viewForm,
        );

        $response = new JsonResponse();
        $response->setData($objeto);

        return $response;
    }

    /**
     * Creates a form to edit a Articulo entity.
     *
     * @param Articulo $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createAddprecioForm(Articulo $entity) {
        $form = $this->createForm(new AddPrecioArticuloType(), $entity, array(
            'action' => $this->generateUrl('unico_articulo_precio_update', array('id' => $entity->getId())),
            'method' => 'POST',
            'attr' => array(
                'id' => 'formPrecio'
            ),
        ));

        $form
                ->add('save', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.save',
                    'attr' => array(
                        'class' => 'btn btn-success'
                    )
                ))
        ;

        return $form;
    }

    /**
     * Edits an existing Articulo entity.
     *
     * @Route("/agregar/precio/update/{id}", name="unico_articulo_precio_update")
     * @Method("POST")
     */
    public function AgregarPrecioAction(Request $request, $id) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaSTOCKBundle:Articulo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Articulo entity.');
        }

        $form = $request->get('formulario');
        parse_str($form, $datos);
        $precio = $datos['sistema_stockbundle_articulo_precio']['precio'];

        $entity->setPrecio($precio);

        $em->flush();

        $objeto = array(
            'id' => $entity->getId(),
            'precio' => $entity->getPrecio(),
        );

        $response = new JsonResponse();
        $response->setData($objeto);

        return $response;
    }

    /**
     * Update addArticulos entity.
     *
     * @Route("/agregar/create_articulos", name="admin_create_articulos")
     * @Method("POST")
     * @Template("SistemaSTOCKBundle:Articulo:addArticulos.html.twig")
     */
    public function createArticulosAction(Request $request) {
        //$this->get('security_role')->controlRolesUser();
        $entity = new AddArticulos();
        $form = $this->createAddCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $ordenService = $this->get('orden');
            $ordenService->actualizarArticulos($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            return $this->redirect($this->generateUrl('admin_add_articulos'));
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a new addArticulos entity.
     *
     * @Route("/agregar/add_articulos", name="admin_add_articulos")
     * @Template()
     */
    public function addArticulosAction(Request $request) {
        //$this->get('security_role')->controlRolesUser();
        $entity = new AddArticulos();
        $cantArticulo = new CantAddArticulo();
        $entity->addArticulo($cantArticulo);
        $form = $this->createAddCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a addArticuloa entity.
     *
     * @param AddArticulos $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createAddCreateForm(AddArticulos $entity) {
        $form = $this->createForm(new AddArticulosType(), $entity, array(
            'action' => $this->generateUrl('admin_create_articulos'),
            'method' => 'POST',
        ));

        $form
                ->add('save', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.save',
                    'attr' => array('class' => 'btn btn-success col-lg-2')
                ))
                ->add('saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
                ))
        ;

        return $form;
    }

}
