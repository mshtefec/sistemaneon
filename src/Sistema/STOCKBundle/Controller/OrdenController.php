<?php

namespace Sistema\STOCKBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\STOCKBundle\Entity\Orden;
use Sistema\FACTURACIONBundle\Entity\FacturaVenta;
use Sistema\FACTURACIONBundle\Form\FacturaVentaType;
use Sistema\STOCKBundle\Form\OrdenType;
use Sistema\STOCKBundle\Form\OrdenFilterType;
use Sistema\STOCKBundle\Form\OrdenAvanceFilterType;
// utilizo para crear la orden de salida desde la orden
use Sistema\STOCKBundle\Entity\OrdenSalida;
use Sistema\STOCKBundle\Entity\CantOrdenSalida;
// utilizo para crear el asiento de la orden
use Sistema\FACTURACIONBundle\Entity\Asiento;
use Exporter\Source\DoctrineORMQuerySourceIterator;
use Exporter\Source\ArraySourceIterator;
use Exporter\Writer\XlsWriter;
use Exporter\Handler;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

// fin

/**
 * Orden controller.
 * @author Gonzalo Alonso <gonkpo@gmail.com>
 *
 * @Route("/admin/orden")
 */
class OrdenController extends Controller
{
    /**
     * Lists all Orden entities.
     *
     * @Route("/", name="admin_orden")
     * @Method("GET")
     */
    public function indexAction($query = '', $listado = 'todos', $pagina = "") {
        $this->get('security_role')->controlRolesUser();
        list($filterForm, $queryBuilder) = $this->filter($query);

        if ($pagina == "") {
            $pagina = $this->get('request')->query->get('page', 1);
        }
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder, $pagina, (isset($this->container->parameters['knp_paginator.page_range'])) ? $this->container->parameters['knp_paginator.page_range'] : 10
        );
        $items = $pagination->getItems();
        if (!empty($items)) {
            $facturaVenta = new FacturaVenta();
            $form = $this->crearFormularioModal($facturaVenta);
            $formModal = $form->createView();
        } else {
            $formModal = false;
        }
        
        if ($listado == "todos") {
            $template = "SistemaSTOCKBundle:Orden:index.html.twig";
        } else {
            $template = "SistemaSTOCKBundle:Orden:ordenConSaldo.html.twig";
        }

        return $this->render($template, array(
            'entities'    => $pagination,
            'filterForm'  => $filterForm->createView(),
            'formModal'   => $formModal,
            'tipoListado' => $listado
        ));
    }

    /**
     * Process filter request.
     *
     * @return array
     */
    protected function filter($query = '') {
        $request = $this->getRequest();
        $session = $request->getSession();
        $filterForm = $this->createFilterForm();
        $em = $this->getDoctrine()->getManager();
        if ($query != '') {
            $queryBuilder = $query;
        } else {
            $queryBuilder = $em->getRepository('SistemaSTOCKBundle:Orden')
                ->createQueryBuilder('a')
                ->select('a, cli, cc, dom, d')
                ->join('a.cliente', 'cli')
                ->join('cli.cuentaCorriente', 'cc')
                ->join('cli.domicilios', 'dom')
                ->leftJoin('a.domicilio', 'd')
                ->orderBy('a.id', 'DESC')
            ;
        }

        // Bind values from the request
        $filterForm->handleRequest($request);
        // Reset filter
        if ($filterForm->get('reset')->isClicked()) {
            $session->remove('OrdenControllerFilter');
            $filterForm = $this->createFilterForm();
        }

        // Filter action
        if ($filterForm->get('filter')->isClicked()) {

            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('OrdenControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('OrdenControllerFilter')) {
                $filterData = $session->get('OrdenControllerFilter');
                $filterForm = $this->createFilterForm($filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }

    /**
     * Create filter form.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createFilterForm($filterData = null) {
        $form = $this->createForm(new OrdenFilterType(), $filterData, array(
            'action' => $this->generateUrl('admin_orden'),
            'method' => 'GET',
        ));

        $form
            ->add('filter', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label' => 'views.index.filter',
                'attr' => array('class' => 'btn btn-success col-lg-1'),
            ))
            ->add('reset', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label' => 'views.index.reset',
                'attr' => array('class' => 'btn btn-danger col-lg-1 col-lg-offset-1'),
            ))
        ;

        return $form;
    }

    /**
     * Creates a new Orden entity.
     *
     * @Route("/", name="admin_orden_create")
     * @Method("POST")
     * @Template("SistemaSTOCKBundle:Orden:new.html.twig")
     */
    public function createAction(Request $request) {
        $this->get('security_role')->controlRolesUser();
        $entity = new Orden();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $ordenService = $this->get('orden');
            $arrayResultado = $ordenService->create($entity, $form);
            if ((isset($arrayResultado["session"])) && (!empty($arrayResultado["session"]))) {
                $this->get('session')->getFlashBag()->add($arrayResultado["session"][0]["tipo"], $arrayResultado["session"][0]["contenido"]);
                
                if ($arrayResultado["reedirigir"] === "admin_orden_create") {

                    return array(
                        'entity' => $entity,
                        'form' => $form->createView(),
                    );
                } else {
                    return $this->redirect($this->generateUrl($arrayResultado["reedirigir"]));
                }
            }
            if ($arrayResultado["resul"] == 'saveAndAdd') {
                $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl('admin_orden_new') : $this->generateUrl('admin_orden_show', array('id' => $entity->getId()))
                ;
            } else {
                $nextAction = $this->generateUrl('admin_orden_show', array('id' => $entity->getId()));
            }

            return $this->redirect($nextAction);
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Orden entity.
     *
     * @param Orden $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Orden $entity, array $opciones = null) {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new OrdenType($em, $opciones), $entity, array(
            'action' => $this->generateUrl('admin_orden_create'),
            'method' => 'POST',
        ));

        $form
            ->add('save', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label' => 'views.new.save',
                'attr' => array('class' => 'btn btn-success col-lg-2')
            ))
            ->add('saveAndAdd', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label' => 'views.new.saveAndAdd',
                'attr' => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
            ))
            ->add('saveAndGenerarSalida', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label' => 'Guardar y generar salida',
                'attr' => array(
                    'class' => 'btn btn-primary col-lg-2 col-lg-offset-1',
                    'style' => 'display:none;'
                )
            ))
        ;

        return $form;
    }

    /**
     * Displays a form to create a new Orden entity.
     *
     * @Route("/new", name="admin_orden_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();
        $ultimoNumeroOrden = $em->getRepository('SistemaSTOCKBundle:Orden')->findUltimoNumeroOrden();
        $entity = new Orden();
        if (!empty($ultimoNumeroOrden)) {
            $entity->setNumero($ultimoNumeroOrden[0]['numero'] + 1);
        }
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Orden entity.
     *
     * @Route("/{id}", name="admin_orden_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaSTOCKBundle:Orden')->findOrden($id);
        //ladybug_dump_die($entity->getEmpleados()[0]->getEmpleadoHoras()[0]->getFecha()->format('d/m/Y'));
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Orden entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Orden entity.
     *
     * @Route("/{id}/edit", name="admin_orden_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        ini_set('memory_limit', '-1');
        $this->get('security_role')->controlRolesUser();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaSTOCKBundle:Orden')->findOrden($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Orden entity.');
        }

        if ($entity->getCerrada()) {
            if (false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
                $this->get('session')->getFlashBag()->add(
                        'danger', 'No es posible Editar una Orden CERRADA, por favor consulte al ADMINISTRADOR para Editarla'
                );

                return $this->redirect($this->generateUrl('admin_orden_show', array('id' => $id)));
            }
        }

        $ordenesSalida = $entity->getOrdenSalida();
        $ordenSalida = $ordenesSalida->getValues();
        // si tiene ordenSalida entra y pasa true al formulario para
        // que lo pinte como read_only a los articulos
        if (!empty($ordenSalida)) {
            // calculo la cantidad de los articulos en las orden de salida y entrada
            // este array contiene el id del articulo y va sumando la cantidad de las salidas.
            $articulosCantidad = array();
            //recorro las ordenes de salida de la orden.
            foreach ($ordenesSalida as $key => $ordenSalida) {
                //recorro las cantidades de la orden de salida.
                foreach ($ordenSalida->getArticulos() as $cantOrdenSalida) {
                    if (!isset($articulosCantidad[$cantOrdenSalida->getArticulo()->getId()])) {
                        //aca entra primera vez guardo el valor de cantidad.
                        $articulosCantidad[$cantOrdenSalida->getArticulo()->getId()] = $cantOrdenSalida->getCantidad();
                    } else {
                        //si ya tiene un valor lo sumo con la cantidad.
                        $articulosCantidad[$cantOrdenSalida->getArticulo()->getId()] = $articulosCantidad[$cantOrdenSalida->getArticulo()->getId()] + $cantOrdenSalida->getCantidad();
                    }
                }
                //recorro las cantidades de la orden de entrada de la orden de salida si tiene.
                if ($ordenSalida->getOrdenEntrada()) {
                    foreach ($ordenSalida->getOrdenEntrada()->getArticulos() as $cantOrdenEntrada) {
                        $articulosCantidad[$cantOrdenEntrada->getArticulo()->getId()] = $articulosCantidad[$cantOrdenEntrada->getArticulo()->getId()] - $cantOrdenEntrada->getCantidad();
                    }
                }
            }
            // recorro los articulos de la orden y actualizo la cantidad segun articulosCantidad.
            foreach ($entity->getArticulos() as $key => $cantOrden) {
                if (array_key_exists($cantOrden->getArticulo()->getId(), $articulosCantidad)) {
                    $cantOrden->getArticulo()->setCantidad($cantOrden->getArticulo()->getCantidad() + $articulosCantidad[$cantOrden->getArticulo()->getId()]);
                }
            }
            $opciones = array(
                'ordenContieneOrdenSalida' => true,
                'dateNow' => false,
                'id' => $entity->getId()
            );
            $editForm = $this->createEditForm($entity, $opciones);
        } else {
            $opciones = array(
                'dateNow' => false,
                'id' => $entity->getId()
            );
            $editForm = $this->createEditForm($entity, $opciones);
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Orden entity.
     *
     * @param Orden $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Orden $entity, array $opciones = null) {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new OrdenType($em, $opciones), $entity, array(
            'action' => $this->generateUrl('admin_orden_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form
            ->add('save', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label' => 'views.new.save',
                'attr' => array('class' => 'btn btn-success col-lg-2')
            ))
            ->add('saveAndAdd', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label' => 'views.new.saveAndAdd',
                'attr' => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
            ))
        ;

        return $form;
    }

    /**
     * Edits an existing Orden entity.
     *
     * @Route("/{id}", name="admin_orden_update")
     * @Method("PUT")
     * @Template("SistemaSTOCKBundle:Orden:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        ini_set('memory_limit', '-1');
        $cantidadHoras = 0;
        $totalhoras    = 0;
        $costoHora     = 0;
        $totalArticulo = 0;

        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaSTOCKBundle:Orden')->findOrden($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Orden entity.');
        }
        $deleteForm = $this->createDeleteForm($id);
        $opciones = array(
            'dateNow' => false,
            'id' => $entity->getId()
        );
        //si reserva materiales entra
        if ($entity->getReservaMateriales()) {
            //recorro para cuardar el articulo y la cantidad de la cantidadOrden original
            foreach ($entity->getArticulos()->getValues() as $key => $co) {
                $oldEntity[$co->getArticulo()->getId()] = $co->getCantidad();
            }
        }

        $editForm = $this->createEditForm($entity, $opciones);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $articulos = array();
            $articulos_count = array();

            foreach ($entity->getArticulos()->getValues() as $co) {
                if (!is_null($co->getArticulo())) {
                    $articulos[] = $co->getArticulo()->getRubro() . $co->getArticulo()->getTipo() . $co->getArticulo()->getDescripcion();
                    if (is_null($co->getPrecio())) {
                        $co->setPrecio($co->getArticulo()->getPrecio());
                    }
                } else {
                    $em->remove($co);
                }
            }

            $articulos_count = array_count_values($articulos);

            foreach ($articulos_count as $key => $articulo) {
                if ($articulo > 1) {
                    $this->get('session')->getFlashBag()->add(
                        'danger', 'El articulo ' . $key . ' esta repetido por favor solo ingrese un articulo'
                    );

                    return $this->redirect($this->generateUrl('admin_orden_edit', array('id' => $id)));
                }
            }
            //verifico el tipo de funcion
            $funcion = $entity->getFuncion();
            //INSTANCIO EL SERVICIO
            $ordenService = $this->get('orden');
            //Cambios Entity Montos
            foreach ($entity->getEmpleados() as $empleado) {
                $costoHora = $empleado->getEmpleado()->getCargo()->getCostoHora();
                //si funcion ==  
                //  0 MANTENIMIENTO
                //  1 OBRA
                //  2 URGENCIA
                if ($funcion == 0){
                    $costoHoraCliente = $empleado->getEmpleado()->getCargo()->getCostoHoraMantenimiento();
                } elseif ($funcion == 1) {
                    $costoHoraCliente = $empleado->getEmpleado()->getCargo()->getCostoHoraCliente();
                } else {                
                    $costoHoraCliente = $empleado->getEmpleado()->getCargo()->getCostoHoraUrgencia();
                }
                //setea costoHoraCliente en EmpleadoOrden
                if (is_null($empleado->getCostoHoraCliente())) {
                    $empleado->setCostoHoraCliente($costoHoraCliente);
                }

                //Recorro EmpleadoHora dentro de EmpleadoOrden
                foreach ($empleado->getEmpleadoHoras() as $horas) {
                    //Si es nulo el costoHora entro y seteo
                    if (is_null($horas->getCostoHora())) {
                        $horas->setCostoHora($costoHora);
                    }
                    if (is_null($horas->getFecha()) || is_null($horas->getDesde()) || is_null($horas->getHasta())) {
                        $this->get('session')->getFlashBag()->add('danger', 'Error el rango de horarios en el empleado ' . $empleado->getEmpleado()->getApellido() . ' ' . $empleado->getEmpleado()->getNombre() . ' es incorrecto.');

                        return $this->redirect($this->generateUrl('admin_orden_edit', array('id' => $id)));
                        //Si esta todo bien busco si existe repetida la fecha y hora del empleado.
                    } else {
                        $fechaFechaHoraEmpleado = $ordenService->existeFechaHoraEmpleado($horas);
                        if ($fechaFechaHoraEmpleado) {
                            $this->get('session')->getFlashBag()
                                ->add('danger', 'Error el rango de fecha ' . $fechaFechaHoraEmpleado . ' en el empleado ' . $empleado->getEmpleado()->getApellido() . ' ' . $empleado->getEmpleado()->getNombre() . '.');

                            return $this->redirect($this->generateUrl('admin_orden_edit', array('id' => $id)));
                        }
                    }
                    $timeInterval      = $horas->getDesde()->diff($horas->getHasta());
                    $intervalInSeconds = (new \DateTime())->setTimeStamp(0)->add($timeInterval)->getTimeStamp();
                    $intervalInMinutes = $intervalInSeconds / 60;
                    $totalIntervalo    = $intervalInMinutes / 60;
                    $cantidadHoras    += ((float) $totalIntervalo);
                }
                $totalhoras   += ($empleado->getCostoHoraCliente() * $cantidadHoras);
                $cantidadHoras = 0;
            }

            foreach ($entity->getArticulos() as $co) {
                $totalArticulo = ($co->getCantidad() * $co->getPrecio()) + $totalArticulo;
                //si reserva materiales entra
                if ($entity->getReservaMateriales()) {
                    $articuloId = $co->getArticulo()->getId();
                    $cantidad[$articuloId] = $co->getCantidad();
                    //si existe el articulo en la entity anterior entra y cuardo la cantidad
                    if (array_key_exists($articuloId, $oldEntity)) {
                        $cantidadOld = $oldEntity[$articuloId];
                    } else {
                        $cantidadOld = null;
                    }
                    //si la cantidadOld no es null quiere decir que el articulo esta siendo editado
                    if (!is_null($cantidadOld)) {
                        //si las cantidad nueva es diferente a la cantidad anterior entra sino no hace nada
                        if ($cantidad[$articuloId] != $cantidadOld) {
                            $oldCantidad[$key] = $co->getCantidad();
                            $oldCantidadReservada[$key] = $co->getCantidadReservada();
                            foreach ($entity->getOrdenSalida() as $keyOS => $os) {
                                foreach ($os->getArticulos() as $keyCOS => $cos) {
                                    $articuloIdCOS = $cos->getArticulo()->getId();
                                    if ($articuloId == $articuloIdCOS) {
                                        $cantidad[$articuloId] = $cantidad[$articuloId] - $cos->getCantidad();
                                    }
                                }
                            }
                            //la nueva cantidad reservada es igual a la cantidad del articulo - la cantidad anterior
                            $cantidadReservada = $cantidad[$articuloId] - $cantidadOld;
                            //para no perder las cantidades reservadas hago get y la sumo
                            //cantidad reservada en la orden
                            $co->setCantidadReservada($co->getCantidadReservada() + $cantidadReservada);
                            //cantidad reservada en el articulo
                            $co->getArticulo()->setCantidadReservada($co->getArticulo()->getCantidadReservada() + $cantidadReservada);
                        }
                    } else {//si el articulo es nuevo setea las cantidades reservadas
                        $co->setCantidadReservada($co->getCantidad());
                        $co->getArticulo()->setCantidadReservada($co->getArticulo()->getCantidadReservada() + $co->getCantidad());
                    }
                }
            }

            $entity->setMontoArticulos($totalArticulo);
            $entity->setMontoEmpleados($totalhoras);

            $entity->setMontoOrden($totalhoras + $totalArticulo);

            //obtengo cuenta corriente del cliente
            $cuentaCorriente = $entity->getCliente()->getCuentaCorriente();
            //descuento por separado
            $descuento = ($entity->getMontoOrden() / 100) * $entity->getDescuentoOrden();
            //Actualizo cuenta corriente y creo nuevo asiento si no tiene
            $montoOrden = $entity->getMontoOrden() - ($entity->getMontoOrden() / 100) * $entity->getDescuentoOrden();
            $montoHoras = 0;
            $montoArticulo = 0;
            //Control para que no se repita el descuento
            $descuentoAplicado = false;

            //Control del aplique de descuento
            if ($totalhoras > 0) {
                if ($totalhoras >= $descuento) {
                    $montoHoras = $totalhoras - $descuento;
                    $descuentoAplicado = true;    
                } else {
                    $descuento = $descuento - $totalhoras;
                    $montoHoras = 0;    
                }
            } 
            if ($descuentoAplicado == false) {
                if ($totalArticulo > 0 && $descuento > 0) {
                    $montoArticulo = $totalArticulo - $descuento;
                } else {
                    $montoArticulo = $totalArticulo;
                }
            } else {
                $montoArticulo = $totalArticulo;
            }
            //fin del control descuento

            //cuentaEmpresa
            $cuentaEmpresa = $em->getRepository('SistemaFACTURACIONBundle:CuentaEmpresa')->find(1);
            //haber
            $asiento1 = $entity->getAsientos()[1];
            //copia de asiento haber
            $asiento11 = $entity->getAsientos()[2];
            //debe
            $asiento2 = $entity->getAsientos()[0];
            //PREGUNTO SI TIENE ASIENTOS PARA CALCULAR SINO
            //AHORA NO ESTOY HACIENDO NADA
            //PERO DESPUES SE PODRIA CREAR LA CUENTA
            if (!empty($asiento2)) {
                $asiento2->setHaber($montoOrden);
                
                if ($asiento1) {
                    //en asiento 1 seteo monto articulos
                    $asiento1->setDebe($montoArticulo);
                } else {
                    //obtengo la cuenta orden
                    $valor = current($em->getRepository('SistemaMWSCONFBundle:Setting')->findByClave("CuentaOrden"))->getValor();
                    $cuenta = $em->getRepository('SistemaPlanDeCuentasBundle:Cuenta')->find($valor);
                    //vinculo con cuenta empresa
                    $asiento1 = new Asiento();
                    $asiento1->setCuenta($cuenta);
                    $asiento1->setDebe($montoArticulo);
                    $asiento1->setFecha($entity->getFecha());
                    $entity->addAsiento($asiento1);
                    $cuentaEmpresa->addAsiento($asiento1);
                }
                //seteo asiento11 si no existe lo creo
                if ($asiento11) {
                    //en asiento 11 se setea el monto mano de obra
                    $asiento11->setDebe($montoHoras);
                } else {
                    //obtengo la cuenta orden mano de obra
                    $valorManoDeObra = current($em->getRepository('SistemaMWSCONFBundle:Setting')->findByClave("cuentaManoDeObra"))->getValor();
                    $cuentaManoDeObra = $em->getRepository('SistemaPlanDeCuentasBundle:Cuenta')->find($valorManoDeObra);
                    //vinculo con cuenta empresa
                    $asiento11 = new Asiento();
                    $asiento11->setCuenta($cuentaManoDeObra);
                    $asiento11->setDebe($montoHoras);
                    $asiento11->setFecha($entity->getFecha());
                    $entity->addAsiento($asiento11);
                    $cuentaEmpresa->addAsiento($asiento11);
                }
            }

            if ($entity->getCerrada()) {
                $this->restarReservado($entity, $em);
            }
            //Fin Cambios Entity Montos
            $em->persist($cuentaCorriente);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked() ? $this->generateUrl('admin_orden_new') : $this->generateUrl('admin_orden_show', array('id' => $id));

            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        return $this->redirect($this->generateUrl('admin_orden_edit', array('id' => $id)));
        /*
          return array(
          'entity'      => $entity,
          'edit_form'   => $editForm->createView(),
          'delete_form' => $deleteForm->createView(),
          ); */
    }

    /**
     * Deletes a Orden entity.
     *
     * @Route("/{id}", name="admin_orden_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $this->get('security_role')->controlRolesUser();
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SistemaSTOCKBundle:Orden')->find($id);

            $empty = $entity->getOrdenSalida()->isEmpty();

            if (!$empty) {
                $this->get('session')->getFlashBag()->add(
                        'danger', 'La Orden Posee Entrega de Materiales, Debe Eliminar las Entregas Para poder Eliminar la Orden'
                );

                return $this->redirect($this->generateUrl('admin_orden_show', array('id' => $id)));
            }

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Orden entity.');
            }

            //este array contiene el id del articulo y va sumando la cantidad de las salidas y entradas.
            //al pasarle el id no lo suma.
            $articulosCantidad = $this->calcularCantidadArticulos($entity, $id);

            $cantOrden = $entity->getArticulos();
            //recorro los articulos de la orden que serian las cantOrden y creo las CantOrdenSalida.
            foreach ($cantOrden as $co) {
                if (empty($articulosCantidad[$co->getArticulo()->getId()])) {
                    $cantidadTotal = 0;
                } else {
                    $cantidadTotal = $articulosCantidad[$co->getArticulo()->getId()];
                }
                //cantidad maxima va a ser igual a la cantidad del articulo de la orden - la cantidad de las salidas.
                $cantidadMaxima = $co->getCantidad() - $cantidadTotal;
                $co->setCantidadMaxima($cantidadMaxima);
                if ($entity->getReservaMateriales()) {
                    $co->getArticulo()->setCantidadReservada($co->getArticulo()->getCantidadReservada() - $co->getCantidadMaxima());
                }
            }
            //Actiualizo cuenta corriente del cliente
            $sumaMontoComprobante = 0;
            $cuentaCorriente = $entity->getCliente()->getCuentaCorriente();
            foreach ($entity->getComprobantes() as $com) {
                $sumaMontoComprobante = $sumaMontoComprobante + $com->getMonto();
            }
            //debe
            $asiento1 = $entity->getAsientos()[0];
            $montoCuentaCorriente = ($sumaMontoComprobante + $cuentaCorriente->getMonto()) - $asiento1->getDebe();
            $cuentaCorriente->setMonto($montoCuentaCorriente);

            //Actualizo cuenta empresa
            $cuentaEmpresa = $em->getRepository('SistemaFACTURACIONBundle:CuentaEmpresa')->find(1);
            $cuentaEmpresa->setMonto($cuentaEmpresa->getMonto() - $sumaMontoComprobante);

            $em->persist($cuentaCorriente);
            $em->persist($cuentaEmpresa);
            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.delete.success');
        }

        return $this->redirect($this->generateUrl('admin_orden'));
    }

    /**
     * Creates a form to delete a Orden entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        $mensaje = $this->get('translator')->trans('views.recordactions.confirm', array(), 'MWSimpleCrudGeneratorBundle');
        $onclick = 'return confirm("' . $mensaje . '");';

        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('admin_orden_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array(
                            'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                            'label' => 'views.recordactions.delete',
                            'attr' => array(
                                'class' => 'btn btn-danger col-lg-11',
                                'onclick' => $onclick,
                            )
                        ))
                        ->getForm()
        ;
    }

    /**
     * @Route("/autocomplete-forms/get-orden", name="autocomplete_get_orden")
     */
    public function getOrdenAction(Request $request) {
        $term = $request->query->get('q', null);
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('SistemaSTOCKBundle:Orden')->likeNumero($term);
        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * @Route("/actualizar/estado-orden/", name="admin_orden_estado_update")
     * @Method("POST")
     * @Template()
     */
    public function ActualizarEstadoAction(Request $request) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();
        $id = $this->getRequest()->get('id');
        $entity = $em->getRepository('SistemaSTOCKBundle:Orden')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Articulo entity.');
        }

        $this->restarReservado($entity, $em);
        $entity->setCerrada(true);
        $em->flush();

        $objeto = array(
            'estado' => $entity->getCerrada(),
        );

        $response = new JsonResponse();
        $response->setData($objeto);

        return $response;
    }

    private function restarReservado($entity, $em) {
        $entity->setReservaMateriales(false);
        $em->persist($entity);
        foreach ($entity->getArticulos()->getValues() as $cantOrden) {
            $cantOrden->getArticulo()->setCantidadReservada($cantOrden->getArticulo()->getCantidadReservada() - $cantOrden->getCantidadReservada());
            $cantOrden->setCantidadReservada(0);
            $em->persist($cantOrden);
        }
    }

    /**
     * Finds and displays a Orden entity.
     *
     * @param type $id Id de la entidad
     *
     * @Route("/imprimir/{id}", name="admin_orden_imprimir")
     * @Method("GET")
     * @Template()
     * @return view
     */
    public function imprimirpdfAction($id) {
        set_time_limit(0);
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('SistemaSTOCKBundle:Orden')->findOrden($id);
        $entityArticulo = $em->getRepository('SistemaSTOCKBundle:Orden')->findArticulosCantRealPorOrden($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Orden entity.');
        }
        $cantidadHoras = 0;
        $mostrarCantidadHoras = "";

        foreach ($entity->getEmpleados() as $empleado) {
            foreach ($empleado->getEmpleadoHoras() as $horas) {
                $timeInterval = $horas->getDesde()->diff($horas->getHasta());
                $intervalInSeconds = (new \DateTime())->setTimeStamp(0)->add($timeInterval)->getTimeStamp();
                $intervalInMinutes = $intervalInSeconds / 60;
                $totalIntervalo = $intervalInMinutes / 60;
                $cantidadHoras = ((float) $totalIntervalo) + $cantidadHoras;
            }
        }
        $mostrarCantidadHoras = (string) $cantidadHoras;
        $mostrarCantidadHoras = str_replace('.5', ':30', $mostrarCantidadHoras);

        $config = array(
            'titulo' => 'Impresion de Orden',
            'PDF_HEADER_LOGO' => 'logo_neon.jpg',
            'PDF_HEADER_LOGO_WIDTH' => '55',
            'PDF_HEADER_TITLE' => '',
            'PDF_HEADER_STRING' => '
                                                                        M.M.O. Raúl Roberto Rojas                                                                                                                                            Av. San Martín 2396 - Resistencia/Chaco                O.T. Nro: ' . $entity->getNumero() . '
                                                                        Tel: 362-4489609/362-4641854                           Fecha: ' . $entity->getCreated()->format('d/m/Y') . '
                                                                        Correo: gerencia@neon3r.com.ar',
            'pie' => '
                        ________________________                                                                         ____________________________
                            Firma Tecnico Instalador                                                                                  Firma Conformidad del Cliente',
        );

        $html = $this->renderView('SistemaSTOCKBundle:Orden:show.pdf.twig', array(
            'entity' => $entity,
            'entityArticulo' => $entityArticulo,
            'totaHorasEmpleado' => $cantidadHoras,
            'mostrarCantidadHoras' => $mostrarCantidadHoras,
        ));

        return $this->get('io_tcpdf_mws')->quick_pdf($html, $config);
    }

    /**
     * Finds and displays a Orden entity.
     *
     * @param type $id Id de la entidad
     *
     * @Route("/imprimir-orden/{id}", name="admin_orden_entrada_salida_imprimir")
     * @Method("GET")
     * @Template()
     * @return view
     */
    public function imprimirConOrdenEntradaOrdenSalidapdfAction($id) {
        set_time_limit(0);
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('SistemaSTOCKBundle:Orden')->findOrden($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Orden entity.');
        }

        $config = array(
            'titulo' => 'Reporte O.T. movimientos de stock',
            'PDF_HEADER_LOGO' => 'logo_neon.jpg',
            'PDF_HEADER_LOGO_WIDTH' => '55',
            'PDF_HEADER_TITLE' => '',
            'PDF_HEADER_STRING' => '
                                                                        Reporte O.T. movimientos de stock
                                                                       M.M.O. Raúl Roberto Rojas
                                                                    Av. San Martín 2396 Resistencia - Chaco              O.T. Nro: ' . $entity->getNumero() . '
                            Tel: 362-4489609 / Cel: 362-4641854           Correo: gerencia@neon3r.com.ar                                            Fecha: ' . $entity->getCreated()->format('d/m/Y'),
            'pie' => ''
        );

        $html = $this->renderView('SistemaSTOCKBundle:Orden:show_ordenSalida_OrdenEntrada.pdf.twig', array('entity' => $entity));

        return $this->get('io_tcpdf_mws')->quick_pdf($html, $config);
    }

    public function calcularCantidadArticulos($orden, $idOrdenSalida = null) {
        //este array contiene el id del articulo y va sumando la cantidad de las salidas.
        $articulosCantidad = array();
        //recorro las ordenes de salida de la orden.
        foreach ($orden->getOrdenSalida() as $key => $ordenSalida) {
            if ($ordenSalida->getId() != $idOrdenSalida) {
                //recorro las cantidades de la orden de salida.
                foreach ($ordenSalida->getArticulos() as $cantOrdenSalida) {
                    if (!isset($articulosCantidad[$cantOrdenSalida->getArticulo()->getId()])) {
                        //aca entra primera vez guardo el valor de cantidad.
                        $articulosCantidad[$cantOrdenSalida->getArticulo()->getId()] = $cantOrdenSalida->getCantidad();
                    } else {
                        //si ya tiene un valor lo sumo con la cantidad.
                        $articulosCantidad[$cantOrdenSalida->getArticulo()->getId()] = $articulosCantidad[$cantOrdenSalida->getArticulo()->getId()] + $cantOrdenSalida->getCantidad();
                    }
                }
                //recorro las cantidades de la orden de entrada de la orden de salida si tiene.
                if ($ordenSalida->getOrdenEntrada()) {
                    foreach ($ordenSalida->getOrdenEntrada()->getArticulos() as $cantOrdenEntrada) {
                        $articulosCantidad[$cantOrdenEntrada->getArticulo()->getId()] = $articulosCantidad[$cantOrdenEntrada->getArticulo()->getId()] - $cantOrdenEntrada->getCantidad();
                    }
                }
            }
        }

        return $articulosCantidad;
    }

    /**
     * @Route("/autocomplete-forms/get-orden-desc", name="autocomplete_get_orden_desc")
     */
    public function getOrdenOrderDescAction(Request $request) {
        $term = $request->query->get('q', null);

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SistemaSTOCKBundle:Orden')->findOrderByDesc($term);

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    private function crearFormularioModal(FacturaVenta $facturaVenta) {
        $form = $this->createForm(new FacturaVentaType(), $facturaVenta, array(
            'action' => $this->generateUrl('generar_factura_orden'),
            'method' => 'POST',
        ));

        $form
            ->add('save', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label' => 'views.new.save',
                'attr' => array('class' => 'btn btn-success col-lg-2')
            ))
        ;

        return $form;
    }

    /**
     * Creates a new Rubro entity.
     *
     * @Route("/generar/factura/orden", name="generar_factura_orden")
     * @Method("POST")
     */
    public function generarFacturaOrden(Request $request) {
        $this->get('security_role')->controlRolesUser();
        $entity = new FacturaVenta();
        $form = $this->crearFormularioModal($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_orden'));
    }

    /**
     * @Route("/precio-final/orden/total/", name="admin_orden_total")
     * @Method("POST")
     */
    public function totalOrdenAction() {
        $this->get('security_role')->controlRolesUser();
        $id = $this->getRequest()->get('id');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('SistemaSTOCKBundle:Orden')->findOrdenbyArticulo($id);
        $entityArticulo = $em->getRepository('SistemaSTOCKBundle:Orden')->findArticulosCantRealconPrecioPorOrden($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Orden entity.');
        }

        $totalArticulos = 0;

        $articulos = array();

        foreach ($entity->getArticulos()->getValues() as $cantOrden) {
            $articulos[$cantOrden->getArticulo()->getId()] = $cantOrden;
        }
        
        foreach ($articulos as $cantArticulo) {
            $totalArticulos = $totalArticulos + ($cantArticulo->getPrecio() * (float)$cantArticulo->getCantidad());
        }

        $total = ( $totalArticulos + $entity->getMontoEmpleados() ) - $entity->getDescuentoOrden();

        $objeto = array(
            'total' => $total,
            'totalArticulos' => $totalArticulos,
            'montoEmpleados' => $entity->getMontoEmpleados(),
            'descuentoOrden' => $entity->getDescuentoOrden(),
        );


        $response = new JsonResponse();
        $response->setData($objeto);

        return $response;
    }

    /**
     * @Route("/descontar/entrada/salida/", name="desc_orden_entrada_salida")
     * @Method("POST")
     */
    public function descontarOrdenEntrSalidaAction() {
        $nroOrden = $this->getRequest()->get('nroOrden');
        $idArticulo = $this->getRequest()->get('idArticulo');
        //cantidad es lo que ingreso el usuario
        $cantidad           = $this->getRequest()->get('cantidad');
        $cantidadAdescontar = $this->getRequest()->get('cantidad');
        $estado = "";
        $totalEntregado = 0;
        $totalDevuelto = 0;
        //total es lo que puedo descontar
        $total = 0;

        $em = $this->getDoctrine()->getManager();

        $orden = $em->getRepository('SistemaSTOCKBundle:Orden')->findArticulosOrden($nroOrden, $idArticulo);
        $cantOrden = $em->getRepository('SistemaSTOCKBundle:Orden')->findArticulosCantOrden($nroOrden, $idArticulo);
        $articuloCantidadId = $cantOrden->getArticulo()->getId();

        if (!is_null($orden->getOrdenSalida())) {
            //=====================calculos=cantidad===============
            foreach ($orden->getOrdenSalida() as $os) {
                if (is_null($os->getOrdenEntrada())) {
                    foreach ($os->getArticulos() as $cantOrdenSalida) {
                        if ($cantOrdenSalida->getArticulo()->getId() == $articuloCantidadId) {
                            $totalEntregado = $cantOrdenSalida->getCantidad() + $totalEntregado;
                            break;
                        }
                    }
                } else {
                    foreach ($os->getArticulos() as $cantOrdenSalida) {
                        if ($cantOrdenSalida->getArticulo()->getId() == $articuloCantidadId) {
                            foreach ($os->getOrdenEntrada()->getArticulos() as $cantOrdenEntrada) {
                                if ($cantOrdenEntrada->getArticulo()->getId() == $articuloCantidadId) {
                                    $totalEntregado = $cantOrdenSalida->getCantidad() + $totalEntregado;
                                    $totalDevuelto = $cantOrdenEntrada->getCantidad() + $totalDevuelto;
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }
            }
            //=================fin=calculos=cantidad===============
            $total = $cantOrden->getCantidad() - ($totalEntregado - $totalDevuelto);
            //Si al cantidad para descontar es mayor al total que puede descontar entra
            if ($cantidad > $total) {
                $estado = "La Cantidad Supera el Maximo a descontar, el Maximo es " . $total;
            //Si se puede descontar entra al else
            } else {
                $totalNoUtilizado = $cantOrden->getCantidad() - $totalEntregado;
                //actualizo la cantidad de la orden restando la cantidad ingresada por usuario
                $cantOrden->setCantidad($cantOrden->getCantidad() - $cantidad);
                //actualizo la cantidad del articulo sumando la cantidad ingresada por usuario
                //$cantOrden->getArticulo()->setCantidadUtilizada($cantOrden->getArticulo()->getCantidadUtilizada() - $cantidad);
                //===========================================================================
                //Si la cantidad a descontar es mayor al totalNoUtilizado
                //Es necesario recorrer las ordenes de salida para descontar segun las devoluciones
                if ($cantidad > $totalNoUtilizado) {
                    //=====================calculos=cantidad===============
                    foreach ($orden->getOrdenSalida() as $os) {
                        //recorro las ordenes de salida que tienen entradas
                        if (is_null($os->getOrdenEntrada())) {
                            
                        } else {
                            foreach ($os->getArticulos() as $cantOrdenSalida) {
                                if ($cantidad > 0 AND $cantOrdenSalida->getArticulo()->getId() == $articuloCantidadId) {
                                    foreach ($os->getOrdenEntrada()->getArticulos() as $cantOrdenEntrada) {
                                        if ($cantOrdenEntrada->getArticulo()->getId() == $articuloCantidadId) {
                                            //$totalEntregado = $cantOrdenSalida->getCantidad();
                                            $totalDevuelto = $cantOrdenEntrada->getCantidad();
                                            //modifico las cantidades en salida
                                            if ($cantidad <= $totalDevuelto) {
                                                $cantOrdenSalida->setCantidad($cantOrdenSalida->getCantidad() - $cantidad);
                                                $cantOrdenEntrada->setCantidad($totalDevuelto - $cantidad);
                                                $cantidad = 0;
                                            } else {
                                                $cantOrdenSalida->setCantidad($cantOrdenSalida->getCantidad() - $totalDevuelto);
                                                $cantOrdenEntrada->setCantidad(0);
                                                $cantidad = $cantidad - $totalDevuelto;
                                            }
                                            //fin modifico
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    //=================fin=calculos=cantidad===============
                }
                $em->persist($cantOrden);
                $em->flush();
                $estado = "Se realizo correctamente el descuento de " . $cantidadAdescontar . " materiales.";
            }
        } else {
            if ($cantidad > $cantOrden->getCantidad()) {
                $estado = "La cantidad supera el maximo a descontar. El maximo es " . $cantOrden->getCantidad();
            } else {
                $cantOrden->setCantidad($cantOrden->getCantidad() - $cantidad);
                $em->persist($cantOrden);
                $em->flush();
                $estado = 'Cantidad actualizada. No existen entregas del material. Cierre este aviso para actualizar los datos de la OT';
            }
        }

        $response = new JsonResponse();
        $response->setData($estado);

        return $response;
    }

    /**
     * @Route("/contar/entrada/salida/", name="cont_orden_entrada_salida")
     * @Method("POST")
     */
    public function contarOrdenEntrSalidaAction() {
        $nroOrden = $this->getRequest()->get('nroOrden');
        $idArticulo = $this->getRequest()->get('idArticulo');
        //cantidad es lo que ingreso el usuario
        $cantidad = $this->getRequest()->get('cantidad');
        $estado = "";
        $totalEntregado = 0;
        $totalDevuelto = 0;
        //total es lo que puedo descontar
        $total = 0;

        $em = $this->getDoctrine()->getManager();

        $orden = $em->getRepository('SistemaSTOCKBundle:Orden')->findArticulosOrden($nroOrden, $idArticulo);
        $cantOrden = $em->getRepository('SistemaSTOCKBundle:Orden')->findArticulosCantOrden($nroOrden, $idArticulo);
        $articuloCantidadId = $cantOrden->getArticulo()->getId();

        $total = $cantOrden->getArticulo()->getCantidadRealReservada();

        if ($cantidad > $total) {
            $estado = "La cantidad supera la cantidad maxima de articulo. Solo posee " . $total . " en stock";
            $resultado = false;
        } else {
            $cantOrden->setCantidad($cantOrden->getCantidad() + $cantidad);
            $resultado = true;
            $em->persist($cantOrden);
            $em->flush();
            $estado = 'Cantidad actualizada. Cierre este aviso para actualizar los datos de la OT';
        }
        $array = array(
            "resultado" => $resultado,
            "estado" => $estado
        );
        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     *
     * @Route("/list/orden-orden-con-saldo/", name="admin_orden_con_saldo", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function ordenConSaldoAction() {
        $this->get('security_role')->controlRolesUser();
        
        $request = $this->getRequest();
        $session = $request->getSession();
        $session->remove('OrdenControllerFilter');

        $em = $this->getDoctrine()->getManager();

        $pagina = $this->get('request')->query->get('page', "1");
        $query = $em->getRepository('SistemaSTOCKBundle:Orden')->findOrdenConSaldo();
        //$query = 0;

        $response = $this->forward('SistemaSTOCKBundle:Orden:index', array(
            'listado' => 'listadoConSaldo',
            'query' => $query,
            'pagina' => $pagina,
        ));
        return $response;
    }

    /**
     *
     * @Route("/exportar/", name="admin_orden_exportar", options={"expose"=true})
     * @Method("GET")
     */
    public function ExportAction() {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();
        $tipo = $this->get('request')->query->get('tipo', "pdf");
        $query = $em->getRepository('SistemaSTOCKBundle:Orden')->findOrdenConSaldo();
    
        if ($tipo == "excel") {
            $entities = $query
                    ->getQuery()
                    ->getArrayResult();
            $format = 'xls';
            $datos = array();

            foreach ($entities as $entity) {
                $cerrada = 'Abierta';
                if ($entity[0]['cerrada']) {
                    $cerrada = 'Cerrada';
                }
                $cliente = '';
                if (is_null($entity[0]['cliente']['empresa'])) {
                    $cliente = $entity[0]['cliente']['apellido'] . ' ' . $entity[0]['cliente']['nombre'];
                } elseif (!is_null($entity[0]['cliente']['apellido']) || !is_null($entity[0]['cliente']['nombre'])) {
                    $cliente = $entity[0]['cliente']['apellido'] . ' ' . $entity[0]['cliente']['nombre'] . ' - ' . $entity[0]['cliente']['empresa'];
                } else {
                    $cliente = $entity[0]['cliente']['empresa'];
                }
                if (!is_null($entity[0]['cliente']['dni'])) {
                    $cliente = $cliente . ' | ' . $entity[0]['cliente']['dni'];
                }
                $datos[] = array(
                    'Numero' => $entity[0]['numero'],
                    'Fecha' => $entity[0]['fecha']->format('d/m/Y'),
                    'Cliente' => $cliente,
                    'Domicilio' => $entity[0]['domicilio']['calle'] . ' ' . $entity[0]['domicilio']['numero'] . ' ' . $entity[0]['domicilio']['localidad'],
                    'Saldo' => round($entity['sumaComprobante'], 2),
                    'Estado' => $cerrada,
                );
            }
            
            $filename = 'Orden_con_saldo.xls';
            switch ($format) {
                case 'xls':
                    $content_type = 'application/vnd.ms-excel';
                    break;
                case 'json':
                    $content_type = 'application/json';
                    break;
                case 'csv':
                    $content_type = 'text/csv';
                    break;
                default:
                    $content_type = 'text/csv';
                    break;
            }
            $writer = new XlsWriter('php://output');
            $contentType = 'application/vnd.ms-excel';
            $source = new \Exporter\Source\ArraySourceIterator($datos);
            $callback = function() use ($source, $writer) {
                $handler = \Exporter\Handler::create($source, $writer);
                $handler->export();
            };

            return new StreamedResponse($callback, 200, array(
                'Content-Type' => $contentType,
                'Content-Disposition' => sprintf('attachment; filename=%s', $filename)
            ));
        } else {
            set_time_limit(0);
            $entities = $query
                    ->getQuery()
                    ->getResult();
            $config = array(
                'titulo' => 'Orden con saldo',
                'PDF_HEADER_LOGO' => 'logo_neon.jpg',
                'PDF_HEADER_LOGO_WIDTH' => '55',
                'PDF_HEADER_TITLE' => '',
                'PDF_HEADER_STRING' => '
              M.M.O. Raúl Roberto Rojas
              Av. San Martín 2369 Resistencia - Chaco
              Tel: 362-4489609 / Cel: 362-4641854                                                       Fecha: ' . date('d/m/Y') .
                '                                       Correo: gerencia@neon3r.com.ar',
                'pie' => ''
            );

            $html = $this->renderView('SistemaSTOCKBundle:Orden:show_ordenConSaldo.pdf.twig', array('entities' => $entities));

            return $this->get('io_tcpdf_mws')->quick_pdf($html, $config);
        }
    }

    /**
     * Export Csv.
     */
    public function exportCsvAction($format, $query) {
        $campos = array();
        $campos[] = 'Id';
        switch ($format) {
            case 'xls':
                $content_type = 'application/vnd.ms-excel';
                break;
            case 'json':
                $content_type = 'application/json';
                break;
            case 'csv':
                $content_type = 'text/csv';
                break;
            default:
                $content_type = 'text/csv';
                break;
        }
// Location to Export this to
        $export_to = 'php://output';
// Data to export
        $exporter_source = new DoctrineORMQuerySourceIterator($query, $campos, "Y-m-d H:i:s");
// Get an Instance of the Writer
        $exporter_writer = '\Exporter\Writer\\' . ucfirst($format) . 'Writer';
        $exporter_writer = new $exporter_writer($export_to);
// Generate response
        $response = new Response();
// Set headers
        $response->headers->set('Cache-Control', 'must-revalidate, post-check=0, pre-check=0');
        $response->headers->set('Content-type', $content_type);
        $response->headers->set('Expires', 0);
//$response->headers->set('Content-length', filesize($filename));
        $response->headers->set('Pragma', 'public');
// Send headers before outputting anything
        $response->sendHeaders();
// Export to the format
        Handler::create($exporter_source, $exporter_writer)->export();
        return $response;
    }

    /**
     * Finds and displays a Orden entity.
     *
     * @Route("/avanceot/{id}", name="admin_orden_avance_nro")
     * @Method("GET")
     * @Template()
     */
    public function avanceOtAction($id) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('SistemaSTOCKBundle:Orden')->findOrden($id);
        
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Orden entity.');
        }

        $fechaOrden = $entity->getFecha();

        $request = $this->getRequest();
        
        $filterForm = $this->crearFormFilterAvance($entity);

        $filterForm->handleRequest($request);
        
        $empleadosCargo = array();

        if ($filterForm->get('save')->isClicked()) {
            $fechaFiltro = $filterForm->getData()->getFecha();
            
            $resultados = $this->avanceHacerFiltro($entity, $fechaOrden, $fechaFiltro);

        } else {

            $resultados = $this->avanceHacerFiltro($entity, $fechaOrden);
        }

        return array(
            'entity'          => $entity,
            'filterForm'      => $filterForm->createView(),
            'fechaOrden'      => $fechaOrden,
            'resultados'      => $resultados,
        );
    }

    private function crearFormFilterAvance(Orden $orden) {
        $form = $this->createForm(new OrdenAvanceFilterType(), $orden, array(
            'action' => $this->generateUrl('admin_orden_avance_nro', array('id' => $orden->getId())),
            'method' => 'GET',
        ));

        $form
            ->add('save', 'submit', array(
                'label' => 'Ver Avance',
                'attr' => array('class' => 'btn btn-success btn-sm col-lg-2')
            ))
        ;

        return $form;
    }

    /**
     * Finds and print a Orden entity.
     *
     * @param type $id Id de la entidad
     *
     * @Route("/imprimir/orden-avance/{id}", name="admin_orden_avance_imprimir")
     * @Method("GET")
     * @Template()
     * @return view
     */
    public function imprimirordenavancepdfAction($id) {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('SistemaSTOCKBundle:Orden')->findOrden($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Orden entity.');
        }

        $fechaOrden = $entity->getFecha();
        $resultados = $this->avanceHacerFiltro($entity, $fechaOrden);

        $config = array(
            'titulo' => 'Reporte Avance de Orden',
            'PDF_HEADER_LOGO' => 'logo_neon.jpg',
            'PDF_HEADER_LOGO_WIDTH' => '55',
            'PDF_HEADER_TITLE' => '',
            'PDF_HEADER_STRING' => '
                                                                        Reporte Avance de Orden
                                                                       M.M.O. Raúl Roberto Rojas
                                                                    Av. San Martín 2396 Resistencia - Chaco              O.T. Nro: ' . $entity->getNumero() . '
                            Tel: 362-4489609 / Cel: 362-4641854           Correo: gerencia@neon3r.com.ar                                            Fecha: ' . $entity->getCreated()->format('d/m/Y'),
            'pie' => ''
        );

        $html = $this->renderView('SistemaSTOCKBundle:Orden:imprimirordenavance.pdf.twig', 
            array(
                'entity'        => $entity,
                'fechaOrden'    => $fechaOrden,
                'resultados'    => $resultados,      
            )
        );

        return $this->get('io_tcpdf_mws')->quick_pdf($html, $config);
    }

    public function avanceHacerFiltro($entity, $fechaOrden, $fechaFiltro = null) {
            
        $empleadosCargo = array();
        
        if ($fechaFiltro) {
         
            $totalHorasOrden = 0;
            $empleados = array();

            foreach ($entity->getEmpleados()->getValues() as $EmpleadoOrdenes) {
                $cantidadHoras = 0;
                $cargoHora = array();
                
                foreach ($EmpleadoOrdenes->getEmpleadoHoras() as $horas) {
                    if ($horas->getFecha() <= $fechaFiltro) {
                        $timeInterval = $horas->getDesde()->diff($horas->getHasta());
                        $intervalInSeconds = (new \DateTime())->setTimeStamp(0)->add($timeInterval)->getTimeStamp();
                        $intervalInMinutes = $intervalInSeconds / 60;
                        $totalIntervalo = $intervalInMinutes / 60;
                        $cantidadHoras = ((float) $totalIntervalo) + $cantidadHoras;
                    }
                }
                $totalHorasOrden += $cantidadHoras;
            
                $cargo = $EmpleadoOrdenes->getEmpleado()->getCargo()->getCargo();
                
                $empleadosCargo[$cargo] = $cantidadHoras;
                
                $cargoHora[$cargo] = $cantidadHoras;
                $nombreEmp = $EmpleadoOrdenes->getEmpleado()->getApellido() . ', ' . $EmpleadoOrdenes->getEmpleado()->getNombre();
                $empleados[$nombreEmp] = $cargoHora;
            }

            $totalHoras = 0;

            if ($entity->getHorasPresupuesto() > 0) {
                $totalHoras = $entity->getHorasPresupuesto();
            } else {
                $presupuesto = $entity->getPresupuesto();
                if (!is_null($presupuesto)) {
                    $presu_tareas = $presupuesto->getTareas();
                    if (!is_null($presu_tareas)) {
                        foreach ($presu_tareas->getValues() as $tareas) {
                            $tarea = $tareas->getTarea();
                            if (!is_null($tarea)) {
                                $totalHoras += $tarea->getCantidadReal();
                            }
                        }
                    }
                }
            }

            if ($totalHoras > 0) {
                $avanceOrden = round(($totalHorasOrden * 100) / $totalHoras);
            } else {
                $avanceOrden = 0;
            } 

        } else { 

            $totalHorasOrden = 0;
            $empleados = array();

            foreach ($entity->getEmpleados()->getValues() as $EmpleadoOrdenes) {   
                $cantidadHoras = 0;
                $cargoHora = array();

                foreach ($EmpleadoOrdenes->getEmpleadoHoras() as $horas) {
                    $timeInterval = $horas->getDesde()->diff($horas->getHasta());
                    $intervalInSeconds = (new \DateTime())->setTimeStamp(0)->add($timeInterval)->getTimeStamp();
                    $intervalInMinutes = $intervalInSeconds / 60;
                    $totalIntervalo = $intervalInMinutes / 60;
                    $cantidadHoras = ((float) $totalIntervalo) + $cantidadHoras;
                }
                $totalHorasOrden += $cantidadHoras;

                $cargo = $EmpleadoOrdenes->getEmpleado()->getCargo()->getCargo();
                
                $empleadosCargo[$cargo] = $cantidadHoras;
                
                $cargoHora[$cargo] = $cantidadHoras;
                $nombreEmp = $EmpleadoOrdenes->getEmpleado()->getApellido() . ', ' . $EmpleadoOrdenes->getEmpleado()->getNombre();
                $empleados[$nombreEmp] = $cargoHora;
            }

            $totalHoras = 0;

            if ($entity->getHorasPresupuesto() > 0) {
                $totalHoras = $entity->getHorasPresupuesto();
            } else {
                $presupuesto = $entity->getPresupuesto();
                if (!is_null($presupuesto)) {
                    $presu_tareas = $presupuesto->getTareas();
                    if (!is_null($presu_tareas)) {
                        foreach ($presu_tareas->getValues() as $tareas) {
                            $tarea = $tareas->getTarea();
                            if (!is_null($tarea)) {
                                $totalHoras += $tarea->getCantidadReal();
                            }
                        }
                    }
                }
            }

            if ($totalHoras > 0) {
                $avanceOrden = round(($totalHorasOrden * 100) / $totalHoras);
            } else {
                $avanceOrden = 0;
            }

        }

        return array(
            'totalHoras'      => $totalHoras,
            'totalHorasOrden' => $totalHorasOrden,
            'avanceOrden'     => $avanceOrden,
            'cargosHoras'     => $empleadosCargo,
            'fechaOrden'      => $fechaOrden,
            'empleados'       => $empleados,
        );       

    }
}
