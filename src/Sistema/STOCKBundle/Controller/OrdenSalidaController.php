<?php

namespace Sistema\STOCKBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\STOCKBundle\Entity\OrdenSalida;
use Sistema\STOCKBundle\Entity\Articulo;
use Sistema\STOCKBundle\Entity\CantOrdenSalida;
use Sistema\STOCKBundle\Form\OrdenSalidaType;
use Sistema\STOCKBundle\Form\OrdenSalidaFilterType;

/**
 * OrdenSalida controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/ordensalida")
 */
class OrdenSalidaController extends Controller
{
    /**
     * Lists all OrdenSalida entities.
     *
     * @Route("/{idOrden}", name="admin_ordensalida")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($idOrden)
    {
        $this->get('security_role')->controlRolesUser();
        list($filterForm, $queryBuilder, $orden) = $this->filter($idOrden);
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder,
            $this->get('request')->query->get('page', 1),
            (isset($this->container->parameters['knp_paginator.page_range'])) ? $this->container->parameters['knp_paginator.page_range'] : 10
        );

        return array(
            'entities'   => $pagination,
            'idOrden'    => $idOrden,
            'nroOrden'   => $orden->getNumero(),
            'filterForm' => $filterForm->createView(),
        );
    }

    /**
    * Process filter request.
    *
    * @return array
    */
    protected function filter($idOrden)
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        $filterForm = $this->createFilterForm($idOrden, null);
        $em = $this->getDoctrine()->getManager();
        $orden = $em->getRepository('SistemaSTOCKBundle:Orden')->find($idOrden);
        $queryBuilder = $em->getRepository('SistemaSTOCKBundle:OrdenSalida')
            ->createQueryBuilder('a')
            ->select('a, o')
            ->join('a.orden', 'o' )
            ->orderBy('o.id', 'DESC')
            ->where('o.id = :idOrden')
            ->setParameter('idOrden', $idOrden)

        ;
        // Bind values from the request
        $filterForm->handleRequest($request);
        // Reset filter
        if ($filterForm->get('reset')->isClicked()) {
            $session->remove('OrdenSalidaControllerFilter');
            $filterForm = $this->createFilterForm($idOrden, null);
        }

        // Filter action
        if ($filterForm->get('filter')->isClicked()) {
            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('OrdenSalidaControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('OrdenSalidaControllerFilter')) {
                $filterData = $session->get('OrdenSalidaControllerFilter');
                $filterForm = $this->createFilterForm($idOrden,$filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder, $orden);
    }
    /**
    * Create filter form.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createFilterForm($idOrden, $filterData = null)
    {
        $form = $this->createForm(new OrdenSalidaFilterType(), $filterData, array(
            'action' => $this->generateUrl('admin_ordensalida', array('idOrden' => $idOrden)),
            'method' => 'GET',
        ));

        $form
            ->add('filter', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label'              => 'views.index.filter',
                'attr'               => array('class' => 'btn btn-success col-lg-1'),
            ))
            ->add('reset', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label'              => 'views.index.reset',
                'attr'               => array('class' => 'btn btn-danger col-lg-1 col-lg-offset-1'),
            ))
        ;

        return $form;
    }

    /**
     * Creates a new OrdenSalida entity.
     *
     * @Route("/create/{idOrden}", name="admin_ordensalida_create", defaults={"idOrden" = null})
     * @Method("POST")
     * @Template("SistemaSTOCKBundle:OrdenSalida:new.html.twig")
     */
    public function createAction($idOrden, Request $request)
    {
        $this->get('security_role')->controlRolesUser();
        $entity = new OrdenSalida();
        $form = $this->createCreateForm($idOrden, $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            /* Art es CantOrdenSalida */
            /* Articulo es el Articulo */
            foreach ($entity->getArticulos() as $art) {
                $articulo = $art->getArticulo();
                $articulo->setCantidadUtilizada($art->getCantidad() + $articulo->getCantidadUtilizada());

                if ($art->getOrdenSalida()->getOrden()->getReservaMateriales()) {
                    foreach ($art->getOrdenSalida()->getOrden()->getArticulos()->getValues() as $cantOrden ) {
                        if ($cantOrden->getArticulo()->getId() == $articulo->getId() ) {
                            $cantOrden->setCantidadReservada($cantOrden->getCantidadReservada() - $art->getCantidad());
                            break;
                        }
                    }

                    $articulo->setCantidadReservada( $articulo->getCantidadReservada() - $art->getCantidad());
                }
                $em->persist($articulo);
            }

            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked()
                    ? $this->generateUrl('admin_ordensalida_new', array('idOrden' => $idOrden))
                    : $this->generateUrl('admin_ordensalida_show', array('id' => $entity->getId(), 'idOrden' => $idOrden));

            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        return array(
            'entity'  => $entity,
            'idOrden' => $idOrden,
            'form'    => $form->createView(),
        );
    }

    /**
    * Creates a form to create a OrdenSalida entity.
    *
    * @param OrdenSalida $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm($idOrden, OrdenSalida $entity, $opciones = null)
    {
        $form = $this->createForm(new OrdenSalidaType($opciones), $entity, array(
            'action' => $this->generateUrl('admin_ordensalida_create', array('idOrden' => $idOrden)),
            'method' => 'POST',
        ));

        $form
            ->add(
                'save', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label'              => 'views.new.save',
                'attr'               => array('class' => 'btn btn-success col-lg-2')
                )
            )
            ->add(
                'saveAndAdd', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label'              => 'views.new.saveAndAdd',
                'attr'               => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
                )
            )
        ;

        return $form;
    }

    /**
     * Displays a form to create a new OrdenSalida entity.
     *
     * @Route("/new/{idOrden}", name="admin_ordensalida_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($idOrden)
    {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();
        $orden = $em->getRepository('SistemaSTOCKBundle:Orden')->findOrden($idOrden);

        $entity = new OrdenSalida();
        $articulos = $orden->getArticulos();
        if (empty($articulos)) {
            $this->get('session')->getFlashBag()->add(
                'danger', 'La Orden no posee articulos, no se puede realizar una Salida'
            );

            return $this->redirect($this->generateUrl('admin_ordensalida', array('idOrden' => $idOrden)));
        }

        $entity->setOrden($orden);

        //este array contiene el id del articulo y va sumando la cantidad de las salidas y entradas.
        $articulosCantidad = $this->calcularCantidadArticulos($orden);

        $cantOrden = $orden->getArticulos();
        //recorro los articulos de la orden que serian las cantOrden y creo las CantOrdenSalida.
        foreach ($cantOrden as $co) {
            if (empty($articulosCantidad[$co->getArticulo()->getId()])) {
                $cantidadTotal = 0;
            } else {
                $cantidadTotal = $articulosCantidad[$co->getArticulo()->getId()];
            }
            //cantidad maxima va a ser igual a la cantidad del articulo de la orden - la cantidad de las salidas.
            $cantidadMaxima = $co->getCantidad() - $cantidadTotal;
            //si cantidad real es >= a la cantidad maxima entra sino utiliza la cantidad real.
            if ($co->getArticulo()->getCantidadReal() >= $cantidadMaxima) {
                $co->getArticulo()->setCantidad($cantidadMaxima);
                $co->getArticulo()->setCantidadUtilizada(0);
            }
            $cantOrdenSalida = new CantOrdenSalida();
            $cantOrdenSalida->setCantidad(0);
            $cantOrdenSalida->setArticulo($co->getArticulo());
            $cantOrdenSalida->setCantidadMaxima($cantidadMaxima);
            $entity->addArticulo($cantOrdenSalida);
        }
        
        $form   = $this->createCreateForm($idOrden, $entity);

        return array(
            'entity'     => $entity,
            'idOrden'    => $idOrden,
            'hideButton' => true,
            'form'       => $form->createView(),
        );
    }

    /**
     * Finds and displays a OrdenSalida entity.
     *
     * @Route("/show/{id}/{idOrden}", name="admin_ordensalida_show",defaults={"idOrden" = null})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id, $idOrden)
    {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaSTOCKBundle:OrdenSalida')->findOrdenSalida($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find OrdenSalida entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'idOrden'     => $idOrden,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing OrdenSalida entity.
     *
     * @Route("/{id}/{idOrden}/edit", name="admin_ordensalida_edit", defaults={"idOrden" = null})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id, $idOrden)
    {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaSTOCKBundle:OrdenSalida')->findOrdenSalida($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find OrdenSalida entity.');
        }

        $orden = $entity->getOrden();
        $articulos = $orden->getArticulos()->getSnapshot();
        if (empty($articulos)) {
            $this->get('session')->getFlashBag()->add(
                'danger', 'La Orden no posee articulos, no se puede realizar una Salida'
            );

            return $this->redirect($this->generateUrl('admin_ordensalida', array('idOrden' => $idOrden)));
        }

        //este array contiene el id del articulo y va sumando la cantidad de las salidas y entradas.
        //al pasarle el id no lo suma.
        $articulosCantidad = $this->calcularCantidadArticulos($orden, $id);
        //seteamos la cantidad maxima en cantOrden
        $this->calculaCantidadMaxima($orden, $articulosCantidad, 'edit');

        $opciones['valCantidadMaxima'] = true;
        $editForm = $this->createEditForm($entity, $opciones);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'idOrden'     => $idOrden,
            'hideButton'  => true,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a OrdenSalida entity.
    *
    * @param OrdenSalida $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(OrdenSalida $entity, $opciones = null)
    {
        $form = $this->createForm(new OrdenSalidaType($opciones), $entity, array(
            'action' => $this->generateUrl('admin_ordensalida_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form
            ->add(
                'save', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label'              => 'views.new.save',
                'attr'               => array('class' => 'btn btn-success col-lg-2')
                )
            )
            ->add(
                'saveAndAdd', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label'              => 'views.new.saveAndAdd',
                'attr'               => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
                )
            )
        ;

        return $form;
    }
    /**
     * Edits an existing OrdenSalida entity.
     *
     * @Route("/{id}", name="admin_ordensalida_update")
     * @Method("PUT")
     * @Template("SistemaSTOCKBundle:OrdenSalida:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaSTOCKBundle:OrdenSalida')->findOrdenSalida($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find OrdenSalida entity.');
        }
        //obtengo orden
        $orden = $entity->getOrden();
        //obtengo id orden
        $idOrden = $orden->getId();
        //controlo si tiene ordenes de entrada
        $tieneOrdenEntrada = $this->poseeEntregas($entity, $idOrden);
        if ($tieneOrdenEntrada) {
            return $this->redirect($tieneOrdenEntrada);
        }
        //obtengo cantidades anteriores
        $oldCantidad = $this->oldCantidad($entity);

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            //recorro cantidad orden salida
            foreach ($entity->getArticulos() as $key => $cos) {
                $articulo = $cos->getArticulo();
                $articulo->setCantidadUtilizada(($articulo->getCantidadUtilizada() - $oldCantidad[$key]) + $cos->getCantidad());
                //si la orden reserva los materiales entra
                if ($entity->getOrden()->getReservaMateriales()) {
                    foreach ($orden->getArticulos()->getValues() as $cantOrden) {
                        $entro = $this->reservaMateriales($em, $orden, $articulo, $cos, $cantOrden, $oldCantidad[$key]);
                        if ($entro) {
                            break;
                        }
                    }
                }
                $em->persist($articulo);
            }

            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked()
                ? $this->generateUrl('admin_ordensalida_new', array('idOrden' => $idOrden))
                : $this->generateUrl('admin_ordensalida_show', array('id' => $id, 'idOrden' => $entity->getOrden()->getId()));

            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'idOrden'     => $idOrden,
        );
    }

    private function calculaCantidadMaxima($orden, $articulosCantidad, $from)
    {
        $cantOrden = $orden->getArticulos();
        //recorro los articulos de la orden que serian las cantOrden y creo las CantOrdenSalida.
        foreach ($cantOrden as $co) {
            if (empty($articulosCantidad[$co->getArticulo()->getId()])) {
                $cantidadTotal = 0;
            } else {
                $cantidadTotal = $articulosCantidad[$co->getArticulo()->getId()];
            }
            //cantidad maxima va a ser igual a la cantidad del articulo de la orden - la cantidad de las salidas.
            $cantidadMaxima = $co->getCantidad() - $cantidadTotal;
            if ($from == 'edit') {
                $co->getArticulo()->setCantidad($cantidadMaxima);
            }
            $co->setCantidadMaxima($cantidadMaxima);
        }
    }

    private function oldCantidad($entity)
    {
        $oldCantidad = array();
        foreach ($entity->getArticulos()->getValues() as $key => $cos) {
            $oldCantidad[$key] = $cos->getCantidad();
        }

        return $oldCantidad;
    }

    private function poseeEntregas($entity, $idOrden)
    {
        if (!is_null($entity->getOrdenEntrada())) {
            $this->get('session')->getFlashBag()->add(
                'danger', 'La Orden Posee una Entrega de Articulos, por lo cual no es posible eliminarla'
            );

            return $this->generateUrl('admin_ordensalida', array('idOrden' => $idOrden));
        }

        return null;
    }

    private function reservaMateriales($em, $orden, $articulo, $cos, $cantOrden, $oldCantidad)
    {
        if ($cantOrden->getArticulo()->getId() == $articulo->getId()) {
            if (is_null($cos->getCantidadMaxima())) {//es delete $cos->getCantidadMaxima() es null
                $cantidadReservadaOrden = (int) $cantOrden->getCantidadReservada() + $cos->getCantidad();
                $cantidadReservada = (int) ($articulo->getCantidadReservada() + $cos->getCantidad());
            } else {//es edit $cantOrden->getCantidadMaxima() es null
                $cantidadReservadaOrden = (int) $cos->getCantidadMaxima() - $cos->getCantidad();
                $cantidadReservada = (int) ($articulo->getCantidadReservada() + $oldCantidad) - $cos->getCantidad();
            }
            $cantOrden->setCantidadReservada($cantidadReservadaOrden);
            $articulo->setCantidadReservada($cantidadReservada);
            $em->persist($cantOrden);

            return true;//entro al if
        }

        return false;//no entro al if
    }

    /**
     * Deletes a OrdenSalida entity.
     *
     * @Route("/{id}", name="admin_ordensalida_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $this->get('security_role')->controlRolesUser();
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $entity = $em->getRepository('SistemaSTOCKBundle:OrdenSalida')->findOrdenSalida($id);
            if (!$entity) {
                throw $this->createNotFoundException('Unable to find OrdenSalida entity.');
            }
            //obtengo orden
            $orden = $entity->getOrden();
            //obtengo id orden
            $idOrden = $orden->getId();
            //controlo si tiene ordenes de entrada
            $tieneOrdenEntrada = $this->poseeEntregas($entity, $idOrden);
            if ($tieneOrdenEntrada) {
                return $this->redirect($tieneOrdenEntrada);
            }
            //obtengo cantidades anteriores
            $oldCantidad = $this->oldCantidad($entity);
            //este array contiene el id del articulo y va sumando la cantidad de las salidas y entradas.
            //al pasarle el id no lo suma.
            $articulosCantidad = $this->calcularCantidadArticulos($orden, $id);
            //seteamos la cantidad maxima en cantOrden
            $this->calculaCantidadMaxima($orden, $articulosCantidad, 'delete');
            //recorro cantidad orden salida
            foreach ($entity->getArticulos() as $key => $cos) {
                $articulo = $cos->getArticulo();
                $articulo->setCantidadUtilizada($articulo->getCantidadUtilizada() - $cos->getCantidad());
                //si la orden reserva los materiales entra
                if ($entity->getOrden()->getReservaMateriales()) {
                    foreach ($orden->getArticulos()->getValues() as $cantOrden) {
                        $entro = $this->reservaMateriales($em, $orden, $articulo, $cos, $cantOrden, $oldCantidad[$key]);
                        if ($entro) {
                            break;
                        }
                    }
                }
                $em->persist($articulo);
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.delete.success');
        }

        return $this->redirect($this->generateUrl('admin_ordensalida', array('idOrden' => $idOrden) ));
    }

    /**
     * Creates a form to delete a OrdenSalida entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        $mensaje = $this->get('translator')->trans('views.recordactions.confirm', array(), 'MWSimpleCrudGeneratorBundle');
        $onclick = 'return confirm("'.$mensaje.'");';

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_ordensalida_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label'              => 'views.recordactions.delete',
                'attr'               => array(
                    'class'   => 'btn btn-danger col-lg-11',
                    'onclick' => $onclick,
                )
            ))
            ->getForm()
        ;
    }

    public function calcularCantidadArticulos($orden, $idOrdenSalida = null)
    {
        //este array contiene el id del articulo y va sumando la cantidad de las salidas.
        $articulosCantidad = array();
        //recorro las ordenes de salida de la orden.
        foreach ($orden->getOrdenSalida() as $key => $ordenSalida) {
            if ($ordenSalida->getId() != $idOrdenSalida) {
                //recorro las cantidades de la orden de salida.
                foreach ($ordenSalida->getArticulos() as $cantOrdenSalida) {
                    if (!isset($articulosCantidad[$cantOrdenSalida->getArticulo()->getId()])) {
                        //aca entra primera vez guardo el valor de cantidad.
                        $articulosCantidad[$cantOrdenSalida->getArticulo()->getId()] = $cantOrdenSalida->getCantidad();
                    } else {
                        //si ya tiene un valor lo sumo con la cantidad.
                        $articulosCantidad[$cantOrdenSalida->getArticulo()->getId()] = $articulosCantidad[$cantOrdenSalida->getArticulo()->getId()] + $cantOrdenSalida->getCantidad();
                    }
                }
                //recorro las cantidades de la orden de entrada de la orden de salida si tiene.
                if ($ordenSalida->getOrdenEntrada()) {
                    foreach ($ordenSalida->getOrdenEntrada()->getArticulos() as $cantOrdenEntrada) {
                        $articulosCantidad[$cantOrdenEntrada->getArticulo()->getId()] = $articulosCantidad[$cantOrdenEntrada->getArticulo()->getId()] - $cantOrdenEntrada->getCantidad();
                    }
                }
            }
        }

        return $articulosCantidad;
    }

    /**
     * @Route("/autocomplete-forms/get-orden-salida", name="autocomplete_get_ordensalida")
     */
    public function getOrdenSalidaAction(Request $request)
    {
        $term = $request->query->get('q', null);

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SistemaSTOCKBundle:OrdenSalida')->likeNumero($term);

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * Finds and displays a Orden entity.
     *
     * @param type $id Id de la entidad
     *
     * @Route("/imprimir/{id}", name="admin_ordensalida_imprimir")
     * @Method("GET")
     * @Template()
     * @return view
     */
    public function imprimirpdfAction($id)
    {
            $this->get('security_role')->controlRolesUser();
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SistemaSTOCKBundle:OrdenSalida')->findOrdenSalida($id);

            if (!$entity) {
            throw $this->createNotFoundException('Unable to find Orden entity.');
            }

            $config = array(
                    'pie' => '
                                ________________________
                                    Firma de '. $entity->getEmpleado().'',
                    'titulo' => 'Impresion Entrega de Materiales',
                    'PDF_HEADER_LOGO' => 'logo_neon.jpg',
                    'PDF_HEADER_LOGO_WIDTH' => '55',
                    'PDF_HEADER_TITLE'=> '',
                    'PDF_HEADER_STRING_FACTURA' => 'X',
                    'PDF_HEADER_STRING_FACTURA_DES' => 'Comprobante Entrega',
                    'PDF_HEADER_STRING' => '
                            M.M.O. Raúl Roberto Rojas
                            Av. San Martín 2386 Resistencia - Chaco                                                         O.T. Nro: ' . $entity->getOrden()->getNumero() .'
                            Tel: 362-4489609 / Cel: 362-4641854                                                Entrega Mat. Nro: ' . $entity->getId(). '
                            mail:gerencia@neon3r.com.ar                                                                Fecha: ' . $entity->getCreated()->format('Y-m-d') . '',
                    );

                $html = $this->renderView('SistemaSTOCKBundle:OrdenSalida:show.pdf.twig', array('entity' => $entity));

                return $this->get('io_tcpdf_mws')->quick_pdf($html, $config);

    }
}
