$("#limpiar").click(function () {
    $("#sistema_stockbundle_articulofiltertype_reset").trigger("click");
});
// metodo para ejecutar el modal con el form para modificar cantidad
function modalCantidad(id) {
    $("#mostrarinfor").empty();
    $.ajax({
        type: "POST",
        url: Routing.generate('admin_articulo_agregar_cantidad'),
        data: {id: id},
        success: function (objeto) {
            $('#mostrarinfor').append(objeto['form']);
            $('#myModalCantidad').modal('show')
        },
        error: function (data) {
            $('#mostrarinfor').innerHTML = "<div class='alert alert-danger'>Error al Crear El Formulario</div>";
            setTimeout(function () {
                $('#mostrarinfor').innerHTML = ""
            }, 5000);
        }
    });
}

function modalPrecio(id) {
    $("#mostrarinfor").empty();
    $.ajax({
        type: "POST",
        url: Routing.generate('admin_articulo_agregar_precio'),
        data: {id: id},
        success: function (objeto) {
            $('#mostrarinfor').append(objeto['form']);
            $('#myModalPrecio').modal('show')
        },
        error: function (data) {
            $('#mostrarinfor').innerHTML = "<div class='alert alert-danger'>Error al Crear El Formulario</div>";
            setTimeout(function () {
                $('#mostrarinfor').innerHTML = ""
            }, 5000);
        }
    });
}

function toogle() {
    if ($('#BajoStock').hasClass('in')) {
        $("#btn-BajoStock").html("Mostrar <span class='caret'></span>");
    } else {
        $("#btn-BajoStock").html("Ocultar <span class='caret'></span>");
    }
}
$(".btnLogArticulo").click(function () {
    window.location = Routing.generate('admin_logentry_show', {'id': $(this).data('id')});
});