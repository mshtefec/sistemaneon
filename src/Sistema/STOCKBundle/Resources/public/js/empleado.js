// Get the ul that holds the collection of escuelas
var collectionEmpleado = jQuery('.empleado');
var index;

jQuery(document).ready(function() {

    collectionEmpleado.data('index', collectionEmpleado.find(':input').length);
    
    jQuery('.empleado').delegate('.btnRemoveEmpleado','click', function(e) {
            // prevent the link from creating a "#" on  the URL
            e.preventDefault();
            // remove the li for the tag form
            var li = jQuery(this).closest('.rowremove').attr("li");
            jQuery('#'+ li).remove();
            jQuery(this).closest('.rowremove').remove();
        });    

    jQuery('.ribon_dom').delegate('.add_empleado_link','click', function(e) {
        // prevent the link from creating a "#" on the URL                
        e.preventDefault();
        // remove the li for the tag form
        index = addForm(collectionEmpleado, jQuery('.empleado'));
        if(index === 0) {
            addUl(index, 'Seleccione empleado');
            jQuery('#empleado-li-'+index).addClass('active');
            jQuery('#empleado_'+index).addClass('active');
        } else {
            addUl(index, 'Seleccione empleado');
        }
    });

});