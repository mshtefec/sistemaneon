// Get the ul that holds the collection of escuelas
var collectionEmpleadoCargo = jQuery('.empleadocargo');

jQuery(document).ready(function () {
    collectionEmpleadoCargo.data('index', collectionEmpleadoCargo.find(':input').length);

    jQuery('.empleadocargo').delegate('.btnRemoveEmpleadoCargo', 'click', function (e) {
        // prevent the link from creating a "#" on  the URL
        e.preventDefault();
        // remove the li for the tag form
        jQuery(this).closest('.rowremove').remove();
    });

    jQuery('.ribon_dom').delegate('.add_empleadocargo_link', 'click', function (e) {
        // prevent the link from creating a "#" on the URL                
        e.preventDefault();
        // remove the li for the tag form
        addForm(collectionEmpleadoCargo, jQuery('.empleadocargo > table > tbody'));
    });
});