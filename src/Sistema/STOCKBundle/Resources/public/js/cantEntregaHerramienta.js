// Get the ul that holds the collection of escuelas
var collectionArticulo = jQuery('.cantEntregaHerramienta');

jQuery(document).ready(function() {
    collectionArticulo.data('index', collectionArticulo.find(':input').length);
    
    jQuery('.cantEntregaHerramienta').delegate('.btnRemoveCantEntregaHerramienta','click', function(e) {
            // prevent the link from creating a "#" on  the URL
            e.preventDefault();
            // remove the li for the tag form
            jQuery(this).closest('.rowremove').remove();
        });    

    jQuery('.ribon_dom').delegate('.add_cantEntregaHerramienta_link','click', function(e) {
        // prevent the link from creating a "#" on the URL                
        e.preventDefault();
        // remove the li for the tag form
        addForm(collectionArticulo, jQuery('.cantEntregaHerramienta > table > tbody'));      
    });
});