function separar(id) {
    var numero=id.split('_');
    return numero[4];
}

function validacion(campo) {
    var num = separar(campo.id);
    if (parseInt(campo.value) > parseInt($('#sistema_stockbundle_orden_articulos_'+num+'_cantidadMaxima').val())) {
        if (!campo.parentNode.classList.contains('has-error')) {
            campo.parentNode.classList.add("has-error");  
            $('#'+campo.id).parent().append("<div id='mjeError"+campo.id+"' Class='text-danger'>Error en la cantidad</div>" );
        }
    } else {
        campo.parentNode.classList.remove("has-error");   
        $('#mjeError'+campo.id).remove();
    }
}  
    function validacionArt(campo) {
        var cantidad = parseInt(campo.value);
        var cantidadUtilizada = parseInt($('#sistema_stockbundle_articulo_cantidadUtilizada').val());
       
    if (cantidad < cantidadUtilizada) {
        if (!campo.parentNode.classList.contains('has-error')) {
            campo.parentNode.classList.add("has-error");  
            $('#'+campo.id).parent().append("<div id='mjeError"+campo.id+"' Class='text-danger'>Error, la cantidad no puede ser menor a la Cantidad de Articulos Utilizados!</div>" );
        }
            $('#'+campo.id).val(cantidadUtilizada);
    } else {
        campo.parentNode.classList.remove("has-error");   
        $('#mjeError'+campo.id).remove();
    }
}