// Get the ul that holds the collection of escuelas
var collectionAddArticulo = jQuery('.cantAddArticulo');

jQuery(document).ready(function() {
    collectionAddArticulo.data('index', collectionAddArticulo.find(':input').length);
    
    jQuery('.cantAddArticulo').delegate('.btnRemoveCantAddArticulo','click', function(e) {
            // prevent the link from creating a "#" on  the URL
            e.preventDefault();
            // remove the li for the tag form
            jQuery(this).closest('.rowremove').remove();
        });    

    jQuery('.ribon_dom').delegate('.add_cantAddArticulo_link','click', function(e) {
        // prevent the link from creating a "#" on the URL                
        e.preventDefault();
        // remove the li for the tag form
        addForm(collectionAddArticulo, jQuery('.cantAddArticulo > table > tbody'));      
    });
});