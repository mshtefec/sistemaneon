<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            // Bundles Propios
            new Sistema\RRHHBundle\SistemaRRHHBundle(),
            new Sistema\STOCKBundle\SistemaSTOCKBundle(),
            new Sistema\FACTURACIONBundle\SistemaFACTURACIONBundle(),
            new Sistema\UserBundle\SistemaUserBundle(),
            new Sistema\MWSCONFBundle\SistemaMWSCONFBundle(),
            new Sistema\ReporteBundle\SistemaReporteBundle(),
            //Bundles d Terceros
            new MWSimple\Bundle\CrudGeneratorBundle\MWSimpleCrudGeneratorBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            new Lexik\Bundle\FormFilterBundle\LexikFormFilterBundle(),
            new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new Sistema\MWSFORMBundle\SistemaMWSFORMBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
            new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
            new Sistema\PresupuestoBundle\SistemaPresupuestoBundle(),
            new Knp\Bundle\GaufretteBundle\KnpGaufretteBundle(),
            new Dizda\CloudBackupBundle\DizdaCloudBackupBundle(),
            new Sistema\SueldoBundle\SistemaSueldoBundle(),
            new Sistema\AuditoriaBundle\SistemaAuditoriaBundle(),
            new Sistema\PlanDeCuentasBundle\SistemaPlanDeCuentasBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            // $bundles[] = new Acme\DemoBundle\AcmeDemoBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
            //Bundles de Terceros
            $bundles[] = new RaulFraile\Bundle\LadybugBundle\RaulFraileLadybugBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
